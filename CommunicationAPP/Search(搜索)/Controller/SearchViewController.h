//
//  SearchViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UIViewController
@property(nonatomic ,copy)NSString *searchText;
@end

NS_ASSUME_NONNULL_END
