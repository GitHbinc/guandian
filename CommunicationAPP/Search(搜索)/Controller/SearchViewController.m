//
//  SearchViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchAnchorCell.h"
#import "SearchLivingCell.h"
#import "SearchVideoCell.h"

#import "SearchHeadView.h"
#import "DynamicModel.h"

@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)SearchHeadView *searchHeadView;
@property(nonatomic ,assign)CGFloat type;//0.综合 1.视频 2.主播 3.直播
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self loadHeadView];
    
     [self loadTableView];
    
    self.type = 0;
   
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_searchHeadView.mas_bottom).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadView{
     DynamicModel *model = [[DynamicModel alloc]init];
    _searchHeadView = [[SearchHeadView alloc]initWithFrame:CGRectZero];
    _searchHeadView.backgroundColor = [UIColor whiteColor];
    _searchHeadView.searchBar.text = self.searchText;
    __weak __typeof(self)weakSelf = self;
    _searchHeadView.SearchCancelClick = ^{//返回
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    _searchHeadView.SearchClickAction = ^(CGFloat tag1, CGFloat tag2) {//选择点击的类目
        weakSelf.type = tag1;
        if (tag1==0) {
            [weakSelf.searchHeadView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(STATUS_BAR_HEIGHT);
                make.left.right.equalTo(weakSelf.view);
                make.height.mas_equalTo(@90);
            }];
        }else{
           
            [weakSelf.searchHeadView mas_updateConstraints:^(MASConstraintMaker *make) {
                 make.top.mas_equalTo(STATUS_BAR_HEIGHT);
                make.left.right.equalTo(weakSelf.view);
                make.height.mas_equalTo(model.headHeight);
            }];
        }
        
        [weakSelf.tableView reloadData];
        
    };
    _searchHeadView.model = model;
    _searchHeadView.size = CGSizeMake(kSCREENWIDTH, model.headHeight);
    [self.view addSubview:_searchHeadView];
    [_searchHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(STATUS_BAR_HEIGHT);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(@90);
    }];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 7;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.type == 2) {
        static NSString *identifier = @"SearchAnchorCell";
        SearchAnchorCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SearchAnchorCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
        
    }else if (self.type == 3){
        static NSString *identifier = @"SearchLivingCell";
        SearchLivingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SearchLivingCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        static NSString *identifier = @"SearchVideoCell";
        SearchVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SearchVideoCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
