//
//  SearchHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SearchHeadView : UIView
@property (nonatomic, strong)DynamicModel *model;
@property (nonatomic, strong)UISearchBar *searchBar;
@property (nonatomic, copy) void(^SearchCancelClick)(void);
@property (nonatomic, copy) void(^SearchClickAction)(CGFloat tag1,CGFloat tag2);


@end

NS_ASSUME_NONNULL_END
