//
//  SearchHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SearchHeadView.h"
@interface SearchHeadView()
@property(nonatomic ,strong)UIButton *cancelBtn;
@property(nonatomic ,strong)UIButton *selectBtn;
@property(nonatomic ,strong)UIButton *selectNameBtn;
@property(nonatomic ,strong)UIView *classfyView;
@property(nonatomic ,strong)NSMutableArray *titleArray;
@property(nonatomic ,strong)NSMutableArray *nameArray;
@property(nonatomic ,assign)CGFloat type;
@property(nonatomic ,strong)UIImageView *yinyingImage;

@end
@implementation SearchHeadView
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _titleArray = [@[@"综合",@"视频",@"主播",@"直播"]mutableCopy];
        _nameArray = [@[@"智能排序",@"最新开播",@"在线人数"]mutableCopy];
        [self setUpUI];
        
    }
    
    return self;
}

-(void)setUpUI{
    _searchBar = [[UISearchBar alloc]init];
    _searchBar.searchBarStyle = UISearchBarStyleMinimal;
    [self addSubview:_searchBar];
    
    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-50);
        make.top.mas_equalTo(self).offset(10);
        make.height.mas_equalTo(@30);
    }];
    
    _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
     _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_cancelBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    [_cancelBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_cancelBtn];
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self->_searchBar.mas_right).offset(0);
        make.centerY.mas_equalTo(self->_searchBar.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(40, 30));
        
    }];
    CGFloat width = 0;
    for (int i = 0; i < _titleArray.count; i++) {
        CGFloat titleW = (kSCREENWIDTH-100)/4;
        CGFloat  titleH = 30;
        CGFloat  titleY = 50;
        CGFloat  titleX = width + i * ((kSCREENWIDTH - (kSCREENWIDTH-100) -40)/(_titleArray.count -1))+20;
        width += titleW;
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [titleBtn addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchUpInside];
        [titleBtn setTitle:_titleArray[i] forState:UIControlStateNormal];
        titleBtn.frame = CGRectMake(titleX, titleY, titleW, titleH);
        titleBtn.clipsToBounds = YES;
        titleBtn.layer.cornerRadius = 15;
        [titleBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [titleBtn setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@"redback"] forState:UIControlStateSelected];
        
        [self addSubview:titleBtn];
        titleBtn.tag = i;
        if (titleBtn.tag ==0) {
            [self clickTitle:titleBtn];
        }
    }
    
    _classfyView = [UIView new];
    _classfyView.backgroundColor = [UIColor whiteColor];
    _classfyView.hidden = YES;
    [self addSubview:_classfyView];
    [_classfyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(@90);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(@40);
        
    }];
    

}

-(void)setModel:(DynamicModel *)model{
    _model = model;
    [self layoutIfNeeded];
    CGFloat tagViewHeight = _classfyView.frame.origin.y;
    model.headHeight = tagViewHeight + 36;
}

-(void)clickTitle:(UIButton *)sender{
    _type = sender.tag;
    if (self.selectBtn) {
        self.selectBtn.selected = NO;
        self.selectBtn.userInteractionEnabled = YES;
    }
    sender.selected = YES;
    sender.userInteractionEnabled = NO;
    self.selectBtn = sender;
    
    UIButton *btn = [_classfyView.subviews firstObject];
    [self clickName:btn];
    if (_SearchClickAction) {
        _SearchClickAction(sender.tag,btn.tag);
    }
    
    if (sender.tag == 0) {
        [self.classfyView setHidden:YES];
    }else{
        [self.classfyView setHidden:NO];
        if (sender.tag == 1) {
            _nameArray = [@[@"智能排序",@"最新上传",@"最多播放"]mutableCopy];
        }else if (sender.tag ==2){
            _nameArray = [@[@"智能排序",@"最多粉丝",@"最多作品"]mutableCopy];
        }else if (sender.tag ==3){
            _nameArray = [@[@"智能排序",@"最新开播",@"在线人数"]mutableCopy];
        }
        [self creatTypes];
        
    }
}

-(void)creatTypes{
    [_classfyView removeAllSubview];
    CGFloat namewidth = 0;
    for (int i = 0; i < _nameArray.count; i++) {
        CGFloat titleW = (kSCREENWIDTH-50)/3;
        CGFloat  titleH = 36;
        CGFloat  titleY = 4
        ;
        CGFloat  titleX = namewidth + i * ((kSCREENWIDTH - (titleW *_nameArray.count) -40)/(_nameArray.count -1))+20;
        namewidth += titleW;
        UIButton *nameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [nameBtn addTarget:self action:@selector(clickName:) forControlEvents:UIControlEventTouchUpInside];
        [nameBtn setTitle:_nameArray[i] forState:UIControlStateNormal];
        nameBtn.frame = CGRectMake(titleX, titleY, titleW, titleH);
        nameBtn.backgroundColor = [UIColor whiteColor];
        [nameBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [nameBtn setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
        [nameBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateSelected];
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:0.6];
        line.frame = CGRectMake(nameBtn.right, 10+4, 2, 16);
        [_classfyView addSubview:nameBtn];
        [_classfyView addSubview:line];
        
        nameBtn.tag = i;
        if (nameBtn.tag ==0) {
            [self clickName:nameBtn];
        }
    }
    UILabel *label = [_classfyView.subviews lastObject];
    label.hidden = YES;
    
    _yinyingImage = [[UIImageView alloc]init];
    _yinyingImage.frame = CGRectMake(0, 0, kSCREENWIDTH, 4);
    _yinyingImage.image =[UIImage imageNamed:@"yinying"];
    [_classfyView addSubview:_yinyingImage];
    
}



-(void)clickName:(UIButton *)sender{
    if (self.selectNameBtn) {
        self.selectNameBtn.selected = NO;
        self.selectNameBtn.userInteractionEnabled = YES;
    }
    sender.selected = YES;
    sender.userInteractionEnabled = NO;
    self.selectNameBtn = sender;
    if (_SearchClickAction) {
        _SearchClickAction(_type,sender.tag);
    }
}

-(void)cancelAction{
    if (_SearchCancelClick) {
        _SearchCancelClick();
    }
}


@end
