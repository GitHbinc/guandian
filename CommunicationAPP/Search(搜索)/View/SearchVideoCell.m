//
//  SearchVideoCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SearchVideoCell.h"
@interface SearchVideoCell()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *IDLabel;

@property (nonatomic ,strong)UILabel *timeLabel;
@property (nonatomic ,strong)UIImageView *timeImage;

@property (nonatomic ,strong)UILabel *bofangLabel;
@property (nonatomic ,strong)UIImageView *bofangImage;

@property (nonatomic ,strong)UILabel *messageLabel;
@property (nonatomic ,strong)UIImageView *messageImage;

@end
@implementation SearchVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _mainImage = [[UIImageView alloc]init];
    _mainImage.image = [UIImage imageNamed:@"test_icon"];
    [_mainImage.layer setMasksToBounds:YES];
    [_mainImage.layer setCornerRadius:8];
    [self.contentView addSubview:_mainImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"miss金曲奖历险记";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
    
    
    _IDLabel = [UILabel new];
    _IDLabel.text = @"ID:3456765536";
    _IDLabel.textColor = HEXCOLOR(0x999999);
    _IDLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_IDLabel];
    
    _timeImage = [[UIImageView alloc]init];
    _timeImage.image = [UIImage imageNamed:@"search_shijian"];
    [self.contentView addSubview:_timeImage];
    
    
    _timeLabel = [UILabel new];
    _timeLabel.text = @"10:12";
    _timeLabel.textColor = HEXCOLOR(0x999999);
    _timeLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_timeLabel];
    
    
    _bofangImage = [[UIImageView alloc]init];
    _bofangImage.image = [UIImage imageNamed:@"search_bofang"];
    [self.contentView addSubview:_bofangImage];
    
    _bofangLabel = [UILabel new];
    _bofangLabel.text = @"1434";
    _bofangLabel.textColor = HEXCOLOR(0x999999);
    _bofangLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_bofangLabel];
    
    _messageImage = [[UIImageView alloc]init];
    _messageImage.image = [UIImage imageNamed:@"search_xiaoxi"];
    [self.contentView addSubview:_messageImage];
    
    _messageLabel = [UILabel new];
    _messageLabel.text = @"50";
    _messageLabel.textColor = HEXCOLOR(0x999999);
    _messageLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_messageLabel];
    
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(self->_mainImage.mas_top)setOffset:0];
    }];
    
    [_IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_mainImage.mas_centerY)setOffset:0];
    }];
    
    [_timeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(self->_mainImage.mas_bottom)setOffset:0];
    }];
    
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_timeImage.mas_right)setOffset:5];
        make.centerY.mas_equalTo(self->_timeImage);
    }];
    
    [_bofangImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_timeLabel.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(self->_mainImage.mas_bottom)setOffset:0];
    }];
    
    [_bofangLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_bofangImage.mas_right)setOffset:5];
        make.centerY.mas_equalTo(self->_bofangImage);
    }];
    
    [_messageImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_bofangLabel.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(self->_mainImage.mas_bottom)setOffset:0];
    }];
    
    [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_messageImage.mas_right)setOffset:5];
        make.centerY.mas_equalTo(self->_messageImage);
    }];
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
