//
//  SearchLivingCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SearchLivingCell.h"
#import "QMUIButton.h"
@interface SearchLivingCell()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *IDLabel;
@property (nonatomic ,strong)UIImageView *livingImage;
@property (nonatomic ,strong)UILabel *amountLabel;

@end
@implementation SearchLivingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _mainImage = [[UIImageView alloc]init];
    _mainImage.image = [UIImage imageNamed:@"test_icon"];
    [_mainImage.layer setMasksToBounds:YES];
    [_mainImage.layer setCornerRadius:8];
    [self.contentView addSubview:_mainImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"我想带你去旅行";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
    
    
    _IDLabel = [UILabel new];
    _IDLabel.text = @"ID:3456765536";
    _IDLabel.textColor = HEXCOLOR(0x999999);
    _IDLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_IDLabel];
    
    _livingImage = [[UIImageView alloc]init];
    _livingImage.image = [UIImage imageNamed:@"zhibozhong"];
    [self.contentView addSubview:_livingImage];
    
    _amountLabel = [UILabel new];
    _amountLabel.text = @"3455人在线";
    _amountLabel.textColor = HEXCOLOR(0xF46C02);
    _amountLabel.font = [UIFont systemFontOfSize:9];
    [self.contentView addSubview:_amountLabel];
    
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(self->_mainImage.mas_top)setOffset:0];
    }];
    
    [_IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_mainImage.mas_centerY)setOffset:0];
    }];
    
    [_livingImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_mainImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(self->_mainImage.mas_bottom)setOffset:0];
         //make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_livingImage.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self->_livingImage);
       
    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
