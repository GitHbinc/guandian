//
//  UserModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/8.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : JSONModel<NSCoding>
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *autograph;
@property (nonatomic, copy) NSString *birth;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *myfollow;
@property (nonatomic, copy) NSString *myfans;
@property (nonatomic, copy) NSString *myplayback;
@property (nonatomic, copy) NSString *mycollect;
@property (nonatomic, copy) NSString *mydian;
@property (nonatomic, copy) NSString *today;
@property (nonatomic, copy) NSString *is_profile;
@property (nonatomic, copy) NSString *jigoureal;

//解档
+ (UserModel *)dealUserModelFromArchiver;
//归档
+ (void)dealUserModelToArchiverWithUserModel:(UserModel *)userModel;

@end

NS_ASSUME_NONNULL_END
