//
//  UserHander.m
//  MiaoWo
//
//  Created by WPY on 2017/2/26.
//  Copyright © 2017年 WPY. All rights reserved.
//

#import "UserHander.h"

@implementation UserHander

static UserHander *hander = nil;
+ (id)shareHander {
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        hander = [[[self class] alloc] init];
    });
    return hander;
}

//自动登录
- (void)autoLogin
{
    self.loginUserInfo = [UserModel dealUserModelFromArchiver];
}

//保存登录信息
- (void)autoSave
{
    [UserModel dealUserModelToArchiverWithUserModel:self.loginUserInfo];
}

// 获取  未读消息数
//- (void)getNotificationUnread
//{
//    NSString *url = @"notification/unread";
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:1];
//    [[NetTools shareNetTools] POSTURLString:url params:params andSuccessBlock:^(id success) {
//        if ([[success objectForKey:@"status"] integerValue] == 0) {
//            [kAppDelegate setIsLogin:YES];
//            if ([success objectForKey:@"data"]) {
//                [[UserHander shareHander] setUnReadNum:[[NSString stringWithFormat:@"%@",success[@"data"][@"unreadNum"]]integerValue]];
//                if ([[UserHander shareHander] unReadNum] > 0) {
//                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[UserHander shareHander] unReadNum]];
//                }
//            }
//        } else {
//           [[UserHander shareHander] setUnReadNum:0];
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//        }
//    } andErrorBlock:^(NSError *error) {
//
//    } withControler:nil];
//}


@end
