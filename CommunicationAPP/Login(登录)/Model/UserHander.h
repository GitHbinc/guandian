//
//  UserHander.h
//  MiaoWo
//
//  Created by WPY on 2017/2/26.
//  Copyright © 2017年 WPY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
//#import "WithdrawListModel.h"

@interface UserHander : NSObject

//@property (nonatomic,assign) NSInteger unReadNum;
//
@property (nonatomic,strong) UserModel *loginUserInfo;
//
//@property (nonatomic, copy) NSString *tradeFeeRatio; //tradeFeeRatio = "0.003";交易手续费
//@property (nonatomic, copy) NSString *withDrawFeeRatio; //withDrawFeeRatio = "0.01";提现手续费
//
//@property (nonatomic, copy) NSString *isWithdraw;
//
//@property (nonatomic, copy) NSString *tradeTips;
//@property (nonatomic, copy) NSString *withdrawMax;
//
//@property (nonatomic, strong) NSArray<WithdrawListModel *> *withdrawList;
//@property (nonatomic, strong) NSArray<WithdrawListModel *> *chargeList;


//自动登录
- (void)autoLogin;

//保存登录信息
- (void)autoSave;

//管理用户的单例
+ (id)shareHander;

// 获取  未读消息数
//- (void)getNotificationUnread;

@end
