//
//  UserModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/8.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "UserModel.h"
#import "NSData+Extension.h"

#define UserModelFileName   @"UserModelFileName"

@implementation UserModel

+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                @"ID": @"id"
                                                                
                                                                }];
}

- (id)initWithCoder:(NSCoder *)coder
{
    if (self = [super init]) {
        self.ID = [coder decodeObjectForKey:@"ID"];
        self.username = [coder decodeObjectForKey:@"username"];
        self.img = [coder decodeObjectForKey:@"img"];
        
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.ID forKey:@"ID"];
    [coder encodeObject:self.username forKey:@"username"];
    [coder encodeObject:self.img forKey:@"img"];
    
   
}

+ (UserModel *)dealUserModelFromArchiver
{
    return (UserModel *)[NSData unArchiverResourceUnArchiverWithDirectotyName:UserModelFileName fileName:UserModelFileName dataArrayKey:UserModelFileName];
}

+ (void)dealUserModelToArchiverWithUserModel:(UserModel *)userModel
{
    [NSData archiverResourceArchiverWithData:userModel directotyName:UserModelFileName fileName:UserModelFileName dataArrayKey:UserModelFileName];
}

@end
