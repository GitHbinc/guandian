//
//  SetPasswordSuccessViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SetPasswordSuccessViewController.h"
#import "LoginViewController.h"

@interface SetPasswordSuccessViewController ()

@end

@implementation SetPasswordSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.title = @"找回密码成功";
    [self createView];
}

-(void)createView{
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"forget_header_icon"];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(100);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"设置密码成功";
    label.textColor = color333333;
    label.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = color666666;
    label1.text = @"请使用新密码登录";
    label1.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(5);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    UIButton *bottomBtn = [[UIButton alloc]init];
    [bottomBtn setTitle:@"返回" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [bottomBtn setBackgroundImage:[UIImage imageNamed:@"bottomBtn_bg_icon"] forState:UIControlStateNormal];
    [bottomBtn addTarget:self action:@selector(bottomBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bottomBtn];
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(40);
        make.left.equalTo(self.view).offset(45);
        make.right.equalTo(self.view).offset(-45);
        make.height.equalTo(@52);
    }];
    
}

-(void)bottomBtnClick{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
