//
//  ForgetPasswordViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "ForgetPasswordCell.h"
#import "ForgetPasswordHeaderView.h"
#import "SetPasswordSuccessViewController.h"

@interface ForgetPasswordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,copy) UITableView *tableView;
@property (nonatomic, copy)NSArray *titleArray;

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"忘记密码";
    _titleArray = @[@"请输入您的手机号码",@"请输入验证码",@"请重置登录密码，不小于6位"];
    // Do any additional setup after loading the view.
    [self createTableView];
    [self createFooterView];
}

#pragma mark 创建tableView
-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kSCREENWIDTH, kSCREENHEIGHT- 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
//    _tableView.backgroundColor = colorf3f3f3;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    _tableView.tableHeaderView = [ForgetPasswordHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 140)];
    
}


-(void)createFooterView{
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 80)];
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"bottomBtn_bg_icon"] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:17];
    [btn addTarget:self action:@selector(bottomBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(footerView.mas_left).offset(45);
        make.right.equalTo(footerView.mas_right).offset(-45);
//        make.centerX.equalTo(footerView.mas_centerX);
        make.top.equalTo(footerView.mas_top).offset(30);
//        make.height.equalTo(@287);
        make.height.equalTo(@52);
    }];
    
    _tableView.tableFooterView = footerView;
    
}

-(void)bottomBtnClick{
    
    SetPasswordSuccessViewController *controller = [[SetPasswordSuccessViewController alloc]init];
    [self.navigationController pushViewController:controller animated:true];
}
#pragma mark tableView delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 57;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"ForgetPasswordCellID";
    ForgetPasswordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[ForgetPasswordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell refresh:_titleArray[indexPath.row] tag:indexPath.row + 101];
//    [cell refreshModel];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
