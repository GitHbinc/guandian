//
//  LoginViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginView.h"
#import "RegisterViewController.h"
#import "ForgetPasswordViewController.h"
#import "MyMD5.h"
#import "UserModel.h"
#import "ResetPasswordViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    LoginView *loginView = [LoginView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
    loginView.backBtn.hidden = self.isCanBack?NO:YES;
   
    loginView.block = ^(UIButton *button, NSString *phoneStr, NSString *password) {
         [self loginViewBtnClick:button.tag withPhone:phoneStr withPassword:password];
    };
   
    [self.view addSubview:loginView];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}


-(void)loginViewBtnClick:(NSInteger)tag withPhone:(NSString *)phone withPassword:(NSString *)password{
    
    switch (tag) {
        case 101://忘记密码
        {
            ResetPasswordViewController* controller = [[ResetPasswordViewController alloc]init];
            controller.type = PasswordTypeForget;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 102://登录
        {
            
            //检查手机号
            NSString *phonestr = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
            if (![CheckNumber checkTelNumber:phonestr]) {
                [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
                return;
            }
            
            if ([self isEmpty:phonestr]) {
                [MBProgressHUD showError:@"请输入手机号" toView:self.view];
                return;
            }
            
           
            //检查密码
            NSString *passWordStr = [password stringByReplacingOccurrencesOfString:@" " withString:@""];
//            if (![CheckNumber checkPassword:passWordStr]) {
//                [MBProgressHUD showError:@"密码格式错误" toView:self.view];
//                return;
//            }
            if ([self isEmpty:passWordStr]) {
                [MBProgressHUD showError:@"请输入密码" toView:self.view];
                return;
            }
            
            NSDictionary *param = @{
                                    @"phone" :phone?:@"",
                                    @"password":[MyMD5 md5uncapitalized:password] ?:@""
                                    };
            
            [[WYNetworking sharedWYNetworking] POST:user_login parameters:param success:^(id  _Nonnull responseObject) {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
                if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                    NSDictionary *dic = [responseObject objectForKey:@"data"];
                    //保存userID
                    [kUserDefault setObject:@(YES) forKey:LOGINSTATUS];
                    [kUserDefault setObject:[dic objectForKey:@"id"] forKey:USERID];
                    [kUserDefault setObject:[dic objectForKey:@"img"] forKey:HEADIMG];
                    [kUserDefault setObject:[dic objectForKey:@"username"] forKey:USERNAME];
                    [kUserDefault synchronize];
                    
                    __weak typeof(self)weakSelf = self;
                    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0/*延迟执行时间*/ * NSEC_PER_SEC));
                    
                    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                        weakSelf.navigationController.navigationBarHidden = false;
                        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                    });
                    
                }
                
            } failure:^(NSError * _Nonnull error) {
                
            }];
        }
            
            break;
        case 103://立即注册
        {
            RegisterViewController *controller = [[RegisterViewController alloc]init];
            [self.navigationController pushViewController:controller animated:true];
        }
            break;
        case 104://微博登录
            break;
        case 105://微信登录
            break;
        case 106://qq登录
            break;
        case 107://支付宝登录
            break;
        case 108://观点使用协议
            break;
        case 109://返回
        {
            [self.navigationController popViewControllerAnimated:true];
        }
            
            break;
        default:
            break;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
