//
//  RegisterViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterView.h"
#import "MyMD5.h"

@interface RegisterViewController ()
@property(nonatomic ,copy)NSString *phoneStr;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
   
    RegisterView *registerView = [RegisterView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
    registerView.block = ^(NSString *phoneStr, NSString *codeStr, NSString *password) {//注册
        
    
        //检查手机号
       NSString *phonestr = [phoneStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if ([self isEmpty:phonestr]) {
            [MBProgressHUD showError:@"请输入手机号" toView:self.view];
            return;
        }
        
        if (![CheckNumber checkTelNumber:phonestr]) {
            [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
            return;
        }
        
        
        //检查验证码
        NSString *codestr = [codeStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([self isEmpty:codestr]) {
            [MBProgressHUD showError:@"请输入验证码" toView:self.view];
            return;
        }
        if (![CheckNumber checkVerificationCode:codestr]) {
            [MBProgressHUD showError:@"请输入正确的验证码" toView:self.view];
            return;
        }
        
        //检查密码
        NSString *passWordStr = [password stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([self isEmpty:passWordStr]) {
            [MBProgressHUD showError:@"请输入密码" toView:self.view];
            return;
        }
        if (![CheckNumber checkPassword:passWordStr]) {
            [MBProgressHUD showError:@"密码格式错误" toView:self.view];
            return;
        }
        
    
        NSDictionary *param = @{
                                @"phone" :phoneStr?:@"",
                                @"code":codestr?:@"",
                                @"password":[MyMD5 md5uncapitalized:passWordStr] ?:@""
                                };
        
        [[WYNetworking sharedWYNetworking] POST:user_register parameters:param success:^(id  _Nonnull responseObject) {
             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            
            if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            }
            
        } failure:^(NSError * _Nonnull error) {

        }];
        
        
    };
    
    
    registerView.registerOtherClick = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 101://返回
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
                break;
            case 102://获取验证码
            {
                //检查手机号
                NSString *phonestr = [self.phoneStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                if (![CheckNumber checkTelNumber:phonestr]) {
                    [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
                    return;
                }
                NSDictionary *param = @{
                                        @"phone" :self.phoneStr?:@"",
                                        @"type":@"1"
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:user_getcode parameters:param success:^(id  _Nonnull responseObject) {
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                    
                } failure:^(NSError * _Nonnull error) {
                    [MBProgressHUD showError:@"验证码发送失败" toView:self.view];
                }];
                
            }
                break;
            case 103:
            {
              [MBProgressHUD showError:@"联系客服" toView:self.view];
             
            }
                break;
                
            default:
                break;
        }
        
    };
    
    //获取手机号码
    registerView.phoneText = ^(NSString * _Nonnull phone) {
        self.phoneStr = phone;
    };
    
    [self.view addSubview:registerView];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
