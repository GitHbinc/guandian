//
//  ForgetPasswordCell.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPasswordCell : UITableViewCell

-(void)refresh:(NSString *)titleStr tag:(NSInteger)tag;

@end

NS_ASSUME_NONNULL_END
