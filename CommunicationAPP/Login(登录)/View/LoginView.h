//
//  LoginView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ButtonBlock)(UIButton *button,NSString *phoneStr,NSString *password);

NS_ASSUME_NONNULL_BEGIN

@interface LoginView : UIView
@property(nonatomic ,strong)UIButton *backBtn;
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property(nonatomic,copy)ButtonBlock block;

@end

NS_ASSUME_NONNULL_END
