//
//  ForgetPasswordCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ForgetPasswordCell.h"

@interface ForgetPasswordCell(){
    
    UITextField *_contentTextField;
    UIButton *_codeBtn;
    
}

@end
@implementation ForgetPasswordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _contentTextField = [[UITextField alloc]init];
    _contentTextField.placeholder = @"";
    _contentTextField.font = [UIFont systemFontOfSize:12];
    [_contentTextField setValue:color666666 forKeyPath:@"_placeholderLabel.textColor"];
    [_contentTextField addTarget:self action:@selector(contentTextFiled:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:_contentTextField];
    [_contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(40);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorD6D6D6;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(40);
        make.top.equalTo(self.contentView.mas_bottom).offset(-1);
        make.right.equalTo(self.contentView).offset(-40);
        make.height.equalTo(@1);
    }];
    
    _codeBtn = [[UIButton alloc]init];
    [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_codeBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    _codeBtn.backgroundColor = MAINCOLOR;
    _codeBtn.layer.cornerRadius = 6.0;
//    [_codeBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    _codeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    _codeBtn.hidden = true;
    [self.contentView addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-40);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@85);
        make.height.equalTo(@33);
    }];
}

-(void)codeBtnClick:(UIButton *)senderBtn{
    
}

-(void)contentTextFiled:(UITextField *)textField{
    
}

-(void)refresh:(NSString *)titleStr tag:(NSInteger)tag{
    _contentTextField.placeholder = titleStr;
    _contentTextField.tag = tag;
    if (tag == 102) {
        _codeBtn.hidden = false;
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
