//
//  RegisterView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RegisterView.h"
#define codeTag  10
@interface RegisterView()<UITextFieldDelegate>
{
    UITextField * _phoneTextField;
    UITextField * _codeTextField;
    UITextField * _pwdTextField;
    UIButton    * _codeBtn;
    NSTimer     * _countTime;
    NSInteger _codetag;
}
@end
@implementation RegisterView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    _codetag = codeTag;
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"login_bg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"register_back_icon"] forState:UIControlStateNormal];
    backBtn.tag = 101;
    [backBtn addTarget:self action:@selector(registerOtherClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.top.equalTo(self).offset(STATUS_BAR_HEIGHT +5);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"手机号注册";
    label.textColor = UIColor.whiteColor;
    label.font = [UIFont systemFontOfSize:16];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(STATUS_BAR_HEIGHT +5);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    UIView *headerView = [[UIView alloc]init];
    headerView.layer.cornerRadius = 39.0;
    headerView.backgroundColor = UIColor.whiteColor;
    [self addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(80);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@78);
        make.width.equalTo(@78);
    }];
    
    UIImageView *headerImageView = [[UIImageView alloc]init];
    headerImageView.image = [UIImage imageNamed:@"header_logo_icon"];
    [headerView addSubview:headerImageView];
    [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView.mas_centerY);
        make.centerX.equalTo(headerView.mas_centerX);
        make.height.equalTo(@74);
        make.width.equalTo(@74);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = UIColor.whiteColor;
    label1.text = @"大陆 +86";
    label1.font = [UIFont systemFontOfSize:14];
    [self addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(headerView.mas_bottom).offset(50);
        make.width.equalTo(@70);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_right).offset(5);
        make.top.equalTo(label1.mas_top).offset(0);
        make.width.equalTo(@1);
        make.height.equalTo(@20);
    }];
    
    _phoneTextField = [[UITextField alloc]init];
    _phoneTextField.textColor = UIColor.whiteColor;
    _phoneTextField.placeholder = @"请输入您的手机号码";
    _phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    // "通过KVC修改占位文字的颜色"
    [_phoneTextField setValue:UIColor.whiteColor forKeyPath:@"_placeholderLabel.textColor"];
    _phoneTextField.font = [UIFont systemFontOfSize:12];
    [self addSubview:_phoneTextField];
    [_phoneTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView.mas_right).offset(10);
        make.top.equalTo(lineView.mas_top).offset(2);
        make.right.equalTo(self).offset(-40);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(label1.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-40);
        make.height.equalTo(@1);
    }];
    
    _codeTextField = [[UITextField alloc]init];
    _codeTextField.textColor = UIColor.whiteColor;
    _codeTextField.placeholder = @"请输入验证码";
    _codeTextField.keyboardType = UIKeyboardTypePhonePad;
    // "通过KVC修改占位文字的颜色"
    [_codeTextField setValue:UIColor.whiteColor forKeyPath:@"_placeholderLabel.textColor"];
    _codeTextField.font = [UIFont systemFontOfSize:12];
    [self addSubview:_codeTextField];
    [_codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(lineView1.mas_bottom).offset(30);
    }];
    
    UIButton *codeBtn = [[UIButton alloc]init];
    [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [codeBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    codeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [codeBtn setBackgroundImage:[UIImage imageNamed:@"code_normal_icon"] forState:UIControlStateNormal];
    codeBtn.tag = 102;
    [codeBtn addTarget:self action:@selector(registerOtherClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:codeBtn];
    _codeBtn = codeBtn;
    [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-40);
        make.top.equalTo(lineView1.mas_bottom).offset(10);
    }];
    
    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(self->_codeTextField.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-40);
        make.height.equalTo(@1);
    }];
    
    _pwdTextField = [[UITextField alloc]init];
    _pwdTextField.textColor = UIColor.whiteColor;
    _pwdTextField.placeholder = @"设置登录密码，不小于6位";
    // "通过KVC修改占位文字的颜色"
    [_pwdTextField setValue:UIColor.whiteColor forKeyPath:@"_placeholderLabel.textColor"];
    _pwdTextField.font = [UIFont systemFontOfSize:12];
    [self addSubview:_pwdTextField];
    [_pwdTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(lineView2.mas_bottom).offset(30);
    }];
    
    UIView *lineView3 = [[UIView alloc]init];
    lineView3.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView3];
    [lineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(self->_pwdTextField.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-40);
        make.height.equalTo(@1);
    }];
    
    UIButton *registerBtn = [[UIButton alloc]init];
    [registerBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"registerBtn_bg_icon"] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [registerBtn addTarget:self action:@selector(registerViewBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView3.mas_bottom).offset(30);
//        make.centerX.equalTo(self.mas_centerX);
        make.left.equalTo(self).offset(45);
        make.right.equalTo(self).offset(-45);
//        make.width.equalTo(@287);
        make.height.equalTo(@52);
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"bottom_des_icon"];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registerBtn.mas_bottom).offset(30);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    UIButton *bottomBtn = [[UIButton alloc]init];
    [bottomBtn setTitle:@"注册遇到问题？联系客服试试吧～" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    bottomBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    bottomBtn.tag = 103;
    [bottomBtn addTarget:self action:@selector(registerOtherClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:bottomBtn];
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    NSLog(@"文字改变：%@",textChange.text);
    if (_phoneText) {
        _phoneText(textChange.text);
    }
}

-(void)registerViewBtnClick{
    if (self.block) {
        self.block(_phoneTextField.text, _codeTextField.text, _pwdTextField.text);
    }
}

-(void)registerOtherClick:(UIButton *)sender{
    
    if (_registerOtherClick) {
        _registerOtherClick(sender);
    }
    
    
    if (sender.tag == 102) {
        NSString *phonestr = [_phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![CheckNumber checkTelNumber:phonestr]) {
            [MBProgressHUD showError:@"请输入正确的手机号" toView:self];
            return;
        }
        _codeBtn.userInteractionEnabled = NO;
        [_codeBtn setTitle:[NSString stringWithFormat:@"%lds",(long)_codetag] forState:UIControlStateNormal];
        _countTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
        
    }
   
}

//验证码倒计时
- (void)countdown{
    NSInteger count = _codetag;
    count --;
    _codetag = count;
    if (count<0){
        _codeBtn.userInteractionEnabled = YES;
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countTime invalidate];
        _codetag = codeTag;
    }else{
        [_codeBtn setTitle:[NSString stringWithFormat:@"%lds",(long)_codetag] forState:UIControlStateNormal];
        
    }
}


@end
