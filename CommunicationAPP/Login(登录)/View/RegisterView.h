//
//  RegisterView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ButtonBlock)(NSString *phoneStr,NSString *codeStr,NSString *password);

NS_ASSUME_NONNULL_BEGIN

@interface RegisterView : UIView
-(void)textFieldTextDidChange:(UITextField *)textChange;
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property(nonatomic,copy)ButtonBlock block;//注册
@property (nonatomic, copy)void(^registerOtherClick)(UIButton * sender);//101:返回 102:获取验证码 103:客服
@property (nonatomic, copy)void(^phoneText)(NSString *phone);
@end

NS_ASSUME_NONNULL_END
