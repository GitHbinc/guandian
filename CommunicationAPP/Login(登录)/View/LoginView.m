//
//  LoginView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "LoginView.h"

@interface LoginView(){
    
    UITextField *_phoneTextField;
    UITextField *_pwdTextField;
    
}

@end
@implementation LoginView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"login_bg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"cancel_icon"] forState:UIControlStateNormal];
    backBtn.tag = 109;
    [backBtn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backBtn];
    _backBtn = backBtn;
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.top.equalTo(self).offset(40);
    }];
    
    UIView *headerView = [[UIView alloc]init];
    headerView.layer.cornerRadius = 39.0;
    headerView.backgroundColor = UIColor.whiteColor;
    [self addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(70);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@78);
        make.width.equalTo(@78);
    }];
    
    UIImageView *headerImageView = [[UIImageView alloc]init];
    headerImageView.image = [UIImage imageNamed:@"header_logo_icon"];
    [headerView addSubview:headerImageView];
    [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView.mas_centerY);
        make.centerX.equalTo(headerView.mas_centerX);
        make.height.equalTo(@74);
        make.width.equalTo(@74);
    }];
    
    UIImageView *phoneImageView = [[UIImageView alloc]init];
    phoneImageView.image = [UIImage imageNamed:@"phone_icon"];
    [self addSubview:phoneImageView];
    [phoneImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(headerImageView.mas_bottom).offset(40);
        make.height.equalTo(@21);
        make.width.equalTo(@15);
    }];
    
    _phoneTextField = [[UITextField alloc]init];
    _phoneTextField.textColor = UIColor.whiteColor;
    _phoneTextField.placeholder = @"请输入你的手机号码";
    _phoneTextField.text = @"13770502801";
    _phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    // "通过KVC修改占位文字的颜色"
    [_phoneTextField setValue:UIColor.whiteColor forKeyPath:@"_placeholderLabel.textColor"];
    _phoneTextField.font = [UIFont systemFontOfSize:12];
    [self addSubview:_phoneTextField];
    [_phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneImageView.mas_right).offset(15);
        make.top.equalTo(phoneImageView.mas_top).offset(2);
        make.right.equalTo(self).offset(-10);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(phoneImageView.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-40);
        make.height.equalTo(@1);
    }];
    
    UIImageView *pwdImageView = [[UIImageView alloc]init];
    pwdImageView.image = [UIImage imageNamed:@"pwd_icon"];
    [self addSubview:pwdImageView];
    [pwdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(lineView.mas_bottom).offset(30);
    }];
    
    _pwdTextField = [[UITextField alloc]init];
    _pwdTextField.textColor = UIColor.whiteColor;
    _pwdTextField.placeholder = @"请输入你的密码";
     _pwdTextField.text = @"111111";
    // "通过KVC修改占位文字的颜色"
    [_pwdTextField setValue:UIColor.whiteColor forKeyPath:@"_placeholderLabel.textColor"];
    _pwdTextField.font = [UIFont systemFontOfSize:12];
    [self addSubview:_pwdTextField];
    [_pwdTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(pwdImageView.mas_right).offset(15);
        make.top.equalTo(pwdImageView.mas_top).offset(2);
        make.right.equalTo(self).offset(-10);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = UIColor.whiteColor;
    [self addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(40);
        make.top.equalTo(pwdImageView.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-40);
        make.height.equalTo(@1);
    }];
    
    UIButton *forgetPwdBtn = [[UIButton alloc]init];
    [forgetPwdBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [forgetPwdBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    forgetPwdBtn.tag = 101;
    [forgetPwdBtn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:forgetPwdBtn];
    [forgetPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-30);
        make.top.equalTo(self->_pwdTextField.mas_top).offset(-8);
    }];
    
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setImage:[UIImage imageNamed:@"loginBtn_icon"] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.tag = 102;
    [self addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView1.mas_bottom).offset(30);
        make.left.equalTo(self).offset(45);
        make.right.equalTo(self).offset(-45);
//        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@52);
//        make.width.equalTo(@287);
    }];
    
    UIView *registerView = [[UIView alloc]init];
    registerView.backgroundColor = UIColor.clearColor;
    [self addSubview:registerView];
    [registerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(loginBtn.mas_bottom).offset(0);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];
    
    UIButton *registerBtn = [[UIButton alloc]init];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [registerBtn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    registerBtn.tag = 103;
    [registerView addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(registerView.mas_left).offset(0);
        make.centerY.equalTo(registerView.mas_centerY);
    }];
    
    UIButton *arrowBtn = [[UIButton alloc]init];
    [arrowBtn setImage:[UIImage imageNamed:@"login_arrow_icon"] forState:UIControlStateNormal];
    [arrowBtn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    arrowBtn.tag = 103;
    [registerView addSubview:arrowBtn];
    [arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(registerView.mas_right).offset(0);
        make.centerY.equalTo(registerView.mas_centerY);
    }];
    
    UIView *thirdLoginView = [[UIView alloc]init];
    thirdLoginView.backgroundColor = UIColor.clearColor;
    [self addSubview:thirdLoginView];
    [thirdLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registerView.mas_bottom).offset(40);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@230);
        make.height.equalTo(@80);
    }];

    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = UIColor.whiteColor;
    [thirdLoginView addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(thirdLoginView.mas_left).offset(0);
        make.top.equalTo(thirdLoginView.mas_top).offset(2);
        make.width.equalTo(@75);
        make.height.equalTo(@1);
    }];

    UILabel *label = [[UILabel alloc]init];
    label.text = @"第三方登录";
    label.textColor = UIColor.whiteColor;
    label.font = [UIFont systemFontOfSize:11];
    [thirdLoginView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(thirdLoginView.mas_top).offset(-4);
        make.centerX.equalTo(thirdLoginView.mas_centerX);
    }];

    UIView *lineView3 = [[UIView alloc]init];
    lineView3.backgroundColor = UIColor.whiteColor;
    [thirdLoginView addSubview:lineView3];
    [lineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(thirdLoginView.mas_right).offset(0);
        make.top.equalTo(thirdLoginView.mas_top).offset(2);
        make.width.equalTo(@75);
        make.height.equalTo(@1);
    }];


    NSArray *imageArray = @[@"login_xinlang_icon",@"login_weixin_icon",@"login_qq_icon",@"login_pay_icon"];
    
    for (int i = 0; i<imageArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 104 + i;
        [btn addTarget:self action:@selector(loginViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [thirdLoginView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(thirdLoginView.mas_left).offset(64 * i);
            make.bottom.equalTo(thirdLoginView.mas_bottom).offset(-10);
            make.width.equalTo(@36);
            make.height.equalTo(@36);
        }];
    }

    UIButton *bottomBtn = [[UIButton alloc]init];
    [bottomBtn setTitle:@"登录即同意《观点使用协议》" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    bottomBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    bottomBtn.tag = 108;
    [self addSubview:bottomBtn];
    [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
}


-(void)loginViewBtnClick:(UIButton *)senderBtn{
    if (self.block) {
        self.block(senderBtn, _phoneTextField.text, _pwdTextField.text);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
