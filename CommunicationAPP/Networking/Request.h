//
//  Request.h
//  MiaoWo
//
//  Created by Willian on 2018/8/24.
//

#ifndef Request_h
#define Request_h

#pragma mark - 登录注册模块
//注册
#define user_register    @"myself/wregister"
//登录
#define user_login     @"myself/wlogin"
//忘记密码
#define user_forgetPassword    @"myself/wforget"
//获取验证码
#define user_getcode    @"myself/codes"

#pragma mark - 首页

#pragma mark - 排行
//短视频类型
#define rank_typelist    @"myself/vtypelist"
//短视频排行
#define rank_topcate    @"myself/topcate"
//短视频详情
#define rank_svideo_info    @"myself/svideo_info"
//短视频相关推荐
#define rank_svideo_recommend    @"myself/svideo_recommend"


#pragma mark - 动态
//动态列表
#define dynamic_typelist    @"Updatings/slefUpdatings"
//动态评论列表
#define dynamic_commentList    @"Updatings/commentList"
//动态转发列表
#define dynamic_repeatList    @"Updatings/repeatList"
//动态转发
#define dynamic_forwardUpdatings    @"Updatings/forwardUpdatings"
//动态删除
#define dynamic_delUpdatings    @"Updatings/delUpdatings"
//发表动态
#define dynamic_commentAdd    @"Updatings/commentAdd"
//动态点赞
#define dynamic_scoreUpdatings    @"Updatings/scoreUpdatings"
//动态收藏
#define dynamic_collect   @"Updatings/collect"
//动态举报
#define dynamic_reportUpdatings   @"Updatings/reportUpdatings"
//感兴趣的直播
#define dynamic_interest   @"Mycenter/interest"



#pragma mark - 个人中心模块
//获取个人资料
#define user_userinfo    @"myself/userinfos"
//进入消息设置
#define user_information   @"Mycenter/information"
//修改消息设置
#define user_information_edit   @"Mycenter/information_edit"
//拉黑
#define user_pullblack   @"Mycenter/pullblack"
//进入黑名单
#define user_blacklist   @"Mycenter/blacklist"
//移出黑名单
#define user_blacklist_edit   @"Mycenter/blacklist_edit"
//进入隐私
#define user_privacy   @"Mycenter/privacy"
//修改隐私
#define user_privacy_eidt   @"Mycenter/privacy_eidt"
//我的竞价
#define user_auction   @"Auction/auction"
//竞拍记录删除
#define user_auction_del   @"Auction/auction_del"
//贡献榜
#define user_contribution   @"Mycenter/contribution"
//认证机构信息
#define user_agency   @"Agency/agency"
//认证机构信息
#define user_realname   @"Amyself/realname"
//收益
#define user_profit   @"Profit/profit"
//时间出售
#define user_sell   @"Profit/sell"
//出售记录
#define user_record   @"Profit/record"
//最近观看
#define user_recently   @"Mycenter/recently"
//获取点包信息
#define user_balancesinfo   @"myself/balancesinfo"
//购买时间
#define user_buytimeorder   @"myself/buytimeorder"
//获取套餐信息
#define user_setmeal   @"myself/setmeal"
//解除绑定银行卡
#define user_delbankcard   @"myself/delbankcard"
//银行卡 -- 绑定银行卡 获取验证码
#define user_checkbank   @"myself/checkbank"
//银行卡 -- 添加银行卡
#define user_addbanks   @"myself/addbanks"
//银行卡 --- 查询 开户银行
#define user_bankname    @"myself/bankname"
//银行卡 -- 查询用户银行卡
#define user_banklist    @"myself/banklist"
//二维码名片
#define user_erweima    @"myself/erweima"
//修改头像
#define user_uploadphoto    @"myself/uploadphoto"
//个人中心 -- 我的粉丝列表
#define user_fanslist    @"myself/fanslist"
//关注 -- 更改关注状态
#define user_editfollow    @"myself/editfollow"
//关注 -- 我的关注列表
#define user_myfollow    @"myself/myfollow"
//个人资料 -- 修改地区
#define user_editarea    @"myself/editarea"
//个人资料 -- 修改生日
#define user_editbirth    @"myself/editbirth"
//个人资料 -- 修改签名
#define user_editautograph    @"myself/editautograph"
//个人资料 -- 修改性别
#define user_editsex    @"myself/editsex"
//个人资料 -- 修改昵称
#define user_editusername    @"myself/editusername"
#endif /* Request_h */
