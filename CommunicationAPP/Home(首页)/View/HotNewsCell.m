//
//  HotNewsCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/3.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "HotNewsCell.h"

@interface HotNewsCell(){
    
    UILabel *_titleLabel;
    UILabel *_timeLabel;
    UIImageView *_contentImageView;
    UIButton *_startBtn;
}

@end

@implementation HotNewsCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(-10);
        make.height.equalTo(@44);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [headerView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(10);
        make.top.equalTo(headerView.mas_top).offset(5);
        make.right.equalTo(headerView.mas_right).offset(-10);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color666666;
    _timeLabel.font = [UIFont systemFontOfSize:11];
    [headerView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.bottom.equalTo(headerView.mas_bottom).offset(-5);
        make.right.equalTo(self->_timeLabel.mas_right).offset(0);
    }];
    
    _contentImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_contentImageView];
    [_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(0);
        make.right.equalTo(headerView.mas_right).offset(0);
        make.top.equalTo(headerView.mas_bottom).offset(0);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(0);
    }];
    
    _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_contentImageView addSubview:_startBtn];
    [_startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self->_contentImageView.mas_centerX);
        make.centerY.equalTo(self->_contentImageView.mas_centerY);
    }];
    
}


-(void)refreshModel{
    
    _titleLabel.text = @"杨幂《何以笙箫默》电影发布会";
    _timeLabel.text = @"1分钟前";
    _contentImageView.image = [UIImage imageNamed:@"test_icon"];
    [_startBtn setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    
}

-(void)startBtnClick{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
