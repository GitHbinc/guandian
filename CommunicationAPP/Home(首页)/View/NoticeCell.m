//
//  NoticeCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "NoticeCell.h"

@interface NoticeCell(){
    
    UIView *_redView;
    UILabel *_titleLabel;
    UILabel *_contentLabel;
    UILabel *_timeLabel;
    UIView *_lineView;
}

@end

@implementation NoticeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UIImageView *leftImageView = [[UIImageView alloc]init];
    leftImageView.image = [UIImage imageNamed:@"notice_icon"];
    [self.contentView addSubview:leftImageView];
    [leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@43);
        make.height.equalTo(@43);
    }];
    
    _redView = [[UIView alloc]init];
    _redView.backgroundColor = UIColor.redColor;
    _redView.layer.cornerRadius = 5.0;
    [self.contentView addSubview:_redView];
    [_redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftImageView.mas_right).offset(-5);
        make.top.equalTo(leftImageView.mas_top).offset(-5);
        make.height.equalTo(@10);
        make.width.equalTo(@10);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftImageView.mas_right).offset(10);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = color999999;
    _contentLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.bottom.equalTo(self.contentView).offset(-15);
        make.right.equalTo(self.contentView).offset(-22);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color999999;
    _timeLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
}

-(void)refreshModel{
    
//    _redView.hidden = true;
    _titleLabel.text = @"观点成功上市";
    _contentLabel.text = @"2019年i一月观点平台成功上市";
    _timeLabel.text = @"18/11/16";
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
