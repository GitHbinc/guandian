//
//  HotNewsHeaderView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/3.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BackButtonBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface HotNewsHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property(nonatomic,copy)BackButtonBlock block;

@end

NS_ASSUME_NONNULL_END
