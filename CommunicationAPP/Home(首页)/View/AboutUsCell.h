//
//  AboutUsCell.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AboutUsCell : UITableViewCell

-(void)refreshTitle:(NSString *)titleStr num:(NSString *)num;

@end

NS_ASSUME_NONNULL_END
