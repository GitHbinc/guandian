//
//  HotNewsHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/3.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "HotNewsHeaderView.h"
#import "SceneHeaderView.h"

@implementation HotNewsHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = UIColor.whiteColor;
    [self addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    SceneHeaderView *headerView = [[SceneHeaderView alloc]init];
    [self addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.centerY.equalTo(self.mas_centerY);
        make.height.equalTo(@38);
        make.right.equalTo(self).offset(0);
    }];
    
    headerView.lineView.hidden = true;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"back_icon"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
}

-(void)backBtnClick{
    
    if (self.block) {
        self.block();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
