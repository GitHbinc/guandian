//
//  AboutUsCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AboutUsCell.h"

@interface AboutUsCell(){
    
    UILabel *_titleLabel;
    UIView *_redView;
    UIView *_lineView;
    UILabel *_numLabel;
}

@end
@implementation AboutUsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{

    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _redView = [[UIView alloc]init];
    _redView.backgroundColor = UIColor.redColor;
    _redView.layer.cornerRadius = 10.0;
    _redView.hidden = true;
    [self.contentView addSubview:_redView];
    [_redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_right).offset(5);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    
    _numLabel = [[UILabel alloc]init];
    _numLabel.textColor = UIColor.whiteColor;
    _numLabel.font = [UIFont systemFontOfSize:13];
    [_redView addSubview:_numLabel];
    [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self->_redView.mas_centerX);
        make.centerY.equalTo(self->_redView.mas_centerY);
    }];
    
    
    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@1);
    }];
    
}

-(void)refreshTitle:(NSString *)titleStr num:(NSString *)num{
    
    if ([titleStr isEqualToString:@"公告"]) {
        _redView.hidden = false;
    }
    if ([titleStr hasPrefix:@"咨询热线"]) {
        _lineView.hidden = true;
    }
    _numLabel.text = num;
    _titleLabel.text = titleStr;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
