//
//  SceneVideoCell.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ButtonBlock)(UIButton *button);

NS_ASSUME_NONNULL_BEGIN

@interface SceneVideoCell : UITableViewCell

@property(nonatomic,copy)ButtonBlock bottomBlock;

@property (nonatomic,copy)UIImageView *moreImageView;

-(void)refreshCellModel;

@end

NS_ASSUME_NONNULL_END
