//
//  CollectionScrollViewHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "CollectionScrollViewHeaderView.h"


@interface CollectionScrollViewHeaderView(){
    
    UIImageView *_headerImageView;
    UILabel *_titleLabel;
    UILabel *_descLabel;
}

@end
@implementation CollectionScrollViewHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.image = [UIImage imageNamed:@"follow_bg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.layer.cornerRadius = 33;
    [self addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(10);
        make.width.equalTo(@65);
        make.height.equalTo(@65);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_right).offset(5);
        make.top.equalTo(self).offset(12);
        make.right.equalTo(self).offset(-10);
    }];
    
    _descLabel = [[UILabel alloc]init];
    _descLabel.textColor = color999999;
    _descLabel.font = [UIFont systemFontOfSize:10];
    [self addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.right.equalTo(self).offset(-10);
    }];
    
    UIButton *followBtn = [[UIButton alloc]init];
    [followBtn setImage:[UIImage imageNamed:@"follow_normal_icon"] forState:UIControlStateNormal];
    [followBtn addTarget:self action:@selector(followBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:followBtn];
    [followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_descLabel.mas_bottom).offset(10);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];

}


-(void)followBtnClick:(UIButton *)senderBtn{
    
}

-(void)refreshModel{
    _headerImageView.image = [UIImage imageNamed:@"header_icon"];
    _titleLabel.text = @"临沂灵动临沂";
    _descLabel.text = @"舞动不停舞动不停";
    
}

@end
