//
//  SceneHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SceneHeaderView.h"

@implementation SceneHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *searchImageView = [[UIImageView alloc]init];
    searchImageView.userInteractionEnabled = true;
    searchImageView.image = [UIImage imageNamed:@"search_bg_icon"];
    [self addSubview:searchImageView];
    [searchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self).offset(-65);
    }];
    
    UIButton *zanBtn = [[UIButton alloc]init];
    [zanBtn setImage:[UIImage imageNamed:@"zan_icon"] forState:UIControlStateNormal];
    zanBtn.tag = 101;
    [zanBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchImageView addSubview:zanBtn];
    [zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchImageView.mas_left).offset(7);
        make.centerY.equalTo(searchImageView.mas_centerY);
        make.height.equalTo(@27);
        make.width.equalTo(@27);
    }];
    
    UIButton *searchBtn = [[UIButton alloc]init];
    [searchBtn setImage:[UIImage imageNamed:@"search_icon"] forState:UIControlStateNormal];
    searchBtn.tag = 102;
    [searchBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchImageView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(zanBtn.mas_right).offset(7);
        make.centerY.equalTo(searchImageView.mas_centerY);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    UIButton *searchTitleBtn = [[UIButton alloc]init];
    [searchTitleBtn setTitle:@"搜索你想要的直播" forState:UIControlStateNormal];
    [searchTitleBtn setTitleColor:color999999 forState:UIControlStateNormal];
    searchTitleBtn.titleLabel.font = [UIFont systemFontOfSize:9];
    searchTitleBtn.tag = 102;
    [searchTitleBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchImageView addSubview:searchTitleBtn];
    [searchTitleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchBtn.mas_right).offset(5);
        make.centerY.equalTo(searchImageView.mas_centerY);
        make.height.equalTo(@15);
    }];
    
    UIButton *shangBtn = [[UIButton alloc]init];
    [shangBtn setImage:[UIImage imageNamed:@"shang_icon"] forState:UIControlStateNormal];
    shangBtn.tag = 103;
    [shangBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchImageView addSubview:shangBtn];
    [shangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(searchImageView.mas_right).offset(-7);
        make.centerY.equalTo(searchImageView.mas_centerY);
        make.height.equalTo(@27);
        make.width.equalTo(@27);
    }];
    
    UIButton *moreBtn = [[UIButton alloc]init];
    [moreBtn setImage:[UIImage imageNamed:@"more_icon"] forState:UIControlStateNormal];
    moreBtn.tag = 105;
    [moreBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIButton *shareBtn = [[UIButton alloc]init];
    [shareBtn setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    shareBtn.tag = 104;
    [shareBtn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(moreBtn.mas_left).offset(-15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _lineView = [[UIView alloc]init];
    _lineView.backgroundColor = colorD6D6D6;
    [self addSubview:_lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(-1);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(@1);
    }];
}


-(void)filterBtnClick:(UIButton *)senderBtn{
    
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
