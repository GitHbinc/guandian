//
//  FollowCollectionViewCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "FollowCollectionViewCell.h"


@interface FollowCollectionViewCell(){
    
    UIImageView *_bgImageView;
    UIImageView *_leftImageView;
    UILabel *_amountLabel;
    UILabel *_scoreLabel;
    UIImageView *_headerImageView;
    UILabel *_titleLabel;
    UILabel *_descLabel;
    
}

@end
@implementation FollowCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //添加自己需要个子视图控件
        [self setUpAllChildView];
    }
    return self;
}

-(void)setUpAllChildView{
    
    _bgImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_bgImageView];
    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    _leftImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    _amountLabel = [[UILabel alloc]init];
    _amountLabel.textColor = colorEF1D1E;
    _amountLabel.font = [UIFont systemFontOfSize:9];
    [_leftImageView addSubview:_amountLabel];
    [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_left).offset(20);
        make.centerY.equalTo(self->_leftImageView.mas_centerY);
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"score_icon"];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    _scoreLabel = [[UILabel alloc]init];
    _scoreLabel.textColor = color666666;
    _scoreLabel.font = [UIFont systemFontOfSize:9];
    [imageView addSubview:_scoreLabel];
    [_scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(imageView.mas_centerY);
        make.centerX.equalTo(imageView.mas_centerX);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.layer.cornerRadius = 15;
    [self.contentView addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = UIColor.whiteColor;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_right).offset(5);
        make.top.equalTo(self->_headerImageView.mas_top).offset(0);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    _descLabel = [[UILabel alloc]init];
    _descLabel.textColor = UIColor.whiteColor;
    _descLabel.font = [UIFont systemFontOfSize:9];
    [self.contentView addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.right.equalTo(self->_titleLabel.mas_right).offset(0);
    }];
    
    UIButton *auctionBtn = [[UIButton alloc]init];
    [auctionBtn setImage:[UIImage imageNamed:@"auction_icon"] forState:UIControlStateNormal];
    [auctionBtn addTarget:self action:@selector(auctionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:auctionBtn];
    [auctionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
}

-(void)auctionBtnClick:(UIButton *)senderBtn{
    
}

-(void)refreshModel{
    
    _bgImageView.image = [UIImage imageNamed:@"test_icon"];
    _leftImageView.image = [UIImage imageNamed:@"bottom_zan_icon"];
    _amountLabel.text = @"1266";
    _scoreLabel.text = @"评分9.0";
    _headerImageView.image = [UIImage imageNamed:@"header_icon"];
    _titleLabel.text = @"函数与积分";
    _descLabel.text = @"张佳飞老师";
    
}
@end
