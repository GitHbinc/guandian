//
//  TotalClassfyViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "TotalClassfyViewCell.h"

@implementation TotalClassfyViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor yellowColor];
        _btn = [QMUIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        _btn.spacingBetweenImageAndTitle = 5.0;
        [_btn setTitle:@"关注" forState:UIControlStateNormal];
        [_btn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btn setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
        [_btn setImage:[UIImage imageNamed:@"xianchang_guanzhu"] forState:UIControlStateNormal];
        [_btn setImagePosition:QMUIButtonImagePositionTop];
        _btn.userInteractionEnabled = NO;
        [self.contentView addSubview:_btn];
    }
    
    return self;
}


@end
