//
//  TotalClassfyViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "TotalClassfyViewController.h"
#import "TotalClassfyViewCell.h"
#import "MineItem.h"
@interface TotalClassfyViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *mainCollectionView;
}


@end

@implementation TotalClassfyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"全部分类";
        
    self.view.backgroundColor = [UIColor whiteColor];
     UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, Navi_STATUS_HEIGHT, kSCREENWIDTH, kSCREENHEIGHT) collectionViewLayout:layout];
    [self.view addSubview:mainCollectionView];
    mainCollectionView.backgroundColor = [UIColor clearColor];
    
   [mainCollectionView registerClass:[TotalClassfyViewCell class] forCellWithReuseIdentifier:@"TotalClassfyViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    
     [self getDataFromPlist];
}

/** 数据源*/
- (void)getDataFromPlist{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ClassfyList" ofType:@"plist"];
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:plistPath];
    for (NSDictionary *dic in array) {
        MineItem *item = [[MineItem alloc]initWithDictionary:dic error:nil];
        [self.dataSource addObject:item];
    }
    [mainCollectionView reloadData];
}




#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    TotalClassfyViewCell *cell = (TotalClassfyViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TotalClassfyViewCell" forIndexPath:indexPath];
    MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
    [cell.btn setTitle:item.name forState:UIControlStateNormal];
    [cell.btn setImage:[UIImage imageNamed:item.icon] forState:UIControlStateNormal];
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kSCREENWIDTH/4, kSCREENWIDTH/4*1.2);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
    if ([self.delegate respondsToSelector:@selector(selectTypeOfTitles:)]) {
        
        [self.delegate selectTypeOfTitles:indexPath.item];
//        [self.delegate selectTypeOfTitles:item.name];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
