//
//  HotNewsViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/3.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "HotNewsViewController.h"
#import "HotNewsHeaderView.h"
#import "HotNewsCell.h"

@interface HotNewsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,copy)UITableView *tableView;

@end

@implementation HotNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HotNewsHeaderView *headerView = [HotNewsHeaderView customHeadViewWithFrame:CGRectMake(0, 20, kSCREENWIDTH, 60)];
    headerView.backgroundColor = colorf3f3f3;
    [self.view addSubview:headerView];
    headerView.block = ^{
        
        [self.navigationController popViewControllerAnimated:true];
    };
    
    [self createTableView];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
}

#pragma mark 创建tableView
-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, kSCREENWIDTH, kSCREENHEIGHT- 80) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = colorf3f3f3;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}


#pragma mark tableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 200;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"findCellID";
    HotNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[HotNewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = colorf3f3f3;
    [cell refreshModel];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
