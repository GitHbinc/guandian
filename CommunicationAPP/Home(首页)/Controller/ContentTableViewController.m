//
//  ContentTableViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ContentTableViewController.h"
//上面的轮播图
#import "PowerfulBannerView.h"
#import "FindPageControl.h"
//公告栏滚动控件
#import "ADRollView.h"
#import "SceneVideoCell.h"
#import "HotNewsViewController.h"

@interface ContentTableViewController ()<UITextFieldDelegate>{
    UIView *_comentView;
    UIView *_bgView;
    UIButton *_commentBtn;
}

@property (nonatomic,strong) PowerfulBannerView *bannerView;
@property (nonatomic, strong) NSMutableArray *bannerArray;
@property (nonatomic, strong) ADRollView *adRollView;
@property (nonatomic, strong) NSMutableArray *adsArray;


@end

@implementation ContentTableViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.adRollView start];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.adRollView stopTimer];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.backgroundColor = colorf3f3f3;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = false;
    NSArray *array = @[@"video_bg_icon",@"bottom_shang_icon",@"bottom_zan_icon",@"redian_icon"];
    self.bannerArray = [NSMutableArray arrayWithArray:array];
    
    NSArray *array1 = @[@"杨幂领衔主演，杨幂领衔主演",@"第二条数据，第二条数据",@"第三天数据",@"redian_icon"];
    self.adsArray = [NSMutableArray arrayWithArray:array1];
    [self setBannerViewUIAndAdRollView];
//    [self.tableView reloadData];
}

#pragma mark 创建bannert视图和广告滚动视图
-(void)setBannerViewUIAndAdRollView{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 200)];
    headerView.backgroundColor = colorf3f3f3;
    
    self.bannerView = [[PowerfulBannerView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 148)];
    self.bannerView.pageControl = [self setUpPageCOntroller:self.bannerView];
    self.bannerView.bannerItemConfigurationBlock = ^(PowerfulBannerView *banner, id item, UIView *reusableView){
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:banner.frame];
        imageView.image = [UIImage imageNamed:item];
//        [imageView sd_setImageWithURL:[NSURL URLWithString:item] placeholderImage:nil];
        return imageView;
    };
    
    self.bannerView.bannerDidSelectBlock = ^(PowerfulBannerView *banner, NSInteger index){  // 广告跳转
        
    };
    self.bannerView.autoLooping = YES;
    [headerView addSubview:self.bannerView];
    
    self.adRollView = [[ADRollView alloc] initWithFrame:CGRectMake(0, 148 + 5, kSCREENWIDTH, 35)];
    self.adRollView.backgroundColor = [UIColor whiteColor];
    
    __weak typeof(self) weakSelf = self;
    self.adRollView.clickBlock = ^(NSInteger index) {
        
        HotNewsViewController *controller = [[HotNewsViewController alloc]init];
        controller.hidesBottomBarWhenPushed = true;
        [weakSelf.navigationController pushViewController:controller animated:true];
    };
    [headerView addSubview:self.adRollView];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(11, 11, 51, 13)];
    imageView.image = [UIImage imageNamed:@"redian_icon"];
    [self.adRollView addSubview:imageView];
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(75, 8, 1, 19)];
    lineView.backgroundColor = colorD6D6D6;
    [self.adRollView addSubview:lineView];
    
    self.tableView.tableHeaderView = headerView;
    
    [self showData];
    [self showAdView];
}

#pragma mark BannerView 显示数据
- (void)showData
{
//    NSMutableArray *imageArr = [[NSMutableArray alloc] init];
//    for (NSInteger i = 0; i< self.bannerArray.count; i++) {
//        BannerList *model = self.bannerArray[i];
//        [imageArr addObject:model.banner];
//    }
//    self.bannerView.items = imageArr;
    self.bannerView.items = self.bannerArray;
    [self.bannerView reloadData];
}

#pragma mark adRollView 显示数据
- (void)showAdView
{
//    NSMutableArray *arr = [[NSMutableArray alloc] init];
//    for (int i = 0; i < self.adsArray.count; i++) {
//        BroadList *model = self.adsArray[i];
//        ADRollModel *model1 = [[ADRollModel alloc] init];
//        model1.noticeType = @"";
//        model1.addTime  = @"";
//        model1.urlString = @"";
//        model1.noticeTitle = model.broadcastTitle;
//
//        [arr addObject:model1];
//    }
    
//    if (arr.count > 0){
//        self.adRollView.count = 0;
//        [self.adRollView setVerticalShowDataArr:arr];
//    }
    
    if (self.adsArray.count > 0){
        self.adRollView.count = 0;
        [self.adRollView setVerticalShowDataArr:self.adsArray];
    }
}

#pragma mark 设置banner图的原点
- (FindPageControl *)setUpPageCOntroller:(PowerfulBannerView *)bannerView
{
    FindPageControl *pg = [[FindPageControl alloc] initWithFrame:CGRectMake(0,148 - 20, kSCREENWIDTH, 20) indicatorMargin:5 indicatorWidth:4 currentIndicatorWidth:8 indicatorHeight:4];
    pg.numberOfPages = 3;
    pg.pageIndicatorColor = [UIColor whiteColor];
    pg.currentPageIndicatorColor = MAINCOLOR;
    pg.alpha = 0.7;
    [bannerView addSubview:pg];
    return pg ;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"findCellID";
    SceneVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[SceneVideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = colorf3f3f3;
    [cell refreshCellModel];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __block SceneVideoCell *blockSelf = cell;
    cell.bottomBlock = ^(UIButton *senderBtn){
        NSLog(@"======%ld",senderBtn.tag);
        
        [self cellBtnClick:blockSelf senderBtn:senderBtn];
    };
    
    // Configure the cell...
    
    return cell;
}


#pragma mark cell底部的按钮。点赞、评论、转发、分享、关注、收藏、排行、竞拍等
-(void)cellBtnClick:(SceneVideoCell *)cell senderBtn:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
            
        case 101://点赞
            break;
        case 102://评论
        {
            
            _commentBtn = senderBtn;
            _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
            _bgView.userInteractionEnabled = true;
            //处理父视图透明度会影响子视图
            _bgView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
            UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeCommentView)];
            [_bgView addGestureRecognizer:reg];

            [[UIApplication sharedApplication].keyWindow addSubview:_bgView];
            
            _comentView = [self setCommentView];
            _comentView.backgroundColor = UIColor.whiteColor;
            [_bgView addSubview:_comentView];

        }
            break;
        case 103://转发
            break;
        case 104:
        {
            
            if (senderBtn.selected) {
                cell.moreImageView.hidden = false;
            }else{
                cell.moreImageView.hidden = true;
            }
        }
            break;
        case 105://分享
            break;
        case 106://关注
            break;
        case 107://收藏
            break;
        case 108://排行
            break;
        case 109://竞拍
            break;
        case 110://举报
            break;
        case 111://拉黑
            break;
        case 112://最大化
            break;
        default:
            break;
    }
    
}

#pragma mark 移除评论s视图
-(void)removeCommentView{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [_bgView removeFromSuperview];
}


#pragma mark 创建评论视图
-(UIView *)setCommentView{
    UIView *commentView = [[UIView alloc]initWithFrame:CGRectMake(0, kSCREENHEIGHT - 410, kSCREENWIDTH, 63)];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [commentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(0);
        make.right.equalTo(commentView.mas_right).offset(0);
        make.top.equalTo(commentView.mas_top).offset(0);
        make.height.equalTo(@1);
    }];
    
    UIButton *expressionBtn = [[UIButton alloc]init];
    [expressionBtn setImage:[UIImage imageNamed:@"comment_expressionBtn_icon"] forState:UIControlStateNormal];
    expressionBtn.tag = 101;
    [expressionBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:expressionBtn];
    [expressionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(10);
//        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.height.equalTo(@28);
        make.width.equalTo(@28);
    }];
    
    UITextField *contentField = [[UITextField alloc]init];
    contentField.placeholder = @"请输入评论内容....";
    contentField.delegate = self;
    [contentField becomeFirstResponder];
    [commentView addSubview:contentField];
    [contentField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(expressionBtn.mas_right).offset(10);
//        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.width.equalTo(@(kSCREENWIDTH - 100 - 48));
        make.height.equalTo(@31);
    }];
    
    UIButton *sendBtn = [[UIButton alloc]init];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:[UIImage imageNamed:@"comment_sendBtn_icon"] forState:UIControlStateNormal];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [sendBtn setTitleColor:color999999 forState:UIControlStateNormal];
    sendBtn.tag = 102;
    [sendBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(commentView.mas_right).offset(-10);
//        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
    }];
    
    return commentView;
}

#pragma mark 点击评论按钮
-(void)commentBtnClick:(UIButton *)senderBtn{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [_bgView removeFromSuperview];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
