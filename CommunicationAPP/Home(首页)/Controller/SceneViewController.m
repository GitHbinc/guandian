//
//  SceneViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SceneViewController.h"
#import "SceneHeaderView.h"
#import "ContentTableViewController.h"
#import "MineViewController.h"
#import "FollowViewController.h"
#import "TotalClassfyViewController.h"
#import "AboutUsViewController.h"
#import "CenterTabBarView.h"
#import "ReleaseVideoViewController.h"
#import "LoginViewController.h"

@interface SceneViewController ()<UIScrollViewDelegate,TotalClassfyViewControllerDelegate>

//
@property(nonatomic,strong) NSArray *arrayLists;
@property (nonatomic, strong)UIScrollView *titleScrollView;
@property (nonatomic, strong)UIScrollView *contentScrollView;
@property (nonatomic, strong) CenterTabBarView *centerView;

@end

@implementation SceneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     SceneHeaderView *headerView = [SceneHeaderView customHeadViewWithFrame:CGRectMake(0, 20, kSCREENWIDTH, 44)];
    headerView.block = ^(UIButton *senderBtn){
        NSLog(@"======%ld",senderBtn.tag);
        [self headerBtnClick:senderBtn];
    };
    [self.view addSubview:headerView];
    
    _arrayLists = @[@"关注",@"热门",@"音乐",@"舞蹈",@"上海",@"教育",@"明星",@"课程",@"财富",@"徐州",@"新沂"];
    
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;

}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}


#pragma mark 头部按钮点击事件 赞、搜索、赏、分享、关于我们
-(void)headerBtnClick:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
        case 101:
            
            break;
        case 102:
            break;
        case 103:
            break;
        case 104:
            break;
        case 105:
        {
            AboutUsViewController *controller = [[AboutUsViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
        }
            break;
        default:
            break;
    }
}

#pragma mark关注、热门等头部视图
-(void)setUI{
    
    self.titleScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(20, 70, kSCREENWIDTH - 50, 44)];
    [self.view addSubview:self.titleScrollView];
    self.titleScrollView.backgroundColor = UIColor.whiteColor;
    
    UIButton *allBtn = [[UIButton alloc]initWithFrame:CGRectMake(kSCREENWIDTH - 50, 70, 50, 44)];
    [allBtn setImage:[UIImage imageNamed:@"fenlei_icon"] forState:UIControlStateNormal];
    [allBtn addTarget:self action:@selector(allBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:allBtn];
    
    
    self.contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 120, kSCREENWIDTH, kSCREENHEIGHT - 120 - 44)];
    [self.view addSubview:self.contentScrollView];

    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.titleScrollView.showsHorizontalScrollIndicator = NO;
    self.titleScrollView.showsVerticalScrollIndicator = NO;
    self.contentScrollView.delegate = self;
    
    [self addController];
    [self addLable];
    
    
    CGFloat contentX = self.childViewControllers.count * [UIScreen mainScreen].bounds.size.width;
    self.contentScrollView.contentSize = CGSizeMake(contentX, 0);
    self.contentScrollView.pagingEnabled = YES;
    
    // 添加默认控制器
    
    FollowViewController *vc = [self.childViewControllers firstObject];
//    ContentTableViewController *vc = [self.childViewControllers firstObject];
    vc.view.frame = self.contentScrollView.bounds;
    [self.contentScrollView addSubview:vc.view];
    UILabel *lable = [self.titleScrollView.subviews firstObject];
    lable.textColor = MAINCOLOR;
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    
}

#pragma mark 全部分类按钮
-(void)allBtnClick{
    
//    LoginViewController *controller = [[LoginViewController alloc]init];
////    [self presentViewController:controller animated:true completion:nil];
//    controller.hidesBottomBarWhenPushed = true;
//    [self.navigationController pushViewController:controller animated:true];
    
//    TotalClassfyViewController *classfy = [TotalClassfyViewController new];
//    classfy.delegate = self;
//    classfy.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:classfy animated:YES];
}

- (void)selectTypeOfTitles:(NSInteger)tag{
    
    CGFloat offsetX = tag * self.contentScrollView.frame.size.width;
    
    CGFloat offsetY = self.contentScrollView.contentOffset.y;
    CGPoint offset = CGPointMake(offsetX, offsetY);
    
    [self.contentScrollView setContentOffset:offset animated:YES];
    NSLog(@"=========%ld",tag);
}
/** 添加子控制器 */
#pragma mark 添加子控制器
- (void)addController
{
    for (int i=0 ; i<self.arrayLists.count ;i++){
        
        if (i == 0) {
            FollowViewController *vc1 = [[FollowViewController alloc]init];
            vc1.title = self.arrayLists[i];
            [self addChildViewController:vc1];
        }else{
        
//            ContentCollectionViewController *vc1 = [[ContentCollectionViewController alloc]init];
            ContentTableViewController *vc1 = [[ContentTableViewController alloc]init];
            vc1.title = self.arrayLists[i];
            
            //        SXTableViewController *vc1 = [[UIStoryboard storyboardWithName:@"News" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
            //        vc1.title = self.arrayLists[i][@"title"];
            //        vc1.urlString = self.arrayLists[i][@"urlString"];
            [self addChildViewController:vc1];
        }
    }
}

/** 添加标题栏 */
#pragma mark 添加标题栏
- (void)addLable
{
    for (int i = 0; i < _arrayLists.count; i++) {
        CGFloat lblW = 50;
        CGFloat lblH = 40;
        CGFloat lblY = 0;
        CGFloat lblX = i * lblW;
        UILabel *lbl1 = [[UILabel alloc]init];
        if (i == 0) {
            
            FollowViewController *vc = self.childViewControllers[i];
            lbl1.text = vc.title;
        }else{
            
//            ContentCollectionViewController *vc = self.childViewControllers[i];
            ContentTableViewController *vc = self.childViewControllers[i];
            lbl1.text = vc.title;
        }
        
        lbl1.frame = CGRectMake(lblX, lblY, lblW, lblH);
        lbl1.font = [UIFont systemFontOfSize:15];
        lbl1.textColor = color666666;
        [self.titleScrollView addSubview:lbl1];
        lbl1.tag = i;
        lbl1.userInteractionEnabled = YES;
        
        [lbl1 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];
    }
    self.titleScrollView.contentSize = CGSizeMake(50 * _arrayLists.count, 0);
    
}

/** 标题栏label的点击事件 */
#pragma mark 标题栏label的点击事件
- (void)lblClick:(UITapGestureRecognizer *)recognizer
{
    UILabel *titlelable = (UILabel *)recognizer.view;
    
    CGFloat offsetX = titlelable.tag * self.contentScrollView.frame.size.width;
    
    CGFloat offsetY = self.contentScrollView.contentOffset.y;
    CGPoint offset = CGPointMake(offsetX, offsetY);
    
    [self.contentScrollView setContentOffset:offset animated:YES];
    
}


#pragma mark - ******************** scrollView代理方法

/** 滚动结束后调用（代码导致） */
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    // 获得索引
    NSUInteger index = scrollView.contentOffset.x / self.contentScrollView.frame.size.width;
    
    // 滚动标题栏
    UILabel *titleLable = (UILabel *)self.titleScrollView.subviews[index];
    
    CGFloat offsetx = titleLable.center.x - self.titleScrollView.frame.size.width * 0.5;
    
    CGFloat offsetMax = self.titleScrollView.contentSize.width - self.titleScrollView.frame.size.width;
    if (offsetx < 0) {
        offsetx = 0;
    }else if (offsetx > offsetMax){
        offsetx = offsetMax;
    }
    
    CGPoint offset = CGPointMake(offsetx, self.titleScrollView.contentOffset.y);
    [self.titleScrollView setContentOffset:offset animated:YES];
    // 添加控制器
    UIViewController *newsVc;

    newsVc = self.childViewControllers[index];
//      ContentCollectionViewController *newsVc = self.childViewControllers[index];
//    ContentTableViewController *newsVc = self.childViewControllers[index];
    //    newsVc.index = index;
    
    [self.titleScrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx != index) {
            UILabel *temlabel = self.titleScrollView.subviews[idx];
            temlabel.textColor = color666666;
            //            temlabel.scale = 0.0;
        }
    }];
    
    if (newsVc.view.superview) return;
    newsVc.view.frame = scrollView.bounds;
    [self.contentScrollView addSubview:newsVc.view];
}

/** 滚动结束（手势导致） */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

/** 正在滚动 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 取出绝对值 避免最左边往右拉时形变超过1
    CGFloat value = ABS(scrollView.contentOffset.x / scrollView.frame.size.width);
    NSUInteger leftIndex = (int)value;
    NSUInteger rightIndex = leftIndex + 1;
    UILabel *labelLeft = self.titleScrollView.subviews[leftIndex];
    labelLeft.textColor = MAINCOLOR;
    // 考虑到最后一个板块，如果右边已经没有板块了 就不在下面赋值scale了
    if (rightIndex < self.titleScrollView.subviews.count) {
        UILabel *labelRight = self.titleScrollView.subviews[rightIndex];
        labelRight.textColor = MAINCOLOR;
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
