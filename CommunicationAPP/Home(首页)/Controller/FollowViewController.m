//
//  FollowViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "FollowViewController.h"
#import "FollowCollectionViewCell.h"
#import "CollectionScrollViewHeaderView.h"

@interface FollowViewController ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)UIScrollView *headerScrollView;
@property(nonatomic,strong) NSArray *arrayLists;

@end

@implementation FollowViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrayLists = @[@"关注",@"热门",@"音乐",@"舞蹈"];
    [self setCollectionUI];
    // Do any additional setup after loading the view.
}


#pragma mark 创建UICollectionView
-(void)setCollectionUI{
    //创建一个流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    CGFloat width = (kSCREENWIDTH - 5) / 2;
    //设置cell的尺寸(宽度和高度)
    layout.itemSize = CGSizeMake(width, width);
    //设置竖直滚动放向(默认是竖直方向)
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    //设置cell与cell之间的列距
    layout.minimumInteritemSpacing = 1;
    //设置cell与cell之间的行距
    layout.minimumLineSpacing = 5;
    //头部的大小
    layout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 210);
    
    //创建UICollectionView
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT - 130 - 44) collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor whiteColor];
    //添加到视图
    [self.view addSubview:collectionView];
    collectionView.scrollsToTop = NO;
    //开启分页
    collectionView.pagingEnabled = YES;
    //不显示滚动条
    collectionView.showsHorizontalScrollIndicator = NO;
    //弹簧效果设置
    collectionView.bounces = NO;
    //设置代理
    collectionView.dataSource = self;
    collectionView.delegate = self;
    //注册cell
    [collectionView registerClass:[FollowCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
    
}

#pragma mark 创建CollectionHeaderView
-(UIView *)setCollectionHeaderViewUI{
    
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = colorf3f3f3;
    UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 44)];
    headerImageView.image = [UIImage imageNamed:@"tuijian_icon"];
    [headerView addSubview:headerImageView];
    
    self.headerScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, kSCREENWIDTH, 110)];
    self.headerScrollView.backgroundColor = UIColor.whiteColor;
    self.headerScrollView.contentSize = CGSizeMake(175, 90);
    self.headerScrollView.showsHorizontalScrollIndicator = NO;//不显示水平拖地的条
    self.headerScrollView.showsVerticalScrollIndicator=NO;//不显示垂直拖动的条
    self.headerScrollView.pagingEnabled = YES;//允许分页滑动
    self.headerScrollView.bounces = NO;//到边了就不能再拖地
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [headerView addSubview:self.headerScrollView];
    headerView.frame = CGRectMake(0, 0, kSCREENWIDTH, 210);
    
    [self addView];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 170, kSCREENWIDTH, 44)];
    imageView.image = [UIImage imageNamed:@"collection_header_icon"];
    [headerView addSubview:imageView];
    
    return headerView;
    
}

#pragma mark添加关注
- (void)addView
{
    for (int i = 0; i < _arrayLists.count; i++) {
        CGFloat lblW = 175;
        CGFloat lblH = 90;
        CGFloat lblY = 10;
        CGFloat lblX = i * lblW;
        CollectionScrollViewHeaderView *view = [[CollectionScrollViewHeaderView alloc]init];
        view.frame = CGRectMake(lblX, lblY, lblW, lblH);
        [self.headerScrollView addSubview:view];
        [view refreshModel];
        
    }
    self.headerScrollView.contentSize = CGSizeMake(175 * _arrayLists.count, 0);
    
}

#pragma mark collection delegate
//设置分组数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
//设置每个分组个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 6;
}
//只有新的cell出现的时候才会调用

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //只要有新的cell出现，就把对应的数据模型添加到新的cell中
    FollowCollectionViewCell *cell = (FollowCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    //移除之前的子控制器的view
    //[cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [cell refreshModel];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView" forIndexPath:indexPath];
    
    UIView *header = [self setCollectionHeaderViewUI];
    [headerView addSubview:header];
    return headerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
