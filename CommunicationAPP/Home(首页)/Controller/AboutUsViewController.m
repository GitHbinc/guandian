//
//  AboutUsViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/3.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AboutUsViewController.h"
#import "HotNewsCell.h"
#import "AboutUsCell.h"
#import "NoticeViewController.h"

@interface AboutUsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,copy) UITableView *tableView;
@property (nonatomic,copy) NSArray *dataArray;
@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = colorf3f3f3;
    self.title = @"关于我们";
    _dataArray = @[@"公告",@"联系我们",@"咨询热线：4009983663"];
    [self createTableView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
}

#pragma mark 创建tableView
-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, kSCREENWIDTH, kSCREENHEIGHT- 80) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = colorf3f3f3;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    _tableView.tableHeaderView = [self createHeaderView];
}

#pragma mark 创建tableViewHeaderView
-(UIView *)createHeaderView{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 140)];
    headerView.backgroundColor = colorf3f3f3;
    
    UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake((kSCREENWIDTH - 67)/2, 15, 67, 67)];
    headerImageView.image = [UIImage imageNamed:@"aboutUs_icon"];
    [headerView addSubview:headerImageView];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 90, kSCREENWIDTH, 20)];
    titleLabel.textColor = color999999;
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"观点";
    [headerView addSubview:titleLabel];
    
    return headerView;
}


#pragma mark tableView delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"AboutUsCellID";
    AboutUsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AboutUsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell refreshTitle:_dataArray[indexPath.row] num:@"1"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
        {
            NoticeViewController *controller = [[NoticeViewController alloc]init];
            [self.navigationController pushViewController:controller animated:true];
        }
            break;
            
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
