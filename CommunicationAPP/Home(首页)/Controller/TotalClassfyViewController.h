//
//  TotalClassfyViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TotalClassfyViewControllerDelegate <NSObject>

@optional

- (void)selectTypeOfTitles:(NSInteger)tag; //分类标题
@end

@interface TotalClassfyViewController : BaseViewController

@property (nonatomic, weak) id <TotalClassfyViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
