//
//  AuctionListViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionListViewCell.h"
@interface AuctionListViewCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *timeLabel;
@property (nonatomic ,strong)UILabel *priceLabel;

@end

@implementation AuctionListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:20];
    [self.contentView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"小白";
    _nameLabel.textColor = HEXCOLOR(0x111111);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_nameLabel];
    
    
    _timeLabel = [UILabel new];
    _timeLabel.text = @"2018.12.18 19:30";
    _timeLabel.textColor = HEXCOLOR(0x999999);
    _timeLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_timeLabel];
    
    _priceLabel = [UILabel new];
    _priceLabel.text = @"¥12.00";
    _priceLabel.textColor = HEXCOLOR(0x999999);
    _priceLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_priceLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.headImage.mas_top)setOffset:2];
    }];
    
    
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(weakSelf.headImage.mas_bottom)setOffset:-2];
    }];
    
    [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf)setOffset:-10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
