//
//  PublishAuctionHeaderView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PublishAuctionHeaderView.h"
@interface PublishAuctionHeaderView()

@property(nonatomic ,strong)UITextField *nameText;
@property(nonatomic ,strong)IQTextView *introduceText;
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *locateLabel;;

@end
@implementation PublishAuctionHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self setUpUI];
        
    }
    
    return self;
}

-(void)setUpUI{
    _nameText = [UITextField new];
    _nameText.textColor = HEXCOLOR(0x333333);
    _nameText.font = [UIFont systemFontOfSize:15];
    _nameText.placeholder = @"请输入群昵称";
    [self addSubview:_nameText];
    [_nameText mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.right.mas_equalTo(self)setOffset:-10];
        make.top.mas_equalTo(self);
        make.height.mas_equalTo(50);
        
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self->_nameText);
        make.bottom.mas_equalTo(self->_nameText.mas_bottom);
        make.height.mas_equalTo(0.8);
        
    }];
    
    _introduceText = [IQTextView new];
    NSString *holderText = @"显示默认的群介绍，如果有默认介绍显示群昵称一样的字体色号，如之前没有设置，提示输入框信息";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:15]
                        range:NSMakeRange(0, holderText.length)];
    
    _introduceText.attributedPlaceholder = placeholder;
    [self addSubview:_introduceText];
    [_introduceText mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.right.mas_equalTo(self)setOffset:-10];
        make.top.mas_equalTo(self->_nameText.mas_bottom);
        make.height.mas_equalTo(100);
        
    }];
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"cartoon.png"];
    _headImage.clipsToBounds = YES;
    _headImage.layer.cornerRadius = 30;
    [self addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        make.top.mas_equalTo(self->_introduceText.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    UIImageView *locateImage = [[UIImageView alloc]init];
    locateImage.image = [UIImage imageNamed:@"jingpailocate"];
    [self addSubview:locateImage];
    [locateImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:20];
        
    }];
    
    _locateLabel = [UILabel new];
    _locateLabel.text = @"上家 长宁区 旭辉虹桥国际";
    _locateLabel.textColor = HEXCOLOR(0x999999);
    _locateLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_locateLabel];
    [_locateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(locateImage.mas_right)setOffset:5];
        make.centerY.mas_equalTo(locateImage.mas_centerY);
        
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
