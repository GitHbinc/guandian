//
//  AuctionPriceViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionPriceViewCell.h"
@interface AuctionPriceViewCell()
@property(nonatomic ,strong)UILabel *priceLabel;

@end
@implementation AuctionPriceViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}



-(void)setModel:(AuctionModel *)model{
    _model = model;
    _nameLabel = [UILabel new];
    _nameLabel.text = @"最低出价";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:10];
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        
    }];
    
    if (model.isInput) {
        _priceTxt = [UITextField new];
        _priceTxt.placeholder = @"¥0.00";
        _priceTxt.font = [UIFont systemFontOfSize:14];
        _priceTxt.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_priceTxt];
        [_priceTxt mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.right.mas_equalTo(self.contentView)setOffset:-10];
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.height.mas_equalTo(self.contentView.mas_height);
            make.width.mas_equalTo(100);
        }];
    }else{
        _priceLabel = [UILabel new];
        _priceLabel.text = @"¥8.00";
        _priceLabel.textColor = HEXCOLOR(0x333333);
        _priceLabel.font =  [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.right.mas_equalTo(self.contentView)setOffset:-10];
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        }];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
