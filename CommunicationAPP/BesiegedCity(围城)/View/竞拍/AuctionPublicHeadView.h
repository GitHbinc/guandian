//
//  AuctionPublicHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuctionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuctionPublicHeadView : UIView
@property(nonatomic ,strong)AuctionModel *model;
@end

NS_ASSUME_NONNULL_END
