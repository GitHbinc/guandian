//
//  AuctionPriceViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuctionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AuctionPriceViewCell : UITableViewCell
@property(nonatomic ,strong)UITextField *priceTxt;
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)AuctionModel *model;
@end

NS_ASSUME_NONNULL_END
