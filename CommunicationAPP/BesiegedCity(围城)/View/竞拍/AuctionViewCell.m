//
//  AuctionViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionViewCell.h"
@interface AuctionViewCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *nickLabel;
@property (nonatomic ,strong)UILabel *contentLabel;

@end

@implementation AuctionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI{
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:18];
    [self.contentView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"吴彦祖";
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_nameLabel];
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"2018-12-05 11:04";
    _dateLabel.textColor = HEXCOLOR(0x333333);
    _dateLabel.font =  [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_dateLabel];
    
    _nickLabel = [UILabel new];
    _nickLabel.text = @"崽小队";
    _nickLabel.textColor = HEXCOLOR(0x333333);
    _nickLabel.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nickLabel];
    
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚";
    _contentLabel.textColor =HEXCOLOR(0x333333);
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.numberOfLines = 2;
    _contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:_contentLabel];

    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.contentView.mas_left)setOffset:10];
        [make.top.mas_equalTo(weakSelf.contentView.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(36, 36));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(weakSelf.headImage.mas_centerY)setOffset:-5];
    }];
    
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom)setOffset:2];
    }];
    
    
    [_nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.dateLabel.mas_bottom)setOffset:20];
    }];
    
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.dateLabel.mas_left)setOffset:0];
        [make.top.mas_equalTo(weakSelf.nickLabel.mas_bottom)setOffset:20];
        make.right.mas_equalTo(weakSelf.contentView.mas_right).offset(-10);
        make.bottom.mas_equalTo(weakSelf.contentView.mas_bottom).offset(-10);
    }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
