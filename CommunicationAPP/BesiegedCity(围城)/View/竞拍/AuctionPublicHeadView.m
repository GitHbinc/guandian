//
//  AuctionPublicHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionPublicHeadView.h"
@interface AuctionPublicHeadView()

@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *contentLabel;
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *locateLabel;

@end
@implementation AuctionPublicHeadView


- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
       }
    
    return self;
}


-(void)setModel:(AuctionModel *)model{
    _model = model;
    _nameLabel = [UILabel new];
    _nameLabel.text = @"崽小队";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.right.mas_equalTo(self)setOffset:-10];
        make.top.mas_equalTo(self);
        make.height.mas_equalTo(50);
        
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self->_nameLabel);
        make.bottom.mas_equalTo(self->_nameLabel.mas_bottom);
        make.height.mas_equalTo(0.8);
        
    }];
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚";
    _contentLabel.textColor =HEXCOLOR(0x333333);
    _contentLabel.font = [UIFont systemFontOfSize:15];
    _contentLabel.numberOfLines = 0;
    _contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.right.mas_equalTo(self)setOffset:-10];
        [make.top.mas_equalTo(self->_nameLabel.mas_bottom)setOffset:15];
        
    }];
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"cartoon.png"];
    _headImage.clipsToBounds = YES;
    _headImage.layer.cornerRadius = 30;
    [self addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:20];
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    UIImageView *locateImage = [[UIImageView alloc]init];
    locateImage.image = [UIImage imageNamed:@"jingpailocate"];
    [self addSubview:locateImage];
    [locateImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:20];
        
    }];
    
    _locateLabel = [UILabel new];
    _locateLabel.text = @"上家 长宁区 旭辉虹桥国际";
    _locateLabel.textColor = HEXCOLOR(0x999999);
    _locateLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_locateLabel];
    [_locateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(locateImage.mas_right)setOffset:5];
        make.centerY.mas_equalTo(locateImage.mas_centerY);
        
    }];
    
    [self layoutIfNeeded];     // 下面会有关于layoutIfNeeded的介绍
    CGFloat tagViewHeight = _locateLabel.frame.origin.y;
    model.headHeight = tagViewHeight + 30;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
