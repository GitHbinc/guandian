//
//  AuctionDateViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionDateViewCell.h"
@interface AuctionDateViewCell()
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *dateLabel;

@end
@implementation AuctionDateViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _nameLabel = [UILabel new];
    _nameLabel.text = @"截止日期";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self addSubview:_nameLabel];
    
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"2018.12.19 16:00";
    _dateLabel.textColor = HEXCOLOR(0x333333);
    _dateLabel.font =  [UIFont systemFontOfSize:14];
    [self addSubview:_dateLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        
    }];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf)setOffset:-10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
