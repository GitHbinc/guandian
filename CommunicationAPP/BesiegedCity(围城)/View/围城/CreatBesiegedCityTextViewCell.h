//
//  CreatBesiegedCityTextViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BesiegedCityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CreatBesiegedCityTextViewCell : UITableViewCell
@property(nonatomic ,strong)BesiegedCityModel *model;
@end

NS_ASSUME_NONNULL_END
