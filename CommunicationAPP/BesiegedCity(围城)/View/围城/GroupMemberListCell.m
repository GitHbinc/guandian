//
//  GroupMemberListCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupMemberListCell.h"
#import "GroupHeadImageViewCell.h"
#import "GroupMemberViewController.h"
#import "MineItem.h"

@interface GroupMemberListCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *mainCollectionView;
}
@property (nonatomic, strong) NSMutableArray *dataSource; //数据源数组

@end
@implementation GroupMemberListCell

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setUpUI];

    }
    return self;
}

#pragma mark - UI
- (void)setUpUI{
   
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    mainCollectionView.frame = CGRectMake(0, 0, kSCREENWIDTH, kSCREENWIDTH/5*2.4);
    mainCollectionView.backgroundColor = [UIColor whiteColor];
    mainCollectionView.showsHorizontalScrollIndicator = NO;
    [mainCollectionView registerClass:[GroupHeadImageViewCell class] forCellWithReuseIdentifier:@"GroupHeadImageViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    [self.contentView addSubview:mainCollectionView];
    
   
}

#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GroupHeadImageViewCell *cell = (GroupHeadImageViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"GroupHeadImageViewCell" forIndexPath:indexPath];
    MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
    cell.item = item;
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kSCREENWIDTH/5, kSCREENWIDTH/5*1.2);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
    if ([item.name isEqualToString:@"更多"]) {
        GroupMemberViewController *member = [GroupMemberViewController new];
        [[self viewController].navigationController pushViewController:member animated:YES];
    }
}

-(void)setModel:(GroupMemberModel *)model{
    _model = model;
    self.dataSource = model.array;
    if (self.dataSource.count <=5) {
        mainCollectionView.frame = CGRectMake(0, 0, kSCREENWIDTH, kSCREENWIDTH/5*1.2);
    }else{
         mainCollectionView.frame = CGRectMake(0, 0, kSCREENWIDTH, kSCREENWIDTH/5*2.4);
    }
    [mainCollectionView reloadData];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**获取当前控制器 */
- (UIViewController *)viewController {
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到它
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    id nextResponder = nil;
    UIViewController *appRootVC = window.rootViewController;
    //1、通过present弹出VC，appRootVC.presentedViewController不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        //2、通过navigationcontroller弹出VC
        NSLog(@"subviews == %@",[window subviews]);
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    //1、tabBarController
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //或者 UINavigationController * nav = tabbar.selectedViewController;
        result = nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        //2、navigationController
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{//3、viewControler
        result = nextResponder;
    }
    return result;
}

@end
