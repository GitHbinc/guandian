//
//  CreatBesiegedCityHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "CreatBesiegedCityHeadView.h"
@interface CreatBesiegedCityHeadView()
@property(nonatomic ,strong)UILabel *addLabel;
@end
@implementation CreatBesiegedCityHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = HEXCOLOR(0xf6f6fa);
        
        [self setUpUI];
        
    }
    
    return self;
}

-(void)setUpUI{
    _headBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _headBtn.clipsToBounds = YES;
    _headBtn.layer.cornerRadius = 50;
    [_headBtn setImage:[UIImage imageNamed:@"chuangjianweicheng"] forState:UIControlStateNormal];
    [_headBtn addTarget:self action:@selector(addHeadAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_headBtn];
    
    _addLabel = [UILabel new];
    _addLabel.font = [UIFont systemFontOfSize:14];
    _addLabel.textColor = HEXCOLOR(0x666666);
    _addLabel.textAlignment = NSTextAlignmentCenter;
    _addLabel.text = @"添加头像";
    [self addSubview:_addLabel];
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_headBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.centerY.mas_equalTo(self)setOffset:-10];
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    [_addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        [make.top.mas_equalTo(self->_headBtn.mas_bottom)setOffset:10];
        
    }];
    
}


-(void)addHeadAction{
    if (_addHeadImageClick) {
        _addHeadImageClick();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
