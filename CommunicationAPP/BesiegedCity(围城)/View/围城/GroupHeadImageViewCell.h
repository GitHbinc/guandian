//
//  GroupHeadImageViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupHeadImageViewCell : UICollectionViewCell
@property(nonatomic ,strong)MineItem *item;
@end

NS_ASSUME_NONNULL_END
