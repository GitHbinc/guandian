//
//  CreatBesiegedCityHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CreatBesiegedCityHeadView : UIView

@property(nonatomic ,strong)UIButton *headBtn;
@property (nonatomic, copy) void(^addHeadImageClick)(void);

@end

NS_ASSUME_NONNULL_END
