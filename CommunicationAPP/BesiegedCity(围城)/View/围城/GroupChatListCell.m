//
//  GroupChatListCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/24.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "GroupChatListCell.h"
@interface GroupChatListCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *timeLabel;
@property (nonatomic ,strong)UILabel *typeLabel;
@end
@implementation GroupChatListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
   
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:25];
    [self.contentView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"小白交流群";
    _nameLabel.textColor = HEXCOLOR(0x111111);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_nameLabel];
    
    
    _timeLabel = [UILabel new];
    _timeLabel.text = @"最近聊天：19:30";
    _timeLabel.textColor = HEXCOLOR(0x999999);
    _timeLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_timeLabel];
    
    _typeLabel = [UILabel new];
    _typeLabel.text = @"查看";
    _typeLabel.clipsToBounds = YES;
    [_typeLabel.layer setCornerRadius:5.0];
    _typeLabel.textAlignment = NSTextAlignmentCenter;
    _typeLabel.textColor = [UIColor whiteColor];
    _typeLabel.font = [UIFont systemFontOfSize:13];
    _typeLabel.backgroundColor = HEXCOLOR(0xEE1C1B);
    _typeLabel.hidden = YES;
    [self addSubview:_typeLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
 
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.headImage.mas_top)setOffset:5];
    }];
    
    
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(weakSelf.headImage.mas_bottom)setOffset:-5];
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.mas_right)setOffset:-20];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
         make.size.mas_equalTo(CGSizeMake(50, 25));
    }];
    
}

-(void)setModel:(GroupListModel *)model{
    _model = model;
    if (model.type) {
        _typeLabel.hidden = NO;
        if ([model.type isEqualToString:@"1"]) {
            _typeLabel.text = @"查看";
            _typeLabel.textColor = [UIColor whiteColor];
            _typeLabel.backgroundColor = HEXCOLOR(0xEE1C1B);
        }else if ([model.type isEqualToString:@"2"]){
            _typeLabel.text = @"已拒绝";
            _typeLabel.textColor = HEXCOLOR(0x999999);
            _typeLabel.backgroundColor = [UIColor whiteColor];
        }else if ([model.type isEqualToString:@"3"]){
            _typeLabel.text = @"支付";
            _typeLabel.textColor = [UIColor whiteColor];
            _typeLabel.backgroundColor = HEXCOLOR(0xF46C02);
        }else{
            _typeLabel.hidden = YES;
        }
    }else{
        _typeLabel.hidden = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
