//
//  CreatBesiegedCityTextViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "CreatBesiegedCityTextViewCell.h"
@interface CreatBesiegedCityTextViewCell()
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)IQTextView *textView;
@end
@implementation CreatBesiegedCityTextViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
  
    _nameLabel = [UILabel new];
    _nameLabel.text = @"群名称";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_nameLabel];
    
    _textView = [[IQTextView alloc]init];
    _textView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_textView];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:15];
        [make.top.mas_equalTo(self.contentView)setOffset:10];
    }];
    
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:15];
        [make.right.mas_equalTo(self.contentView)setOffset:0];
        [make.top.mas_equalTo(self->_nameLabel.mas_bottom)setOffset:10];
        make.height.mas_equalTo(60);
        
    }];
    
}

-(void)setModel:(BesiegedCityModel *)model{
    _model = model;
    _nameLabel.text = model.name;
    NSString *holderText = model.placeholder;
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:14]
                        range:NSMakeRange(0, holderText.length)];
    _textView.attributedPlaceholder = placeholder;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
