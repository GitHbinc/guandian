//
//  GroupHeadImageViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupHeadImageViewCell.h"
@interface GroupHeadImageViewCell()

@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *nameLabel;

@end
@implementation GroupHeadImageViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    _headImage = [[UIImageView alloc]init];
    _headImage.contentMode = UIViewContentModeScaleAspectFit;
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    _headImage.layer.masksToBounds = YES;
    [_headImage.layer setCornerRadius:kSCREENWIDTH/14];
    [self.contentView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY).offset(-10);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH/7, kSCREENWIDTH/7));
    }];
    
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textColor = HEXCOLOR(0x666666);
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.font = [UIFont systemFontOfSize:12];
    _nameLabel.text = @"小熊";
    [self.contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.equalTo(self->_headImage.mas_bottom).offset(5);
    }];
    
}

-(void)setItem:(MineItem *)item{
    _item = item;
    if ([item.name isEqualToString:@"邀请"]) {
        _nameLabel.textColor = HEXCOLOR(0x9C386A);
    }
    _headImage.image = [UIImage imageNamed:item.icon];
    _nameLabel.text = item.name;
}
@end
