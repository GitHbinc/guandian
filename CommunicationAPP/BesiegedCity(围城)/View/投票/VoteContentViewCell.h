//
//  VoteContentViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VoteContentViewCell : UITableViewCell
@property(nonatomic ,assign)BOOL multiChoose;
@property(nonatomic ,strong)UIButton *chooseButton;
@property(nonatomic ,strong)UILabel *content;
@end

NS_ASSUME_NONNULL_END
