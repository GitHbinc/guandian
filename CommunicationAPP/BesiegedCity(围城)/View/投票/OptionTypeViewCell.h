//
//  OptionTypeViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OptionTypeViewCell : UITableViewCell
@property (nonatomic ,strong)UILabel *titleLabel;
@property(nonatomic ,strong)OptionModel *model;
@end

NS_ASSUME_NONNULL_END
