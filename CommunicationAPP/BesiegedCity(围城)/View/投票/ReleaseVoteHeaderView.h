//
//  ReleaseVoteHeaderView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReleaseVoteHeaderView : UIView
@property (nonatomic, copy) void(^addTitleImageClick)(void);
@property(nonatomic ,strong)IQTextView *title;
@property(nonatomic ,strong)UIButton *addBtn;
@end

NS_ASSUME_NONNULL_END
