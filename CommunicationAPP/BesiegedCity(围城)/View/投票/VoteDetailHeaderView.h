//
//  VoteDetailHeaderView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSInteger, WithoutVoteDetailType)
{
    WithoutVoteDetailTypeWords,           //未投票只有文字
    WithoutVoteDetailTypePicAndWords,     //未投票有图片并且有文字
   
};
@interface VoteDetailHeaderView : UIView
@property(nonatomic ,strong)OptionModel *model;

@end

NS_ASSUME_NONNULL_END
