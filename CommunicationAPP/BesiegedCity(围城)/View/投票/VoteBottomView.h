//
//  VoteBottomView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, VoteCellDetailType)
{
    VoteCellDetailTypeWithoutVote,   //未投票底图
    VoteCellDetailTypeHaveVoted,     //已投票
    
};
@interface VoteBottomView : UIView
@property (nonatomic, copy) void(^voteButtonClick)(UIButton *sender);
@property (nonatomic ,assign)VoteCellDetailType type;
@end

NS_ASSUME_NONNULL_END
