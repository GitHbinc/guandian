//
//  AddOptionViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AddOptionViewCell.h"

@implementation AddOptionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI{
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addBtn setImage:[UIImage imageNamed:@"vote_add.png"] forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(addOptionAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:14];
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
    
    UILabel *addLabel = [UILabel new];
    addLabel.text = @"添加选项";
    addLabel.textColor = HEXCOLOR(0x999999);
    addLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:addLabel];
    [addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(addBtn.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
}

-(void)addOptionAction{
    if (_addButtonClick) {
        _addButtonClick();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
