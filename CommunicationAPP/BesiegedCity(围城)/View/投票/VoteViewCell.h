//
//  NotVoteViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/27.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, VoteType)
{
    VoteTypeWithoutReleaseWords,           //未投票只有文字
    VoteTypeWithoutReleasePicAndWords,     //未投票有图片并且有文字
    VoteTypeReleaseWords,                  //已投票只有文字
    VoteTypeReleasePicAndWords,            //已投票有图片并且有文字
};

@interface VoteViewCell : UITableViewCell
@property(nonatomic ,strong)OptionModel *model;
@end

NS_ASSUME_NONNULL_END
