//
//  ReleaseVoteHeaderView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseVoteHeaderView.h"

@implementation ReleaseVoteHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self setUpUI];
        
    }
    
    return self;
}

-(void)setUpUI{
  
    _title = [IQTextView new];
    NSString *holderText = @"请输入投票主题";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:13]
                        range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x999999)
                        range:NSMakeRange(0, holderText.length)];
    _title.attributedPlaceholder = placeholder;
    [self addSubview:_title];
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.right.mas_equalTo(self)setOffset:-10];
        [make.top.mas_equalTo(self)setOffset:10];
        make.height.mas_equalTo(80);
        
    }];
    
    _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_addBtn setImage:[UIImage imageNamed:@"vote_image"] forState:UIControlStateNormal];
    [_addBtn addTarget:self action:@selector(addTitleClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_addBtn];
    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self->_title.mas_bottom)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    UILabel *tip = [UILabel new];
    tip.text = @"可上传一张与投票主题相关的图片";
    tip.font = [UIFont systemFontOfSize:9];
    tip.textColor = HEXCOLOR(0x999999);
    [self addSubview:tip];
    [tip mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self->_addBtn.mas_bottom)setOffset:10];
        
    }];
    
}

-(void)addTitleClick{
    if (_addTitleImageClick) {
        _addTitleImageClick();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
