//
//  VoteContentViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VoteContentViewCell.h"

@implementation VoteContentViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _content = [UILabel new];
    _content.text = @"甜";
    _content.textColor = HEXCOLOR(0x666666);
    _content.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_content];
    
    
    _chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_chooseButton setImage:[UIImage imageNamed:@"vote_nomal"] forState:UIControlStateNormal];
    [_chooseButton setImage:[UIImage imageNamed:@"vote_select"] forState:UIControlStateSelected];
    _chooseButton.userInteractionEnabled = NO;
    [self.contentView addSubview:_chooseButton];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_chooseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        make.centerY.mas_equalTo(self); 
    }];
    
    
    [_content mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_chooseButton.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (_multiChoose) {  // 多选
        // 选中 cell ，并且 之前 未选中 cell，选中它！！！
        if (selected && !_chooseButton.selected){
            _chooseButton.selected = YES;
        }
        
        // 取消选中 cell ，并且 之前 选中 cell
        else if (!selected && _chooseButton.selected){
            // 不处理
        }
        
        // 选中 cell ，并且 之前 选中 cell，取消选中！！！
        else if (selected && _chooseButton.selected) {
            _chooseButton.selected = NO;
        }
        
    }else{  // 单选
        _chooseButton.selected = selected;
    }
    
}

@end
