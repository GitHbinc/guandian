//
//  VoteDetailHeaderView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VoteDetailHeaderView.h"
@interface VoteDetailHeaderView()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *typeLabel;

@end
@implementation VoteDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self setUpUI];
        
    }
    
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:20];
    [self addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"论坛小秘书";
    _nameLabel.textColor = HEXCOLOR(0x9C386A);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_nameLabel];
    
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"11-15";
    _dateLabel.textColor = HEXCOLOR(0xB1B1B1);
    _dateLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_dateLabel];
    
    _titleLabel = [UILabel new];
    _titleLabel.text = @"参加直播";
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    _titleLabel.textColor = HEXCOLOR(0x343434);
    _titleLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_titleLabel];
    
    _typeLabel = [UILabel new];
    _typeLabel.text = @"进行中";
    _typeLabel.textAlignment = NSTextAlignmentCenter;
    _typeLabel.textColor = [UIColor whiteColor];
    _typeLabel.backgroundColor = HEXCOLOR(0xFE3C2E);
    _typeLabel.font =  [UIFont systemFontOfSize:11];
    [self addSubview:_typeLabel];
    
    _mainImage = [[UIImageView alloc]init];
    _mainImage.image = [UIImage imageNamed:@"test_icon"];
    [self addSubview:_mainImage];
    
}


#pragma mark - 布局
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        [make.top.mas_equalTo(self.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:-5];
    }];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(self->_nameLabel.mas_bottom)setOffset:2];
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:10];
        make.left.mas_equalTo(self->_headImage.mas_left);
        make.right.mas_equalTo(self).offset(-10);
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(45, 30));
        
    }];
}

-(void)setModel:(OptionModel *)model{
    _model = model;
    if (model.type == WithoutVoteDetailTypeWords) {
        
        _titleLabel.text = @"参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播";
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:10];
            make.left.mas_equalTo(self->_headImage.mas_left);
            make.right.mas_equalTo(self).offset(-10);
        }];
        _mainImage.hidden = YES;
        [self layoutIfNeeded];     // 下面会有关于layoutIfNeeded的介绍
        CGFloat tagViewHeight = _titleLabel.bottom;
        model.headHeight = tagViewHeight + 10;
        
    }else if (model.type == WithoutVoteDetailTypePicAndWords){
        
        _titleLabel.text = @"参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播参加直播";
        _mainImage.hidden = NO;
        [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(10);
            make.right.mas_equalTo(self).offset(-10);
            [make.top.mas_equalTo(self->_titleLabel.mas_bottom)setOffset:10];
            make.height.mas_equalTo(@220);
            
        }];
        
        [self layoutIfNeeded];     // 下面会有关于layoutIfNeeded的介绍
        CGFloat tagViewHeight = _mainImage.frame.origin.y+220;
        model.headHeight = tagViewHeight + 10;

       }
   
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
