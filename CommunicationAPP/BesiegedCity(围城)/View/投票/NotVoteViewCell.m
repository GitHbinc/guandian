//
//  NotVoteViewCell.m
//  ViewpointProject
//
//  Created by 陈华 on 2018/12/27.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VoteViewCell.h"
@interface VoteViewCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *contentLabel;
@property (nonatomic ,strong)UILabel *typeLabel;
@property (nonatomic ,strong)UILabel *totalLabel;
@property (nonatomic ,strong)UILabel *touLabel;
@property (nonatomic ,strong)UILabel * optionLabel;
@property (nonatomic ,strong)UIView * tiview;


@end
@implementation VoteViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setUpUI];
    }
    return self;
}


- (void)setUpUI{
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:18];
    [self.contentView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
        [make.top.mas_equalTo(self.contentView.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(36, 36));
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"吴彦祖";
    _nameLabel.textColor = HEXCOLOR(0x9C386A);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:-5];
    }];
    
    _typeLabel = [UILabel new];
    _typeLabel.text = @"进行中";
    _typeLabel.textAlignment = NSTextAlignmentCenter;
    _typeLabel.textColor = [UIColor whiteColor];
    _typeLabel.backgroundColor = HEXCOLOR(0xFE3C2E);
    _typeLabel.font =  [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(45, 30));
        
    }];
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"2018-12-05 11:04";
    _dateLabel.textColor = HEXCOLOR(0x999999);
    _dateLabel.font =  [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_dateLabel];
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(self->_nameLabel.mas_bottom)setOffset:2];
    }];
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚";
    _contentLabel.textColor =HEXCOLOR(0x333333);
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.numberOfLines = 0;
    _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.contentView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
        [make.top.mas_equalTo(self->_dateLabel.mas_bottom)setOffset:10];
        make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
        
    }];
    
    _mainImage = [[UIImageView alloc]init];
    _mainImage.image = [UIImage imageNamed:@"test_icon"];
    _mainImage.hidden = YES;
    [self.contentView addSubview:_mainImage];
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
        [make.right.mas_equalTo(self.contentView.mas_right)setOffset:-10];
        [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:20];
        make.height.mas_equalTo(200);
    }];
    
    _optionLabel = [UILabel new];
    _optionLabel.textColor =HEXCOLOR(0x666666);
    _optionLabel.font = [UIFont systemFontOfSize:13];
    _optionLabel.text = @"选项预览";
    [self.contentView addSubview:_optionLabel];
    [_optionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
        [make.top.mas_equalTo(self->_mainImage.mas_bottom)setOffset:10];
    }];
    
    _totalLabel = [UILabel new];
    _totalLabel.text = @"共0票";
    _totalLabel.textColor =HEXCOLOR(0x999999);
    _totalLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_totalLabel];
    [_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
        make.top.mas_equalTo(self->_optionLabel.mas_top);
    }];
    
    _tiview = [UIView new];
    [self.contentView addSubview:_tiview];
    

    _touLabel = [UILabel new];
    _touLabel.text = @"立即投票";
    _touLabel.textColor =HEXCOLOR(0x9C386A);
    _touLabel.textAlignment = NSTextAlignmentCenter;
    [_touLabel.layer setBorderWidth:0.8];
    [_touLabel.layer setBorderColor:HEXCOLOR(0x9C386A).CGColor];
    _touLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_touLabel];


}


-(void)setModel:(OptionModel *)model{
    _model = model;
    if (model.type == VoteTypeWithoutReleaseWords) {
        _mainImage.hidden = YES;
        [_optionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:10];
        }];
        
        [_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.top.mas_equalTo(self->_optionLabel.mas_top);
        }];
        
        
        [_tiview mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            [make.top.mas_equalTo(self->_totalLabel.mas_bottom)setOffset:10];
            make.height.mas_equalTo(2 *36);
        }];
        
        

         [_tiview removeAllSubview];
        NSArray *titleArray = @[@"喜欢",@"不喜欢"];
        for (int i = 0; i < titleArray.count; i++) {
            CGFloat lblW = kSCREENWIDTH -20;
            CGFloat lblH = 36;
            CGFloat lblY = i*36;
            CGFloat lblX = 10;
            UILabel *title = [[UILabel alloc]init];
            title.frame = CGRectMake(lblX, lblY, lblW, lblH);
            title.font = [UIFont systemFontOfSize:15];
            title.textColor = color666666;
            title.text = titleArray[i];
            [_tiview addSubview:title];
        }


        [_touLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self->_tiview.mas_bottom).offset(20);
            make.height.mas_equalTo(45);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(0);

        }];
    }else if(model.type == VoteTypeWithoutReleasePicAndWords){
        
        _mainImage.hidden = NO;
        [_mainImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.right.mas_equalTo(self.contentView.mas_right)setOffset:-10];
            [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:20];
            make.height.mas_equalTo(200);
        }];
        

        [_optionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.top.mas_equalTo(self->_mainImage.mas_bottom)setOffset:10];
        }];
        

        [_totalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.top.mas_equalTo(self->_optionLabel.mas_top);
        }];
        
       
        [_tiview mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            [make.top.mas_equalTo(self->_totalLabel.mas_bottom)setOffset:10];
            make.height.mas_equalTo(2 *36);
        }];
        
        
      
         [_tiview removeAllSubview];
        NSArray *titleArray = @[@"喜欢",@"不喜欢"];
        for (int i = 0; i < titleArray.count; i++) {
            CGFloat lblW = kSCREENWIDTH -20;
            CGFloat lblH = 36;
            CGFloat lblY = i*36;
            CGFloat lblX = 10;
            UILabel *title = [[UILabel alloc]init];
            title.frame = CGRectMake(lblX, lblY, lblW, lblH);
            title.font = [UIFont systemFontOfSize:15];
            title.textColor = color666666;
            title.text = titleArray[i];
            [_tiview addSubview:title];
        }
        

        [_touLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self->_tiview.mas_bottom).offset(20);
            make.height.mas_equalTo(45);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(0);
            
        }];
    }else if(model.type == VoteTypeReleaseWords){
        _mainImage.hidden = YES;
        [_optionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:10];
        }];
        
        [_totalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.top.mas_equalTo(self->_optionLabel.mas_top);
        }];
        
        
        [_tiview mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            [make.top.mas_equalTo(self->_totalLabel.mas_bottom)setOffset:10];
            make.height.mas_equalTo(2 *36);
        }];
        
        
        
        [_tiview removeAllSubview];
        NSArray *titleArray = @[@"喜欢",@"不喜欢"];
        NSArray *ticketArray = @[@"1票",@"2票"];
        for (int i = 0; i < titleArray.count; i++) {
            CGFloat lblW = kSCREENWIDTH -20;
            CGFloat lblH = 36;
            CGFloat lblY = i*36;
            CGFloat lblX = 40;
            UILabel *title = [[UILabel alloc]init];
            title.frame = CGRectMake(lblX, lblY, lblW, lblH);
            title.font = [UIFont systemFontOfSize:15];
            title.textColor = color666666;
            title.text = titleArray[i];
            [_tiview addSubview:title];
            
            
            UILabel *ticket = [[UILabel alloc]init];
            ticket.frame = CGRectMake(kSCREENWIDTH -60, lblY, 50, lblH);
            ticket.font = [UIFont systemFontOfSize:14];
            ticket.textAlignment = NSTextAlignmentCenter;
            ticket.centerY = title.centerY;
            ticket.textColor = HEXCOLOR(0x333333);
            ticket.text = ticketArray[i];
            [_tiview addSubview:ticket];
            
            
            if (i == 0) {
                UIImageView *selectImage = [[UIImageView alloc]init];
                selectImage.image = [UIImage imageNamed:@"vote_select"];
                selectImage.frame = CGRectMake(10, lblY, 16, 16);
                selectImage.centerY = title.centerY;
                [_tiview addSubview:selectImage];
            }
            
        }
        
        [_touLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self->_tiview.mas_bottom).offset(20);
            make.height.mas_equalTo(45);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(0);
            
        }];
        
    }else{
        
        _mainImage.hidden = NO;
        [_mainImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.right.mas_equalTo(self.contentView.mas_right)setOffset:-10];
            [make.top.mas_equalTo(self->_contentLabel.mas_bottom)setOffset:20];
            make.height.mas_equalTo(200);
        }];
        
        
        [_optionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            [make.top.mas_equalTo(self->_mainImage.mas_bottom)setOffset:10];
        }];
        
        
        [_totalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.top.mas_equalTo(self->_optionLabel.mas_top);
        }];
        
        
        [_tiview mas_remakeConstraints:^(MASConstraintMaker *make) {
            [make.left.mas_equalTo(self.contentView.mas_left)setOffset:10];
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            [make.top.mas_equalTo(self->_totalLabel.mas_bottom)setOffset:10];
            make.height.mas_equalTo(2 *36);
        }];
        
        
        
        [_tiview removeAllSubview];
        NSArray *titleArray = @[@"喜欢",@"不喜欢"];
        NSArray *ticketArray = @[@"1票",@"2票"];
        for (int i = 0; i < titleArray.count; i++) {
            CGFloat lblW = kSCREENWIDTH -20;
            CGFloat lblH = 36;
            CGFloat lblY = i*36;
            CGFloat lblX = 40;
            UILabel *title = [[UILabel alloc]init];
            title.frame = CGRectMake(lblX, lblY, lblW, lblH);
            title.font = [UIFont systemFontOfSize:15];
            title.textColor = color666666;
            title.text = titleArray[i];
            [_tiview addSubview:title];
            
            UILabel *ticket = [[UILabel alloc]init];
            ticket.frame = CGRectMake(kSCREENWIDTH -60, lblY, 50, lblH);
            ticket.font = [UIFont systemFontOfSize:14];
            ticket.centerY = title.centerY;
            ticket.textAlignment = NSTextAlignmentCenter;
            ticket.textColor = HEXCOLOR(0x333333);
            ticket.text = ticketArray[i];
            [_tiview addSubview:ticket];
            
            if (i == 0) {
                UIImageView *selectImage = [[UIImageView alloc]init];
                selectImage.image = [UIImage imageNamed:@"vote_select"];
                selectImage.frame = CGRectMake(10, lblY, 16, 16);
                selectImage.centerY = title.centerY;
                [_tiview addSubview:selectImage];
            }
        }
        
        
        [_touLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self->_tiview.mas_bottom).offset(20);
            make.height.mas_equalTo(45);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(0);
            
        }];

    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
