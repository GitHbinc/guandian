//
//  VoteResultViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "VoteResultViewCell.h"
@interface VoteResultViewCell()
@property(nonatomic ,strong)UIImageView *selectImage;
@property(nonatomic ,strong)UILabel *content;
@property(nonatomic ,strong)UILabel *num;

@end
@implementation VoteResultViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

- (void)setUpUI
{
    _selectImage = [[UIImageView alloc]init];
    _selectImage.image = [UIImage imageNamed:@"vote_select"];
    [self.contentView addSubview:_selectImage];
    
    _content = [UILabel new];
    _content.text = @"甜";
    _content.textColor = HEXCOLOR(0x666666);
    _content.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_content];
    
    
    _num = [UILabel new];
    _num.text = @"1票";
    _num.textColor = HEXCOLOR(0x333333);
    _num.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_num];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_selectImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        make.centerY.mas_equalTo(self);
    }];
    
    
    [_content mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_selectImage.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self);
    }];
    
    [_num mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self)setOffset:-10];
        make.centerY.mas_equalTo(self);
    }];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
