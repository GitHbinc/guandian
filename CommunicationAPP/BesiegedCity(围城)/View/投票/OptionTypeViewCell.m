//
//  OptionTypeViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "OptionTypeViewCell.h"
@interface OptionTypeViewCell()
@property (nonatomic ,strong)UILabel *nameLabel;
@end

@implementation OptionTypeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"投票类型";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
    
    
    _titleLabel = [UILabel new];
    _titleLabel.text = @"";
    _titleLabel.textColor = HEXCOLOR(0x999999);
    _titleLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_titleLabel];
    
   
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        make.centerY.mas_equalTo(self);
        make.width.mas_equalTo(60);
    }];
    
 
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_nameLabel.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self);
    }];
    
}

-(void)setModel:(OptionModel *)model{
    _model = model;
    _nameLabel.text = model.name;
    //_titleLabel.text = model.title;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
