//
//  ReleaseOptionViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseOptionViewCell.h"

@implementation ReleaseOptionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI{
    _optionTxt = [[UITextField alloc]init];
    _optionTxt.font =[UIFont systemFontOfSize:13];
    NSString *holderText = @"选项1";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:13]
                        range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x666666)
                        range:NSMakeRange(0, holderText.length)];
    
    _optionTxt.attributedPlaceholder = placeholder;
    [self.contentView addSubview:_optionTxt];
    [_optionTxt mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.mas_left)setOffset:50];
        [make.right.mas_equalTo(self.mas_right)setOffset:-10];
        make.top.bottom.mas_equalTo(self.contentView);
    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
