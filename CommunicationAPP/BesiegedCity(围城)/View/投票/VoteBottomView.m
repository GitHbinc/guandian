//
//  VoteBottomView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "VoteBottomView.h"
@interface VoteBottomView()
@property(nonatomic ,strong)UILabel *dateLabel;
@end
@implementation VoteBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        //[self setUpUI];
        
    }
    
    return self;
}

//-(void)setUpUI{
//
//}

-(void)setType:(VoteCellDetailType)type{
    _type = type;
    
    if (_type == VoteCellDetailTypeWithoutVote) {
        _dateLabel = [UILabel new];
        _dateLabel.text = @"截止日期: 2018.12.11 16:00";
        _dateLabel.textColor = HEXCOLOR(0x999999);
        _dateLabel.font = [UIFont systemFontOfSize:11];
        _dateLabel.frame = CGRectMake(10, 0, kSCREENWIDTH, 30);
        [self addSubview:_dateLabel];
        
        UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 160-30, kSCREENWIDTH - 38*2, 44)];
        [footerBtn setTitle:@"投票" forState:UIControlStateNormal];
        [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
        [footerBtn addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:footerBtn];

    }else{
        
        _dateLabel = [UILabel new];
        _dateLabel.text = @"截止日期: 2018.12.11 16:00";
        _dateLabel.textColor = HEXCOLOR(0x999999);
        _dateLabel.font = [UIFont systemFontOfSize:11];
        _dateLabel.frame = CGRectMake(10, 0, kSCREENWIDTH, 30);
        [self addSubview:_dateLabel];
        
        UILabel *label = [UILabel new];
        label.text = @"已投票";
        label.textColor = HEXCOLOR(0x666666);
        label.font = [UIFont systemFontOfSize:13];
        label.frame = CGRectMake(10, 40, kSCREENWIDTH, 30);
        [self addSubview:label];
        
        UIScrollView *scr = [[UIScrollView alloc]init];
        scr.backgroundColor = [UIColor colorWithRed:245/255.0 green:248/255.0 blue:250/255.0 alpha:1.0];
        scr.showsHorizontalScrollIndicator = NO;
        scr.frame = CGRectMake(0, 70, kSCREENWIDTH, 60);
        [self addSubview:scr];
        
        for (int i =0; i<5; i++) {
            CGFloat imageX = i *36+10*(i+1);
            UIImageView *image = [[UIImageView alloc]init];
            image.image = [UIImage imageNamed:@"center_avatar.jpeg"];
            image.layer.masksToBounds = YES;
            image.layer.cornerRadius = 18;
            [scr addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(scr.mas_left).offset(imageX);
                make.size.mas_equalTo(CGSizeMake(36, 36));
                make.centerY.mas_equalTo(scr.mas_centerY);
                
            }];
            scr.contentSize = CGSizeMake(46*(i+1)+10, 60);
        }
   }
}

-(void)footerBtnClick:(UIButton *)sender{
    
    if (_voteButtonClick) {
        _voteButtonClick(sender);
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
