//
//  WantAuctionViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "WantAuctionViewController.h"
#import "AuctionPublicHeadView.h"
#import "AuctionPriceViewCell.h"
#import "AuctionDetailViewController.h"

@interface WantAuctionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *timeLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@end

@implementation WantAuctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"竞拍详情";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    
    [self loadTableView];
    [self loadHeadViewWithBottonView];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadViewWithBottonView{
    UIView *headerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = [UIColor whiteColor];
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:20];
    [headerView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(headerView)setOffset:10];
        [make.top.mas_equalTo(headerView)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"小白";
    _nameLabel.textColor = HEXCOLOR(0x111111);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [headerView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(self->_headImage.mas_top)setOffset:2];
    }];
    
    
    _timeLabel = [UILabel new];
    _timeLabel.text = @"2018.12.18 19:30";
    _timeLabel.textColor = HEXCOLOR(0x999999);
    _timeLabel.font = [UIFont systemFontOfSize:12];
    [headerView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(self->_headImage.mas_bottom)setOffset:-2];
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = HEXCOLOR(0xf6f6fa);
    [headerView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self->_headImage.mas_left);
        make.right.mas_equalTo(headerView.mas_right).offset(-10);
        
        [make.bottom.mas_equalTo(self->_headImage.mas_bottom)setOffset:10];
        make.height.mas_equalTo(0.8);
        
    }];
    
    
    AuctionModel *model = [[AuctionModel alloc]init];
    AuctionPublicHeadView *head = [[AuctionPublicHeadView alloc]initWithFrame:CGRectMake(0, 60, kSCREENWIDTH, 400)];
    head.backgroundColor = UIColor.whiteColor;
    head.model = model;
    head.size = CGSizeMake(kSCREENWIDTH, model.headHeight);
    headerView.size = CGSizeMake(kSCREENWIDTH, model.headHeight +60);
    [headerView addSubview:head];
    self.tableView.tableHeaderView = headerView;
    
    
    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 160)];
    _dateLabel = [UILabel new];
    _dateLabel.text = @"截止日期: 2018.12.11 16:00";
    _dateLabel.textColor = HEXCOLOR(0x999999);
    _dateLabel.font = [UIFont systemFontOfSize:11];
    _dateLabel.frame = CGRectMake(10, 0, kSCREENWIDTH, 30);
    [footerView addSubview:_dateLabel];
    
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 160-34, kSCREENWIDTH - 38*2, 44)];
    [footerBtn setTitle:@"我要竞拍" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerBtn];
    _tableView.tableFooterView = footerView;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return 50;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"AuctionPriceViewCell";
    AuctionPriceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AuctionPriceViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AuctionModel *model = [AuctionModel new];
    if (indexPath.row == 0) {
        model.isInput = NO;
        cell.model = model;
        cell.nameLabel.text = @"最低出价";
    }else{
        model.isInput = YES;
        cell.model = model;
        cell.nameLabel.text = @"出价";
    }
    
    
    return cell;
    
}

-(void)footerBtnClick{
    AuctionDetailViewController *detail = [AuctionDetailViewController new];
    [self.navigationController pushViewController:detail animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
