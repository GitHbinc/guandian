//
//  AuctionViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/25.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionChatViewController.h"
#import "AuctionViewCell.h"
#import "PublishAuctionViewController.h"
#import "AuctionDetailViewController.h"
#import "WantAuctionViewController.h"

@interface AuctionChatViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation AuctionChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"竞拍";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    
     UIBarButtonItem * auctionBtn = [self getBarButtonItemWithTitleStr:@"发布竞拍" Sel:@selector(auctionAction)];
     self.navigationItem.rightBarButtonItems = @[auctionBtn];
    
    [self loadTableView];
    
}

-(void)auctionAction{
    
    PublishAuctionViewController *controller = [PublishAuctionViewController new];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    //设置自动计算行号模式
    tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    tableView.estimatedRowHeight = 200;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    static NSString *cellID = @"AuctionViewCell";
    AuctionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AuctionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WantAuctionViewController *detail = [WantAuctionViewController new];
    [self.navigationController pushViewController:detail animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
