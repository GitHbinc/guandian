//
//  PublishAuctionViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/24.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PublishAuctionViewController.h"
#import "AuctionPriceViewCell.h"
#import "AuctionDateViewCell.h"
#import "PublishAuctionHeaderView.h"
#import "MyAuctionViewController.h"

@interface PublishAuctionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation PublishAuctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布竞拍";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self loadTableView];
    
    [self loadHeadViewWithBottonView];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadViewWithBottonView{
    PublishAuctionHeaderView *headerView= [[PublishAuctionHeaderView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 260)];
    headerView.backgroundColor = [UIColor whiteColor];
   _tableView.tableHeaderView = headerView;
    

    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 160)];
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 160-34, kSCREENWIDTH - 38*2, 44)];
    [footerBtn setTitle:@"立即拍卖" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerBtn];
    _tableView.tableFooterView = footerView;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *cellID = @"AuctionPriceViewCell";
        AuctionPriceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[AuctionPriceViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        AuctionModel *model = [AuctionModel new];
        model.isInput = YES;
        cell.model = model;
        return cell;
    }
    static NSString *cellID = @"AuctionDateViewCell";
    AuctionDateViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AuctionDateViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    return cell;
}


-(void)footerBtnClick{
    MyAuctionViewController *auction = [MyAuctionViewController new];
    [self.navigationController pushViewController:auction animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
