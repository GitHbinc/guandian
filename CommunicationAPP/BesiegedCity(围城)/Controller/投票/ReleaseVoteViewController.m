//
//  ReleaseVoteViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseVoteViewController.h"
#import "ReleaseOptionViewCell.h"
#import "AddOptionViewCell.h"
#import "ReleaseVoteHeaderView.h"
#import "OptionTypeViewCell.h"
#import "OptionModel.h"
#import "CGXPickerView.h"
#import "BaseNavigationController.h"
#import "QDSingleImagePickerPreviewViewController.h"

#define SingleImagePickingTag 1048
static QMUIAlbumContentType const kAlbumContentType = QMUIAlbumContentTypeAll;

@interface ReleaseVoteViewController ()<UITableViewDelegate,UITableViewDataSource,QMUIAlbumViewControllerDelegate,QMUIImagePickerViewControllerDelegate,QDSingleImagePickerPreviewViewControllerDelegate>

@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)UITableView *typeTableView;
@property(nonatomic ,strong)ReleaseVoteHeaderView *headView;

@property (nonatomic, strong) NSMutableArray *typeSource;
@property (nonatomic ,copy)NSString *typeStr;
@property (nonatomic ,copy)NSString *endStr;
@property (nonatomic ,copy)NSString *remindStr;

@end

@implementation ReleaseVoteViewController


-(NSMutableArray *)typeSource{
    if (!_typeSource) {
        _typeSource = [NSMutableArray array];
    }
    return _typeSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"发布投票";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
   
    UIBarButtonItem * auctionBtn = [self getBarButtonItemWithTitleStr:@"发布" Sel:@selector(releaseAction)];
    self.navigationItem.rightBarButtonItems = @[auctionBtn];
    
    [self loadTableView];
    [self loadHeadView];
    
     self.dataSource = [@[@"1",@"2",@"3"]mutableCopy];
    NSArray *array1 = @[@"投票类型",@"结束时间",@"提醒"];
    //NSArray *array2 = @[@"单选",@"2018-12-19 11:11",@"提前30分钟"];
    
    for (int i =0; i <array1.count; i++) {
        OptionModel *model = [OptionModel new];
        model.name = array1[i];
        //model.title = array2[i];
        [self.typeSource addObject:model];
    }
    [self.tableView reloadData];
}

-(void)releaseAction{
    
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView setEditing:YES animated:YES];
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadView{
    ReleaseVoteHeaderView *head = [[ReleaseVoteHeaderView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 230)];
    head.addTitleImageClick = ^{
        
        QMUIAlbumViewController *albumViewController = [[QMUIAlbumViewController alloc] init];
        albumViewController.albumViewControllerDelegate = self;
        albumViewController.contentType = kAlbumContentType;
        albumViewController.title = @"选择图片";
        albumViewController.view.tag = SingleImagePickingTag;
        BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:albumViewController];
        
        // 获取最近发送图片时使用过的相簿，如果有则直接进入该相簿
        [albumViewController pickLastAlbumGroupDirectlyIfCan];
        
        [self presentViewController:navigationController animated:YES completion:NULL];
    };
    self.tableView.tableHeaderView = head;
    self.headView = head;
    
    UITableView *typeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 180 +50) style:UITableViewStylePlain];
    self.typeTableView = typeTableView;
    typeTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    typeTableView.tableFooterView = [[UIView alloc]init];
    typeTableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    typeTableView.delegate = self;
    typeTableView.dataSource = self;
    self.tableView.tableFooterView = typeTableView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.typeTableView) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
        headerView.backgroundColor = [UIColor clearColor];
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, kSCREENWIDTH, 30)];
        headerLabel.font = [UIFont systemFontOfSize:10];
        headerLabel.textColor = HEXCOLOR(0x999999);
        headerLabel.text = @"最多支持15个选项，每个选项不超过40个字";
        [headerView addSubview:headerLabel];
        
        return headerView;
    }
    return [UIView new];
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView) {
        return 10;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (tableView == self.typeTableView) {
        return 50;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        return self.dataSource.count+1;
    }
        return  self.typeSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView) {
        if (indexPath.row == self.dataSource.count) {
            static NSString *cellID = @"AddOptionViewCell";
            AddOptionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (cell == nil) {
                cell = [[AddOptionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.addButtonClick = ^{
                NSIndexPath *addIndexPath = [NSIndexPath indexPathForRow:self.dataSource.count inSection:0];
                [self.dataSource insertObject:@"1" atIndex:addIndexPath.row];
                [self.tableView reloadData];
                //[self.tableView insertRowsAtIndexPaths:@[addIndexPath]  withRowAnimation:UITableViewRowAnimationFade];
              
            };
            return cell;
        }
        
        
        static NSString *cellID = @"ReleaseOptionViewCell";
        ReleaseOptionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[ReleaseOptionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString *holderText = [NSString stringWithFormat:@"选项%ld",indexPath.row+1];
        NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
        [placeholder addAttribute:NSFontAttributeName
                            value:[UIFont systemFontOfSize:13]
                            range:NSMakeRange(0, holderText.length)];
        [placeholder addAttribute:NSForegroundColorAttributeName
                            value:HEXCOLOR(0x666666)
                            range:NSMakeRange(0, holderText.length)];
        
        cell.optionTxt.attributedPlaceholder = placeholder;
        
        return cell;
    }
    
    static NSString *cellID = @"OptionTypeViewCell";
    OptionTypeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[OptionTypeViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.model = [self.typeSource objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {
        cell.titleLabel.text = self.typeStr;
    }else if (indexPath.row == 1){
        cell.titleLabel.text = self.endStr;
    }else{
        cell.titleLabel.text = self.remindStr;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.typeTableView) {
        __weak typeof(self) weakSelf = self;
        if (indexPath.row == 0) {
            [CGXPickerView showStringPickerWithTitle:@"投票类型" DataSource:@[@"单选", @"多选，最多2项", @"多选，无限制"] DefaultSelValue:@"单选" IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
                NSLog(@"%@",selectValue);
                weakSelf.typeStr = selectValue;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.typeTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }];
        }else if (indexPath.row == 1){
            [CGXPickerView showDatePickerWithTitle:@"结束时间" DateType:UIDatePickerModeDateAndTime DefaultSelValue:nil MinDateStr:nil MaxDateStr:nil IsAutoSelect:NO Manager:nil ResultBlock:^(NSString *selectValue) {
                NSLog(@"%@",selectValue);
                weakSelf.endStr = selectValue;;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
                [self.typeTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }];
        }else{
            [CGXPickerView showStringPickerWithTitle:@"提醒" DataSource:@[@"提前30分钟", @"提前12小时", @"提前24小时", @"不提醒"] DefaultSelValue:@"提前30分钟" IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
                NSLog(@"%@",selectValue);
                weakSelf.remindStr = selectValue;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
                [self.typeTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView) {
        if (indexPath.row ==0 || indexPath.row ==1 || indexPath.row == self.dataSource.count) {
            return NO;
        }
        return YES;
    }
   
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            [self.dataSource removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
   
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}


#pragma mark - <QMUIAlbumViewControllerDelegate>

- (QMUIImagePickerViewController *)imagePickerViewControllerForAlbumViewController:(QMUIAlbumViewController *)albumViewController {
    QMUIImagePickerViewController *imagePickerViewController = [[QMUIImagePickerViewController alloc] init];
    imagePickerViewController.imagePickerViewControllerDelegate = self;
    imagePickerViewController.view.tag = albumViewController.view.tag;
    if (albumViewController.view.tag == SingleImagePickingTag) {
        imagePickerViewController.allowsMultipleSelection = NO;
    }
    
    return imagePickerViewController;
}

#pragma mark - <QMUIImagePickerViewControllerDelegate>

- (void)imagePickerViewController:(QMUIImagePickerViewController *)imagePickerViewController didFinishPickingImageWithImagesAssetArray:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    // 储存最近选择了图片的相册，方便下次直接进入该相册
    [QMUIImagePickerHelper updateLastestAlbumWithAssetsGroup:imagePickerViewController.assetsGroup ablumContentType:kAlbumContentType userIdentify:nil];
    
    [self sendImageWithImagesAssetArray:imagesAssetArray];
}

- (QMUIImagePickerPreviewViewController *)imagePickerPreviewViewControllerForImagePickerViewController:(QMUIImagePickerViewController *)imagePickerViewController {
    
    QDSingleImagePickerPreviewViewController *imagePickerPreviewViewController = [[QDSingleImagePickerPreviewViewController alloc] init];
    imagePickerPreviewViewController.delegate = self;
    imagePickerPreviewViewController.assetsGroup = imagePickerViewController.assetsGroup;
    imagePickerPreviewViewController.view.tag = imagePickerViewController.view.tag;
    return imagePickerPreviewViewController;
    
}


- (void)sendImageWithImagesAssetArray:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    __weak __typeof(self)weakSelf = self;
    
    for (QMUIAsset *asset in imagesAssetArray) {
        [QMUIImagePickerHelper requestImageAssetIfNeeded:asset completion:^(QMUIAssetDownloadStatus downloadStatus, NSError *error) {
            if (downloadStatus == QMUIAssetDownloadStatusDownloading) {
                [weakSelf startLoadingWithText:@"从 iCloud 加载中"];
            } else if (downloadStatus == QMUIAssetDownloadStatusSucceed) {
                [weakSelf sendImageWithImagesAssetArrayIfDownloadStatusSucceed:imagesAssetArray];
            } else {
                [weakSelf showTipLabelWithText:@"iCloud 下载错误，请重新选图"];
            }
        }];
    }
}

- (void)sendImageWithImagesAssetArrayIfDownloadStatusSucceed:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    if ([QMUIImagePickerHelper imageAssetsDownloaded:imagesAssetArray]) {
        // 所有资源从 iCloud 下载成功，模拟发送图片到服务器
        // 显示发送中
        [self showTipLabelWithText:@"发送中"];
        // 使用 delay 模拟网络请求时长
        [self performSelector:@selector(showTipLabelWithText:) withObject:[NSString stringWithFormat:@"成功发送%@个资源", @([imagesAssetArray count])] afterDelay:1.5];
    }
}

#pragma mark - <QDSingleImagePickerPreviewViewControllerDelegate>

- (void)imagePickerPreviewViewController:(QDSingleImagePickerPreviewViewController *)imagePickerPreviewViewController didSelectImageWithImagesAsset:(QMUIAsset *)imageAsset {
    // 储存最近选择了图片的相册，方便下次直接进入该相册
    [QMUIImagePickerHelper updateLastestAlbumWithAssetsGroup:imagePickerPreviewViewController.assetsGroup ablumContentType:kAlbumContentType userIdentify:nil];
    // 显示 loading
    [self startLoading];
    [imageAsset requestImageData:^(NSData *imageData, NSDictionary<NSString *,id> *info, BOOL isGif, BOOL isHEIC) {
        UIImage *targetImage = nil;
        if (isGif) {
            targetImage = [UIImage qmui_animatedImageWithData:imageData];
        } else {
            targetImage = [UIImage imageWithData:imageData];
            if (isHEIC) {
                // iOS 11 中新增 HEIF/HEVC 格式的资源，直接发送新格式的照片到不支持新格式的设备，照片可能会无法识别，可以先转换为通用的 JPEG 格式再进行使用。
                // 详细请浏览：https://github.com/QMUI/QMUI_iOS/issues/224
                targetImage = [UIImage imageWithData:UIImageJPEGRepresentation(targetImage, 1)];
            }
        }
        [self performSelector:@selector(setAvatarWithAvatarImage:) withObject:targetImage afterDelay:1.8];
    }];
}

- (void)setAvatarWithAvatarImage:(UIImage *)avatarImage {
    [self stopLoading];
    [self.headView.addBtn setImage:avatarImage forState:UIControlStateNormal];
    [self.tableView reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
