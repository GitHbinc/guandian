//
//  VoteDetailViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, VoteDetailType)
{
    VoteDetailTypeWithoutVote,   //未投票详情
    VoteDetailTypeHaveVoted,     //已投票详情
    
};

@interface VoteDetailViewController : BaseViewController
@property(nonatomic ,assign)VoteDetailType type;
@end

NS_ASSUME_NONNULL_END
