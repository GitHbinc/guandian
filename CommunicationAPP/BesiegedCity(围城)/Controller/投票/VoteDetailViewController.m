//
//  VoteDetailViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VoteDetailViewController.h"
#import "VoteContentViewCell.h"
#import "VoteResultViewCell.h"
#import "VoteDetailHeaderView.h"
#import "VoteBottomView.h"
#import "OptionModel.h"
@interface VoteDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation VoteDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"投票详情";//未投票
    [self setNaviBarButtonItem];
    [self loadTableView];
    [self loadHeadWithBottomView];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"vote_delete"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(deleteAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)loadHeadWithBottomView{
    OptionModel *model = [[OptionModel alloc]init];
    model.type = 1;
    VoteDetailHeaderView *head = [[VoteDetailHeaderView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 400)];
    head.backgroundColor = [UIColor whiteColor];
    head.model = model;
    head.size = CGSizeMake(kSCREENWIDTH, model.headHeight);
    self.tableView.tableHeaderView = head;
    
    VoteBottomView * bottom = [[VoteBottomView alloc]init];
    bottom.frame = CGRectMake(0, 0, kSCREENWIDTH, 180);
    bottom.backgroundColor = [UIColor whiteColor];
    bottom.voteButtonClick = ^(UIButton * _Nonnull sender) {//投票
        
    };
    if (_type == VoteDetailTypeWithoutVote) {
        bottom.type = VoteCellDetailTypeWithoutVote;
    }else{
        bottom.type = VoteCellDetailTypeHaveVoted;
    }
    self.tableView.tableFooterView = bottom;
}


-(void)deleteAction{
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_type == VoteDetailTypeWithoutVote) {
        
        static NSString *cellID = @"VoteContentViewCell";
        VoteContentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[VoteContentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.multiChoose = YES;
        return cell;
    }
    
    static NSString *cellID = @"VoteResultViewCell";
    VoteResultViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[VoteResultViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    NotVoteDetailViewController *detail = [NotVoteDetailViewController new];
//    [self.navigationController pushViewController:detail animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30.0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.font = [UIFont systemFontOfSize:13];
    headerLabel.textColor = HEXCOLOR(0x555555);
    headerLabel.text = @"多选";
   [headerView addSubview:headerLabel];
    
    return headerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
