//
//  VoteListViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/27.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VoteListViewController.h"
#import "ReleaseVoteViewController.h"
#import "VoteViewCell.h"
#import "VoteDetailViewController.h"

@interface VoteListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation VoteListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"投票";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    
    UIBarButtonItem * auctionBtn = [self getBarButtonItemWithTitleStr:@"发布投票" Sel:@selector(voteAction)];
    self.navigationItem.rightBarButtonItems = @[auctionBtn];
     [self loadTableView];
    
    NSArray *typeArray = @[@0,@1,@2,@3];
    for (int i =0; i <typeArray.count; i++) {
        OptionModel *model = [OptionModel new];
        model.type = [typeArray[i] floatValue];
       [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}


-(void)voteAction{
    ReleaseVoteViewController *controller = [[ReleaseVoteViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    //设置自动计算行号模式
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    self.tableView.estimatedRowHeight = 200;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 20;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *cellID = @"VoteViewCell";
        VoteViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[VoteViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = [self.dataSource objectAtIndex:indexPath.section];
        [cell layoutIfNeeded];
        return cell;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OptionModel *model = [self.dataSource objectAtIndex:indexPath.section];
    VoteDetailViewController *detail = [VoteDetailViewController new];
    if (model.type ==0 || model.type ==1) {
        detail.type = VoteDetailTypeWithoutVote;
    }else{
        detail.type = VoteDetailTypeHaveVoted;
    }
    [self.navigationController pushViewController:detail animated:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
