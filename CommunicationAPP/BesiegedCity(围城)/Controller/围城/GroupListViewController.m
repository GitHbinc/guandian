//
//  GroupListViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/24.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "GroupListViewController.h"
#import "GroupChatListCell.h"
#import "GroupListModel.h"
#import "ApplicationTitleViewController.h"

#define IS_IOS_VERSION floorf([[UIDevice currentDevice].systemVersion floatValue])
@interface GroupListViewController ()<UISearchBarDelegate>
@property (nonatomic ,strong)UISearchBar *searchBar;
@property (nonatomic, weak) YUFoldingTableView *foldingTableView;

@end

@implementation GroupListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"围城列表";
    self.view.backgroundColor = [UIColor whiteColor];
    // 创建tableView
    [self setupFoldingTableView];
}

// 创建tableView
- (void)setupFoldingTableView
{
   self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIView *headview = [[UIView alloc]initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT +44, kSCREENWIDTH, 36)];
    headview.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self.view addSubview:headview];
    
    _searchBar = [[UISearchBar alloc]init];
    [headview addSubview:_searchBar];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0) { // iOS 11
        [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(headview).offset(10);
                    make.right.mas_equalTo(headview).offset(-10);
                    make.centerY.mas_equalTo(headview.mas_centerY);
                    make.height.mas_equalTo(@30);
                }];

    } else {
        _searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    _searchBar.placeholder = [NSBundle py_localizedStringForKey:PYSearchSearchPlaceholderText];
    _searchBar.backgroundImage = [NSBundle py_imageNamed:@"clearImage"];
    _searchBar.delegate = self;
    for (UIView *subView in [[_searchBar.subviews lastObject] subviews]) {
        if ([[subView class] isSubclassOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)subView;
            textField.font = [UIFont systemFontOfSize:15];
            textField.placeholder = @"请输入你想要的围城";
            break;
        }
    }
    
    CGFloat topHeight = [[UIApplication sharedApplication] statusBarFrame].size.height + 44 +36;
    YUFoldingTableView *foldingTableView = [[YUFoldingTableView alloc] initWithFrame:CGRectMake(0, topHeight, self.view.bounds.size.width, self.view.bounds.size.height - topHeight)];
    foldingTableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _foldingTableView = foldingTableView;
    [self.view addSubview:foldingTableView];
    
    
    foldingTableView.foldingDelegate = self;

}

#pragma mark - YUFoldingTableViewDelegate / required（必须实现的代理）
//分组几个
- (NSInteger )numberOfSectionForYUFoldingTableView:(YUFoldingTableView *)yuTableView
{
    return 3;
}
//这个分组内面有多个条
- (NSInteger )yuFoldingTableView:(YUFoldingTableView *)yuTableView numberOfRowsInSection:(NSInteger )section
{
    return 3;
}
//这个分组的分组头的高度
- (CGFloat )yuFoldingTableView:(YUFoldingTableView *)yuTableView heightForHeaderInSection:(NSInteger )section
{
    return 50;
}
//这个分组的内面每一条的行高
- (CGFloat )yuFoldingTableView:(YUFoldingTableView *)yuTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}
- (UITableViewCell *)yuFoldingTableView:(YUFoldingTableView *)yuTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"GroupChatListCell";
    GroupChatListCell *cell = [yuTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[GroupChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
  
    if (indexPath.section == 0) {
        GroupListModel *model = [GroupListModel new];
        if (indexPath.row == 0) {
            model.type = @"1";
        }else if (indexPath.row ==1){
            model.type = @"2";
        }else{
            model.type = @"3";
        }
        cell.model = model;
    }else{
         GroupListModel *model = [GroupListModel new];
        model.type = @"";
         cell.model = model;
    }
    
    return cell;
}
#pragma mark - YUFoldingTableViewDelegate / optional （可选择实现的）

- (NSString *)yuFoldingTableView:(YUFoldingTableView *)yuTableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [NSString stringWithFormat:@"申请冠名"];
    }else if (section == 1){
         return [NSString stringWithFormat:@"我创建的围城（6）"];
    }else{
         return [NSString stringWithFormat:@"我加入的围城（6）"];
    }
   
}

- (void )yuFoldingTableView:(YUFoldingTableView *)yuTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [yuTableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            ApplicationTitleViewController *title = [ApplicationTitleViewController new];
            [self.navigationController pushViewController:title animated:YES];
        }
    }
   
}

// 返回箭头的位置
- (YUFoldingSectionHeaderArrowPosition)perferedArrowPositionForYUFoldingTableView:(YUFoldingTableView *)yuTableView{
   
    return YUFoldingSectionHeaderArrowPositionRight;
}

- (UIColor *)yuFoldingTableView:(YUFoldingTableView *)yuTableView backgroundColorForHeaderInSection:(NSInteger )section{
    
    return [UIColor whiteColor];
}

- (UIFont *)yuFoldingTableView:(YUFoldingTableView *)yuTableView fontForTitleInSection:(NSInteger )section{
    
    return [UIFont systemFontOfSize:14];
}


- (UIColor *)yuFoldingTableView:(YUFoldingTableView *)yuTableView textColorForTitleInSection:(NSInteger )section{
    
    return HEXCOLOR(0x111111);
}

- (UIImage *)yuFoldingTableView:(YUFoldingTableView *)yuTableView arrowImageForSection:(NSInteger )section{
    return [UIImage imageNamed:@"guan_right"];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
