//
//  CreatBesiegedCityController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//
#import "CreatBesiegedCityController.h"
#import "CreatBesiegedCityTextViewCell.h"
#import "BesiegedCityModel.h"
#import "CreatBesiegedCityHeadView.h"
#import "AddressFromMapViewController.h"
#import "BaseNavigationController.h"
#import "QDSingleImagePickerPreviewViewController.h"

#define SingleImagePickingTag 1048
static QMUIAlbumContentType const kAlbumContentType = QMUIAlbumContentTypeAll;

@interface CreatBesiegedCityController ()<UITableViewDelegate,UITableViewDataSource,QMUIAlbumViewControllerDelegate,QMUIImagePickerViewControllerDelegate,QDSingleImagePickerPreviewViewControllerDelegate>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)CreatBesiegedCityHeadView *headView;
@end

@implementation CreatBesiegedCityController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"创建围城";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
     [self loadTableView];
    
    [self loadHeadViewWithBottonView];
    
    NSArray *titleArray = @[@"围城名称",
                            @"围城介绍"
                            ];
    NSArray *placeArray = @[@"填写名称（2-30字）",
                            @"填写围城介绍，让大家更了解你的围城"
                            ];
    for (int i = 0; i <titleArray.count; i++) {
        BesiegedCityModel *item = [[BesiegedCityModel alloc]init];
        item.name = titleArray[i];
        item.placeholder = placeArray[i];
        [self.dataSource addObject:item];
    }
    [self.tableView reloadData];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadViewWithBottonView{
    CreatBesiegedCityHeadView *head = [[CreatBesiegedCityHeadView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENWIDTH *0.5)];
    head.addHeadImageClick = ^{
        QMUIAlbumViewController *albumViewController = [[QMUIAlbumViewController alloc] init];
        albumViewController.albumViewControllerDelegate = self;
        albumViewController.contentType = kAlbumContentType;
        albumViewController.title = @"选择图片";
        albumViewController.view.tag = SingleImagePickingTag;
        BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:albumViewController];
        
        // 获取最近发送图片时使用过的相簿，如果有则直接进入该相簿
        [albumViewController pickLastAlbumGroupDirectlyIfCan];
        
        [self presentViewController:navigationController animated:YES completion:NULL];
    };
    self.tableView.tableHeaderView = head;
    self.headView = head;
    
    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 80)];
    
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 30, kSCREENWIDTH - 38*2, 44)];
    [footerBtn setTitle:@"立即创建" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerBtn];
    _tableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return self.dataSource.count +1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        return 50;
    }
    return 110;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        static NSString *identifier = @"mycell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.textLabel.text = @"围城地点";
        cell.detailTextLabel.text = @"旭辉虹桥国际";
        cell.textLabel.textColor = HEXCOLOR(0x333333);
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    static NSString *cellID = @"CreatBesiegedCityTextViewCell";
    CreatBesiegedCityTextViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[CreatBesiegedCityTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.dataSource objectAtIndex:indexPath.section];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 2) {
        AddressFromMapViewController *title = [AddressFromMapViewController new];
        [self.navigationController pushViewController:title animated:YES];
    }
}


-(void)footerBtnClick{
    
}

#pragma mark - <QMUIAlbumViewControllerDelegate>

- (QMUIImagePickerViewController *)imagePickerViewControllerForAlbumViewController:(QMUIAlbumViewController *)albumViewController {
    QMUIImagePickerViewController *imagePickerViewController = [[QMUIImagePickerViewController alloc] init];
    imagePickerViewController.imagePickerViewControllerDelegate = self;
    imagePickerViewController.view.tag = albumViewController.view.tag;
    if (albumViewController.view.tag == SingleImagePickingTag) {
        imagePickerViewController.allowsMultipleSelection = NO;
    }

    return imagePickerViewController;
}

#pragma mark - <QMUIImagePickerViewControllerDelegate>

- (void)imagePickerViewController:(QMUIImagePickerViewController *)imagePickerViewController didFinishPickingImageWithImagesAssetArray:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    // 储存最近选择了图片的相册，方便下次直接进入该相册
    [QMUIImagePickerHelper updateLastestAlbumWithAssetsGroup:imagePickerViewController.assetsGroup ablumContentType:kAlbumContentType userIdentify:nil];
    
    [self sendImageWithImagesAssetArray:imagesAssetArray];
}

- (QMUIImagePickerPreviewViewController *)imagePickerPreviewViewControllerForImagePickerViewController:(QMUIImagePickerViewController *)imagePickerViewController {
    
    QDSingleImagePickerPreviewViewController *imagePickerPreviewViewController = [[QDSingleImagePickerPreviewViewController alloc] init];
    imagePickerPreviewViewController.delegate = self;
    imagePickerPreviewViewController.assetsGroup = imagePickerViewController.assetsGroup;
    imagePickerPreviewViewController.view.tag = imagePickerViewController.view.tag;
    return imagePickerPreviewViewController;
  
}


- (void)sendImageWithImagesAssetArray:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    __weak __typeof(self)weakSelf = self;
    
    for (QMUIAsset *asset in imagesAssetArray) {
        [QMUIImagePickerHelper requestImageAssetIfNeeded:asset completion:^(QMUIAssetDownloadStatus downloadStatus, NSError *error) {
            if (downloadStatus == QMUIAssetDownloadStatusDownloading) {
                [weakSelf startLoadingWithText:@"从 iCloud 加载中"];
            } else if (downloadStatus == QMUIAssetDownloadStatusSucceed) {
                [weakSelf sendImageWithImagesAssetArrayIfDownloadStatusSucceed:imagesAssetArray];
            } else {
                [weakSelf showTipLabelWithText:@"iCloud 下载错误，请重新选图"];
            }
        }];
    }
}

- (void)sendImageWithImagesAssetArrayIfDownloadStatusSucceed:(NSMutableArray<QMUIAsset *> *)imagesAssetArray {
    if ([QMUIImagePickerHelper imageAssetsDownloaded:imagesAssetArray]) {
        // 所有资源从 iCloud 下载成功，模拟发送图片到服务器
        // 显示发送中
        [self showTipLabelWithText:@"发送中"];
        // 使用 delay 模拟网络请求时长
        [self performSelector:@selector(showTipLabelWithText:) withObject:[NSString stringWithFormat:@"成功发送%@个资源", @([imagesAssetArray count])] afterDelay:1.5];
    }
}

#pragma mark - <QDSingleImagePickerPreviewViewControllerDelegate>

- (void)imagePickerPreviewViewController:(QDSingleImagePickerPreviewViewController *)imagePickerPreviewViewController didSelectImageWithImagesAsset:(QMUIAsset *)imageAsset {
    // 储存最近选择了图片的相册，方便下次直接进入该相册
    [QMUIImagePickerHelper updateLastestAlbumWithAssetsGroup:imagePickerPreviewViewController.assetsGroup ablumContentType:kAlbumContentType userIdentify:nil];
    // 显示 loading
    [self startLoading];
    [imageAsset requestImageData:^(NSData *imageData, NSDictionary<NSString *,id> *info, BOOL isGif, BOOL isHEIC) {
        UIImage *targetImage = nil;
        if (isGif) {
            targetImage = [UIImage qmui_animatedImageWithData:imageData];
        } else {
            targetImage = [UIImage imageWithData:imageData];
            if (isHEIC) {
                // iOS 11 中新增 HEIF/HEVC 格式的资源，直接发送新格式的照片到不支持新格式的设备，照片可能会无法识别，可以先转换为通用的 JPEG 格式再进行使用。
                // 详细请浏览：https://github.com/QMUI/QMUI_iOS/issues/224
                targetImage = [UIImage imageWithData:UIImageJPEGRepresentation(targetImage, 1)];
            }
        }
        [self performSelector:@selector(setAvatarWithAvatarImage:) withObject:targetImage afterDelay:1.8];
    }];
}

- (void)setAvatarWithAvatarImage:(UIImage *)avatarImage {
    [self stopLoading];
    [self.headView.headBtn setImage:avatarImage forState:UIControlStateNormal];
    [self.tableView reloadData];
}




@end
