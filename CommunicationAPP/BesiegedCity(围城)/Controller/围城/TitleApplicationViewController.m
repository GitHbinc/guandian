//
//  TitleApplicationViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/3.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "TitleApplicationViewController.h"

@interface TitleApplicationViewController ()
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *addressLabel;
@property(nonatomic ,strong)IQTextView *inforTextView;
@property(nonatomic ,strong)UILabel *dateLabel;
@end

@implementation TitleApplicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"冠名申请";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self creatUI];
}

-(void)creatUI{
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:30];
    [self.view addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.view)setOffset:20];
        [make.top.mas_equalTo(self.view)setOffset:64 +STATUS_BAR_HEIGHT];
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"小白交流群";
    _nameLabel.textColor = HEXCOLOR(0x111111);
    _nameLabel.font =  [UIFont systemFontOfSize:16];
    [self.view addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:-15];
    }];
    
    _addressLabel = [UILabel new];
    _addressLabel.text = @"长宁区";
    _addressLabel.textColor = HEXCOLOR(0x999999);
    _addressLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:_addressLabel];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:15];
    }];
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.text = @"填写申请信息";
    infoLabel.textColor = HEXCOLOR(0x999999);
    infoLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:infoLabel];
    [infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         [make.left.mas_equalTo(self.view)setOffset:20];
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:15];
    }];
    
    _inforTextView = [[IQTextView alloc]init];
    _inforTextView.font = [UIFont systemFontOfSize:15];
    _inforTextView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_inforTextView];
    [_inforTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.right.mas_equalTo(self.view)setOffset:0];
        [make.top.mas_equalTo(infoLabel.mas_bottom)setOffset:15];
        make.height.mas_equalTo(100);
    }];
    
    UIView *timeView = [UIView new];
    timeView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:timeView];
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.right.mas_equalTo(self.view)setOffset:0];
        [make.top.mas_equalTo(self->_inforTextView.mas_bottom)setOffset:15];
        make.height.mas_equalTo(50);
    }];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.text = @"时间";
    timeLabel.textColor = HEXCOLOR(0x333333);
    timeLabel.font = [UIFont systemFontOfSize:15];
    [timeView addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(timeView)setOffset:20];
        make.top.height.mas_equalTo(timeView);
    }];
    
    UIImageView *right = [[UIImageView alloc]init];
    right.image = [UIImage imageNamed:@"guan_right"];
    [timeView addSubview:right];
    [right mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(timeView.mas_right)setOffset:-15];
        make.centerY.mas_equalTo(timeView.mas_centerY);
        //make.size.mas_equalTo(CGSizeMake(8, 16));
    }];
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"一个月";
    _dateLabel.textColor = HEXCOLOR(0x999999);
    _dateLabel.font = [UIFont systemFontOfSize:13];
    [timeView addSubview:_dateLabel];
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(right.mas_left)setOffset:-10];
         make.centerY.mas_equalTo(timeView.mas_centerY);
    }];
    
    UILabel *tipLabel = [UILabel new];
    tipLabel.text = @"提示：冠名费每天所需10点币";
    tipLabel.textColor = HEXCOLOR(0x999999);
    tipLabel.font = [UIFont systemFontOfSize:12];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.right.mas_equalTo(self.view)setOffset:0];
       [make.top.mas_equalTo(timeView.mas_bottom)setOffset:15];
    }];
    
    UIButton *footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [footerBtn setTitle:@"发送" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:footerBtn];
    [footerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        [make.left.mas_equalTo(self.view)setOffset:38];
        [make.right.mas_equalTo(self.view)setOffset:-38];
        [make.top.mas_equalTo(tipLabel.mas_bottom)setOffset:120];
    }];
    
}

-(void)footerBtnClick{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
