//
//  NoticeViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/4.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "NoticeChatViewController.h"

@interface NoticeChatViewController ()

@property(nonatomic ,strong)IQTextView *inforTextView;
@property(nonatomic ,strong)UIButton *saveBtn;

@end

@implementation NoticeChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"公告";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self setNaviBarButtonItem];
    [self creatUI];
}

-(void)setNaviBarButtonItem{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"编辑" forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    [btn setTitle:@"取消" forState:UIControlStateSelected];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn addTarget:self action:@selector(editNoticeAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    if (self.isOwner) {
        self.navigationItem.rightBarButtonItems = @[btnItem];
    }
    
}

-(void)creatUI{
    _inforTextView = [[IQTextView alloc]init];
    _inforTextView.font = [UIFont systemFontOfSize:15];
    _inforTextView.text = @"金牛座不招人待见的一个原因就是他们非常的固执，只要是金牛座认定的事情，那么谁都别想要改变，谁也都别想要说服他们。哪怕金牛座做的是非常错误的决定，众人都劝说他们，金牛座依然是不为所动，他们觉得所有人都不理解自己，他们不想受人影响。";
    _inforTextView.placeholder = @"字数最多15字";
    _inforTextView.clipsToBounds = YES;
    _inforTextView.layer.cornerRadius = 8.0;
    _inforTextView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _inforTextView.editable = NO;
    [self.view addSubview:_inforTextView];
    [_inforTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.view)setOffset:15];
        [make.right.mas_equalTo(self.view)setOffset:-15];
        [make.top.mas_equalTo(self.view)setOffset:64 +STATUS_BAR_HEIGHT];
        make.height.mas_equalTo(200);
    }];
    
    _saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(40, 350, kSCREENWIDTH - 80, 40)];
    [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_saveBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    _saveBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    _saveBtn.hidden = YES;
    [self.view addSubview:_saveBtn];

    
}

-(void)editNoticeAction:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.inforTextView.editable = YES;
        self.inforTextView.backgroundColor = [UIColor whiteColor];
        self.saveBtn.hidden = NO;
    }else{
        self.inforTextView.editable = NO;
        self.inforTextView.backgroundColor = HEXCOLOR(0xf6f6fa);
        self.saveBtn.hidden = YES;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
