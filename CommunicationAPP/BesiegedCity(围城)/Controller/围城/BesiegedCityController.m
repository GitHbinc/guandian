//
//  BesiegedCityController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BesiegedCityController.h"
#import "CreatBesiegedCityController.h"
#import "GroupListViewController.h"
#import "PublishAuctionViewController.h"
#import "AuctionChatViewController.h"
#import "VoteListViewController.h"
#import "GroupIntroduceViewController.h"
#import "GroupInformationViewController.h"
#import "GroupOwnerInformationViewController.h"

#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
@interface BesiegedCityController ()

@end

@implementation BesiegedCityController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self creatMap];
    [self creatUI];
    // Do any additional setup after loading the view.
}

-(void)creatMap{
    [AMapServices sharedServices].enableHTTPS = YES;
    ///初始化地图
    MAMapView *_mapView = [[MAMapView alloc] initWithFrame:self.view.bounds];
    
    ///把地图添加至view
    [self.view addSubview:_mapView];
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
}

-(void)creatUI{
    
    UIButton *chatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [chatBtn setTitle:@"建围城" forState:UIControlStateNormal];
    [chatBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [chatBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    chatBtn.tag = 1001;
    chatBtn.clipsToBounds = YES;
    [chatBtn.layer setCornerRadius:25];
    chatBtn.backgroundColor = [UIColor whiteColor];
    [chatBtn addTarget:self action:@selector(besiegeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:chatBtn];
    [chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20);
        make.left.mas_equalTo(self.view).offset(20);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    UIButton *listBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [listBtn setTitle:@"围城列表" forState:UIControlStateNormal];
    [listBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [listBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    listBtn.tag = 1002;
    listBtn.clipsToBounds = YES;
    [listBtn.layer setCornerRadius:4];
    listBtn.backgroundColor = [UIColor whiteColor];
    [listBtn addTarget:self action:@selector(besiegeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:listBtn];
    [listBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(chatBtn.mas_centerY);
        make.right.mas_equalTo(self.view).offset(-20);
        make.size.mas_equalTo(CGSizeMake(70, 30));
    }];
    
    NSArray *segmentArray = @[@"0.5km",@"1km",@"10km"];
    UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:segmentArray];
    segment.selectedSegmentIndex = 0;
    segment.tintColor = HEXCOLOR(0x9C386A);
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15]
};
    
    [segment setTitleTextAttributes:dic forState:UIControlStateNormal];
    [self.view addSubview:segment];
    [segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view).offset(-40);
        make.left.mas_equalTo(self.view).offset(20);
        make.right.mas_equalTo(self.view).offset(-20);
        make.height.mas_equalTo(50);
    }];
    
    
    UIButton *changeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [changeBtn setTitle:@"换一组" forState:UIControlStateNormal];
    [changeBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [changeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    changeBtn.tag = 1003;
    changeBtn.backgroundColor =HEXCOLOR(0x9C386A);
    changeBtn.clipsToBounds = YES;
    [changeBtn.layer setCornerRadius:20];
    [changeBtn addTarget:self action:@selector(besiegeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:changeBtn];
    [changeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(segment.mas_top).offset(-20);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    
}

-(void)besiegeAction:(UIButton *)sender{
    switch (sender.tag) {
        case 1001:
        {
            CreatBesiegedCityController *controller = [[CreatBesiegedCityController alloc]init];
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 1002:
        {
            GroupListViewController *controller = [[GroupListViewController alloc]init];
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 1003:
        {
            VoteListViewController *controller = [[VoteListViewController alloc]init];
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        default:
            break;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
