//
//  AddressFromMapViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/4.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface AddressFromMapViewController : BaseViewController<MAMapViewDelegate,AMapSearchDelegate>
@property (nonatomic,  copy ) void(^selectedEvent)(CLLocationCoordinate2D coordinate , NSString *addressName, NSString *province, NSString *city, NSString *distract, NSString *address);

@end

NS_ASSUME_NONNULL_END
