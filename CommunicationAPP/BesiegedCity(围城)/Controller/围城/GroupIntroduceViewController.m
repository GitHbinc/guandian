//
//  GroupIntroduceViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupIntroduceViewController.h"
#import "TitleApplicationViewController.h"
#import "GroupMemberListCell.h"
#import "SetUpViewCell.h"
#import "GroupMemberModel.h"
#import "OptionModel.h"

@interface GroupIntroduceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *contentLabel;

@property (nonatomic, strong) NSMutableArray *arraySource;

@end

@implementation GroupIntroduceViewController

-(NSMutableArray *)arraySource{
    if (!_arraySource) {
        _arraySource = [NSMutableArray array];
    }
    return _arraySource;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"新电影发布围城";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self loadTableView];
    
    [self loadHeadViewWithBottonView];
    
    NSArray *nameArray = @[@"小熊可怜",@"长大成人",@"德国科隆",@"豆腐块",@"德国科隆",@"豆腐块",@"小熊可怜",@"长大成人",@"德国科隆",@"豆腐块",@"德国科隆"];
    NSArray *iconArray = @[@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg"];
    
    if (nameArray.count <=9) {
        for (int i =0; i <nameArray.count; i++) {
            MineItem *item = [MineItem new];
            item.name = nameArray[i];
            item.icon = iconArray[i];
            [self.dataSource addObject:item];
        }
    }else{
        for (int i =0; i <9; i++) {
            MineItem *item = [MineItem new];
            item.name = nameArray[i];
            item.icon = iconArray[i];
            [self.dataSource addObject:item];
        }
        MineItem *item = [MineItem new];
        item.name = @"更多";
        item.icon = @"chat_more";
        [self.dataSource addObject:item];
    }


    NSArray *titleArray = @[@"围城成员",
                            @"围城创建位置",
                            @"围城创建时间",
                            @"申请冠名"
                           ];
    NSArray *contentArray = @[@"12人",
                            @"虹桥国际",
                            @"2019-01-01",
                            @""
                            ];
    
    for (int i = 0; i <titleArray.count; i++) {
        OptionModel *item = [[OptionModel alloc]init];
        item.name = titleArray[i];
        item.title = contentArray[i];
        [self.arraySource addObject:item];
    }
    [self.tableView reloadData];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadViewWithBottonView{
    UIView *headerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 100)];
    headerView.backgroundColor = [UIColor whiteColor];
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:30];
    [headerView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(headerView)setOffset:10];
        make.centerY.mas_equalTo(headerView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"新电影发布";
    _nameLabel.textColor = HEXCOLOR(0x9C386A);
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [headerView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(headerView.mas_centerY)setOffset:-10];
    }];
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"各种各样好看的电影应有尽有";
    _contentLabel.textColor = HEXCOLOR(0x666666);
    _contentLabel.font = [UIFont systemFontOfSize:12];
    [headerView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(headerView.mas_centerY)setOffset:10];
    }];

    self.tableView.tableHeaderView = headerView;
    
    
    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 120)];
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(40, 60, kSCREENWIDTH - 80, 40)];
    [footerBtn setTitle:@"加入该围城" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerView addSubview:footerBtn];
    self.tableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if (self.dataSource.count<=5) {
            return kSCREENWIDTH/5*1.2;
        }
        return kSCREENWIDTH/5*2.4;
    }else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
         static NSString *cellID = @"SetUpViewCell";
        SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = [self.arraySource objectAtIndex:indexPath.section];
        return cell;
        
    }else if (indexPath.section == 1) {
        static NSString *cellID = @"GroupMemberListCell";
        GroupMemberListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[GroupMemberListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        GroupMemberModel *model = [GroupMemberModel new];
        model.array = self.dataSource;
        cell.model = model;
        
        return cell;
    }
    static NSString *cellID = @"SetUpViewCell";
    SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.arraySource objectAtIndex:indexPath.section-1];
    if (indexPath.section == 4) {
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 4) {
        TitleApplicationViewController *title = [TitleApplicationViewController new];
         [self.navigationController pushViewController:title animated:YES];
    }
}


-(void)footerBtnClick{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
