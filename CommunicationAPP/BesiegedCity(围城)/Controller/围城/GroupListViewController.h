//
//  GroupListViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/24.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"
#import "YUFoldingTableView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupListViewController : BaseViewController<YUFoldingTableViewDelegate>
@property (nonatomic, assign) NSInteger index;
@end

NS_ASSUME_NONNULL_END
