//
//  ApplicationTitleViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/3.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplicationTitleViewController : BaseViewController

@end

NS_ASSUME_NONNULL_END
