//
//  ApplicationTitleViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/3.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "ApplicationTitleViewController.h"

@interface ApplicationTitleViewController ()
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *IDLabel;
@property(nonatomic ,strong)IQTextView *inforTextView;

@end

@implementation ApplicationTitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"申请冠名";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self creatUI];
}

-(void)creatUI{
    UIView *topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.right.mas_equalTo(self.view)setOffset:0];
        [make.top.mas_equalTo(self.view)setOffset:54 +STATUS_BAR_HEIGHT];
        make.height.mas_equalTo(85);
    }];
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:25];
    [topView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(topView)setOffset:20];
        make.centerY.mas_equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"李大狗";
    _nameLabel.textColor = HEXCOLOR(0x111111);
    _nameLabel.font =  [UIFont systemFontOfSize:16];
    [topView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:-12];
    }];
    
    _IDLabel = [UILabel new];
    _IDLabel.text = @"ID:3524564322";
    _IDLabel.textColor = HEXCOLOR(0x999999);
    _IDLabel.font = [UIFont systemFontOfSize:14];
    [topView addSubview:_IDLabel];
    [_IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:12];
    }];
    
    UIImageView *right = [[UIImageView alloc]init];
    right.image = [UIImage imageNamed:@"guan_right"];
    [topView addSubview:right];
    [right mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(topView.mas_right)setOffset:-15];
        make.centerY.mas_equalTo(topView);
    }];

    UIView *middleView = [UIView new];
    middleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:middleView];
    [middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.right.mas_equalTo(self.view)setOffset:0];
        [make.top.mas_equalTo(topView.mas_bottom)setOffset:2];
        make.height.mas_equalTo(90);
    }];
    
    UILabel *shenqing = [UILabel new];
    shenqing.text = @"申请留言";
    shenqing.textAlignment = NSTextAlignmentCenter;
    shenqing.backgroundColor = [UIColor whiteColor];
    shenqing.textColor = HEXCOLOR(0x999999);
    shenqing.font = [UIFont systemFontOfSize:13];
    [middleView addSubview:shenqing];
    [shenqing mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(middleView)setOffset:10];
        [make.top.mas_equalTo(middleView)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    
    _inforTextView = [[IQTextView alloc]init];
    _inforTextView.font = [UIFont systemFontOfSize:15];
    _inforTextView.backgroundColor = [UIColor whiteColor];
    [middleView addSubview:_inforTextView];
    [_inforTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(shenqing.mas_right)setOffset:10];
         [make.right.mas_equalTo(self.view)setOffset:-10];
         make.top.mas_equalTo(shenqing.mas_top);
        [make.bottom.mas_equalTo(middleView.mas_bottom)setOffset:-10];
    }];
    NSArray *array = @[@"拒绝",@"同意"];
    for (int i = 0; i<array.count; i++) {
        CGFloat left = 20*(i+1) + ((kSCREENWIDTH - 60)/2)*i;
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        button.frame = CGRectMake(left, STATUS_BAR_HEIGHT +300, (kSCREENWIDTH - 20*3)/2, 45);
        button.clipsToBounds = YES;
        button.layer.cornerRadius = 5.0;
        [button setTitle:array[i] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
        if([button.titleLabel.text isEqualToString:@"拒绝"]) {
            button.backgroundColor = [UIColor whiteColor];
            [button setTitleColor:HEXCOLOR(0x111111) forState:UIControlStateNormal];
        }else{
            button.backgroundColor = HEXCOLOR(0x9C386A);
             [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
    }
   
}

-(void)buttonClick:(UIButton *)sender{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
