//
//  GroupOwnerInformationViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/4.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupOwnerInformationViewController.h"
#import "GroupMemberListCell.h"
#import "SetUpViewCell.h"
#import "SetUpMiddleCell.h"
#import "GroupMemberModel.h"
#import "OptionModel.h"
#import "SetupNicknameViewController.h"
#import "AuctionChatViewController.h"
#import "TitleApplicationViewController.h"
#import "NoticeChatViewController.h"

@interface GroupOwnerInformationViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arraySource;

@end

@implementation GroupOwnerInformationViewController



-(NSMutableArray *)arraySource{
    if (!_arraySource) {
        _arraySource = [NSMutableArray array];
    }
    return _arraySource;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"围城设置";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self loadTableView];
    [self setNaviBarButtonItem];
    NSArray *nameArray = @[@"小熊可怜",@"长大成人",@"德国科隆",@"小熊可怜",@"长大成人",@"德国科隆",@"小熊可怜",@"长大成人",@"德国科隆",@"小熊可怜"];
    NSArray *iconArray = @[@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg"];
    
    if (nameArray.count <=9) {
        for (int i =0; i <nameArray.count; i++) {
            MineItem *item = [MineItem new];
            item.name = nameArray[i];
            item.icon = iconArray[i];
            [self.dataSource addObject:item];
        }
    }else{
        for (int i =0; i <9; i++) {
            MineItem *item = [MineItem new];
            item.name = nameArray[i];
            item.icon = iconArray[i];
            [self.dataSource addObject:item];
        }
        MineItem *item = [MineItem new];
        item.name = @"邀请";
        item.icon = @"chat_add";
        [self.dataSource addObject:item];
    }
    
    
    NSArray *titleArray = @[@"围城名称",
                            @"地点",
                            @"竞拍",
                            @"续费",
                            @"企业冠名",
                            @"更换视频",
                            @"围城公告"
                            ];
    NSArray *contentArray = @[@"小白交流群",
                              @"长宁区",
                              @"",
                              @"",
                              @"",
                              @"",
                              @""
                              ];
    
    for (int i = 0; i <titleArray.count; i++) {
        OptionModel *item = [[OptionModel alloc]init];
        item.name = titleArray[i];
        item.title = contentArray[i];
        [self.arraySource addObject:item];
    }
    [self.tableView reloadData];
    
}

-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"dian_more"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(moreAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 8;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 ||section == 1||section == 4 ||section == 6) {
        return 1;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (self.dataSource.count<=5) {
            return kSCREENWIDTH/5*1.2;
        }
        return kSCREENWIDTH/5*2.4;
    }else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *cellID = @"GroupMemberListCell";
        GroupMemberListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[GroupMemberListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        GroupMemberModel *model = [GroupMemberModel new];
        model.array = self.dataSource;
        cell.model = model;
        
        return cell;
    }
    
    static NSString *cellID = @"SetUpViewCell";
    SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.arraySource objectAtIndex:indexPath.section-1];
    if (indexPath.section != 2) {
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        
        SetupNicknameViewController *controller = [SetupNicknameViewController new];
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if(indexPath.section == 3){
        
        AuctionChatViewController *controller = [AuctionChatViewController new];
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if(indexPath.section == 4){
        
    }else if(indexPath.section == 5){
        
        TitleApplicationViewController *controller = [TitleApplicationViewController new];
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if(indexPath.section == 6){
        
    }else if(indexPath.section == 7){
        NoticeChatViewController *controller = [NoticeChatViewController new];
        controller.isOwner = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


-(void)footerBtnClick{
    
}

-(void)moreAction{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
