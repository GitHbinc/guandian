//
//  GroupMemberViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/3.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupMemberViewController.h"
#import "GroupHeadImageViewCell.h"
@interface GroupMemberViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UISearchBarDelegate>{
    UICollectionView *mainCollectionView;
}

@end

@implementation GroupMemberViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"围城成员";
    self.view.backgroundColor = [UIColor whiteColor];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.headerReferenceSize = CGSizeMake(kSCREENWIDTH, 42.0f);
   
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, Navi_STATUS_HEIGHT, kSCREENWIDTH, kSCREENHEIGHT) collectionViewLayout:layout];
    [self.view addSubview:mainCollectionView];
    mainCollectionView.backgroundColor = [UIColor clearColor];
     [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];  //  一定要设置
    [mainCollectionView registerClass:[GroupHeadImageViewCell class] forCellWithReuseIdentifier:@"GroupHeadImageViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    
    NSArray *nameArray = @[@"小熊可怜",@"长大成人",@"德国科隆",@"豆腐块",@"德国科隆",@"豆腐块",@"小熊可怜",@"长大成人",@"德国科隆",@"豆腐块",@"德国科隆"];
    NSArray *iconArray = @[@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg",@"center_avatar.jpeg"];
    
    for (int i =0; i <nameArray.count; i++) {
        MineItem *item = [MineItem new];
        item.name = nameArray[i];
        item.icon = iconArray[i];
        [self.dataSource addObject:item];
    }
    MineItem *item = [MineItem new];
    item.name = @"邀请";
    item.icon = @"chat_add";
    [self.dataSource addObject:item];
}

#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GroupHeadImageViewCell *cell = (GroupHeadImageViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"GroupHeadImageViewCell" forIndexPath:indexPath];
    MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
    cell.item = item;
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kSCREENWIDTH/5, kSCREENWIDTH/5*1.2);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    MineItem *item = [self.dataSource objectAtIndex:indexPath.item];
//    if ([item.name isEqualToString:@"更多"]) {
//        GroupMemberViewController *member = [GroupMemberViewController new];
//        [[self viewController].navigationController pushViewController:member animated:YES];
//    }
}

- (UICollectionReusableView *) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    header.backgroundColor = HEXCOLOR(0xf6f6fa);

    
  
    UISearchBar * searchBar = [[UISearchBar alloc]init];
    [header addSubview:searchBar];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0) { // iOS 11
        [searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(header).offset(10);
            make.right.mas_equalTo(header).offset(-10);
            make.centerY.mas_equalTo(header.mas_centerY);
            make.height.mas_equalTo(@30);
        }];

    } else {
        searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    searchBar.placeholder = [NSBundle py_localizedStringForKey:PYSearchSearchPlaceholderText];
    searchBar.backgroundImage = [NSBundle py_imageNamed:@"clearImage"];
    searchBar.delegate = self;
    for (UIView *subView in [[searchBar.subviews lastObject] subviews]) {
        if ([[subView class] isSubclassOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)subView;
            textField.font = [UIFont systemFontOfSize:15];
            textField.placeholder = @"搜索";
            break;
        }
    }
    return header;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
