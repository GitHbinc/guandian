//
//  SetupNicknameViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/4.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "SetupNicknameViewController.h"

@interface SetupNicknameViewController ()
@property(nonatomic ,strong)IQTextView *inforTextView;

@end

@implementation SetupNicknameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"设置昵称";
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self setNaviBarButtonItem];
    [self creatUI];
}

-(void)setNaviBarButtonItem{
    UIBarButtonItem * auctionBtn = [self getBarButtonItemWithTitleStr:@"保存" Sel:@selector(saveAction)];
    self.navigationItem.rightBarButtonItems = @[auctionBtn];
    
}

-(void)saveAction{
    
}

-(void)creatUI{
    _inforTextView = [[IQTextView alloc]init];
    _inforTextView.font = [UIFont systemFontOfSize:15];
    _inforTextView.placeholder = @"字数最多15字";
    _inforTextView.clipsToBounds = YES;
    _inforTextView.layer.cornerRadius = 5.0;
    _inforTextView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_inforTextView];
    [_inforTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.view)setOffset:15];
        [make.right.mas_equalTo(self.view)setOffset:-15];
        [make.top.mas_equalTo(self.view)setOffset:64 +STATUS_BAR_HEIGHT];
        make.height.mas_equalTo(100);
    }];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
