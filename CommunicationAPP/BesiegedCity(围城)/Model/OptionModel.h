//
//  OptionModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OptionModel : NSObject
@property(nonatomic ,copy) NSString *name;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,assign)CGFloat type;
@property(nonatomic ,assign)CGFloat headHeight;

@end

NS_ASSUME_NONNULL_END
