//
//  GroupListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/3.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupListModel : NSObject
@property(nonatomic ,copy) NSString *name;
@property(nonatomic ,copy) NSString *content;
@property(nonatomic ,copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
