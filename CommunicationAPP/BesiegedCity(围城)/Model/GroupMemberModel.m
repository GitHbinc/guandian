//
//  GroupMemberModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "GroupMemberModel.h"

@implementation GroupMemberModel

-(NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}


@end
