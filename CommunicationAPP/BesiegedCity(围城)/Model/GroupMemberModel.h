//
//  GroupMemberModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/2.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupMemberModel : NSObject
@property(nonatomic ,strong)NSMutableArray *array;
@end

NS_ASSUME_NONNULL_END
