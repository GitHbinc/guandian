//
//  AuctionModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/26.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AuctionModel : NSObject
@property(nonatomic ,assign)CGFloat headHeight;
@property(nonatomic ,assign)BOOL isInput;

@end

NS_ASSUME_NONNULL_END
