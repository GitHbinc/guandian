//
//  BesiegedCityModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BesiegedCityModel : NSObject
@property(nonatomic ,copy) NSString *name;//名字
@property(nonatomic ,copy) NSString *placeholder;
@end

NS_ASSUME_NONNULL_END
