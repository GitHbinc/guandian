//
//  VideoCommentViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoCommentViewController : BaseViewController
@property (nonatomic ,strong)RankVideoModel *model;

@end

NS_ASSUME_NONNULL_END
