//
//  NearbyViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "NearbyViewController.h"
#import "VideoDetailViewController.h"
#import "NearbyViewCell.h"
#import "RankVideoModel.h"
#import "MineItem.h"
@interface NearbyViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *mainCollectionView;
}

@end

@implementation NearbyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadCollectionView];
    
    mainCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getDataFromSever];
    }];
    
    [mainCollectionView.mj_header beginRefreshing];
    
   
}

-(void)loadCollectionView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.view addSubview:mainCollectionView];
    [mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    mainCollectionView.backgroundColor = [UIColor clearColor];
    mainCollectionView.alwaysBounceVertical = YES;  // 垂直
    [mainCollectionView registerClass:[NearbyViewCell class] forCellWithReuseIdentifier:@"NearbyViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"type" :self.type?:@"",
                            @"vtype":self.model.sort?:@"",
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"longitude":[kUserDefault objectForKey:Longitude]?:@"",
                            @"latitude":[kUserDefault objectForKey:Latitude]?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:rank_topcate parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        [self endRefresh];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            for (NSDictionary *dic in [responseObject objectForKey:@"data"]) {
                RankVideoModel *item = [[RankVideoModel alloc]initWithDictionary:dic error:nil];
                [weakSelf.dataSource addObject:item];
            }
            [self->mainCollectionView reloadData];
        }else{
             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NearbyViewCell *cell = (NearbyViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"NearbyViewCell" forIndexPath:indexPath];
    cell.isNearby = self.isNearby;
    cell.model = [self.dataSource objectAtIndex:indexPath.item];
   
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((kSCREENWIDTH - 30)/2, 165);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoDetailViewController *detail = [VideoDetailViewController new];
    RankVideoModel *model = [self.dataSource objectAtIndex:indexPath.item];
    detail.model = model;
    detail.URLString = [NSString stringWithFormat:@"%@%@",kWEBURL,model.video];
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
    
}

- (void)endRefresh{
    [mainCollectionView.mj_header endRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
