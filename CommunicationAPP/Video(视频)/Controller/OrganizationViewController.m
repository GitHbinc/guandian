//
//  OrganizationViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "OrganizationViewController.h"
#import "OrganizationViewCell.h"
#import "VideoDetailViewController.h"
@interface OrganizationViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    UIView *_comentView;
    UIView *_bgView;
    UIButton *_commentBtn;
}
@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation OrganizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self loadTableView];
   
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 7;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    VideoDetailViewController *detail = [VideoDetailViewController new];
    detail.URLString = @"http://admin.weixin.ihk.cn/ihkwx_upload/test.mp4";
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"OrganizationViewCell";
    OrganizationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[OrganizationViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = colorf3f3f3;
    [cell refreshCellModel];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __block OrganizationViewCell *blockSelf = cell;
    cell.bottomBlock = ^(UIButton *senderBtn){
        NSLog(@"======%ld",senderBtn.tag);
        
        [self cellBtnClick:blockSelf senderBtn:senderBtn];
    };
    
    // Configure the cell...
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

-(void)cellBtnClick:(OrganizationViewCell *)cell senderBtn:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
            
        case 101://点赞
            break;
        case 102://评论
        {
            
            _commentBtn = senderBtn;
            _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
            _bgView.userInteractionEnabled = true;
            //处理父视图透明度会影响子视图
            _bgView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
            UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeCommentView)];
            [_bgView addGestureRecognizer:reg];
            
            [[UIApplication sharedApplication].keyWindow addSubview:_bgView];
            
            _comentView = [self setCommentView];
            _comentView.backgroundColor = UIColor.whiteColor;
            [_bgView addSubview:_comentView];
            
        }
            break;
        case 103://转发
            break;
        case 104:
        {
            
            if (senderBtn.selected) {
                cell.moreImageView.hidden = false;
            }else{
                cell.moreImageView.hidden = true;
            }
        }
            break;
        case 105://分享
            break;
        case 106://关注
            break;
        case 107://收藏
            break;
        case 108://排行
            break;
        case 109://竞拍
            break;
        case 110://举报
            break;
        case 111://拉黑
            break;
        case 112://最大化
            break;
        default:
            break;
    }
    
}

-(void)removeCommentView{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [_bgView removeFromSuperview];
}


-(UIView *)setCommentView{
    UIView *commentView = [[UIView alloc]initWithFrame:CGRectMake(0, kSCREENHEIGHT - 410, kSCREENWIDTH, 63)];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [commentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(0);
        make.right.equalTo(commentView.mas_right).offset(0);
        make.top.equalTo(commentView.mas_top).offset(0);
        make.height.equalTo(@1);
    }];
    
    UIButton *expressionBtn = [[UIButton alloc]init];
    [expressionBtn setImage:[UIImage imageNamed:@"comment_expressionBtn_icon"] forState:UIControlStateNormal];
    expressionBtn.tag = 101;
    [expressionBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:expressionBtn];
    [expressionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.height.equalTo(@28);
        make.width.equalTo(@28);
    }];
    
    UITextField *contentField = [[UITextField alloc]init];
    contentField.placeholder = @"请输入评论内容....";
    contentField.delegate = self;
    [contentField becomeFirstResponder];
    [commentView addSubview:contentField];
    [contentField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(expressionBtn.mas_right).offset(10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.width.equalTo(@(kSCREENWIDTH - 100 - 48));
        make.height.equalTo(@31);
    }];
    
    UIButton *sendBtn = [[UIButton alloc]init];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:[UIImage imageNamed:@"comment_sendBtn_icon"] forState:UIControlStateNormal];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [sendBtn setTitleColor:color999999 forState:UIControlStateNormal];
    sendBtn.tag = 102;
    [sendBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(commentView.mas_right).offset(-10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
    }];
    
    return commentView;
}

-(void)commentBtnClick:(UIButton *)senderBtn{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [_bgView removeFromSuperview];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
