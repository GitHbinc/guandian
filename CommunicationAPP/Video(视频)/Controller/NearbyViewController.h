//
//  NearbyViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankSortModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NearbyViewController : BaseViewController
@property(nonatomic ,strong)RankSortModel *model;//短视频模型
@property(nonatomic ,copy)NSString *type;//类型
@property(nonatomic ,assign)BOOL isNearby;
@end

NS_ASSUME_NONNULL_END
