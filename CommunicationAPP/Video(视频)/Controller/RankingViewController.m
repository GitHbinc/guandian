//
//  RankingViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RankingViewController.h"
#import "RankClassfyView.h"
#import "SearchViewController.h"
#import "RankSortModel.h"

@interface RankingViewController ()<PYSearchViewControllerDelegate>
@property(nonatomic ,strong)RankClassfyView *headerView;
@end

@implementation RankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     NSLog(@"********%f*********",Navi_BAR_HEIGHT);
    [self addClassfyView];
    [self getDataFromSever];
    
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    [[WYNetworking sharedWYNetworking] POST:rank_typelist parameters:@{@"uid":[kUserDefault objectForKey:USERID]?:@""} success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            for (NSDictionary *dic in [responseObject objectForKey:@"data"]) {
                NSError* err = nil;
                RankSortModel *item = [[RankSortModel alloc]initWithDictionary:dic error:&err];
                [weakSelf.dataSource addObject:item];
            }
            weakSelf.headerView.sortArray = weakSelf.dataSource;
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

-(void)addClassfyView{
     RankClassfyView *headerView = [[RankClassfyView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
    _headerView = headerView;
    headerView.rankButtonClick = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 1001://搜索
            {
                NSArray *hotSeaches = @[@"huahua", @"东晨", @"超哥", @"小白biubiu~", @"安然", @"夏日", @"乐乐", @"李大狗"];
                PYSearchViewController *searchViewController = [PYSearchViewController searchViewControllerWithHotSearches:hotSeaches searchBarPlaceholder:@"搜索你想要的内容" didSearchBlock:^(PYSearchViewController *searchViewController, UISearchBar *searchBar, NSString *searchText) {
                   SearchViewController *search =  [[SearchViewController alloc] init];
                    search.searchText = searchText;
                  [searchViewController.navigationController pushViewController:search animated:YES];
                   
                }];
                searchViewController.hotSearchStyle = PYHotSearchStyleRankTag;
                searchViewController.searchHistoryStyle = PYHotSearchStyleDefault;
                  searchViewController.delegate = self;
                 searchViewController.searchViewControllerShowMode = PYSearchViewControllerShowDefault;
    
                searchViewController.hidesBottomBarWhenPushed = YES;
                 [self.navigationController pushViewController:searchViewController animated:YES];
            }
                break;
                
            case 1002://分享
            {
                
            }
                break;
                
            default:
                break;
        }
    };
    [self.view addSubview:headerView];
}


- (void)didClickCancel:(PYSearchViewController *)searchViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PYSearchViewControllerDelegate
- (void)searchViewController:(PYSearchViewController *)searchViewController searchTextDidChange:(UISearchBar *)seachBar searchText:(NSString *)searchText
{
    if (searchText.length) {
        // Simulate a send request to get a search suggestions
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSMutableArray *searchSuggestionsM = [NSMutableArray array];
            for (int i = 0; i < arc4random_uniform(5) + 10; i++) {
                NSString *searchSuggestion = [NSString stringWithFormat:@"Search suggestion %d", i];
                [searchSuggestionsM addObject:searchSuggestion];
            }
            // Refresh and display the search suggustions
            searchViewController.searchSuggestions = searchSuggestionsM;
        });
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
