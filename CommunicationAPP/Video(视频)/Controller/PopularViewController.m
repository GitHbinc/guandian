//
//  PopularViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PopularViewController.h"
#import "PopularViewCell.h"
#import "VideoDetailViewController.h"
#import "RankVideoModel.h"

@interface PopularViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation PopularViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getDataFromSever];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
  
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"type" :self.type?:@"",
                            @"vtype":self.model.sort?:@"",
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:rank_topcate parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
         [self endRefresh];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            for (NSDictionary *dic in [responseObject objectForKey:@"data"]) {
                RankVideoModel *item = [[RankVideoModel alloc]initWithDictionary:dic error:nil];
                [weakSelf.dataSource addObject:item];
            }
            
            [weakSelf.tableView reloadData];
            
        }else{
             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"PopularViewCell";
    PopularViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[PopularViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
    cell.rankLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    VideoDetailViewController *detail = [VideoDetailViewController new];
    RankVideoModel *model = [self.dataSource objectAtIndex:indexPath.row];
    detail.model = model;
    detail.URLString = [NSString stringWithFormat:@"%@%@",kWEBURL,model.video];
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
