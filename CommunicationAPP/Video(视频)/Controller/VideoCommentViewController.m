//
//  VideoCommentViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VideoCommentViewController.h"
#import "CommentVideoCell.h"
#import "VideoHeadView.h"
@interface VideoCommentViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UIView *_comentView;
    UIView *_bgView;
    UIButton *_commentBtn;
}
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)VideoHeadView *headerView;

@end

@implementation VideoCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
    [self loadHeadView];
    
    [self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    [[WYNetworking sharedWYNetworking] POST:rank_svideo_info parameters:@{@"sid":_model.ID?:@"",@"uid":[kUserDefault objectForKey:USERID]?:@""} success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            RankVideoDetailModel *item = [[RankVideoDetailModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            weakSelf.headerView.model = weakSelf.model;
            weakSelf.headerView.DetailModel = item;
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}



-(void)loadHeadView{
    VideoHeadView *headerView = [[VideoHeadView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 130)];
    _headerView = headerView;
    headerView.bottomBlock = ^(UIButton *senderBtn){
        NSLog(@"======%ld",senderBtn.tag);
        [self cellBtnClicksenderBtn:senderBtn];
    };
    self.tableView.tableHeaderView = headerView;
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    //设置自动计算行号模式
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    self.tableView.estimatedRowHeight = 200;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CommentVideoCell";
    CommentVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[CommentVideoCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    [cell layoutIfNeeded];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont systemFontOfSize:13];
    headerLabel.textColor = HEXCOLOR(0x555555);
    headerLabel.text = @"全部评论";
    [headerView addSubview:headerLabel];
    
    return headerView;
}

-(void)cellBtnClicksenderBtn:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
            
        case 101://点赞
            break;
        case 102://评论
        {
            
            _commentBtn = senderBtn;
            _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
            _bgView.userInteractionEnabled = true;
            //处理父视图透明度会影响子视图
            _bgView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
            UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeCommentView)];
            [_bgView addGestureRecognizer:reg];
            
            [[UIApplication sharedApplication].keyWindow addSubview:_bgView];
            
            _comentView = [self setCommentView];
            _comentView.backgroundColor = UIColor.whiteColor;
            [_bgView addSubview:_comentView];
            
        }
            break;
        case 103://转发
            break;
        case 104:
        {
            
            //            if (senderBtn.selected) {
            //                cell.moreImageView.hidden = false;
            //            }else{
            //                cell.moreImageView.hidden = true;
            //            }
        }
            break;
        case 105://分享
            break;
        case 106://关注
            break;
        case 107://收藏
            break;
        case 108://排行
            break;
        case 109://竞拍
            break;
        case 110://举报
            break;
        case 111://拉黑
            break;
        case 112://最大化
            break;
        default:
            break;
    }
    
}

-(void)removeCommentView{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [_bgView removeFromSuperview];
}


-(UIView *)setCommentView{
    UIView *commentView = [[UIView alloc]initWithFrame:CGRectMake(0, kSCREENHEIGHT - 410, kSCREENWIDTH, 63)];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [commentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(0);
        make.right.equalTo(commentView.mas_right).offset(0);
        make.top.equalTo(commentView.mas_top).offset(0);
        make.height.equalTo(@1);
    }];
    
    UIButton *expressionBtn = [[UIButton alloc]init];
    [expressionBtn setImage:[UIImage imageNamed:@"comment_expressionBtn_icon"] forState:UIControlStateNormal];
    expressionBtn.tag = 101;
    [expressionBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:expressionBtn];
    [expressionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentView.mas_left).offset(10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.height.equalTo(@28);
        make.width.equalTo(@28);
    }];
    
    UITextField *contentField = [[UITextField alloc]init];
    contentField.placeholder = @"请输入评论内容....";
    contentField.delegate = self;
    [contentField becomeFirstResponder];
    [commentView addSubview:contentField];
    [contentField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(expressionBtn.mas_right).offset(10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
        make.width.equalTo(@(kSCREENWIDTH - 100 - 48));
        make.height.equalTo(@31);
    }];
    
    UIButton *sendBtn = [[UIButton alloc]init];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:[UIImage imageNamed:@"comment_sendBtn_icon"] forState:UIControlStateNormal];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [sendBtn setTitleColor:color999999 forState:UIControlStateNormal];
    sendBtn.tag = 102;
    [sendBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [commentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(commentView.mas_right).offset(-10);
        //        make.top.equalTo(commentView.mas_top).offset(20);
        make.centerY.equalTo(commentView.mas_centerY);
    }];
    
    return commentView;
}

-(void)commentBtnClick:(UIButton *)senderBtn{
    
    _commentBtn.selected = !_commentBtn.selected;
    [_commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    [_bgView removeFromSuperview];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
    CGFloat sectionHeaderHeight=50;
    if(scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0){
        scrollView.contentInset=UIEdgeInsetsMake(-scrollView.contentOffset.y,0,0,0);
        
    }else if(scrollView.contentOffset.y>=sectionHeaderHeight) {
        
        scrollView.contentInset=UIEdgeInsetsMake(-sectionHeaderHeight,0,0,0);
        
    }
    
}



@end
