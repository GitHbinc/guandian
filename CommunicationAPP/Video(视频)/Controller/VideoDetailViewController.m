//
//  VideoDetailViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VideoDetailViewController.h"
#import "WMPlayer.h"
#import "MineViewController.h"
#import "VideoIntrodutionController.h"
#import "VideoCommentViewController.h"
#import "RankVideoDetailModel.h"

@interface VideoDetailViewController ()<UIScrollViewDelegate,WMPlayerDelegate>
{
    WMPlayer  *wmPlayer;
    CGRect     playerFrame;
}
@property (nonatomic, strong)UIView *titleView;
@property (nonatomic ,strong)UILabel *pointLabel;
@property (nonatomic, strong)UIScrollView *contentScrollView;
@end

@implementation VideoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setupVideoPlayView];
    
    [self setUI];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //旋转屏幕通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onDeviceOrientationChange)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     self.navigationController.navigationBarHidden = NO;
}


- (void)setupVideoPlayView{

    playerFrame =CGRectMake(0,  0, kSCREENWIDTH, kSCREENHEIGHT * 5 / 16);
    wmPlayer = [[WMPlayer alloc]initWithFrame:playerFrame];
    wmPlayer.delegate = self;
    
    wmPlayer.URLString = self.URLString;
    wmPlayer.titleLabel.text = self.title;
    wmPlayer.closeBtn.hidden = NO;
    [self.view addSubview:wmPlayer];
    [wmPlayer play];
    
}

-(void)setUI{
    
    self.titleView = [[UIView alloc]initWithFrame:CGRectMake(0, kSCREENHEIGHT* 5 / 16, kSCREENWIDTH, 44)];
    [self.view addSubview:self.titleView];
    self.titleView.backgroundColor = UIColor.whiteColor;
    
    
    self.contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kSCREENHEIGHT* 5 / 16 +44, kSCREENWIDTH, kSCREENHEIGHT* 11 / 16 - 44)];
    [self.view addSubview:self.contentScrollView];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.contentScrollView.delegate = self;
    
    [self addController];
    [self addLable];
    
    
    CGFloat contentX = self.childViewControllers.count * [UIScreen mainScreen].bounds.size.width;
    self.contentScrollView.contentSize = CGSizeMake(contentX, 0);
    self.contentScrollView.pagingEnabled = YES;
    
    // 添加默认控制器
    
    MineViewController *vc = [self.childViewControllers firstObject];
    vc.view.frame = self.contentScrollView.bounds;
    [self.contentScrollView addSubview:vc.view];
    UILabel *lable = [self.titleView.subviews firstObject];
    lable.textColor = MAINCOLOR;
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    
    self.pointLabel = [UILabel new];
    self.pointLabel.backgroundColor =  HEXCOLOR(0x9C386A);
    [lable addSubview:self.pointLabel];
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lable.mas_bottom);
        make.centerX.equalTo(lable.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(30, 3));
        
    }];
    
}

#pragma mark 添加子控制器
- (void)addController{
    NSArray *arrayLists = @[@"简介",@"评论"];
    for (int i=0 ; i<arrayLists.count ;i++){
        
        if (i == 0) {
            VideoIntrodutionController *vc1 = [[VideoIntrodutionController alloc]init];
            vc1.model = self.model;
            [self addChildViewController:vc1];
        }else{
            VideoCommentViewController*vc1 = [[VideoCommentViewController alloc]init];
            vc1.model = self.model;
          [self addChildViewController:vc1];
        }
    }
}

- (void)addLable{
    UILabel *lbl1 = [[UILabel alloc]init];
    lbl1.text = @"简介";
    lbl1.frame = CGRectMake((kSCREENWIDTH -100)/3, 0, 50, 40);
    lbl1.font = [UIFont systemFontOfSize:13];
    lbl1.textColor = color666666;
    lbl1.textAlignment = NSTextAlignmentCenter;
    [self.titleView addSubview:lbl1];
    lbl1.tag = 0;
    lbl1.userInteractionEnabled = YES;
    [lbl1 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];
    
    UILabel *lbl2 = [[UILabel alloc]init];
    lbl2.text = @"评论";
    lbl2.frame = CGRectMake((kSCREENWIDTH -100) *2/3 +50, 0, 50, 40);
    lbl2.font = [UIFont systemFontOfSize:13];
    lbl2.textColor = color666666;
    lbl2.textAlignment = NSTextAlignmentCenter;
    [self.titleView addSubview:lbl2];
    lbl2.tag = 1;
    lbl2.userInteractionEnabled = YES;
    [lbl2 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];

}

- (void)lblClick:(UITapGestureRecognizer *)recognizer{
    UILabel *titlelable = (UILabel *)recognizer.view;
    CGFloat offsetX = titlelable.tag * self.contentScrollView.frame.size.width;
    CGFloat offsetY = self.contentScrollView.contentOffset.y;
    CGPoint offset = CGPointMake(offsetX, offsetY);
    [self.contentScrollView setContentOffset:offset animated:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 获得索引
    NSUInteger index = scrollView.contentOffset.x / self.contentScrollView.frame.size.width;
    // 添加控制器
    UIViewController *newsVc;
    newsVc = self.childViewControllers[index];
    [self.titleView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx != index) {
            UILabel *temlabel = self.titleView.subviews[idx];
            temlabel.textColor = color666666;
        }
    }];
    if (newsVc.view.superview) return;
    newsVc.view.frame = scrollView.bounds;
    [self.contentScrollView addSubview:newsVc.view];
    
}

/** 滚动结束（手势导致） */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

/** 正在滚动 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 取出绝对值 避免最左边往右拉时形变超过1
    CGFloat value = ABS(scrollView.contentOffset.x / scrollView.frame.size.width);
    NSUInteger leftIndex = (int)value;
    NSUInteger rightIndex = leftIndex + 1;
    UILabel *labelLeft = self.titleView.subviews[leftIndex];
    labelLeft.textColor = HEXCOLOR(0x9C386A);
    [labelLeft addSubview:self.pointLabel];
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(labelLeft.mas_bottom);
        make.centerX.equalTo(labelLeft.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(30, 2));
        
    }];
    // 考虑到最后一个板块，如果右边已经没有板块了 就不在下面赋值scale了
    if (rightIndex < self.titleView.subviews.count) {
        UILabel *labelRight = self.titleView.subviews[rightIndex];
        labelRight.textColor = HEXCOLOR(0x9C386A);
    }
    
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}

/** 全屏 */
- (void)toFullScreenWithInterfaceOrientation:(UIInterfaceOrientation )interfaceOrientation
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [wmPlayer removeFromSuperview];
    wmPlayer.transform = CGAffineTransformIdentity;
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft) {
        wmPlayer.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }else if(interfaceOrientation==UIInterfaceOrientationLandscapeRight){
        wmPlayer.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    wmPlayer.frame = CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT);
    wmPlayer.playerLayer.frame =  CGRectMake(0,0, kSCREENHEIGHT,kSCREENWIDTH);
    
    [wmPlayer.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(kSCREENWIDTH-40);
        make.width.mas_equalTo(kSCREENHEIGHT);
    }];
    [wmPlayer.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.left.equalTo(self->wmPlayer).with.offset(0);
        make.width.mas_equalTo(kSCREENHEIGHT);
    }];
    [wmPlayer.closeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->wmPlayer.topView).with.offset(5);
        make.height.mas_equalTo(30);
        make.top.equalTo(self->wmPlayer.topView).with.offset(5);
        make.width.mas_equalTo(30);
    }];
    [wmPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->wmPlayer.topView).with.offset(45);
        make.right.equalTo(self->wmPlayer.topView).with.offset(-45);
        make.center.equalTo(self->wmPlayer.topView);
        make.top.equalTo(self->wmPlayer.topView).with.offset(0);
        
    }];
    [wmPlayer.loadFailedLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kSCREENHEIGHT);
        make.center.mas_equalTo(CGPointMake(kSCREENWIDTH/2-36, -(kSCREENWIDTH/2)+36));
        make.height.equalTo(@30);
    }];
    [wmPlayer.loadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointMake(kSCREENWIDTH/2-37, -(kSCREENWIDTH/2-37)));
    }];
    [[UIApplication sharedApplication].keyWindow addSubview:wmPlayer];
    wmPlayer.fullScreenBtn.selected = YES;
    [wmPlayer bringSubviewToFront:wmPlayer.bottomView];
    
}

/** 正常屏幕 */
-(void)toNormal{
    [wmPlayer removeFromSuperview];
    [UIView animateWithDuration:0.5f animations:^{
        self->wmPlayer.transform = CGAffineTransformIdentity;
        self->wmPlayer.frame =CGRectMake(self->playerFrame.origin.x, self->playerFrame.origin.y, self->playerFrame.size.width, self->playerFrame.size.height);
        self->wmPlayer.playerLayer.frame =  self->wmPlayer.bounds;
        [self.view addSubview:self->wmPlayer];
        [self->wmPlayer.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer).with.offset(0);
            make.right.equalTo(self->wmPlayer).with.offset(0);
            make.height.mas_equalTo(40);
            make.bottom.equalTo(self->wmPlayer).with.offset(0);
        }];
        
        
        [self->wmPlayer.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer).with.offset(0);
            make.right.equalTo(self->wmPlayer).with.offset(0);
            make.height.mas_equalTo(40);
            make.top.equalTo(self->wmPlayer).with.offset(0);
        }];
        
        
        [self->wmPlayer.closeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer.topView).with.offset(5);
            make.height.mas_equalTo(30);
            make.top.equalTo(self->wmPlayer.topView).with.offset(5);
            make.width.mas_equalTo(30);
        }];
        
        
        [self->wmPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer.topView).with.offset(45);
            make.right.equalTo(self->wmPlayer.topView).with.offset(-45);
            make.center.equalTo(self->wmPlayer.topView);
            make.top.equalTo(self->wmPlayer.topView).with.offset(0);
        }];
        
        [self->wmPlayer.loadFailedLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self->wmPlayer);
            make.width.equalTo(self->wmPlayer);
            make.height.equalTo(@30);
        }];
        
    }completion:^(BOOL finished) {
        self->wmPlayer.isFullscreen = NO;
        [self setNeedsStatusBarAppearanceUpdate];
        self->wmPlayer.fullScreenBtn.selected = NO;
        
    }];
}

///播放器事件
-(void)wmplayer:(WMPlayer *)wmplayer clickedCloseButton:(UIButton *)closeBtn{
    NSLog(@"clickedCloseButton");
    [self releaseWMPlayer];
    [self.navigationController popViewControllerAnimated:YES];
    
}
///播放暂停
-(void)wmplayer:(WMPlayer *)wmplayer clickedPlayOrPauseButton:(UIButton *)playOrPauseBtn{
    NSLog(@"clickedPlayOrPauseButton");
}
///全屏按钮
-(void)wmplayer:(WMPlayer *)wmplayer clickedFullScreenButton:(UIButton *)fullScreenBtn{
    if (fullScreenBtn.isSelected) {//全屏显示
        wmPlayer.isFullscreen = YES;
        [self setNeedsStatusBarAppearanceUpdate];
        [self toFullScreenWithInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
    }else{
        [self toNormal];
    }
}
///单击播放器
-(void)wmplayer:(WMPlayer *)wmplayer singleTaped:(UITapGestureRecognizer *)singleTap{
    NSLog(@"didSingleTaped");
}
///双击播放器
-(void)wmplayer:(WMPlayer *)wmplayer doubleTaped:(UITapGestureRecognizer *)doubleTap{
    NSLog(@"didDoubleTaped");
}
///播放状态
-(void)wmplayerFailedPlay:(WMPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state{
    NSLog(@"wmplayerDidFailedPlay");
}
-(void)wmplayerReadyToPlay:(WMPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state{
    NSLog(@"wmplayerDidReadyToPlay");
}
-(void)wmplayerFinishedPlay:(WMPlayer *)wmplayer{
    NSLog(@"wmplayerDidFinishedPlay");
}
/**
 *  旋转屏幕通知
 */
- (void)onDeviceOrientationChange{
    if (wmPlayer==nil||wmPlayer.superview==nil){
        return;
    }
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortraitUpsideDown:{
            NSLog(@"第3个旋转方向---电池栏在下");
        }
            break;
        case UIInterfaceOrientationPortrait:{
            NSLog(@"第0个旋转方向---电池栏在上");
            if (wmPlayer.isFullscreen) {
                [self toNormal];
            }
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            NSLog(@"第2个旋转方向---电池栏在左");
            wmPlayer.isFullscreen = YES;
            [self setNeedsStatusBarAppearanceUpdate];
            [self toFullScreenWithInterfaceOrientation:interfaceOrientation];
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            NSLog(@"第1个旋转方向---电池栏在右");
            wmPlayer.isFullscreen = YES;
            [self setNeedsStatusBarAppearanceUpdate];
            [self toFullScreenWithInterfaceOrientation:interfaceOrientation];
        }
            break;
        default:
            break;
    }
}

- (void)releaseWMPlayer
{
    [wmPlayer.player.currentItem cancelPendingSeeks];
    [wmPlayer.player.currentItem.asset cancelLoading];
    [wmPlayer pause];
    [wmPlayer removeFromSuperview];
    [wmPlayer.playerLayer removeFromSuperlayer];
    [wmPlayer.player replaceCurrentItemWithPlayerItem:nil];
    wmPlayer.player = nil;
    wmPlayer.currentItem = nil;
    //释放定时器，否侧不会调用WMPlayer中的dealloc方法
    [wmPlayer.autoDismissTimer invalidate];
    wmPlayer.autoDismissTimer = nil;
    
    
    wmPlayer.playOrPauseBtn = nil;
    wmPlayer.playerLayer = nil;
    wmPlayer = nil;
}

- (void)dealloc
{
    [self releaseWMPlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"DetailViewController deallco");
}



@end
