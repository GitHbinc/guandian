//
//  RankVideoModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/9.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankVideoModel : JSONModel
@property(nonatomic ,copy) NSString *click;
@property(nonatomic ,copy) NSString *comment;
@property(nonatomic ,copy) NSString *create_time;
@property(nonatomic ,copy) NSString *deleted;
@property(nonatomic ,copy) NSString *edit_time;
@property(nonatomic ,copy) NSString *fans;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *img;
@property(nonatomic ,copy) NSString *is_follow;
@property(nonatomic ,copy) NSString *logo;
@property(nonatomic ,copy) NSString *pub_type;
@property(nonatomic ,copy) NSString *report_num;
@property(nonatomic ,copy) NSString *s_num;
@property(nonatomic ,copy) NSString *score;
@property(nonatomic ,copy) NSString *sort;
@property(nonatomic ,copy) NSString *status;
@property(nonatomic ,copy) NSString *surl;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *types;
@property(nonatomic ,copy) NSString *uid;
@property(nonatomic ,copy) NSString *username;
@property(nonatomic ,copy) NSString *video;
@property(nonatomic ,copy) NSString *z_num;
@property(nonatomic ,copy) NSString *zstype;
@property(nonatomic ,copy) NSString *distance;

@end

NS_ASSUME_NONNULL_END
