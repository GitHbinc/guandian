//
//  RankRecommendModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/10.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankRecommendModel : JSONModel
@property(nonatomic ,copy) NSString *username;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *click;
@property(nonatomic ,copy) NSString *logo;

@end

NS_ASSUME_NONNULL_END
