//
//  RankSortModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/9.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "RankSortModel.h"

@implementation RankSortModel

+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
