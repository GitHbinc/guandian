//
//  RankVideoDetailModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/10.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "RankVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankVideoDetailModel : JSONModel

@property(nonatomic ,copy) NSString *comment;
@property(nonatomic ,copy) NSString *create_time;
@property(nonatomic ,copy) NSString *forwar;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *surl;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *video;
@property(nonatomic ,copy) NSString *z_num;
@property(nonatomic ,copy) NSString *s_num;
@property(nonatomic ,copy) NSString *score;
@property(nonatomic ,copy) NSString *is_follow;
@property(nonatomic ,copy) NSString *zstype;


@end

NS_ASSUME_NONNULL_END
