//
//  RankSortModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/9.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankSortModel : JSONModel

@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *sort;
@property(nonatomic ,copy) NSString *type;
@property(nonatomic ,copy) NSString <Optional>* qqqq;

@end

NS_ASSUME_NONNULL_END
