//
//  RankRecommendModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/10.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "RankRecommendModel.h"

@implementation RankRecommendModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
