//
//  RankClassfyView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RankClassfyView.h"
#import "ContentTableViewController.h"
#import "FollowViewController.h"
#import "PopularViewController.h"
#import "RedPeopleViewController.h"
#import "OrganizationViewController.h"
#import "NearbyViewController.h"
#import "RankSortModel.h"

@interface RankClassfyView()<UIScrollViewDelegate>

@property (nonatomic ,strong)UIView *topView;
@property (nonatomic ,strong)UIView *BottonView;
@property (nonatomic ,strong)UIImageView *backimage;
@property (nonatomic ,strong)UILabel *title;
@property (nonatomic ,strong)UILabel *pointLabel;
@property (nonatomic ,strong)UIImageView *pointImage;
@property (nonatomic ,strong)UIButton *searchBtn;
@property (nonatomic ,strong)UIButton *shareBtn;
@property (nonatomic ,strong)UIButton *selectBtn;
@property (nonatomic ,strong)NSMutableArray *titleArray;
@property (nonatomic ,strong)NSMutableArray *nameArray;
@property (nonatomic, strong)UIScrollView *titleScrollView;
@property (nonatomic, strong)UIScrollView *contentScrollView;
@property (nonatomic, assign)NSInteger classfTag;
@property (nonatomic, assign)NSInteger totalwidth;

@end

@implementation RankClassfyView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _titleArray = [@[@"推荐",@"热点",@"评分",@"附近"]mutableCopy];
        _nameArray = [NSMutableArray array];
//        _nameArray = [@[@"课程",@"舞蹈",@"明星",@"体育",@"游戏",@"教育",@"同城",@"旅游",@"课程",@"舞蹈",@"明星"]mutableCopy];
        [self setUpUI];
       
    }
    
    return self;
}


-(void)setUpUI{
      __weak __typeof(self)weakSelf = self;
    _topView = [UIView new];
    _topView.backgroundColor = [UIColor greenColor];
     [self addSubview:_topView];
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.equalTo(self).offset(0);
        make.size.height.equalTo(@(100 +STATUS_BAR_HEIGHT));
        
    }];
   
    
    _BottonView = [UIView new];
    _BottonView.backgroundColor = HEXCOLOR(0x9C386A);
    [self addSubview:_BottonView];
    [_BottonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.topView.mas_bottom);
        make.left.right.equalTo(self).offset(0);
        make.size.height.equalTo(@40);
        
    }];
    
    
    _backimage = [[UIImageView alloc]init];
    _backimage.image = [UIImage imageNamed:@"rank_headback"];
    [_topView addSubview:_backimage];
    [_backimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.equalTo(self).offset(0);
        make.size.height.equalTo(@(100 +STATUS_BAR_HEIGHT));
        
    }];
    
    
    _title = [UILabel new];
    _title.text = @"观点排行";
    _title.font = [UIFont boldSystemFontOfSize:16];
    _title.textAlignment = NSTextAlignmentCenter;
    _title.textColor = [UIColor whiteColor];
    [_topView addSubview:_title];
    [_title  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf).offset(STATUS_BAR_HEIGHT+10);
        //make.centerY.mas_equalTo(weakSelf.topView.mas_centerY);
        make.centerX.mas_equalTo(weakSelf.topView.mas_centerX);
    }];
    
    
    _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_searchBtn setImage:[UIImage imageNamed:@"rank_sousuo"] forState:UIControlStateNormal];
    _searchBtn.tag = 1001;
    [_searchBtn addTarget:self action:@selector(touchRankAction:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_searchBtn];
    [_searchBtn  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(@20);
        make.left.equalTo(weakSelf.topView).offset(20);
        make.centerY.equalTo(weakSelf.title.mas_centerY).offset(0);
    }];
    
    _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_shareBtn setImage:[UIImage imageNamed:@"rank_fenxiang"] forState:UIControlStateNormal];
    _shareBtn.tag = 1002;
    [_shareBtn addTarget:self action:@selector(touchRankAction:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_shareBtn];
    [_shareBtn  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(@20);
        make.right.equalTo(weakSelf.topView).offset(-20);
        make.centerY.equalTo(weakSelf.title.mas_centerY).offset(0);
    }];
    
    
    _titleScrollView = [[UIScrollView alloc]init];
    _titleScrollView.showsHorizontalScrollIndicator = NO;
    _titleScrollView.showsVerticalScrollIndicator = NO;
    [_BottonView addSubview:_titleScrollView];
    [_titleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(weakSelf.BottonView);
        make.left.right.equalTo(self).offset(0);
        make.size.height.equalTo(@40);
        
    }];
    
    
    for (int i = 0; i < _titleArray.count; i++) {
        CGFloat titleW = (kSCREENWIDTH-40)/_titleArray.count;
        CGFloat  titleH = 40;
        CGFloat  titleY = STATUS_BAR_HEIGHT +56;
        CGFloat  titleX = i * titleW +20;
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [titleBtn addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchUpInside];
        [titleBtn setTitle:_titleArray[i] forState:UIControlStateNormal];
        titleBtn.frame = CGRectMake(titleX, titleY, titleW, titleH);
        [titleBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
         [titleBtn setBackgroundImage:[UIImage imageNamed:@"rank_tuoyuan"] forState:UIControlStateSelected];
       
        [_topView addSubview:titleBtn];
        titleBtn.tag = i;
        if (titleBtn.tag ==0) {
            titleBtn.selected = YES;
            self.selectBtn = titleBtn;
            //[self clickTitle:titleBtn];
        }
    }
    
    //[self creatLabelsWith:_nameArray];

    
}

-(void)setSortArray:(NSMutableArray *)sortArray{
    _sortArray = sortArray;
    for (RankSortModel *model in _sortArray) {
        [_nameArray addObject:model.title];
    }
    [self creatLabelsWith:_nameArray];
}


-(BOOL)getwidthWithIsOrNo{
    CGFloat width = 0;
    for (int i = 0; i < _nameArray.count; i++) {
        CGFloat lblW = [self calculateRowWidth:_nameArray[i]];
        width +=lblW;
    }
    _totalwidth = width;
    if (width > kSCREENWIDTH) {
        return NO;
    }
    return YES;
   
}

-(void)creatLabelsWith:(NSMutableArray *)nameArrays{
    
    for (UIView *view in [_titleScrollView subviews]) {
        [view removeFromSuperview];
    }
    CGFloat width = 0;//qqq
    for (int i = 0; i < _nameArray.count; i++) {
        
        CGFloat lblW = [self calculateRowWidth:_nameArray[i]];
        CGFloat lblH = 40;
        CGFloat lblY = 0;
        CGFloat lblX = i * lblW;
        
        if ([self getwidthWithIsOrNo]) {
            lblX = width + i * ((kSCREENWIDTH - self.totalwidth -40)/(_nameArray.count -1))+20;
        }
        width +=lblW;
        UILabel *lbl1 = [[UILabel alloc]init];
        lbl1.text = _nameArray[i];
        lbl1.frame = CGRectMake(lblX, lblY, lblW, lblH);
        lbl1.font = [UIFont systemFontOfSize:13];
        lbl1.textColor = [UIColor whiteColor];
        lbl1.textAlignment = NSTextAlignmentCenter;
//        lbl1.backgroundColor = [UIColor yellowColor];
        lbl1.tag = i;
        lbl1.userInteractionEnabled = YES;
        [_titleScrollView addSubview:lbl1];
        [lbl1 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];
        
    }
    //_totalwidth = width;
    _titleScrollView.contentSize = CGSizeMake(width, 0);
    
    _contentScrollView = [[UIScrollView alloc]init];
    _contentScrollView.showsHorizontalScrollIndicator = NO;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    _contentScrollView.delegate = self;
    _contentScrollView.frame = CGRectMake(0, 140 +STATUS_BAR_HEIGHT, kSCREENWIDTH, kSCREENHEIGHT - 140 - STATUS_BAR_HEIGHT- TABBAR_BAR_HEIGHT);
    [self addSubview:_contentScrollView];
    
    [self addController];
    
    CGFloat contentX = [self viewController].childViewControllers.count * [UIScreen mainScreen].bounds.size.width;
    self.contentScrollView.contentSize = CGSizeMake(contentX, 0);
    self.contentScrollView.pagingEnabled = YES;
    
    // 添加默认控制器
    FollowViewController *vc = [[self viewController].childViewControllers firstObject];
    vc.view.frame = self.contentScrollView.bounds;
    [self.contentScrollView addSubview:vc.view];
    UILabel *lable = [self.titleScrollView.subviews firstObject];
    lable.textColor = [UIColor whiteColor];
    
    self.pointImage = [UIImageView new];
    self.pointImage.image = [UIImage imageNamed:@"rank_sanjiao"];
    [lable addSubview:self.pointImage];
    [self.pointImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lable.mas_bottom);
        make.centerX.equalTo(lable.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(15, 10));
        
    }];
    
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
}

/** 添加子控制器 */
- (void)addController{
   for (UIViewController *vc in [self viewController].childViewControllers) {
        [vc.view removeFromSuperview];
        [vc removeFromParentViewController];
    }
    
    switch (self.classfTag) {
        case 0:
        {
            for (RankSortModel *model in _sortArray) {
                PopularViewController *vc1 = [[PopularViewController alloc]init];
                vc1.model = model;
                vc1.type = [NSString stringWithFormat:@"%ld",self.classfTag+1];
                [[self viewController] addChildViewController:vc1];
            }
            
        }
            break;
        case 1:
        {
            for (RankSortModel *model in _sortArray) {
                PopularViewController *vc1 = [[PopularViewController alloc]init];
                vc1.model = model;
                vc1.type = [NSString stringWithFormat:@"%ld",self.classfTag+1];
                [[self viewController] addChildViewController:vc1];
            }
        }
            break;
      
        case 2:
        {
            for (RankSortModel *model in _sortArray) {
                NearbyViewController *vc1 = [[NearbyViewController alloc]init];
                vc1.model = model;
                vc1.type = [NSString stringWithFormat:@"%ld",self.classfTag+1];
                [[self viewController] addChildViewController:vc1];
            }
        }
            break;
        case 3:
        {
            for (RankSortModel *model in _sortArray) {
                NearbyViewController *vc1 = [[NearbyViewController alloc]init];
                vc1.model = model;
                vc1.isNearby = YES;
                vc1.type = [NSString stringWithFormat:@"%ld",self.classfTag+1];
                [[self viewController] addChildViewController:vc1];
            }
        }
            break;
            
        default:
            break;
    }

}

-(void)touchRankAction:(UIButton *)sender{
    if (_rankButtonClick) {
        _rankButtonClick(sender);
    }
}

-(void)clickTitle:(UIButton *)sender{
    if (self.selectBtn) {
        self.selectBtn.selected = NO;
        self.selectBtn.userInteractionEnabled = YES;
    }
    sender.selected = YES;
    sender.userInteractionEnabled = NO;
    self.selectBtn = sender;
    //[_nameArray removeAllObjects];
    _classfTag = sender.tag;
    if (sender.tag ==0) {//热门
        [self creatLabelsWith:_nameArray];
    }else if (sender.tag ==1){//红人

        [self creatLabelsWith:_nameArray];
    }else if (sender.tag ==2){//评分
 
        [self creatLabelsWith:_nameArray];
    }else if (sender.tag ==3){//附近
  
        [self creatLabelsWith:_nameArray];
    }
}



- (void)lblClick:(UITapGestureRecognizer *)recognizer{
    UILabel *titlelable = (UILabel *)recognizer.view;
    CGFloat offsetX = titlelable.tag * self.contentScrollView.frame.size.width;
    CGFloat offsetY = self.contentScrollView.contentOffset.y;
    CGPoint offset = CGPointMake(offsetX, offsetY);
    [self.contentScrollView setContentOffset:offset animated:YES];
}


/**获取当前控制器 */
- (UIViewController *)viewController {
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到它
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    id nextResponder = nil;
    UIViewController *appRootVC = window.rootViewController;
    //1、通过present弹出VC，appRootVC.presentedViewController不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        //2、通过navigationcontroller弹出VC
        NSLog(@"subviews == %@",[window subviews]);
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    //1、tabBarController
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //或者 UINavigationController * nav = tabbar.selectedViewController;
        result = nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        //2、navigationController
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{//3、viewControler
        result = nextResponder;
    }
    return result;
}



#pragma mark - ******************** scrollView代理方法

/** 滚动结束后调用（代码导致） */
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
  
    // 获得索引
    NSUInteger index = scrollView.contentOffset.x / self.contentScrollView.frame.size.width;
    
    // 滚动标题栏
    UILabel *titleLable = (UILabel *)self.titleScrollView.subviews[index];
    
    CGFloat offsetx = titleLable.center.x - self.titleScrollView.frame.size.width * 0.5;
    
    CGFloat offsetMax = self.titleScrollView.contentSize.width - self.titleScrollView.frame.size.width;
    if (offsetx < 0) {
        offsetx = 0;
    }else if (offsetx > offsetMax){
        offsetx = offsetMax;
    }
    
    CGPoint offset = CGPointMake(offsetx, self.titleScrollView.contentOffset.y);
    if (self.totalwidth > kSCREENWIDTH) {
         [self.titleScrollView setContentOffset:offset animated:YES];
    }
   
    // 添加控制器
    UIViewController *newsVc;
    
    //newsVc = self.childViewControllers[index];
    newsVc = [self viewController].childViewControllers[index];
    //      ContentCollectionViewController *newsVc = self.childViewControllers[index];
    //    ContentTableViewController *newsVc = self.childViewControllers[index];
    //    newsVc.index = index;
    
    [self.titleScrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx != index) {
            UILabel *temlabel = self.titleScrollView.subviews[idx];
            temlabel.textColor = [UIColor whiteColor];
            //            temlabel.scale = 0.0;
        }
    }];
    
    if (newsVc.view.superview) return;
    newsVc.view.frame = scrollView.bounds;
    [self.contentScrollView addSubview:newsVc.view];
}

/** 滚动结束（手势导致） */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

/** 正在滚动 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 取出绝对值 避免最左边往右拉时形变超过1
    CGFloat value = ABS(scrollView.contentOffset.x / scrollView.frame.size.width);
    NSUInteger leftIndex = (int)value;
    NSUInteger rightIndex = leftIndex + 1;
    UILabel *labelLeft = self.titleScrollView.subviews[leftIndex];
    labelLeft.textColor = [UIColor whiteColor];

    [labelLeft addSubview:self.pointImage];
    [self.pointImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(labelLeft.mas_bottom);
        make.centerX.equalTo(labelLeft.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(15, 10));
        
    }];
    
    // 考虑到最后一个板块，如果右边已经没有板块了 就不在下面赋值scale了
    if (rightIndex < self.titleScrollView.subviews.count) {
        UILabel *labelRight = self.titleScrollView.subviews[rightIndex];
        labelRight.textColor = [UIColor whiteColor];
    }
    
}

/** 计算标题宽度 */
- (CGFloat)calculateRowWidth:(NSString *)string {
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};  //指定字号
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 50)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width +20;
}



@end
