//
//  ContributionViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/7.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ContributionViewCell.h"
@interface ContributionViewCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *dianLabel;
@end
@implementation ContributionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _rankLabel = [UILabel new];
    _rankLabel.textColor = HEXCOLOR(0x999999);
    _rankLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:17];
    [self addSubview:_rankLabel];
    
    _headImage = [[UIImageView alloc]init];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:25];
    [_headImage.layer setBorderColor:[UIColor orangeColor].CGColor];
    [_headImage.layer setBorderWidth:1.0];
    [self addSubview:_headImage];
    
    _guanImage = [[UIImageView alloc]init];
    _guanImage.image = [UIImage imageNamed:@"rank_yi"];
    _guanImage.hidden = YES;
    [self addSubview:_guanImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self addSubview:_nameLabel];
    
    
    _dianLabel = [UILabel new];
    _dianLabel.textColor = [UIColor redColor];
    _dianLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_dianLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.rankLabel.mas_right)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [_guanImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.headImage.mas_right)setOffset:8];
        [make.top.mas_equalTo(weakSelf.headImage.mas_top)setOffset:0];
      
       
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
         make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
    
    [_dianLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.mas_right)setOffset:-20];
         make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
}

-(void)setModel:(ContributionListModel *)model{
    _model = model;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.profile]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nameLabel.text = model.name;
    _dianLabel.text = model.money;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
