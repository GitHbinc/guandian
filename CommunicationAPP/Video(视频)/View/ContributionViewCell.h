//
//  ContributionViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/7.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContributionListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ContributionViewCell : UITableViewCell
@property(nonatomic ,strong)UIImageView *guanImage;
@property(nonatomic ,strong)UILabel *rankLabel;
@property(nonatomic ,strong)ContributionListModel *model;
@end

NS_ASSUME_NONNULL_END
