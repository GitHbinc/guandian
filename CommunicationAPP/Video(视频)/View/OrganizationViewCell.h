//
//  OrganizationViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ButtonBlock)(UIButton *button);
NS_ASSUME_NONNULL_BEGIN

@interface OrganizationViewCell : UITableViewCell

@property(nonatomic,copy)ButtonBlock bottomBlock;

@property (nonatomic,copy)UIImageView *moreImageView;

-(void)refreshCellModel;
@end

NS_ASSUME_NONNULL_END
