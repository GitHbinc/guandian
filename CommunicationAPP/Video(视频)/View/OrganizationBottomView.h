//
//  OrganizationBottomView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ButtonBlock)(UIButton *button);

NS_ASSUME_NONNULL_BEGIN

@interface OrganizationBottomView : UIView
@property(nonatomic,copy)ButtonBlock block;
-(void)refreshModel;

@end

NS_ASSUME_NONNULL_END
