//
//  OrganizationBottomView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "OrganizationBottomView.h"
@interface OrganizationBottomView(){
    
    //赞、赏图片
    UIImageView *_leftImageView;
    //赞、赏数量
    UILabel *_leftLabel;
    //点赞评分
    UILabel *_fabulousLabel;
    //评论数量
    UILabel *_commentLabel;
    //转发数量
    UILabel *_forwardLabel;
    //机构名称
    UILabel * _backLabel;
    //头像
    UIButton * _headBtn;
    //关注
    UIButton * _attentBtn;
    
}

@end
@implementation OrganizationBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIButton *moreBtn = [[UIButton alloc]init];
    [moreBtn setImage:[UIImage imageNamed:@"bottom_more_normal_icon"] forState:UIControlStateNormal];
    moreBtn.tag = 104;
    [moreBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(0);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _forwardLabel = [[UILabel alloc]init];
    _forwardLabel.textColor = color666666;
    _forwardLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_forwardLabel];
    [_forwardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(moreBtn.mas_left).offset(-8);
        make.centerY.equalTo(self.mas_centerY);
    }];
    UIButton *forwardBtn = [[UIButton alloc]init];
        [forwardBtn setImage:[UIImage imageNamed:@"zhuanfa_normal_icon"] forState:UIControlStateNormal];
        forwardBtn.tag = 103;
        [forwardBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:forwardBtn];
        [forwardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self->_forwardLabel.mas_left).offset(-8);
            make.centerY.equalTo(self.mas_centerY);
        }];
    
    
    _commentLabel = [[UILabel alloc]init];
        _commentLabel.textColor = color666666;
        _commentLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:_commentLabel];
        [_commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(forwardBtn.mas_left).offset(-10);
            make.centerY.equalTo(self.mas_centerY);
        }];
    UIButton *commentBtn = [[UIButton alloc]init];
        [commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
        commentBtn.tag = 102;
        [commentBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:commentBtn];
        [commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self->_commentLabel.mas_left).offset(-8);
            make.centerY.equalTo(self.mas_centerY);
        }];
    
    
    
    _fabulousLabel = [[UILabel alloc]init];
    _fabulousLabel.textColor = color666666;
    _fabulousLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_fabulousLabel];
    [_fabulousLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(commentBtn.mas_left).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
    }];
    UIButton *fabulousBtn = [[UIButton alloc]init];
        [fabulousBtn setImage:[UIImage imageNamed:@"dianzan_normal_icon"] forState:UIControlStateNormal];
        fabulousBtn.tag = 101;
        [fabulousBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:fabulousBtn];
        [fabulousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self->_fabulousLabel.mas_left).offset(-8);
            make.centerY.equalTo(self.mas_centerY);
        }];
    
    
    
    _leftImageView = [[UIImageView alloc]init];
    [self addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(fabulousBtn.mas_left).offset(-50);
        make.centerY.equalTo(self.mas_centerY);
    }];
    _leftLabel = [[UILabel alloc]init];
    _leftLabel.textColor = colorF46C02;
    _leftLabel.font = [UIFont systemFontOfSize:9];
    [_leftImageView addSubview:_leftLabel];
    [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_left).offset(25);
        make.centerY.equalTo(self->_leftImageView.mas_centerY);
    }];


    _backLabel = [UILabel new];
    _backLabel.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    _backLabel.textAlignment = NSTextAlignmentCenter;
    _backLabel.clipsToBounds = YES;
    _backLabel.font = [UIFont systemFontOfSize:10];
    [_backLabel.layer setCornerRadius:12];
    [self addSubview:_backLabel];
    [_backLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.equalTo(self).offset(10);
       make.size.mas_equalTo(CGSizeMake(100, 24));
       make.centerY.equalTo(self.mas_centerY);
    }];
    
    _headBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _headBtn.clipsToBounds = YES;
//    [_headBtn.layer setCornerRadius:12];
    _headBtn.tag = 105;
    [_headBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_headBtn];
    [_headBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_backLabel.mas_left).offset(0);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
    _attentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _attentBtn.clipsToBounds = YES;
//    [_attentBtn.layer setCornerRadius:12];
    _attentBtn.tag = 106;
    [_attentBtn setImage: [UIImage imageNamed:@"rank_add"] forState:UIControlStateNormal];
    [_attentBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_attentBtn];
    [_attentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_backLabel.mas_right).offset(0);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
    
    
    
}

-(void)refreshModel{
    
    _leftImageView.image = [UIImage imageNamed:@"shang_icon"];
    [_headBtn setImage: [UIImage imageNamed:@"shang_icon"] forState:UIControlStateNormal];
    _leftLabel.text = @"1626";
    _fabulousLabel.text = @"9.6";
    _commentLabel.text = @"50";
    _forwardLabel.text = @"26";
    _backLabel.text = @"兄弟连影视";
}

-(void)bottomViewBtnClick:(UIButton *)senderBtn{
    
    senderBtn.selected = !senderBtn.selected;
    
    if (self.block) {
        self.block(senderBtn);
    }
    
    switch (senderBtn.tag) {
        case 101:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"dianzan_normal_icon" imageSelStr:@"dianzan_selected_icon"];
            
        }
            break;
        case 102:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"message_normal_icon" imageSelStr:@"message_selected_icon"];
            
        }
            break;
        case 103:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"zhuanfa_normal_icon" imageSelStr:@"zhuanfa_selected_icon"];
            
        }
            break;
        case 104:
        {
            [self changeBtnImage:senderBtn imageNormalStr:@"bottom_more_normal_icon" imageSelStr:@"bottom_more_selected_icon"];
            
        }
            break;
        default:
            break;
    }
    //    NSLog(@"======%ld",senderBtn.tag);
}

-(void)changeBtnImage:(UIButton *)senderBtn imageNormalStr:(NSString *)imageNormalStr imageSelStr:(NSString *)imageSelStr{
    if (senderBtn.selected) {
        [senderBtn setImage:[UIImage imageNamed:imageSelStr] forState:UIControlStateNormal];
    }else{
        [senderBtn setImage:[UIImage imageNamed:imageNormalStr] forState:UIControlStateNormal];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
