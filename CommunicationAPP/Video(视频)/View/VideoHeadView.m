//
//  VideoHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VideoHeadView.h"
#import "ContentBottomView.h"
@interface VideoHeadView()
@property(nonatomic ,strong)UILabel *title;
@property(nonatomic ,strong)UILabel *time;
@property(nonatomic ,strong)UILabel *amount;
@property(nonatomic ,strong)ContentBottomView *bottomView;
@property(nonatomic ,strong)UIView *inforView;
@property(nonatomic ,strong)UIView *lineView;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UIButton *attentionBtn;


@end

@implementation VideoHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    
        [self setUpUI];
        
    }
    
    return self;
}

-(void)setUpUI{
    
    _title = [UILabel new];
    _title.font = [UIFont systemFontOfSize:12];
    [self addSubview:_title];
    
    _time = [UILabel new];
    _time.textColor = HEXCOLOR(0x999999);
    _time.font = [UIFont systemFontOfSize:9];
    [self addSubview:_time];
    
    _amount = [UILabel new];
    //_amount.text = @"观看：83.29万";
    _amount.textColor = HEXCOLOR(0x999999);
    _amount.font = [UIFont systemFontOfSize:9];
    [self addSubview:_amount];
    
    _bottomView = [[ContentBottomView alloc]init];
    _bottomView.backgroundColor = UIColor.whiteColor;
    
    __weak typeof(self) weakSelf = self;
    _bottomView.block = ^(UIButton *senderBtn){
        if (weakSelf.bottomBlock) {
            weakSelf.bottomBlock(senderBtn);
        }
        
    };
    [self addSubview:_bottomView];
    
    _inforView = [UIView new];
    _inforView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_inforView];
    
    _lineView = [UIView new];
    _lineView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
    [_inforView addSubview:_lineView];
    
    
    
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:15];
    [_inforView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.font =  [UIFont systemFontOfSize:12];
    [_inforView addSubview:_nameLabel];
    
    _attentionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_attentionBtn setBackgroundImage:[UIImage imageNamed:@"rank_attention"] forState:UIControlStateNormal];
    [_attentionBtn setBackgroundImage:[UIImage imageNamed:@"rank_attentioned"] forState:UIControlStateSelected];
    [_attentionBtn addTarget:self action:@selector(clickAttention:) forControlEvents:UIControlEventTouchUpInside];
    [_inforView addSubview:_attentionBtn];
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(10);
        make.left.equalTo(self).offset(10);
        
    }];
    
    [_time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.title.mas_bottom).offset(8);
        make.left.equalTo(weakSelf.title).offset(0);
        
    }];
    
    [_amount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.title.mas_bottom).offset(8);
        make.left.equalTo(weakSelf.time.mas_right).offset(10);
        
    }];
    
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(weakSelf.amount.mas_bottom).offset(5);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(@30);
    }];
    //[_bottomView refreshModel];
    
    [_inforView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(weakSelf.bottomView.mas_bottom).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.inforView.mas_left).offset(0);
        make.top.equalTo(weakSelf.inforView.mas_top).offset(0);
        make.right.equalTo(weakSelf.inforView.mas_right).offset(0);
        make.height.equalTo(@1);
    }];
    
    
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.inforView.mas_left)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.inforView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
         make.centerY.mas_equalTo(weakSelf.inforView.mas_centerY);
    }];
    
    [_attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.inforView.mas_right)setOffset:-20];
        make.centerY.mas_equalTo(weakSelf.inforView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 25));
    }];
    
}

-(void)setDetailModel:(RankVideoDetailModel *)DetailModel{
    _DetailModel = DetailModel;
    _title.text =DetailModel.title;
    _amount.text = [NSString stringWithFormat:@"观看：2443555"];
    _time.text = [NSString stringWithFormat:@"发布时间：%@",DetailModel.create_time];
    _bottomView.model = DetailModel;
    if ([DetailModel.is_follow isEqualToString:@"1"]) {
        _attentionBtn.selected = YES;
    }else{
        _attentionBtn.selected = NO;
    }
    
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,_model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nameLabel.text = _model.username;

}

-(void)setModel:(RankVideoModel *)model{
    _model = model;
}

-(void)setDymodel:(DynamicListModel *)dymodel{
    _dymodel = dymodel;
    _title.text =dymodel.title;
    _amount.text = [NSString stringWithFormat:@"观看：%@",dymodel.click];
    _time.text = [NSString stringWithFormat:@"发布时间：%@",dymodel.create_time];
    _bottomView.dynamicmodel = dymodel;
//    if ([dymodel.is_follow isEqualToString:@"1"]) {
//        _attentionBtn.selected = YES;
//    }else{
//        _attentionBtn.selected = NO;
//    }
    
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,dymodel.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nameLabel.text = dymodel.username;
    
}

-(void)clickAttention:(UIButton *)sender{
    sender.selected = !sender.selected;
}

@end
