//
//  PopularViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopularViewCell : UITableViewCell
@property (nonatomic ,strong)UILabel *rankLabel;
@property (nonatomic ,strong)RankVideoModel *model;
@end

NS_ASSUME_NONNULL_END
