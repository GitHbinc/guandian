//
//  OrganizationViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "OrganizationViewCell.h"
#import "OrganizationBottomView.h"

@interface OrganizationViewCell(){
    //视频头像
    UIImageView *_headerImageView;
    //视频名称
    UILabel *_titleLabel;
    //视频时间
    UILabel *_timeLabel;
    //右边标识log
    UIImageView *_rightImageView;
//    //广告btn
//    UIButton *_adBtn;
    //时长
    UILabel * _durationLabel;
    //内容
    UILabel * _contentLabel;
    UIImageView *_mainImageView;
    OrganizationBottomView *_bottomView;
}
@end
@implementation OrganizationViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _mainImageView = [[UIImageView alloc]init];
    _mainImageView.image = [UIImage imageNamed:@"test_icon"];
    [self.contentView addSubview:_mainImageView];
    [_mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-40);
    }];
    
    UIImageView *headerBgImageView = [[UIImageView alloc]init];
    headerBgImageView.image = [UIImage imageNamed:@"video_bg_icon"];
    [_mainImageView addSubview:headerBgImageView];
    [headerBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_mainImageView.mas_left).offset(0);
        make.top.equalTo(self->_mainImageView.mas_top).offset(0);
        make.right.equalTo(self->_mainImageView.mas_right).offset(0);
        make.height.equalTo(@60);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.layer.cornerRadius = 15.0;
    [_mainImageView addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_mainImageView.mas_left).offset(10);
        make.top.equalTo(self->_mainImageView.mas_top).offset(10);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = UIColor.whiteColor;
    _titleLabel.font = [UIFont systemFontOfSize:12];
    [_mainImageView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_right).offset(5);
        make.top.equalTo(self->_mainImageView.mas_top).offset(14);
        make.right.equalTo(self->_mainImageView.mas_right).offset(-60);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = UIColor.whiteColor;
    _timeLabel.font = [UIFont systemFontOfSize:9];
    [_mainImageView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.right.equalTo(self->_titleLabel.mas_right).offset(0);
    }];
    
    _rightImageView = [[UIImageView alloc]init];
    _rightImageView.layer.cornerRadius = 13.0;
    [_mainImageView addSubview:_rightImageView];
    [_rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_mainImageView.mas_right).offset(-15);
        make.top.equalTo(self->_mainImageView.mas_top).offset(15);
        make.width.equalTo(@26);
        make.height.equalTo(@26);
    }];
    
    _durationLabel = [UILabel new];
    _durationLabel.textColor = UIColor.whiteColor;
    _durationLabel.font = [UIFont systemFontOfSize:10];
    [_mainImageView addSubview:_durationLabel];
    [_durationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_mainImageView.mas_right).offset(-15);
        make.bottom.equalTo(self->_mainImageView.mas_bottom).offset(-10);
        make.height.equalTo(@30);
            }];
    
    _contentLabel = [UILabel new];
    _contentLabel.textColor = UIColor.whiteColor;
    _contentLabel.font = [UIFont systemFontOfSize:12];
    [_mainImageView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_left).offset(0);
        make.centerY.equalTo(self->_durationLabel.mas_centerY).offset(0);
        
    }];

    
    _bottomView = [[OrganizationBottomView alloc]init];
    _bottomView.backgroundColor = UIColor.whiteColor;
    
    __weak typeof(self) weakSelf = self;
    _bottomView.block = ^(UIButton *senderBtn){
        //        NSLog(@"======%ld",senderBtn.tag);
        if (weakSelf.bottomBlock) {
            weakSelf.bottomBlock(senderBtn);
        }
        
    };
    [self.contentView addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self->_mainImageView.mas_bottom).offset(5);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    
    _moreImageView = [[UIImageView alloc]init];
    _moreImageView.image = [UIImage imageNamed:@"more_bg_icon"];
    _moreImageView.hidden = true;
    [self.contentView addSubview:_moreImageView];
    [_moreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-3);
        make.bottom.equalTo(self->_bottomView.mas_bottom).offset(-15);
        make.height.equalTo(@236);
        make.width.equalTo(@114);
    }];
    
    NSArray *imageArray = @[@"share_icon",@"concern_normal_icon",@"collection_normal_icon",@"ranking_icon",@"jingpai_icon",@"jubao_icon",@"lahei_normal_icon",@"big_icon"];
    NSArray *titleArray = @[@"分享",@"关注",@"收藏",@"排行",@"竞拍",@"举报",@"拉黑",@"最大化"];
    
    [self setMoreUI:imageArray titleArray:titleArray];
}

-(void)setMoreUI:(NSArray *)imageArray titleArray:(NSArray *)titleArray{
    
    for (int i = 0; i < titleArray.count; i++) {
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = UIColor.clearColor;
        [_moreImageView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->_moreImageView.mas_left).offset(0);
            make.right.equalTo(self->_moreImageView.mas_right).offset(0);
            make.top.equalTo(self->_moreImageView.mas_top).offset(28 * i);
            make.height.equalTo(@28);
        }];
        
        UIButton *btn = [[UIButton alloc]init];
        [btn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        btn.tag = 105 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view.mas_left).offset(30);
            make.centerY.equalTo(view.mas_centerY);
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.text = titleArray[i];
        label.textColor = color333333;
        label.font = [UIFont systemFontOfSize:13];
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(btn.mas_right).offset(5);
            make.centerY.equalTo(view.mas_centerY);
        }];
        
        if (i != titleArray.count - 1) {
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = colorCCCCCC;
            [view addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(view.mas_left).offset(2);
                make.right.equalTo(view.mas_right).offset(-2);
                make.bottom.equalTo(view.mas_bottom).offset(-1);
                make.height.equalTo(@1);
            }];
        }
    }
}

-(void)refreshCellModel{
    
    _headerImageView.image = [UIImage imageNamed:@"zhuanfa_logo_icon"];
    _titleLabel.text = @"兄弟连影视";
    _timeLabel.text = @"2018-11-12 15:32";
    _durationLabel.text = @"5:32";
    _contentLabel.text = @"吴彦祖新电影发布会现场，快来看吧...";
    _rightImageView.image = [UIImage imageNamed:@"yuanchuang_icon"];
    [_bottomView refreshModel];
    
}

-(void)adBtnClick{
    
}

-(void)btnClick:(UIButton *)senderBtn{
    
    if (self.bottomBlock) {
        self.bottomBlock(senderBtn);
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
