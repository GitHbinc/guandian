//
//  RankClassfyView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RankClassfyView : UIView
@property (nonatomic ,strong)NSMutableArray *sortArray;
@property (nonatomic, copy) void(^rankButtonClick)(UIButton * sender);

@end

NS_ASSUME_NONNULL_END
