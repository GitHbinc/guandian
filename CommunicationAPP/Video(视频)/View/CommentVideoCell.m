//
//  CommentVideoCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "CommentVideoCell.h"
@interface CommentVideoCell()
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *contentLabel;
@property (nonatomic ,strong)UILabel *amountLabel;
@property (nonatomic ,strong)UIButton *fabulousBtn;

@end
@implementation CommentVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI{
   
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:18];
    [self.contentView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"吴彦祖";
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_nameLabel];
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"2018-12-05 11:04";
    _dateLabel.textColor = HEXCOLOR(0xB2B2B2);
    _dateLabel.font =  [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_dateLabel];
    
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚,哇，非常流畅，而且看的清清楚楚";
    _contentLabel.textColor =HEXCOLOR(0x555555);
    _contentLabel.font = [UIFont systemFontOfSize:12];
    _contentLabel.numberOfLines = 0;
    _contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:_contentLabel];
    
    
    _amountLabel = [UILabel new];
    _amountLabel.text = @"20";
    _amountLabel.textColor = HEXCOLOR(0xB2B2B2);
    _amountLabel.font =  [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_amountLabel];
    
    
    _fabulousBtn = [[UIButton alloc]init];
    [_fabulousBtn setImage:[UIImage imageNamed:@"dianzan_normal_icon"] forState:UIControlStateNormal];
     [_fabulousBtn setImage:[UIImage imageNamed:@"dianzan_selected_icon"] forState:UIControlStateSelected];
    
    [_fabulousBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_fabulousBtn];
  
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.contentView.mas_left)setOffset:10];
        [make.top.mas_equalTo(weakSelf.contentView.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(36, 36));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(weakSelf.headImage.mas_centerY)setOffset:-5];
    }];
    
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom)setOffset:2];
    }];
    
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.dateLabel.mas_left)setOffset:0];
        [make.top.mas_equalTo(weakSelf.dateLabel.mas_bottom)setOffset:10];
         make.right.mas_equalTo(weakSelf.contentView.mas_right).offset(-50);
         make.bottom.mas_equalTo(weakSelf.contentView.mas_bottom).offset(-10);
    }];
    
    
    [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.contentView.mas_right)setOffset:-10];
         make.centerY.equalTo(weakSelf.headImage.mas_centerY);
    }];
    
    [_fabulousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.amountLabel.mas_left)setOffset:-5];
        make.centerY.equalTo(weakSelf.headImage.mas_centerY);
    }];
    
   
}

-(void)bottomViewBtnClick:(UIButton *)senderBtn{
    senderBtn.selected = !senderBtn.selected;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
