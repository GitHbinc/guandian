//
//  PopularViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PopularViewCell.h"
@interface PopularViewCell()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nickLabel;
@property (nonatomic ,strong)UILabel *followLabel;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *commentLabel;
@property (nonatomic ,strong)UILabel *playLabel;


@end

@implementation PopularViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _mainImage = [[UIImageView alloc]init];
    [_mainImage.layer setMasksToBounds:YES];
    [_mainImage.layer setCornerRadius:5.0];
    [self addSubview:_mainImage];
    
    _titleLabel = [UILabel new];
    _titleLabel.font =  [UIFont systemFontOfSize:14];
    [self addSubview:_titleLabel];
    
    _rankLabel = [UILabel new];
    _rankLabel.textColor = [UIColor redColor];
    _rankLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [self addSubview:_rankLabel];
    
    _commentLabel = [UILabel new];
    _commentLabel.textColor = HEXCOLOR(0x999999);
    _commentLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_commentLabel];
    
    _playLabel = [UILabel new];
    _playLabel.textColor = HEXCOLOR(0x999999);
    _playLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_playLabel];
    
    _headImage = [[UIImageView alloc]init];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:16];
    [self addSubview:_headImage];
    
    _nickLabel = [UILabel new];
    _nickLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_nickLabel];
    
    _followLabel = [UILabel new];
    _followLabel.textColor = HEXCOLOR(0x999999);
    _followLabel.font = [UIFont systemFontOfSize:10];
    [self addSubview:_followLabel];
    
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
     __weak __typeof(self)weakSelf = self;
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        [make.top.mas_equalTo(self)setOffset:10];
         make.size.mas_equalTo(CGSizeMake(130, 80));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.mainImage.mas_right)setOffset:5];
        [make.top.mas_equalTo(weakSelf.mainImage.mas_top)setOffset:5];
    }];
    
    [_rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.mas_right)setOffset:-10];
        make.centerY.mas_equalTo(weakSelf.titleLabel.mas_centerY);
    }];
    
    
    [_playLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.mainImage.mas_right)setOffset:5];
        [make.bottom.mas_equalTo(weakSelf.mainImage.mas_bottom)setOffset:-5];
    }];
    
    [_commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.mainImage.mas_right)setOffset:5];
        [make.bottom.mas_equalTo(weakSelf.playLabel.mas_top)setOffset:-5];
    }];
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.mainImage);
        [make.top.mas_equalTo(weakSelf.mainImage.mas_bottom)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
    
    [_nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:5];
        [make.top.mas_equalTo(weakSelf.headImage.mas_top)setOffset:0];
    }];
    
    [_followLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:5];
        [make.bottom.mas_equalTo(weakSelf.headImage.mas_bottom)setOffset:0];
    }];
    
}

-(void)setModel:(RankVideoModel *)model{
    _model = model;
    [_mainImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
    _titleLabel.text = model.title;
    _commentLabel.text = [NSString stringWithFormat:@"评论：%@",model.comment];
    _playLabel.text = [NSString stringWithFormat:@"总播放量：%@",model.click];
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nickLabel.text = model.username;
    _followLabel.text =  [NSString stringWithFormat:@"%@人关注",model.fans];
    int R = (arc4random() % 256) ;
    int G = (arc4random() % 256) ;
    int B = (arc4random() % 256) ;
    _rankLabel.textColor = [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
