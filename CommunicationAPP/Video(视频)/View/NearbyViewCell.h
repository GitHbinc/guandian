//
//  NearbyViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NearbyViewCell : UICollectionViewCell
@property(nonatomic ,strong)RankVideoModel *model;
@property(nonatomic ,assign)BOOL isNearby;
@end

NS_ASSUME_NONNULL_END
