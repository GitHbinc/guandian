//
//  RedsRankCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RedsRankCell.h"
@interface RedsRankCell()
@property (nonatomic ,strong)UIButton *headBtn;
@property (nonatomic ,strong)UILabel *rankLabel;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *dianLabel;
@property (nonatomic ,strong)UIButton *attentionBtn;


@end
@implementation RedsRankCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _rankLabel = [UILabel new];
    _rankLabel.text = @"1";
    _rankLabel.textColor = [UIColor redColor];
    _rankLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [self addSubview:_rankLabel];
    
    _headBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_headBtn setImage:[UIImage imageNamed:@"test_icon"] forState:UIControlStateNormal];
    [_headBtn.layer setMasksToBounds:YES];
    [_headBtn.layer setCornerRadius:25];
    _headBtn.tag = 1001;
     [_headBtn addTarget:self action:@selector(clickAttention:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_headBtn];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"吴彦祖";
    _nameLabel.font =  [UIFont systemFontOfSize:14];
    [self addSubview:_nameLabel];
    
    
    _dianLabel = [UILabel new];
    _dianLabel.text = @"收到点：456354万";
    _dianLabel.textColor = HEXCOLOR(0x999999);
    _dianLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:_dianLabel];
    
    _attentionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_attentionBtn setBackgroundImage:[UIImage imageNamed:@"rank_attention"] forState:UIControlStateNormal];
    [_attentionBtn setBackgroundImage:[UIImage imageNamed:@"rank_attentioned"] forState:UIControlStateSelected];
    _attentionBtn.tag = 1002;
    [_attentionBtn addTarget:self action:@selector(clickAttention:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_attentionBtn];
    
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    [_rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
    }];
    
    
    [_headBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.rankLabel.mas_right)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headBtn.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.headBtn.mas_top)setOffset:5];
    }];
    
   
    [_dianLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headBtn.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(weakSelf.headBtn.mas_bottom)setOffset:-5];
    }];
    
   
    [_attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.mas_right)setOffset:-20];
         make.centerY.mas_equalTo(weakSelf.mas_centerY);
         make.size.mas_equalTo(CGSizeMake(50, 24));
    }];
    
}

-(void)clickAttention:(UIButton *)sender{
    if (_rankbuttonClick) {
        _rankbuttonClick(sender);
    }
    switch (sender.tag) {
        case 1001:
        {
            
        }
            break;
            
        case 1002:
        {
            sender.selected = !sender.selected;
        }
            break;
            
        default:
            break;
    }
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
