//
//  RecommendVideoCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RecommendVideoCell.h"
@interface RecommendVideoCell()
@property (nonatomic ,strong)UIImageView *videoImage;
@property (nonatomic ,strong)UIImageView *playImage;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *amountLabel;
@property (nonatomic ,strong)UIView *lineView;

@end
@implementation RecommendVideoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    
    _videoImage = [[UIImageView alloc]init];
    _videoImage.image = [UIImage imageNamed:@"test_icon"];
    [_videoImage.layer setMasksToBounds:YES];
    [_videoImage.layer setCornerRadius:5];
    [self addSubview:_videoImage];
    
    _titleLabel = [UILabel new];
    _titleLabel.text = @"[王子城堡]新闻早知道。。。";
    _titleLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_titleLabel];
    
    _nameLabel = [UILabel new];
    _nameLabel.text = @"小白biubiu";
    _nameLabel.textColor = HEXCOLOR(0xB2B2B2);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_nameLabel];
    
    _amountLabel = [UILabel new];
    _amountLabel.text = @"8885";
    _amountLabel.textColor = [UIColor whiteColor];
    _amountLabel.font =  [UIFont systemFontOfSize:10];
    [self addSubview:_amountLabel];
    
    _playImage = [[UIImageView alloc]init];
    _playImage.image = [UIImage imageNamed:@"rank_bofang"];
    [self addSubview:_playImage];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
 
    [_videoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.mas_left)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(110, 70));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.videoImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(weakSelf.videoImage.mas_centerY)setOffset:-10];
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.videoImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.titleLabel.mas_bottom)setOffset:5];
    }];
    
    [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.videoImage.mas_right)setOffset:-8];
        [make.top.mas_equalTo(weakSelf.videoImage.mas_top)setOffset:8];
    }];
    
    [_playImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.amountLabel.mas_left)setOffset:-2];
        [make.centerY.mas_equalTo(weakSelf.amountLabel.mas_centerY)setOffset:0];
    }];
    
   
    
}

-(void)setModel:(RankRecommendModel *)model{
    _model = model;
     [_videoImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
    _nameLabel.text = model.username;
    _titleLabel.text = model.title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
