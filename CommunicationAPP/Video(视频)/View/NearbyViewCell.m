//
//  NearbyViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "NearbyViewCell.h"
@interface NearbyViewCell(){
    
    UIImageView *_bgImageView;
    UIImageView *_leftImageView;
    UIImageView *_locationImageView;
    UILabel *_amountLabel;
    UILabel *_distanceLabel;
    UIButton *_attentBtn;
    UILabel *_contentLabel;
    UILabel *_nameLabel;
    
}

@end

@implementation NearbyViewCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
    }
    
    return self;
}


-(void)setUpUI{
    _bgImageView = [[UIImageView alloc]init];
    _bgImageView.clipsToBounds = YES;
    [_bgImageView.layer setCornerRadius:5.0];
    [self.contentView addSubview:_bgImageView];
    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@110);
    }];
    
    _leftImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    _amountLabel = [[UILabel alloc]init];
    _amountLabel.textColor = colorEF1D1E;
    _amountLabel.font = [UIFont systemFontOfSize:9];
    [_leftImageView addSubview:_amountLabel];
    [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_left).offset(20);
        make.centerY.equalTo(self->_leftImageView.mas_centerY);
    }];
    
    _attentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_attentBtn setBackgroundImage:[UIImage imageNamed:@"rank_guanzhu"] forState:UIControlStateNormal];
    [_attentBtn setBackgroundImage:[UIImage imageNamed:@"rank_yiguanzhu"] forState:UIControlStateSelected];
    [_attentBtn addTarget:self action:@selector(clickAttention:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_attentBtn];
    [_attentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self->_bgImageView.mas_right)setOffset:-10];
        [make.bottom.mas_equalTo(self->_bgImageView.mas_bottom)setOffset:-10];
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    _distanceLabel = [[UILabel alloc]init];
    _distanceLabel.textColor = HEXCOLOR(0xffffff);
    _distanceLabel.font = [UIFont systemFontOfSize:9];
    _distanceLabel.hidden = YES;
    [self addSubview:_distanceLabel];
    [_distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self->_bgImageView.mas_right)setOffset:-10];
        [make.bottom.mas_equalTo(self->_bgImageView.mas_bottom)setOffset:-10];
    }];

    _locationImageView = [[UIImageView alloc]init];
    _locationImageView.image = [UIImage imageNamed:@"sence_location"];
    _locationImageView.hidden = YES;
    [self addSubview:_locationImageView];
    [_locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self->_distanceLabel.mas_left)setOffset:-5];
        [make.bottom.mas_equalTo(self->_bgImageView.mas_bottom)setOffset:-10];
    }];
    
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_bgImageView.mas_left).offset(0);
        make.top.equalTo(self->_bgImageView.mas_bottom).offset(10);
    }];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.font = [UIFont systemFontOfSize:13];
    _nameLabel.textColor =  [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1.0];
    [self addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_bgImageView.mas_left).offset(0);
        make.top.equalTo(self->_contentLabel.mas_bottom).offset(5);
    }];
    
}

-(void)setModel:(RankVideoModel *)model{
    _model = model;

    [_bgImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,_model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
    
    _nameLabel.text = model.username;
    _contentLabel.text = model.title;
    if ([model.zstype isEqualToString:@"1"]) {
        _leftImageView.image = [UIImage imageNamed:@"bottom_zan_icon"];
        _amountLabel.text = model.z_num.length>0?model.z_num:@"0";
    }else{
        _leftImageView.image = [UIImage imageNamed:@"bottom_shang_icon"];
        _amountLabel.text = model.s_num.length >0?model.z_num:@"0";
    }
    
    if (self.isNearby) {
        _attentBtn.hidden = YES;
        _locationImageView.hidden = NO;
        _distanceLabel.hidden = NO;
        _distanceLabel.text = model.distance.length>0?model.distance:@"0km";
    }else{
        _attentBtn.hidden = NO;
        _locationImageView.hidden = YES;
        _distanceLabel.hidden = YES;
        if ([model.is_follow isEqualToString:@"1"]) {
            _attentBtn.selected = YES;
        }else{
            _attentBtn.selected = NO;
        }
    }
}


-(void)clickAttention:(UIButton *)sender{
    sender.selected = !sender.selected;
}
@end
