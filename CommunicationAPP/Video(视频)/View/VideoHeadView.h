//
//  VideoHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankVideoDetailModel.h"
#import "RankVideoModel.h"
#import "DynamicListModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^ButtonBlock)(UIButton *button);
@interface VideoHeadView : UIView
@property(nonatomic,copy)ButtonBlock bottomBlock;
@property(nonatomic ,strong)RankVideoDetailModel *DetailModel;
@property(nonatomic ,strong)RankVideoModel *model;
@property(nonatomic ,strong)DynamicListModel *dymodel;

@end

NS_ASSUME_NONNULL_END
