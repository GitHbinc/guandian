//
//  DynamicCommentModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/16.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DynamicCommentModel : JSONModel
@property (nonatomic, copy) NSString *aid;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *pubdate;
@property (nonatomic, copy) NSString <Optional>*upcount;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString <Optional>*dytype;//0 评论 1转发


@end

NS_ASSUME_NONNULL_END
