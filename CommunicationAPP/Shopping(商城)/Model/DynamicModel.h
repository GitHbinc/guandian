//
//  DynamicModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DynamicModel : NSObject
@property(nonatomic ,assign)CGFloat headHeight;
@property(nonatomic ,assign)CGFloat type;

@end

NS_ASSUME_NONNULL_END
