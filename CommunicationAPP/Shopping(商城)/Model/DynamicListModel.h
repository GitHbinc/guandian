//
//  DynamicListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/11.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DynamicListModel : JSONModel
@property (nonatomic, copy) NSString *click;
@property (nonatomic, copy) NSString *comment_count;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *deleted;
@property (nonatomic, copy) NSString *edit_time;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *iscollect;
@property (nonatomic, copy) NSString *iscomment;
@property (nonatomic, copy) NSString *isscore;
@property (nonatomic, copy) NSString *issent;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *pub_type;
@property (nonatomic, copy) NSString *pubdate;
@property (nonatomic, copy) NSString *re_id;
@property (nonatomic, copy) NSString *report_num;
@property (nonatomic, copy) NSString *s_num;
@property (nonatomic, copy) NSString *sent_count;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *surl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *types;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *video;
@property (nonatomic, copy) NSString *z_num;
@property (nonatomic, copy) NSString *zstype;

@property (nonatomic, copy) NSString<Optional>*content;
@property (nonatomic, copy) NSString<Optional>*re_username;
@property (nonatomic, copy) NSString<Optional>*re_uid;
@property (nonatomic ,copy) NSString<Optional>*headHeight;
@property (nonatomic ,copy) NSString<Optional>*cellHeight;




@end

NS_ASSUME_NONNULL_END
