//
//  DynamicCommentModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/16.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "DynamicCommentModel.h"

@implementation DynamicCommentModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}

@end
