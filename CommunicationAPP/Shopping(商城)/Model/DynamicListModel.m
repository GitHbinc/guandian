//
//  DynamicListModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/11.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "DynamicListModel.h"

@implementation DynamicListModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}

@end
