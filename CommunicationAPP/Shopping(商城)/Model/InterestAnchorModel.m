//
//  InterestAnchorModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/17.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "InterestAnchorModel.h"

@implementation InterestAnchorModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
