//
//  InterestAnchorModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/17.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InterestAnchorModel : JSONModel
@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *profile;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *uname;
@property (nonatomic, copy) NSString *ID;
@end

NS_ASSUME_NONNULL_END
