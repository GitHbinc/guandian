//
//  AttentionDynamicController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AttentionDynamicController.h"
#import "AttentionDynamicViewCell.h"
#import "InterestDynamicCell.h"
#import "DynamicDetailController.h"
#import "VideoDynamicDetailController.h"
#import "DynamicListModel.h"
#import "InterestAnchorModel.h"
#import "CGXPickerView.h"
#import "XHInputView.h"

@interface AttentionDynamicController ()<UITableViewDelegate,UITableViewDataSource,XHInputViewDelagete>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)UIView *shareView;
@property(nonatomic, strong)QMUIPopupMenuView *popupByWindow;
@property(nonatomic, strong)NSMutableArray *anchorArray;
@property(nonatomic, strong)DynamicListModel *model;
@property(nonatomic, strong)NSIndexPath *indexPath;
@end

@implementation AttentionDynamicController

-(NSMutableArray *)anchorArray{
    if (!_anchorArray) {
        _anchorArray = [NSMutableArray array];
    }
    return _anchorArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableView];
    
    [self loadShareView];
    
    [self loadPopView];
    
    __weak typeof(self)weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        [weakSelf getDataFromSever];
        [weakSelf getInterestData];
       
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
         [weakSelf getDataFromSever];
    }];
    
   [self.tableView.mj_header beginRefreshing];
    
}


-(void)getInterestData{
     __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_interest parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [weakSelf.anchorArray addObjectsFromArray:[InterestAnchorModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                
            }
            
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"v_uid":@"",
                            @"type":@"2",
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_typelist parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            }else{
                if ([DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    [MBProgressHUD showError:@"真的没有了~" toView:self.view];
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.dataSource.count<3) {
        return self.dataSource.count;
    }
    return self.dataSource.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.dataSource.count<3){
        static NSString *cellID = @"AttentionDynamicViewCell";
        AttentionDynamicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[AttentionDynamicViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = [self.dataSource objectAtIndex:indexPath.section];
        [cell layoutIfNeeded];
        cell.ButtonBlock = ^(UIButton * _Nonnull button, DynamicListModel * _Nonnull model) {
            [self cellBtnClick:button withModel:model indexPath:indexPath];
        };
        
        return cell;
    }else{
        
        if (indexPath.section == 2) {
            static NSString *cellID = @"InterestDynamicCell";
            InterestDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (cell == nil) {
                cell = [[InterestDynamicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.array = self.anchorArray;
            [cell layoutIfNeeded];
            return cell;
        }
        
        static NSString *cellID = @"AttentionDynamicViewCell";
        AttentionDynamicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[AttentionDynamicViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.dataSource.count >2) {
            if (indexPath.section == 0 || indexPath.section == 1) {
                cell.model = [self.dataSource objectAtIndex:indexPath.section];
            }else{
                cell.model = [self.dataSource objectAtIndex:indexPath.section-1];
            }
            
        }
        
        [cell layoutIfNeeded];
        cell.ButtonBlock = ^(UIButton * _Nonnull button, DynamicListModel * _Nonnull model) {
            [self cellBtnClick:button withModel:model indexPath:indexPath];
        };
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.dataSource.count <3) {
        DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section];
        [self pushNextController:model];
        
    }else{
        if (indexPath.section == 0 || indexPath.section ==1) {
            DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section];
            [self pushNextController:model];
        }else{
            DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section -1];
            [self pushNextController:model];
        }
    }
}

-(void)pushNextController:(DynamicListModel *)model{
    if ([model.pub_type isEqualToString:@"1"]) {//短视频
        VideoDynamicDetailController *detail = [VideoDynamicDetailController new];
        detail.model = model;
        detail.URLString = [NSString stringWithFormat:@"%@%@",kWEBURL,model.video];
        detail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detail animated:YES];
        
    }else{//图片
        DynamicDetailController *detail = [DynamicDetailController new];
        detail.model = model;
        detail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detail animated:YES];
    }
}


-(void)cellBtnClick:(UIButton *)senderBtn withModel:(DynamicListModel *)model indexPath:(NSIndexPath *)indexPath{
    self.model = model;
    self.indexPath = indexPath;
    switch (senderBtn.tag) {
        case 101://点赞
        {
            
        }
            break;
        case 102://评论
        {
           [self showXHInputViewWithStyle:InputViewStyleLarge];
            
            
        }
            break;
        case 103://转发
        {
            NSDictionary *param = @{
                                    @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                    @"aid":model.ID?:@""
                                    };
            
            [[WYNetworking sharedWYNetworking] POST:dynamic_forwardUpdatings parameters:param success:^(id  _Nonnull responseObject) {
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
                if (self.dataSource.count >2) {
                    if (indexPath.section == 0 || indexPath.section == 1) {
                       DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section];
                          model.sent_count=[NSString stringWithFormat:@"%ld",[model.sent_count integerValue]+1];
                    }else{
                       DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section-1];
                          model.sent_count=[NSString stringWithFormat:@"%ld",[model.sent_count integerValue]+1];
                    }
                    
                }else{
                     DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section];
                      model.sent_count=[NSString stringWithFormat:@"%ld",[model.sent_count integerValue]+1];
                }
                
              
              [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
               

            } failure:^(NSError * _Nonnull error) {
                
            }];
        }
            break;
            
        case 104://更多
            
           
        {
            [self.popupByWindow layoutWithTargetView:senderBtn];
            [self.popupByWindow showWithAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

-(void)showXHInputViewWithStyle:(InputViewStyle)style{
    
    [XHInputView showWithStyle:style configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            
            NSDictionary *param = @{
                                    @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                    @"content" :text,
                                    @"upd_id":self.model.ID?:@""
                                    };
            
            [[WYNetworking sharedWYNetworking] POST:dynamic_commentAdd parameters:param success:^(id  _Nonnull responseObject) {
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                if (self.dataSource.count >2) {
                    if (self.indexPath.section == 0 || self.indexPath.section == 1) {
                        DynamicListModel *model = [self.dataSource objectAtIndex:self.indexPath.section];
                        model.comment_count=[NSString stringWithFormat:@"%ld",[model.comment_count integerValue]+1];
                    }else{
                        DynamicListModel *model = [self.dataSource objectAtIndex:self.indexPath.section-1];
                        model.comment_count=[NSString stringWithFormat:@"%ld",[model.comment_count integerValue]+1];
                    }
                    
                }else{
                    DynamicListModel *model = [self.dataSource objectAtIndex:self.indexPath.section];
                    model.comment_count=[NSString stringWithFormat:@"%ld",[model.comment_count integerValue]+1];
                }
                
                [self.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
               } failure:^(NSError * _Nonnull error) {
                
            }];
            
            NSLog(@"输入的信息为:%@",text);
           
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
    
}

#pragma mark - XHInputViewDelagete
/** XHInputView 将要显示 */
-(void)xhInputViewWillShow:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要显示时将其关闭 */

    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].enable = NO;
    
}

/** XHInputView 将要影藏 */
-(void)xhInputViewWillHide:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要影藏时将其打开 */
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].enable = YES;
}

//拉黑
-(void)pullblack{
    NSDictionary *param = @{
                            @"b_uid":self.model.uid?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_pullblack parameters:param success:^(id  _Nonnull responseObject) {
       
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                [self getDataFromSever];
                [self.tableView reloadData];
            }
         
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

//收藏
-(void)collectAction{
    NSDictionary *param = @{
                            @"upd_id":self.model.ID?:@"",
                            @"type":@"1",//1 收藏 2移除
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_collect parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
               
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


//举报
-(void)ReportAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请选择你举报该账号的原因" preferredStyle:UIAlertControllerStyleActionSheet];
    static NSString *title = @"";
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
      
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"政治敏感" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
         title = @"1";
        [self replort:title];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"色情低俗" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
         title = @"2";
        [self replort:title];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"广告骚扰" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
         title = @"3";
        [self replort:title];
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"人身攻击" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
         title = @"4";
        [self replort:title];
    }];
    UIAlertAction *action5= [UIAlertAction actionWithTitle:@"其他" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
         title = @"5";
        [self replort:title];
    }];
    
    [alertController addAction:action];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [alertController addAction:action3];
    [alertController addAction:action4];
    [alertController addAction:action5];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}

-(void)replort:(NSString *)name{
    NSDictionary *param = @{
                            @"aid":self.model.ID?:@"",
                            @"replort":name,
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_reportUpdatings parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    //设置自动计算行号模式
    tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    tableView.estimatedRowHeight = 200;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}


-(void)loadShareView{
    _shareView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 50)];
    _shareView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *head =[[UIImageView alloc]init];
    [head sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[kUserDefault objectForKey:HEADIMG]]] placeholderImage:[UIImage imageNamed:@"default_square"]];
  
    [head.layer setMasksToBounds:YES];
    [head.layer setCornerRadius:15];
    [_shareView addSubview:head];
    
    [head mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self->_shareView.left).offset(10);
        make.centerY.mas_equalTo(self->_shareView);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    UILabel * nameLabel = [UILabel new];
    nameLabel.text = @"分享我的动态";
    nameLabel.textColor = HEXCOLOR(0x343434);
    nameLabel.font =  [UIFont systemFontOfSize:14];
    [_shareView addSubview:nameLabel];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(head.mas_right)setOffset:10];
        make.centerY.mas_equalTo(head.mas_centerY);
    }];
    
    UIImageView *share =[[UIImageView alloc]init];
    share.image = [UIImage imageNamed:@"gaixie"];
    [_shareView addSubview:share];
    
    [share mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self->_shareView.right).offset(-20);
        make.centerY.mas_equalTo(self->_shareView);
        
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = HEXCOLOR(0xf6f6fa);
    [_shareView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.mas_equalTo(self->_shareView);
        make.height.mas_equalTo(@1);
        
    }];
    
    _tableView.tableHeaderView = _shareView;
    
}

-(void)loadPopView{
    self.popupByWindow = [[QMUIPopupMenuView alloc] init];
    self.popupByWindow.automaticallyHidesWhenUserTap = YES;// 点击空白地方消失浮层
    self.popupByWindow.shouldShowItemSeparator = YES;
    self.popupByWindow.maskViewBackgroundColor = [UIColor clearColor];
    self.popupByWindow.itemHeight = 30;
    self.popupByWindow.itemTitleColor = HEXCOLOR(0x676767);
    self.popupByWindow.itemTitleFont = [UIFont systemFontOfSize:11];
    self.popupByWindow.itemConfigurationHandler = ^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuButtonItem *aItem, NSInteger section, NSInteger index) {
        
    };
    self.popupByWindow.items = @[
                                 
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"share_icon"] title:@"分享" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"concern_normal_icon"] title:@"关注" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"collection_normal_icon"] title:@"收藏" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     [self collectAction];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"ranking_icon"] title:@"排行" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jingpai_icon"]title:@"竞拍" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jubao_icon"] title:@"举报" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     [self ReportAction];

                                     
                                }],
                                [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"lahei_normal_icon"] title:@"拉黑" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     [self pullblack];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"big_icon"] title:@"最大化" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }]
                                 ];
    
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
