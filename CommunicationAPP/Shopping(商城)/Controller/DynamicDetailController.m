//
//  DynamicDetailController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "DynamicDetailController.h"
#import "DynamicCommentViewCell.h"
#import "DynamicHeadView.h"
#import "DynamicCommentModel.h"
#import "XHInputView.h"

typedef NS_OPTIONS(NSInteger, DynamicType)
{
    DynamicTypeComment,             //评论
    DynamicTypeForward,             //转发
    
};

@interface DynamicDetailController ()<UITableViewDelegate,UITableViewDataSource,XHInputViewDelagete>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)DynamicHeadView *head;
@property(nonatomic, strong)QMUIPopupMenuView *popupByWindow;
@property(nonatomic ,assign)DynamicType type;

@end

@implementation DynamicDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"动态详情";
    [self loadTableView];
    
    [self loadPopView];
    
    [self loadHeadView];
    
    __weak typeof(self)weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        if (weakSelf.type == DynamicTypeComment) {
             [weakSelf getDataFromSever:dynamic_commentList];
        }else{
             [weakSelf getDataFromSever:dynamic_repeatList];
        }
       
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        if (self.type == DynamicTypeComment) {
            [weakSelf getDataFromSever:dynamic_commentList];
        }else{
            [weakSelf getDataFromSever:dynamic_repeatList];
        }
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
}

-(void)getDataFromSever:(NSString *)request{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"upd_id":self.model.ID?:@"",
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10"
                            };
    [[WYNetworking sharedWYNetworking] POST:request parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];

            }else{
                if ([DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
               
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    //设置自动计算行号模式
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    self.tableView.estimatedRowHeight = 200;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(44 +STATUS_BAR_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadView{
    DynamicHeadView *head = [[DynamicHeadView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 400)];
    _head = head;
    __weak typeof(self)weakSelf = self;
    head.bottomBlock = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 101://点赞
            {
                
            }
                break;
            case 102://评论
            {
                [self showXHInputViewWithStyle:InputViewStyleLarge];
            }
                break;
            case 103://转发
            {
                NSDictionary *param = @{
                                        @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                        @"aid":weakSelf.model.ID?:@""
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:dynamic_forwardUpdatings parameters:param success:^(id  _Nonnull responseObject) {
                    
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:weakSelf.view];
                    
                    self.model.sent_count=[NSString stringWithFormat:@"%ld",[self.model.sent_count integerValue]+1];
                    self.head.model = self.model;
                    
                    [self.tableView reloadData];
                    
                } failure:^(NSError * _Nonnull error) {
                    
                }];
            }
                break;
                
            case 104://更多
                
                
            {
                [self.popupByWindow layoutWithTargetView:sender];
                [self.popupByWindow showWithAnimated:YES];
            }
                break;
                
            default:
                break;
        }
    };
    
    head.DynamicBottomClick = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 0://评论
            {
                self.type =DynamicTypeComment;
                self.currentPage = 1;
                [self getDataFromSever:dynamic_commentList];
               
                
            }
                break;
            case 1://转发
            {
                self.type =DynamicTypeForward;
                self.currentPage = 1;
                [self getDataFromSever:dynamic_repeatList];
              
            }
                break;
            default:
                break;
        }
    };
    head.backgroundColor = UIColor.whiteColor;
    head.model = self.model;
    head.size = CGSizeMake(kSCREENWIDTH, [self.model.headHeight floatValue]);
    self.tableView.tableHeaderView = head;

}


-(void)showXHInputViewWithStyle:(InputViewStyle)style{
    
    [XHInputView showWithStyle:style configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            
            NSDictionary *param = @{
                                    @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                    @"content" :text,
                                    @"upd_id":self.model.ID?:@""
                                    };
            
            [[WYNetworking sharedWYNetworking] POST:dynamic_commentAdd parameters:param success:^(id  _Nonnull responseObject) {
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
                 self.model.comment_count=[NSString stringWithFormat:@"%ld",[self.model.comment_count integerValue]+1];
                self.head.model = self.model;
                
                self.currentPage = 1;
                if (self.type == DynamicTypeComment) {
                    [self getDataFromSever:dynamic_commentList];
                }else{
                    [self getDataFromSever:dynamic_repeatList];
                }
                
            } failure:^(NSError * _Nonnull error) {
                
            }];
            
            NSLog(@"输入的信息为:%@",text);
            
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
    
}

#pragma mark - XHInputViewDelagete
/** XHInputView 将要显示 */
-(void)xhInputViewWillShow:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要显示时将其关闭 */
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].enable = NO;
    
}

/** XHInputView 将要影藏 */
-(void)xhInputViewWillHide:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要影藏时将其打开 */
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].enable = YES;
}

//拉黑
-(void)pullblack{
    NSDictionary *param = @{
                            @"b_uid":self.model.uid?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_pullblack parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

//收藏
-(void)collectAction{
    NSDictionary *param = @{
                            @"upd_id":self.model.ID?:@"",
                            @"type":@"1",//1 收藏 2移除
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_collect parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


//举报
-(void)ReportAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请选择你举报该账号的原因" preferredStyle:UIAlertControllerStyleActionSheet];
    static NSString *title = @"";
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"政治敏感" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"1";
        [self replort:title];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"色情低俗" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"2";
        [self replort:title];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"广告骚扰" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"3";
        [self replort:title];
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"人身攻击" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"4";
        [self replort:title];
    }];
    UIAlertAction *action5= [UIAlertAction actionWithTitle:@"其他" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"5";
        [self replort:title];
    }];
    
    [alertController addAction:action];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [alertController addAction:action3];
    [alertController addAction:action4];
    [alertController addAction:action5];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}

-(void)replort:(NSString *)name{
    NSDictionary *param = @{
                            @"aid":self.model.ID?:@"",
                            @"replort":name,
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_reportUpdatings parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"DynamicCommentViewCell";
    DynamicCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[DynamicCommentViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    DynamicCommentModel *model = [self.dataSource objectAtIndex:indexPath.row];
    if (self.type == DynamicTypeComment) {
        model.dytype = @"0";
    }else{
         model.dytype = @"1";
    }
    cell.model = model;
    [cell layoutIfNeeded];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30.0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.font = [UIFont systemFontOfSize:13];
    headerLabel.textColor = HEXCOLOR(0x555555);
//    if (section==0) {
//        headerLabel.text = @"热门评论";
//    }else if(section== 1){
    if (self.type == DynamicTypeComment) {
        headerLabel.text = @"最新评论";
    }else{
        headerLabel.text = @"最新转发";
    }
    
//    }
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

-(void)loadPopView{
    self.popupByWindow = [[QMUIPopupMenuView alloc] init];
    self.popupByWindow.automaticallyHidesWhenUserTap = YES;// 点击空白地方消失浮层
    self.popupByWindow.shouldShowItemSeparator = YES;
    self.popupByWindow.maskViewBackgroundColor = [UIColor clearColor];
    self.popupByWindow.itemHeight = 30;
    self.popupByWindow.itemTitleColor = HEXCOLOR(0x676767);
    self.popupByWindow.itemTitleFont = [UIFont systemFontOfSize:11];
    self.popupByWindow.itemConfigurationHandler = ^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuButtonItem *aItem, NSInteger section, NSInteger index) {
        
    };
    self.popupByWindow.items = @[
                                 
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"share_icon"] title:@"分享" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"concern_normal_icon"] title:@"关注" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"collection_normal_icon"] title:@"收藏" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     [self collectAction];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"ranking_icon"] title:@"排行" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jingpai_icon"]title:@"竞拍" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jubao_icon"] title:@"举报" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                    [self ReportAction];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"lahei_normal_icon"] title:@"拉黑" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                      [self pullblack];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"big_icon"] title:@"最大化" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     
                                 }]
                                 ];
    
}


- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
