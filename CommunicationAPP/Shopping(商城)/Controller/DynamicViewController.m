//
//  DynamicViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "DynamicViewController.h"
#import "MineViewController.h"
#import "AttentionDynamicController.h"
#import "BesiegedCityController.h"

@interface DynamicViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIView *titleView;
@property (nonatomic ,strong)UILabel *pointLabel;
@property (nonatomic, strong)UIScrollView *contentScrollView;
@end

@implementation DynamicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}


#pragma mark关注、热门等头部视图
-(void)setUI{
    
    self.titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, STATUS_BAR_HEIGHT +44)];
    [self.view addSubview:self.titleView];
   
//    self.navigationItem.titleView = self.titleView;
    self.titleView.backgroundColor = [UIColor whiteColor];
  
    self.contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:self.contentScrollView];
    
    [self.contentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.contentScrollView.delegate = self;
    
    [self addController];
    [self addLable];
    
    
    CGFloat contentX = self.childViewControllers.count * [UIScreen mainScreen].bounds.size.width;
    self.contentScrollView.contentSize = CGSizeMake(contentX, 0);
    self.contentScrollView.pagingEnabled = YES;
    
    // 添加默认控制器
    MineViewController *vc = [self.childViewControllers firstObject];
    vc.view.frame = self.contentScrollView.bounds;
    [self.contentScrollView addSubview:vc.view];
    UILabel *lable = [self.titleView.subviews firstObject];
    lable.textColor = MAINCOLOR;
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    
    
    self.pointLabel = [UILabel new];
    self.pointLabel.backgroundColor =  HEXCOLOR(0x9C386A);
    [lable addSubview:self.pointLabel];
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lable.mas_bottom);
        make.centerX.equalTo(lable.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(40, 3));
        
    }];
    
}

#pragma mark 添加子控制器
- (void)addController{
    
    for (int i=0 ; i<2;i++){
        
        if (i == 0) {
            AttentionDynamicController *vc1 = [[AttentionDynamicController alloc]init];
            [self addChildViewController:vc1];
        }else{
            BesiegedCityController*vc1 = [[BesiegedCityController alloc]init];
            [self addChildViewController:vc1];
        }
    }
}

/** 添加标题栏 */
#pragma mark 添加标题栏
- (void)addLable{
    UILabel *lbl1 = [[UILabel alloc]init];
    lbl1.text = @"关注的动态";
    lbl1.frame = CGRectMake((kSCREENWIDTH -160)/3, STATUS_BAR_HEIGHT, 80, 44);
    lbl1.font = [UIFont systemFontOfSize:15];
    lbl1.textColor = color666666;
    lbl1.textAlignment = NSTextAlignmentCenter;
    [self.titleView addSubview:lbl1];
    lbl1.tag = 0;
    lbl1.userInteractionEnabled = YES;
    [lbl1 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];
    
    UILabel *lbl2 = [[UILabel alloc]init];
    lbl2.text = @"围城";
    lbl2.frame = CGRectMake((kSCREENWIDTH -160) *2/3 +80, STATUS_BAR_HEIGHT, 80, 44);
    lbl2.font = [UIFont systemFontOfSize:15];
    lbl2.textColor = color666666;
    lbl2.textAlignment = NSTextAlignmentCenter;
    [self.titleView addSubview:lbl2];
    lbl2.tag = 1;
    lbl2.userInteractionEnabled = YES;
    [lbl2 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblClick:)]];
    
}

- (void)lblClick:(UITapGestureRecognizer *)recognizer{
    UILabel *titlelable = (UILabel *)recognizer.view;
    CGFloat offsetX = titlelable.tag * self.contentScrollView.frame.size.width;
    CGFloat offsetY = self.contentScrollView.contentOffset.y;
    CGPoint offset = CGPointMake(offsetX, offsetY);
    [self.contentScrollView setContentOffset:offset animated:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 获得索引
    NSUInteger index = scrollView.contentOffset.x / self.contentScrollView.frame.size.width;
    // 添加控制器
    UIViewController *newsVc;
    newsVc = self.childViewControllers[index];
    [self.titleView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx != index) {
            UILabel *temlabel = self.titleView.subviews[idx];
            temlabel.textColor = color666666;
        }
    }];
    if (newsVc.view.superview) return;
    newsVc.view.frame = scrollView.bounds;
    [self.contentScrollView addSubview:newsVc.view];
    
}

/** 滚动结束（手势导致） */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

/** 正在滚动 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 取出绝对值 避免最左边往右拉时形变超过1
    CGFloat value = ABS(scrollView.contentOffset.x / scrollView.frame.size.width);
    NSUInteger leftIndex = (int)value;
    NSUInteger rightIndex = leftIndex + 1;
    UILabel *labelLeft = self.titleView.subviews[leftIndex];
    labelLeft.textColor = HEXCOLOR(0x9C386A);
    [labelLeft addSubview:self.pointLabel];
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(labelLeft.mas_bottom);
        make.centerX.equalTo(labelLeft.mas_centerX).offset(0);
        make.size.mas_equalTo(CGSizeMake(40, 3));
        
    }];
    // 考虑到最后一个板块，如果右边已经没有板块了 就不在下面赋值scale了
    if (rightIndex < self.titleView.subviews.count) {
        UILabel *labelRight = self.titleView.subviews[rightIndex];
        labelRight.textColor = HEXCOLOR(0x9C386A);
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
