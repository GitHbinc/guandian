//
//  VideoDynamicDetailController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "VideoDynamicDetailController.h"
#import "WMPlayer.h"
#import "DynamicCommentViewCell.h"
#import "ContentBottomView.h"
#import "VideoHeadView.h"
#import "DynamicBottomView.h"
#import "XHInputView.h"

typedef NS_OPTIONS(NSInteger, VideoDynamicType)
{
    VideoDynamicTypeComment,             //评论
    VideoDynamicTypeForward,             //转发
    
};
@interface VideoDynamicDetailController ()<UITableViewDelegate,UITableViewDataSource,WMPlayerDelegate,XHInputViewDelagete>
{
    WMPlayer  *wmPlayer;
    CGRect     playerFrame;
    
}
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)UIButton *selectBtn;
@property(nonatomic ,strong)ContentBottomView *bottomView;
@property(nonatomic ,strong)DynamicBottomView *dynamicBottomView;
@property(nonatomic ,strong)VideoHeadView *headerView;
@property(nonatomic, strong)QMUIPopupMenuView *popupByWindow;
@property(nonatomic ,assign)VideoDynamicType type;
@end

@implementation VideoDynamicDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self setupVideoPlayView];
    [self loadTableView];
    [self loadPopView];
    
    __weak typeof(self)weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentPage = 1;
        if (weakSelf.type == VideoDynamicTypeComment) {
            [weakSelf getDataFromSever:dynamic_commentList];
        }else{
            [weakSelf getDataFromSever:dynamic_repeatList];
        }
        
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        if (self.type == VideoDynamicTypeComment) {
            [weakSelf getDataFromSever:dynamic_commentList];
        }else{
            [weakSelf getDataFromSever:dynamic_repeatList];
        }
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
}

-(void)getDataFromSever:(NSString *)request{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"upd_id":self.model.ID?:@"",
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10"
                            };
    [[WYNetworking sharedWYNetworking] POST:request parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                
            }else{
                if ([DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    //[MBProgressHUD showError:@"真的没有了~" toView:self.view];
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[DynamicCommentModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [weakSelf endRefresh];
    }];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //旋转屏幕通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onDeviceOrientationChange)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->wmPlayer).offset(kSCREENHEIGHT * 5 / 16 +130);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    __weak typeof(self)weakSelf = self;
    _dynamicBottomView = [[DynamicBottomView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 50)];
    _dynamicBottomView.model = self.model;
    _dynamicBottomView.backgroundColor = [UIColor whiteColor];
    _dynamicBottomView.DynamicBottomClick = ^(UIButton * _Nonnull sender) {
        
        switch (sender.tag) {
            case 0://评论
            {
                weakSelf.type =VideoDynamicTypeComment;
                [weakSelf getDataFromSever:dynamic_commentList];
                
            }
                break;
            case 1://转发
            {
                weakSelf.type =VideoDynamicTypeForward;
                [weakSelf getDataFromSever:dynamic_repeatList];
                
            }
                break;
            default:
                break;
        }

    };
    tableView.tableHeaderView =_dynamicBottomView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"DynamicCommentViewCell";
    DynamicCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[DynamicCommentViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    DynamicCommentModel *model = [self.dataSource objectAtIndex:indexPath.row];
    if (self.type == VideoDynamicTypeComment) {
        model.dytype = @"0";
    }else{
        model.dytype = @"1";
    }
    cell.model = model;
    [cell layoutIfNeeded];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30.0;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.font = [UIFont systemFontOfSize:13];
    headerLabel.textColor = HEXCOLOR(0x555555);

    if (self.type == VideoDynamicTypeComment) {
        headerLabel.text = @"最新评论";
    }else{
        headerLabel.text = @"最新转发";
    }
    
  
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (void)setupVideoPlayView{
    
    playerFrame =CGRectMake(0,  0, kSCREENWIDTH, kSCREENHEIGHT * 5 / 16);
    wmPlayer = [[WMPlayer alloc]initWithFrame:playerFrame];
    wmPlayer.delegate = self;
    
    wmPlayer.URLString = self.URLString;
    wmPlayer.titleLabel.text = self.title;
    wmPlayer.closeBtn.hidden = NO;
    [self.view addSubview:wmPlayer];
    [wmPlayer play];
    
     __weak typeof(self)weakSelf = self;
    _headerView = [[VideoHeadView alloc]init];
    _headerView.bottomBlock = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 101://点赞
            {
                
            }
                break;
            case 102://评论
            {
                [weakSelf showXHInputViewWithStyle:InputViewStyleLarge];
            }
                break;
            case 103://转发
            {
                NSDictionary *param = @{
                                        @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                        @"aid":weakSelf.model.ID?:@""
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:dynamic_forwardUpdatings parameters:param success:^(id  _Nonnull responseObject) {
                    
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:weakSelf.view];
                    
                } failure:^(NSError * _Nonnull error) {
                    
                }];
            }
                break;
                
            case 104://更多
                
                
            {
                [weakSelf.popupByWindow layoutWithTargetView:sender];
                [weakSelf.popupByWindow showWithAnimated:YES];
            }
                break;
                
            default:
                break;
        }
    };

    _headerView.dymodel = self.model;
     [self.view  addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view).offset(0);
                make.top.equalTo(self->wmPlayer.mas_bottom).offset(0);
                make.right.equalTo(self.view).offset(0);
                make.height.equalTo(@130);
            }];
}

-(void)showXHInputViewWithStyle:(InputViewStyle)style{
    
    [XHInputView showWithStyle:style configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            
            NSDictionary *param = @{
                                    @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                    @"content" :text,
                                    @"upd_id":self.model.ID?:@""
                                    };
            
            [[WYNetworking sharedWYNetworking] POST:dynamic_commentAdd parameters:param success:^(id  _Nonnull responseObject) {
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
                
                
            } failure:^(NSError * _Nonnull error) {
                
            }];
            
            NSLog(@"输入的信息为:%@",text);
            
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
    
}

#pragma mark - XHInputViewDelagete
/** XHInputView 将要显示 */
-(void)xhInputViewWillShow:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要显示时将其关闭 */
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].enable = NO;
    
}

/** XHInputView 将要影藏 */
-(void)xhInputViewWillHide:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要影藏时将其打开 */
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].enable = YES;
}

//拉黑
-(void)pullblack{
    NSDictionary *param = @{
                            @"b_uid":self.model.uid?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_pullblack parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
              
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

//收藏
-(void)collectAction{
    NSDictionary *param = @{
                            @"upd_id":self.model.ID?:@"",
                            @"type":@"1",//1 收藏 2移除
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_collect parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


//举报
-(void)ReportAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请选择你举报该账号的原因" preferredStyle:UIAlertControllerStyleActionSheet];
    static NSString *title = @"";
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"政治敏感" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"1";
        [self replort:title];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"色情低俗" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"2";
        [self replort:title];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"广告骚扰" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"3";
        [self replort:title];
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"人身攻击" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"4";
        [self replort:title];
    }];
    UIAlertAction *action5= [UIAlertAction actionWithTitle:@"其他" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        title = @"5";
        [self replort:title];
    }];
    
    [alertController addAction:action];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [alertController addAction:action3];
    [alertController addAction:action4];
    [alertController addAction:action5];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}

-(void)replort:(NSString *)name{
    NSDictionary *param = @{
                            @"aid":self.model.ID?:@"",
                            @"replort":name,
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_reportUpdatings parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                
            }
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}

/** 全屏 */
- (void)toFullScreenWithInterfaceOrientation:(UIInterfaceOrientation )interfaceOrientation
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [wmPlayer removeFromSuperview];
    wmPlayer.transform = CGAffineTransformIdentity;
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft) {
        wmPlayer.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }else if(interfaceOrientation==UIInterfaceOrientationLandscapeRight){
        wmPlayer.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    wmPlayer.frame = CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT);
    wmPlayer.playerLayer.frame =  CGRectMake(0,0, kSCREENHEIGHT,kSCREENWIDTH);
    
    [wmPlayer.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(kSCREENWIDTH-40);
        make.width.mas_equalTo(kSCREENHEIGHT);
    }];
    [wmPlayer.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.left.equalTo(self->wmPlayer).with.offset(0);
        make.width.mas_equalTo(kSCREENHEIGHT);
    }];
    [wmPlayer.closeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->wmPlayer.topView).with.offset(5);
        make.height.mas_equalTo(30);
        make.top.equalTo(self->wmPlayer.topView).with.offset(5);
        make.width.mas_equalTo(30);
    }];
    [wmPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->wmPlayer.topView).with.offset(45);
        make.right.equalTo(self->wmPlayer.topView).with.offset(-45);
        make.center.equalTo(self->wmPlayer.topView);
        make.top.equalTo(self->wmPlayer.topView).with.offset(0);
        
    }];
    [wmPlayer.loadFailedLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kSCREENHEIGHT);
        make.center.mas_equalTo(CGPointMake(kSCREENWIDTH/2-36, -(kSCREENWIDTH/2)+36));
        make.height.equalTo(@30);
    }];
    [wmPlayer.loadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointMake(kSCREENWIDTH/2-37, -(kSCREENWIDTH/2-37)));
    }];
    [[UIApplication sharedApplication].keyWindow addSubview:wmPlayer];
    wmPlayer.fullScreenBtn.selected = YES;
    [wmPlayer bringSubviewToFront:wmPlayer.bottomView];
    
}

/** 正常屏幕 */
-(void)toNormal{
    [wmPlayer removeFromSuperview];
    [UIView animateWithDuration:0.5f animations:^{
        self->wmPlayer.transform = CGAffineTransformIdentity;
        self->wmPlayer.frame =CGRectMake(self->playerFrame.origin.x, self->playerFrame.origin.y, self->playerFrame.size.width, self->playerFrame.size.height);
        self->wmPlayer.playerLayer.frame =  self->wmPlayer.bounds;
        [self.view addSubview:self->wmPlayer];
        [self->wmPlayer.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer).with.offset(0);
            make.right.equalTo(self->wmPlayer).with.offset(0);
            make.height.mas_equalTo(40);
            make.bottom.equalTo(self->wmPlayer).with.offset(0);
        }];
        
        
        [self->wmPlayer.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer).with.offset(0);
            make.right.equalTo(self->wmPlayer).with.offset(0);
            make.height.mas_equalTo(40);
            make.top.equalTo(self->wmPlayer).with.offset(0);
        }];
        
        
        [self->wmPlayer.closeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer.topView).with.offset(5);
            make.height.mas_equalTo(30);
            make.top.equalTo(self->wmPlayer.topView).with.offset(5);
            make.width.mas_equalTo(30);
        }];
        
        
        [self->wmPlayer.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->wmPlayer.topView).with.offset(45);
            make.right.equalTo(self->wmPlayer.topView).with.offset(-45);
            make.center.equalTo(self->wmPlayer.topView);
            make.top.equalTo(self->wmPlayer.topView).with.offset(0);
        }];
        
        [self->wmPlayer.loadFailedLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self->wmPlayer);
            make.width.equalTo(self->wmPlayer);
            make.height.equalTo(@30);
        }];
        
    }completion:^(BOOL finished) {
        self->wmPlayer.isFullscreen = NO;
        [self setNeedsStatusBarAppearanceUpdate];
        self->wmPlayer.fullScreenBtn.selected = NO;
        
    }];
}

///播放器事件
-(void)wmplayer:(WMPlayer *)wmplayer clickedCloseButton:(UIButton *)closeBtn{
    NSLog(@"clickedCloseButton");
    [self releaseWMPlayer];
    [self.navigationController popViewControllerAnimated:YES];
    
}
///播放暂停
-(void)wmplayer:(WMPlayer *)wmplayer clickedPlayOrPauseButton:(UIButton *)playOrPauseBtn{
    NSLog(@"clickedPlayOrPauseButton");
}
///全屏按钮
-(void)wmplayer:(WMPlayer *)wmplayer clickedFullScreenButton:(UIButton *)fullScreenBtn{
    if (fullScreenBtn.isSelected) {//全屏显示
        wmPlayer.isFullscreen = YES;
        [self setNeedsStatusBarAppearanceUpdate];
        [self toFullScreenWithInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
    }else{
        [self toNormal];
    }
}
///单击播放器
-(void)wmplayer:(WMPlayer *)wmplayer singleTaped:(UITapGestureRecognizer *)singleTap{
    NSLog(@"didSingleTaped");
}
///双击播放器
-(void)wmplayer:(WMPlayer *)wmplayer doubleTaped:(UITapGestureRecognizer *)doubleTap{
    NSLog(@"didDoubleTaped");
}
///播放状态
-(void)wmplayerFailedPlay:(WMPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state{
    NSLog(@"wmplayerDidFailedPlay");
}
-(void)wmplayerReadyToPlay:(WMPlayer *)wmplayer WMPlayerStatus:(WMPlayerState)state{
    NSLog(@"wmplayerDidReadyToPlay");
}
-(void)wmplayerFinishedPlay:(WMPlayer *)wmplayer{
    NSLog(@"wmplayerDidFinishedPlay");
}
/**
 *  旋转屏幕通知
 */
- (void)onDeviceOrientationChange{
    if (wmPlayer==nil||wmPlayer.superview==nil){
        return;
    }
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortraitUpsideDown:{
            NSLog(@"第3个旋转方向---电池栏在下");
        }
            break;
        case UIInterfaceOrientationPortrait:{
            NSLog(@"第0个旋转方向---电池栏在上");
            if (wmPlayer.isFullscreen) {
                [self toNormal];
            }
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            NSLog(@"第2个旋转方向---电池栏在左");
            wmPlayer.isFullscreen = YES;
            [self setNeedsStatusBarAppearanceUpdate];
            [self toFullScreenWithInterfaceOrientation:interfaceOrientation];
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            NSLog(@"第1个旋转方向---电池栏在右");
            wmPlayer.isFullscreen = YES;
            [self setNeedsStatusBarAppearanceUpdate];
            [self toFullScreenWithInterfaceOrientation:interfaceOrientation];
        }
            break;
        default:
            break;
    }
}

- (void)releaseWMPlayer
{
    [wmPlayer.player.currentItem cancelPendingSeeks];
    [wmPlayer.player.currentItem.asset cancelLoading];
    [wmPlayer pause];
    [wmPlayer removeFromSuperview];
    [wmPlayer.playerLayer removeFromSuperlayer];
    [wmPlayer.player replaceCurrentItemWithPlayerItem:nil];
    wmPlayer.player = nil;
    wmPlayer.currentItem = nil;
    //释放定时器，否侧不会调用WMPlayer中的dealloc方法
    [wmPlayer.autoDismissTimer invalidate];
    wmPlayer.autoDismissTimer = nil;
    
    
    wmPlayer.playOrPauseBtn = nil;
    wmPlayer.playerLayer = nil;
    wmPlayer = nil;
}

-(void)loadPopView{
    self.popupByWindow = [[QMUIPopupMenuView alloc] init];
    self.popupByWindow.automaticallyHidesWhenUserTap = YES;// 点击空白地方消失浮层
    self.popupByWindow.shouldShowItemSeparator = YES;
    self.popupByWindow.maskViewBackgroundColor = [UIColor clearColor];
    self.popupByWindow.itemHeight = 30;
    self.popupByWindow.itemTitleColor = HEXCOLOR(0x676767);
    self.popupByWindow.itemTitleFont = [UIFont systemFontOfSize:11];
    self.popupByWindow.itemConfigurationHandler = ^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuButtonItem *aItem, NSInteger section, NSInteger index) {
        
    };
    self.popupByWindow.items = @[
                                 
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"share_icon"] title:@"分享" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"concern_normal_icon"] title:@"关注" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"collection_normal_icon"] title:@"收藏" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                      [self collectAction];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"ranking_icon"] title:@"排行" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jingpai_icon"]title:@"竞拍" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"jubao_icon"] title:@"举报" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                      [self ReportAction];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"lahei_normal_icon"] title:@"拉黑" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                     [self pullblack];
                                 }],
                                 [QMUIPopupMenuButtonItem itemWithImage:[UIImage imageNamed:@"big_icon"] title:@"最大化" handler:^(QMUIPopupMenuButtonItem *aItem) {
                                     [aItem.menuView hideWithAnimated:YES];
                                 }]
                                 ];
    
}


- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}


- (void)dealloc
{
    [self releaseWMPlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"DetailViewController deallco");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
