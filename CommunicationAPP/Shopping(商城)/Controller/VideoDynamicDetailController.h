//
//  VideoDynamicDetailController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/13.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VideoDynamicDetailController : BaseViewController
@property (nonatomic, retain)NSString * URLString;
@property (nonatomic, strong)DynamicListModel *model;
@end

NS_ASSUME_NONNULL_END
