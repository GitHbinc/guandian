//
//  DynamicBottomView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "DynamicBottomView.h"
@interface DynamicBottomView()
@property(nonatomic ,strong)UIButton *selectBtn;
@end
@implementation DynamicBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
       
        
        //[self setUpUI];
        
    }
    
    return self;
}

//-(void)setUpUI{
//    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 10)];
//    line.backgroundColor = HEXCOLOR(0xf6f6fa);
//    [self addSubview:line];
//    
//    NSArray * titleArray = @[@"评论50",@"转发26"];
//    for (int i = 0; i < titleArray.count; i++) {
//        CGFloat titleW = (kSCREENWIDTH-40)/5;
//        CGFloat  titleH = 30;
//        CGFloat  titleY = 15;
//        CGFloat  titleX = i * titleW + 10 ;
//        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [titleBtn addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchUpInside];
//        [titleBtn setTitle:titleArray[i] forState:UIControlStateNormal];
//        titleBtn.frame = CGRectMake(titleX, titleY, titleW, titleH);
//        [titleBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
//        [titleBtn setTitleColor:HEXCOLOR(0xAAAAAA) forState:UIControlStateNormal];
//        [titleBtn setTitleColor:HEXCOLOR(0x121212) forState:UIControlStateSelected];
//        [titleBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
//        [titleBtn setBackgroundImage:[UIImage imageNamed:@"tuoyuan"] forState:UIControlStateSelected];
//        
//        [self addSubview:titleBtn];
//        titleBtn.tag = i;
//        if (titleBtn.tag ==0) {
//            [self clickTitle:titleBtn];
//        }
//    }
//}

-(void)setModel:(DynamicListModel *)model{
    _model = model;
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 10)];
    line.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self addSubview:line];
    
    NSArray * titleArray = @[[NSString stringWithFormat:@"评论%@",model.comment_count],[NSString stringWithFormat:@"转发%@",model.sent_count]];
    for (int i = 0; i < titleArray.count; i++) {
        CGFloat titleW = (kSCREENWIDTH-40)/5;
        CGFloat  titleH = 30;
        CGFloat  titleY = 15;
        CGFloat  titleX = i * titleW + 10 ;
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [titleBtn addTarget:self action:@selector(clickTitle:) forControlEvents:UIControlEventTouchUpInside];
        [titleBtn setTitle:titleArray[i] forState:UIControlStateNormal];
        titleBtn.frame = CGRectMake(titleX, titleY, titleW, titleH);
        [titleBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [titleBtn setTitleColor:HEXCOLOR(0xAAAAAA) forState:UIControlStateNormal];
        [titleBtn setTitleColor:HEXCOLOR(0x121212) forState:UIControlStateSelected];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        [titleBtn setBackgroundImage:[UIImage imageNamed:@"tuoyuan"] forState:UIControlStateSelected];
        
        [self addSubview:titleBtn];
        titleBtn.tag = i;
        if (titleBtn.tag ==0) {
            [self clickTitle:titleBtn];
        }
    }
    
}

-(void)clickTitle:(UIButton *)sender{
    if (self.selectBtn) {
        self.selectBtn.selected = NO;
        self.selectBtn.userInteractionEnabled = YES;
    }
    sender.selected = YES;
    sender.userInteractionEnabled = NO;
    self.selectBtn = sender;
    if (_DynamicBottomClick) {
        _DynamicBottomClick(sender);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
