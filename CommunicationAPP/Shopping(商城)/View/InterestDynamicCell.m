//
//  InterestDynamicCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "InterestDynamicCell.h"
#import "InterestAnchorViewCell.h"

@interface InterestDynamicCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *mainCollectionView;
}

@end

@implementation InterestDynamicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.array = [NSMutableArray array];
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}


#pragma mark - UI
- (void)setUpUI{
    UILabel *interest = [UILabel new];
    interest.text = @"可能感兴趣的直播";
    interest.textColor = HEXCOLOR(0xB1B1B1);
    interest.font =  [UIFont systemFontOfSize:11];
    [self.contentView addSubview:interest];
    
    [interest mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(10);
        [make.top.mas_equalTo(self.contentView.mas_top)setOffset:10];
    }];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    mainCollectionView.backgroundColor = [UIColor whiteColor];
    mainCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:mainCollectionView];
    
    [mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(interest.mas_bottom).offset(10);
        make.left.equalTo(interest);
        make.height.mas_equalTo(@160);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-10);

    }];
    
    [mainCollectionView registerClass:[InterestAnchorViewCell class] forCellWithReuseIdentifier:@"InterestAnchorViewCell"];
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    
    
}

-(void)setArray:(NSMutableArray *)array{
    _array = array;
    [mainCollectionView reloadData];
}

#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    InterestAnchorViewCell *cell = (InterestAnchorViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"InterestAnchorViewCell" forIndexPath:indexPath];
    cell.model = [self.array objectAtIndex:indexPath.item];
    return cell;
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(120, 170);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 0, 10, 0);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//点击item方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    VideoDetailViewController *detail = [VideoDetailViewController new];
//    detail.URLString = @"http://admin.weixin.ihk.cn/ihkwx_upload/test.mp4";
//    detail.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:detail animated:YES];
    
}

    

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
