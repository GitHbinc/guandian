//
//  AttentionDynamicViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AttentionDynamicViewCell : UITableViewCell
@property(nonatomic, strong)DynamicListModel *model;
@property(nonatomic,copy)void (^ButtonBlock)(UIButton *button,DynamicListModel *model);

@end

NS_ASSUME_NONNULL_END
