//
//  InterestAnchorViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestAnchorModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InterestAnchorViewCell : UICollectionViewCell
@property(nonatomic ,strong)InterestAnchorModel *model;
@end

NS_ASSUME_NONNULL_END
