//
//  InterestAnchorViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/11.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "InterestAnchorViewCell.h"
@interface InterestAnchorViewCell()
@property (nonatomic ,strong)UIImageView *bgImageView;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *nameLabel;

@end
@implementation InterestAnchorViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];

        [self setUpUI];
    }
    
    return self;
}


-(void)setUpUI{
    _bgImageView = [[UIImageView alloc]init];
    //_bgImageView.image = [UIImage imageNamed:@"test_icon"];
    _bgImageView.layer.masksToBounds = YES;
    [_bgImageView.layer setCornerRadius:6.0];
    [self.contentView addSubview:_bgImageView];
    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@120);
    }];

    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = HEXCOLOR(0x020202);
    _titleLabel.font = [UIFont systemFontOfSize:13];
    //_titleLabel.text = @"[liv3]这里有...";
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_bgImageView);
        make.top.equalTo(self->_bgImageView.mas_bottom).offset(5);
    }];
    
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.font = [UIFont systemFontOfSize:11];
//    _nameLabel.text = @"小白biubiu...";
    _nameLabel.textColor =  HEXCOLOR(0xA7A7A7);
    [self addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_bgImageView.mas_left);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
    }];
}

-(void)setModel:(InterestAnchorModel *)model{
    _model = model;
     [_bgImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.profile]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _titleLabel.text = model.title;
    _nameLabel.text = model.name;
}


@end
