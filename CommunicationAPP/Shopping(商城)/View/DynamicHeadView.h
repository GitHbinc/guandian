//
//  DynamicHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, DynamicHeadType)
{
    DynamicHeadTypePicAndWords,             //有图片并且有文字
    DynamicHeadTypePic,                     //只有图片
    DynamicHeadTypeForward,                 //转发
    
    
};
@interface DynamicHeadView : UIView
@property (nonatomic, strong)DynamicListModel *model;
@property (nonatomic, copy) void(^DynamicBottomClick)(UIButton * sender);
@property (nonatomic, copy) void(^bottomBlock)(UIButton * sender);

@end

NS_ASSUME_NONNULL_END
