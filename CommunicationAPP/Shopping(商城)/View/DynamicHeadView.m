//
//  DynamicHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "DynamicHeadView.h"
#import "ContentBottomView.h"
#import "DynamicBottomView.h"

@interface DynamicHeadView()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nameLabel;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *forwardTitle;
@property (nonatomic ,strong)UIImageView *forwardImage;
@property (nonatomic ,strong)UIView *forwardView;
@property (nonatomic ,strong)ContentBottomView *bottomView;
@property (nonatomic ,strong)DynamicBottomView *dynamicBottomView;

@end

@implementation DynamicHeadView
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
       
        [self setUpUI];
        
    }
    
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _headImage = [[UIImageView alloc]init];
    //_headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:20];
    [self addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    //_nameLabel.text = @"论坛小秘书";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_nameLabel];
    
    
    _dateLabel = [UILabel new];
    //_dateLabel.text = @"11-15";
    _dateLabel.textColor = HEXCOLOR(0xB1B1B1);
    _dateLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_dateLabel];
    
    _titleLabel = [UILabel new];
    //_titleLabel.text = @"参加直播";
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    _titleLabel.textColor = HEXCOLOR(0x343434);
    _titleLabel.font =  [UIFont systemFontOfSize:13];
    [self addSubview:_titleLabel];
    
    _mainImage = [[UIImageView alloc]init];
    //_mainImage.image = [UIImage imageNamed:@"test_icon"];
    [self addSubview:_mainImage];
    
    _forwardView = [UIView new];
    _forwardView.backgroundColor = [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0];
    _forwardView.clipsToBounds = YES;
    _forwardView.layer.cornerRadius = 6.5;
    [self addSubview:_forwardView];
    
    _forwardTitle = [UILabel new];
    //_forwardTitle.text = @"参加直播参加";
    _forwardTitle.numberOfLines = 0;
    _forwardTitle.lineBreakMode = NSLineBreakByCharWrapping;
    _forwardTitle.textColor = HEXCOLOR(0x343434);
    _forwardTitle.font =  [UIFont systemFontOfSize:13];
    [_forwardView addSubview:_forwardTitle];
    
    _forwardImage = [[UIImageView alloc]init];
    //_forwardImage.image = [UIImage imageNamed:@"test_icon"];
    [_forwardView addSubview:_forwardImage];
    
    _mainImage = [[UIImageView alloc]init];
    //_mainImage.image = [UIImage imageNamed:@"test_icon"];
    [self addSubview:_mainImage];
    
     __weak typeof(self)weakSelf = self;
    _bottomView = [[ContentBottomView alloc]init];
    _bottomView.backgroundColor = UIColor.whiteColor;
    _bottomView.block = ^(UIButton *senderBtn){
        if (weakSelf.bottomBlock) {
            weakSelf.bottomBlock(senderBtn);
        }
        
    };
    [self addSubview:_bottomView];
    _dynamicBottomView = [[DynamicBottomView alloc]init];
    _dynamicBottomView.backgroundColor = [UIColor whiteColor];
    _dynamicBottomView.DynamicBottomClick = ^(UIButton * _Nonnull sender) {
        if (weakSelf.DynamicBottomClick) {
           weakSelf.DynamicBottomClick(sender);
        }
    };
     [self addSubview:_dynamicBottomView];
   


}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];

    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        [make.top.mas_equalTo(self.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];

    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self->_headImage.mas_centerY);
    }];

    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-10);
        make.centerY.mas_equalTo(self->_headImage.mas_centerY);
    }];

    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:10];
        make.left.mas_equalTo(self->_headImage.mas_left);
        make.right.mas_equalTo(self).offset(-10);
    }];
}


-(void)ChangeLayout:(CGFloat)size{
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        make.right.mas_equalTo(self).offset(-10);
        [make.top.mas_equalTo(self->_titleLabel.mas_bottom)setOffset:size];
        make.height.mas_equalTo(@220);
    }];
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self->_mainImage.mas_bottom).offset(5);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(@40);
    }];
}

-(void)setModel:(DynamicListModel *)model{
    _model = model;
    _nameLabel.text = model.username;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,_model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _dateLabel.text = model.create_time;
    _dynamicBottomView.model = model;
    if (![self isEmpty:model.re_id]) {//转发
        _titleLabel.text = model.content;
        _mainImage.hidden = YES;
        _forwardView.hidden = NO;
        _forwardTitle.text = model.title;
        [_forwardImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
        [_forwardTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.top.mas_equalTo(self->_forwardView.mas_top)setOffset:10];
            [make.left.mas_equalTo(self->_forwardView.mas_left)setOffset:10];
            make.right.mas_equalTo(self->_forwardView.mas_right).offset(-10);
        }];
        
        [_forwardImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self->_forwardView).offset(10);
            make.right.mas_equalTo(self->_forwardView).offset(-10);
            [make.top.mas_equalTo(self->_forwardTitle.mas_bottom)setOffset:10];
            make.height.mas_equalTo(@220);
            
        }];
        
        [_forwardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(10);
            make.right.mas_equalTo(self).offset(-10);
            [make.top.mas_equalTo(self->_titleLabel.mas_bottom)setOffset:10];
            make.bottom.mas_equalTo(self->_forwardImage.mas_bottom).offset(10);
            
        }];
        
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(0);
            make.top.equalTo(self->_forwardView.mas_bottom).offset(5);
            make.right.equalTo(self).offset(0);
            make.height.equalTo(@40);
            //make.bottom.equalTo(self).offset(0);
        }];
    }else{
        if (![self isEmpty:model.title]) {//文字加图片
           
            _titleLabel.text = model.title;
            _mainImage.hidden = NO;
            [_mainImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
            _forwardView.hidden = YES;
            [self ChangeLayout:10];
            
        }else{//图片
            _titleLabel.text = model.title;
            _mainImage.hidden = NO;
            [_mainImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
            _forwardView.hidden = YES;
            [self ChangeLayout:0];
        }
    }
    
    _bottomView.dynamicmodel = model;
    
    [_dynamicBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self->_bottomView.mas_bottom).offset(5);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(@50);
        //make.bottom.equalTo(self).offset(0);
    }];
    
    [self layoutIfNeeded];     // 下面会有关于layoutIfNeeded的介绍
    CGFloat tagViewHeight = _dynamicBottomView.frame.origin.y;
    model.headHeight = [NSString stringWithFormat:@"%f",tagViewHeight + 50];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
