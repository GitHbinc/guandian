//
//  RechargeView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/7.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RechargeView.h"

@interface RechargeView(){
    
    UILabel *_moneyLabel;
    UILabel *_typeLabel;
    UIImageView *_typeImageView;
    UIImageView *_chooseMoneyImageView;
    NSArray *_chooseMoneyArray;
    UIImageView *_chooseTypeImageView;
    NSArray *_chooseTypeArray;
}

@end
@implementation RechargeView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"QLbg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = UIColor.whiteColor;
    [bgImageView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(bgImageView.mas_bottom).offset(0);
        make.height.equalTo(@200);
        make.left.equalTo(bgImageView.mas_left).offset(0);
        make.right.equalTo(bgImageView.mas_right).offset(0);
    }];
    
    
    NSArray *titleArr = @[@"余额不足，请先购买",@"购买金额",@"购买方式"];
    for (int i = 0; i<titleArr.count; i++) {
        UILabel *label = [self setLabel:titleArr[i] font:15 color:color333333];
        [contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentView.mas_left).offset(15);
            make.top.equalTo(contentView.mas_top).offset(18 + 50 * i);
        }];
        
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = colorCCCCCC;
        [contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_left).offset(0);
            make.right.equalTo(contentView.mas_right).offset(0);
            make.top.equalTo(contentView.mas_top).offset(49 * (i + 1));
            make.height.equalTo(@1);
        }];
    }
    
    _moneyLabel = [self setLabel:@"余额：0.00点币" font:12 color:color666666];
    [contentView addSubview:_moneyLabel];
    [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-20);
        make.top.equalTo(contentView.mas_top).offset(18);
    }];
    
    
    UIButton *choseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [choseBtn setImage:[UIImage imageNamed:@"Rup_icon"] forState:UIControlStateNormal];
    [choseBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    choseBtn.tag = 101;
    [contentView addSubview:choseBtn];
    [choseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-20);
        make.top.equalTo(self->_moneyLabel.mas_bottom).offset(30);
    }];
    
    UILabel *descLabel = [self setLabel:@"6点（6元）" font:12 color:color666666];
    [contentView addSubview:descLabel];
    [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(choseBtn.mas_left).offset(-5);
        make.top.equalTo(choseBtn.mas_top).offset(5);
    }];
    
    UIButton *choseBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [choseBtn1 setImage:[UIImage imageNamed:@"Rup_icon"] forState:UIControlStateNormal];
    [choseBtn1 addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    choseBtn1.tag = 102;
    [contentView addSubview:choseBtn1];
    [choseBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-20);
        make.top.equalTo(choseBtn.mas_bottom).offset(30);
    }];
    
    _typeLabel = [self setLabel:@"时间代理支付" font:12 color:color666666];
    [contentView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(choseBtn1.mas_left).offset(-5);
        make.top.equalTo(choseBtn1.mas_top).offset(3);
    }];
    
    _typeImageView = [[UIImageView alloc]init];
    _typeImageView.image = [UIImage imageNamed:@"RGD_icon"];
    [contentView addSubview:_typeImageView];
    [_typeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_typeLabel.mas_left).offset(-5);
        make.top.equalTo(self->_typeLabel.mas_top).offset(-3);
    }];
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"确定购买" forState:UIControlStateNormal];
    btn.tag = 103;
    [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    [contentView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(0);
        make.right.equalTo(contentView.mas_right).offset(0);
        make.bottom.equalTo(contentView.mas_bottom).offset(0);
        make.height.equalTo(@50);
    }];
}

-(UILabel *)setLabel:(NSString *)titleStr font:(CGFloat)font color:(UIColor *)color{
    
    UILabel *label = [[UILabel alloc]init];
    label.text = titleStr;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}


-(void)allBtnClick:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
        case 101:
            senderBtn.selected = !senderBtn.selected;
            if (senderBtn.selected) {
                
                [self setChooseMoneyView];
            }else{
                [_chooseMoneyImageView removeFromSuperview];
            }
            
            break;
        case 102:
            senderBtn.selected = !senderBtn.selected;
            if (senderBtn.selected) {
                
                [self setChooseTypeImageView];
            }else{
                [_chooseTypeImageView removeFromSuperview];
            }
            
            break;
        case 103:
            break;
        case 104:
        case 105:
        case 106:
        case 107:
        {
            UIButton *btn = (UIButton *)[self viewWithTag:101];
            btn.selected = !btn.selected;
            _moneyLabel.text = [NSString stringWithFormat:@"余额：%@",_chooseMoneyArray[senderBtn.tag - 104]];
            [_chooseMoneyImageView removeFromSuperview];
        }
            
            break;
        case 108:
        case 109:
        case 110:
        {
            UIButton *btn = (UIButton *)[self viewWithTag:102];
            btn.selected = !btn.selected;
            _typeLabel.text = _chooseTypeArray[1][senderBtn.tag - 108];
            _typeImageView.image = [UIImage imageNamed:_chooseTypeArray[0][senderBtn.tag - 108]];
            [_chooseTypeImageView removeFromSuperview];
        }
            
            break;
        default:
            break;
    }
}

#pragma mark 选择购买金额的弹窗
-(void)setChooseMoneyView{
    
    _chooseMoneyImageView = [[UIImageView alloc]init];
    _chooseMoneyImageView.userInteractionEnabled = true;
    _chooseMoneyImageView.image = [UIImage imageNamed:@"RmoneyBg_icon"];
    [self addSubview:_chooseMoneyImageView];
    [_chooseMoneyImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-130);
        make.right.equalTo(self).offset(0);
    }];
    
     _chooseMoneyArray= @[@"更多（6点/元=1小时）",@"24点（24元）",@"18点（18元）",@"12点（12元）"];
    for (int i = 0; i < _chooseMoneyArray.count; i++) {
        
        UIButton *btn = [[UIButton alloc]init];
        [btn setTitle:_chooseMoneyArray[i] forState:UIControlStateNormal];
        [btn setTitleColor:color333333 forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        btn.tag = 104 + i;
        [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_chooseMoneyImageView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self->_chooseMoneyImageView.mas_top).offset(7 + 30 * i);
            make.centerX.equalTo(self->_chooseMoneyImageView.mas_centerX);
        }];

        if (i < _chooseMoneyArray.count - 1) {
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = colorCCCCCC;
            [_chooseMoneyImageView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_chooseMoneyImageView.mas_left).offset(10);
                make.right.equalTo(self->_chooseMoneyImageView.mas_right).offset(-10);
                make.top.equalTo(btn.mas_bottom).offset(0);
                make.height.equalTo(@1);
            }];
        }
    }
}

#pragma mark 选择购买方式的弹窗
-(void)setChooseTypeImageView{
    
    _chooseTypeImageView = [[UIImageView alloc]init];
    _chooseTypeImageView.image = [UIImage imageNamed:@"RtypeBg_icon"];
    _chooseTypeImageView.userInteractionEnabled = true;
    [self addSubview:_chooseTypeImageView];
    [_chooseTypeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-80);
        make.right.equalTo(self).offset(0);
    }];
    
    if ([_typeLabel.text containsString:@"时间代理支付"]) {
        _chooseTypeArray= @[@[@"Rpay_icon",@"Rwechat_icon",@"Rbank_icon"],@[@"支付宝",@"微信支付",@"银联支付"]];
    }else if ([_typeLabel.text containsString:@"支付宝"]){
        _chooseTypeArray= @[@[@"RGD_icon",@"Rwechat_icon",@"Rbank_icon"],@[@"时间代理支付",@"微信支付",@"银联支付"]];
    }else if ([_typeLabel.text containsString:@"微信支付"]){
        _chooseTypeArray= @[@[@"RGD_icon",@"Rpay_icon",@"Rbank_icon"],@[@"时间代理支付",@"支付宝",@"银联支付"]];
    }else{
        _chooseTypeArray= @[@[@"RGD_icon",@"Rpay_icon",@"Rwechat_icon"],@[@"时间代理支付",@"支付宝",@"微信支付"]];
    }
    
    NSLog(@"%@",_chooseTypeArray);
    
    for (int i = 0; i < 3; i++) {
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:_chooseTypeArray[0][i]] ;
        [_chooseTypeImageView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self->_chooseTypeImageView.mas_left).offset(28);
            make.top.equalTo(self->_chooseTypeImageView.mas_top).offset(9 + 30 * i);
        }];
        
        UIButton *btn = [[UIButton alloc]init];
        [btn setTitle:_chooseTypeArray[1][i] forState:UIControlStateNormal];
        [btn setTitleColor:color333333 forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        btn.tag = 108 + i;
        [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_chooseTypeImageView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imageView.mas_top).offset(-3);
            make.left.equalTo(imageView.mas_right).offset(3);
        }];
        
        if (i < 2) {
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = colorCCCCCC;
            [_chooseTypeImageView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_chooseTypeImageView.mas_left).offset(10);
                make.right.equalTo(self->_chooseTypeImageView.mas_right).offset(-10);
                make.top.equalTo(btn.mas_bottom).offset(0);
                make.height.equalTo(@1);
            }];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
