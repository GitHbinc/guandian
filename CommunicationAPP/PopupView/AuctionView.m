//
//  AuctionView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionView.h"

@interface AuctionView(){
    UITextField *_moneyTextField;
}

@end
@implementation AuctionView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"QLbg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = UIColor.whiteColor;
    contentView.layer.cornerRadius = 2.0;
    [bgImageView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgImageView.mas_centerY);
        make.height.equalTo(@165);
        make.left.equalTo(bgImageView.mas_left).offset(45);
        make.right.equalTo(bgImageView.mas_right).offset(-45);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"竞价连麦";
    label.textColor = color333333;
    label.font = [UIFont systemFontOfSize:17];
    [contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_top).offset(20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];


    UIImageView *bgImage = [[UIImageView alloc]init];
    bgImage.userInteractionEnabled = true;
    bgImage.image = [UIImage imageNamed:@"textField_bg_icon"];
    [contentView addSubview:bgImage];
    [bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(25);
        make.right.equalTo(contentView.mas_right).offset(-25);
        make.top.equalTo(label.mas_bottom).offset(20);
    }];

    _moneyTextField = [[UITextField alloc]init];
    _moneyTextField.textColor = color333333;
    _moneyTextField.placeholder = @"请输入竞价金额";
    // "通过KVC修改占位文字的颜色"
    [_moneyTextField setValue:color999999 forKeyPath:@"_placeholderLabel.textColor"];
    _moneyTextField.font = [UIFont systemFontOfSize:13];
    [bgImage addSubview:_moneyTextField];
    [_moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImage.mas_left).offset(10);
        make.right.equalTo(bgImage.mas_right).offset(-10);
        make.centerY.equalTo(bgImage.mas_centerY);
        make.height.equalTo(@30);
    }];

    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorCCCCCC;
    [contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(contentView.mas_bottom).offset(-50);
        make.left.equalTo(contentView.mas_left).offset(0);
        make.right.equalTo(contentView.mas_right).offset(0);
        make.height.equalTo(@1);
    }];

    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = colorCCCCCC;
    [contentView addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(contentView.mas_bottom).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.width.equalTo(@1);
        make.centerX.equalTo(contentView.mas_centerX);
    }];

    UIButton *cancelBtn = [[UIButton alloc]init];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:color666666 forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    cancelBtn.tag = 101;
    [cancelBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.right.equalTo(lineView1.mas_left).offset(0);
        make.bottom.equalTo(contentView.mas_bottom).offset(0);
    }];

    UIButton *sureBtn = [[UIButton alloc]init];
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    sureBtn.tag = 102;
    [sureBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.left.equalTo(lineView1.mas_right).offset(0);
        make.bottom.equalTo(contentView.mas_bottom).offset(0);
    }];
    
}


-(void)allBtnClick:(UIButton *)senderBtn{
 
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
