//
//  TotastView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TotastView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame  titleStr:(NSString *)titleStr;

@end

NS_ASSUME_NONNULL_END
