//
//  TotastView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "TotastView.h"

@implementation TotastView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame titleStr:(NSString *)titleStr{
    
    return [[self alloc] initWithFrame:frame titleStr:titleStr];
}

- (instancetype)initWithFrame:(CGRect)frame titleStr:(NSString *)titleStr{
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView:titleStr];
    }
    return self;
}
- (void)initView:(NSString *)titleStr {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"QLbg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    //计算文字高度
    NSStringDrawingOptions options =  NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGRect rect = [titleStr boundingRectWithSize:CGSizeMake(MAXFLOAT,MAXFLOAT) options:options attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    CGFloat width = 50 + rect.size.height;
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = UIColor.whiteColor;
    contentView.layer.cornerRadius = 2.0;
    [bgImageView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgImageView.mas_centerY);
        make.height.equalTo(@(width));
        make.left.equalTo(bgImageView.mas_left).offset(45);
        make.right.equalTo(bgImageView.mas_right).offset(-45);
    }];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = color333333;
    titleLabel.text = titleStr;
    titleLabel.numberOfLines = 0;
    
    //添加行间距
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10 - (titleLabel.font.lineHeight - titleLabel.font.pointSize);
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleLabel.text attributes:attributes];

    [contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(25);
        make.right.equalTo(contentView.mas_right).offset(-25);
        make.centerY.equalTo(contentView.mas_centerY);
    }];
    
    if ([titleStr containsString:@"连麦成功"]) {
        NSArray *array = [titleStr componentsSeparatedByString:@"连麦成功"];
        NSArray *array1 = [titleStr componentsSeparatedByString:@"一"];
        NSString *str1 = array[0];
        NSString *str2 = array1[0];
        // 创建Attributed
        NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
        [noteStr addAttribute:NSForegroundColorAttributeName value:MAINCOLOR range:NSMakeRange(str1.length, 4)];
        [noteStr addAttribute:NSForegroundColorAttributeName value:MAINCOLOR range:NSMakeRange(str2.length, 1)];
        titleLabel.attributedText = noteStr;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
