//
//  PersonalInformationView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PersonalInformationView.h"

@interface PersonalInformationView(){
    
    UIImageView *_headerImageView;
    UILabel *_titleLabel;
    UILabel *_audienceLabel;
    UILabel *_fansLabel;
    UILabel *_accountLabel;
    UILabel *_autographLabel;
    
}

@end
@implementation PersonalInformationView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"QLbg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = UIColor.whiteColor;
    contentView.layer.cornerRadius = 8.0;
    [bgImageView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgImageView.mas_centerY);
        make.height.equalTo(@300);
        make.left.equalTo(bgImageView.mas_left).offset(30);
        make.right.equalTo(bgImageView.mas_right).offset(-30);
    }];
    
    UIImageView *bgImage = [[UIImageView alloc]init];
    bgImage.image = [UIImage imageNamed:@"PIbg_icon"];
    [contentView addSubview:bgImage];
    [bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(0);
        make.top.equalTo(contentView.mas_top).offset(0);
        make.right.equalTo(contentView.mas_right).offset(0);
        make.height.equalTo(@123);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.layer.cornerRadius = 39;
    [bgImage addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImage.mas_top).offset(28);
        make.centerX.equalTo(bgImage.mas_centerX);
        make.height.equalTo(@78);
        make.width.equalTo(@78);
    }];
    
    
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    [close setImage:[UIImage imageNamed:@"PIclose_icon"] forState:UIControlStateNormal];
    close.tag = 101;
    [close addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImage addSubview:close];
    [close mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgImage.mas_right).offset(-10);
        make.top.equalTo(bgImage.mas_top).offset(10);
    }];
    

    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:16];
    [contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImage.mas_bottom).offset(20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    _audienceLabel = [[UILabel alloc]init];
    _audienceLabel.textColor = MAINCOLOR;
    _audienceLabel.font = [UIFont systemFontOfSize:12];
    [contentView addSubview:_audienceLabel];
    [_audienceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    _fansLabel = [[UILabel alloc]init];
    _fansLabel.textColor = color999999;
    _fansLabel.font = [UIFont systemFontOfSize:11];
    [contentView addSubview:_fansLabel];
    [_fansLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_audienceLabel.mas_bottom).offset(20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    _accountLabel = [[UILabel alloc]init];
    _accountLabel.textColor = color999999;
    _accountLabel.font = [UIFont systemFontOfSize:11];
    [contentView addSubview:_accountLabel];
    [_accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_fansLabel.mas_bottom).offset(5);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    _autographLabel = [[UILabel alloc]init];
    _autographLabel.textColor = color999999;
    _autographLabel.font = [UIFont systemFontOfSize:11];
    [contentView addSubview:_autographLabel];
    [_autographLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_accountLabel.mas_bottom).offset(5);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    NSArray *titleArr = @[@"关注",@"私聊",@"主页"];
    CGFloat width = (kSCREENWIDTH - 60)/3;
    for (int i = 0; i < titleArr.count; i++) {
        UIButton *btn = [[UIButton alloc]init];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:color333333 forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 102 + i;
        [contentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentView.mas_left).offset(width * i);
            make.bottom.equalTo(contentView.mas_bottom).offset(0);
            make.height.equalTo(@30);
            make.width.equalTo(@(width));
        }];
    }
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorCCCCCC;
    [contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView.mas_left).offset(width);
        make.bottom.equalTo(contentView.mas_bottom).offset(-8);
        make.height.equalTo(@15);
        make.width.equalTo(@1);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = colorCCCCCC;
    [contentView addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-width);
        make.bottom.equalTo(contentView.mas_bottom).offset(-8);
        make.height.equalTo(@15);
        make.width.equalTo(@1);
    }];
}

-(void)refresh{
    
    _titleLabel.text = @"彭于晏";
    _audienceLabel.text = @"观众：2066万";
    _fansLabel.text = @"粉丝：1686";
    _accountLabel.text = @"观点号：018687544212";
    _autographLabel.text = @"签名：有趣的灵魂独一无二";
    _headerImageView.image = [UIImage imageNamed:@"header_icon"];
}

-(void)allBtnClick:(UIButton *)senderBtn{
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
