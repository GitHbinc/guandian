//
//  QuickLoginView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/6.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "QuickLoginView.h"

@implementation QuickLoginView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"QLbg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UIView *contentView = [[UIView alloc]init];
    contentView.backgroundColor = UIColor.whiteColor;
    contentView.layer.cornerRadius = 2.0;
    [bgImageView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgImageView.mas_centerY);
        make.height.equalTo(@250);
        make.left.equalTo(bgImageView.mas_left).offset(45);
        make.right.equalTo(bgImageView.mas_right).offset(-45);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"微信登录";
    label.textColor = color999999;
    label.font = [UIFont systemFontOfSize:14];
    [contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_top).offset(20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"QLclose_icon"] forState:UIControlStateNormal];
    closeBtn.tag = 101;
    [closeBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-10);
        make.top.equalTo(contentView.mas_top).offset(10);
        make.width.equalTo(@18);
        make.height.equalTo(@18);
    }];
    
    UIButton *weChatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [weChatBtn setImage:[UIImage imageNamed:@"QLwechat_icon"] forState:UIControlStateNormal];
    weChatBtn.tag = 102;
    [weChatBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:weChatBtn];
    [weChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView.mas_right).offset(-10);
        make.left.equalTo(contentView.mas_left).offset(10);
        make.top.equalTo(closeBtn.mas_bottom).offset(30);
    }];
    
    UIView *view = [[UIView alloc]init];
    [contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weChatBtn.mas_bottom).offset(10);
        make.width.equalTo(@150);
        make.height.equalTo(@20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorCCCCCC;
    [view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(0);
        make.top.equalTo(view.mas_top).offset(7);
        make.width.equalTo(@60);
        make.height.equalTo(@1);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.text = @"OR";
    label1.textColor = color999999;
    label1.font = [UIFont systemFontOfSize:14];
    [view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_top).offset(0);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = colorCCCCCC;
    [view addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(0);
        make.top.equalTo(view.mas_top).offset(7);
        make.width.equalTo(@60);
        make.height.equalTo(@1);
    }];
    
    CGFloat x = (kSCREENWIDTH - 90 - 180 - 36)/2;
    NSArray *imageArray = @[@"QLxinlang_icon",@"QLphone_icon",@"QLqq_icon",@"QLpay_icon"];
    for (int i = 0; i < imageArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 103 + i;
        [contentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentView.mas_left).offset(x + 60 * i);
            make.top.equalTo(view.mas_bottom).offset(20);
        }];
    }
    
    UIView *registerView = [[UIView alloc]init];
    [contentView addSubview:registerView];
    [registerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(contentView.mas_bottom).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
        make.centerX.equalTo(contentView.mas_centerX);
    }];
    
    UIButton *registerBtn = [[UIButton alloc]init];
    [registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
    [registerBtn setTitleColor:color666666 forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [registerBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    registerBtn.tag = 107;
    [registerView addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(registerView.mas_left).offset(0);
        make.centerY.equalTo(registerView.mas_centerY);
    }];
    
    UIButton *arrowBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [arrowBtn setImage:[UIImage imageNamed:@"QLarrow_icon"] forState:UIControlStateNormal];
    [arrowBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    arrowBtn.tag = 108;
    [registerView addSubview:arrowBtn];
    [arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(registerView.mas_right).offset(0);
        make.centerY.equalTo(registerView.mas_centerY);
    }];
}


-(void)allBtnClick:(UIButton *)senderBtn{
    
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
