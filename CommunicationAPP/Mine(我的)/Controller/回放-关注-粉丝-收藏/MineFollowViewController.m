//
//  MineFollowViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineFollowViewController.h"
#import "FollowCell.h"
#import "AttentionListModel.h"
#import "PersonalCenterViewController.h"

@interface MineFollowViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MineFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type == InterestTypeAttention) {
        self.navigationItem.title =@"关注";
    }else{
        self.navigationItem.title =@"粉丝";
    }
   
    [self createRightBtn];
    
    [self createTableView];
    
    self.currentPage = 1;
    [self getDataFromSever];
    
    __weak typeof(self)weakSelf = self;
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        [self getDataFromSever];
    }];
    // Do any additional setup after loading the view.
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    NSString *urlStr =nil;;
    if (self.type == InterestTypeAttention) {
        self.navigationItem.title =@"关注";
        urlStr = user_myfollow;
    }else{
        self.navigationItem.title =@"粉丝";
        urlStr = user_fanslist;
    }
    [[WYNetworking sharedWYNetworking] POST:urlStr parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[AttentionListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            }else{
                if ([AttentionListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    [MBProgressHUD showError:@"真的没有了~" toView:self.view];
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[AttentionListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  67;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"followCellID";
    FollowCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[FollowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
    cell.attentionbuttonClick = ^(AttentionListModel *model) {//关注
        NSDictionary *param = @{
                                @"fid":model.stock_id,
                                @"uid":[kUserDefault objectForKey:USERID]?:@"",
                                };
        [[WYNetworking sharedWYNetworking] POST:user_editfollow parameters:param success:^(id  _Nonnull responseObject) {
            if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            }else{
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            }
            
        } failure:^(NSError * _Nonnull error) {
            
        }];
    };
    
    cell.headBtnClick = ^(AttentionListModel *model) {//点击头像
        PersonalCenterViewController *anchor = [PersonalCenterViewController new];
        anchor.userID = model.stock_id;
        anchor.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:anchor animated:YES];
    };
    
    return cell;
}

- (void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
