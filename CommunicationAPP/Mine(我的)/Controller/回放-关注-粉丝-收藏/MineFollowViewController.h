//
//  MineFollowViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSInteger, InterestType)
{
    InterestTypeAttention,         //关注
    PasswordTypeFans,              //粉丝
    
};
NS_ASSUME_NONNULL_BEGIN

@interface MineFollowViewController : BaseViewController
@property(nonatomic ,assign)InterestType type;

@end

NS_ASSUME_NONNULL_END
