//
//  ContributionViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/7.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ContributionViewController.h"
#import "WMPanGestureRecognizer.h"
#import "RedPeopleViewController.h"
static CGFloat const kWMMenuViewHeight = 44.0;
#define kNavigationBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
@interface ContributionViewController () <UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSArray *musicCategories;
@property (nonatomic, strong) WMPanGestureRecognizer *panGesture;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, strong) UIView *redView;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *nickName;


@end

@implementation ContributionViewController

- (NSArray *)musicCategories {
    if (!_musicCategories) {
        _musicCategories = @[@"贡献日榜", @"贡献周榜", @"贡献总榜"];
    }
    return _musicCategories;
}

- (instancetype)init {
    if (self = [super init]) {
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.menuItemWidth = [UIScreen mainScreen].bounds.size.width / self.musicCategories.count;
        self.viewTop = kNavigationBarHeight + kWMHeaderViewHeight;
        self.titleColorSelected = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
        self.titleColorNormal = [UIColor colorWithRed:0.4 green:0.8 blue:0.1 alpha:1.0];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
     [self.navigationItem setTitle:@"贡献榜"];
    NSLog(@"********%f*********",Navi_BAR_HEIGHT);
    [self setNaviBarButtonItem];
    
    [self setHeadView];
    
}


-(void)setNaviBarButtonItem{
  UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}


-(void)shareAction{
    
    
}

-(void)setModel:(UserModel *)model{
    _model = model;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nickName.text = model.username;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ContributionUserID" object:nil userInfo:@{@"userId":model.ID}];
}

-(void)setHeadView{//
    
    UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(0, kNavigationBarHeight, [UIScreen mainScreen].bounds.size.width, kWMHeaderViewHeight)];
    redView.backgroundColor = [UIColor whiteColor];
    self.redView = redView;
    [self.view addSubview:self.redView];
    
    self.panGesture = [[WMPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnView:)];
    [self.view addGestureRecognizer:self.panGesture];
    
    _headImage = [[UIImageView alloc] init];
    _headImage.layer.cornerRadius = 45.0;
    _headImage.clipsToBounds = YES;
   [self.redView addSubview:_headImage];
     __weak __typeof(self)weakSelf = self;
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.redView).offset(20);
        make.centerY.equalTo(weakSelf.redView.mas_centerY);
        make.width.height.mas_equalTo(90);
        
    }];
    
    _nickName = [[UILabel alloc] init];
    _nickName.font = [UIFont systemFontOfSize:18];
    [self.redView addSubview:_nickName];
    [_nickName  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weakSelf.headImage);
        make.left.equalTo(weakSelf.headImage.mas_right).offset(10);
    }];
}

- (void)btnClicked:(id)sender {
    NSLog(@"touch up inside");
}

- (void)panOnView:(WMPanGestureRecognizer *)recognizer {
    NSLog(@"pannnnnning received..");
    
    CGPoint currentPoint = [recognizer locationInView:self.view];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.lastPoint = currentPoint;
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [recognizer velocityInView:self.view];
        CGFloat targetPoint = velocity.y < 0 ? kNavigationBarHeight : kNavigationBarHeight + kWMHeaderViewHeight;
        NSTimeInterval duration = fabs((targetPoint - self.viewTop) / velocity.y);
        
        if (fabs(velocity.y) * 1.0 > fabs(targetPoint - self.viewTop)) {
            NSLog(@"velocity: %lf", velocity.y);
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.viewTop = targetPoint;
            } completion:nil];
            
            return;
        }
        
    }
    CGFloat yChange = currentPoint.y - self.lastPoint.y;
    
    self.viewTop += yChange;
    self.lastPoint = currentPoint;
}

// MARK: ChangeViewFrame (Animatable)
- (void)setViewTop:(CGFloat)viewTop {
    _viewTop = viewTop;
    
    if (_viewTop <= kNavigationBarHeight) {
        _viewTop = kNavigationBarHeight;
    }
    
    if (_viewTop > kWMHeaderViewHeight + kNavigationBarHeight) {
        _viewTop = kWMHeaderViewHeight + kNavigationBarHeight;
    }
    
    self.redView.frame = ({
        CGRect oriFrame = self.redView.frame;
        oriFrame.origin.y = _viewTop - kWMHeaderViewHeight;
        oriFrame;
    });
    
    [self forceLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Datasource & Delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.musicCategories.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    RedPeopleViewController *vc = [[RedPeopleViewController alloc] init];
    vc.index = index;
    vc.userID = self.model.ID;
    vc.isConbution = YES;
    return vc;
    
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, _viewTop, self.view.frame.size.width, kWMMenuViewHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = _viewTop + kWMMenuViewHeight;
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
