//
//  RedPeopleViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "RedPeopleViewController.h"
#import "RedsRankCell.h"
#import "ContributionViewCell.h"
#import "ContributionViewController.h"
#import "PersonalCenterViewController.h"
#import "ContributionListModel.h"

@interface RedPeopleViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation RedPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getUserId:) name:@"ContributionUserID" object:nil];
    [self loadTableView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}


-(void)getUserId:(NSNotification*)dic{
    NSDictionary *userDic =dic.userInfo;
    self.userID = [userDic objectForKey:@"userId"];
    [self getDataFromSever];
   
}



-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"type":@(_index+1),
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":self.userID?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_contribution parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [weakSelf.dataSource addObjectsFromArray:[ContributionListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                
            }
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isConbution) {
        static NSString *identifier = @"ContributionViewCell";
        ContributionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[ContributionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0 ||indexPath.row == 1 || indexPath.row ==2) {
            cell.rankLabel.textColor = [UIColor redColor];
            cell.guanImage.hidden = NO;
        }
       
        cell.rankLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        cell.model = [self.dataSource objectAtIndex:indexPath.row];
     
        return cell;
    }
    
    static NSString *identifier = @"RedsRankCell";
    RedsRankCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[RedsRankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.rankbuttonClick = ^(UIButton * _Nonnull btn) {
        switch (btn.tag) {
            case 1001://头像
            {
                PersonalCenterViewController *anchor = [PersonalCenterViewController new];
                
                anchor.hidesBottomBarWhenPushed =YES;
                [self.navigationController pushViewController:anchor animated:YES];
            }
                break;
            case 1002://关注
            {
                
            }
                break;
                
            default:
                break;
        }
    };
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (!self.isConbution) {
        ContributionViewController *detail = [ContributionViewController new];
        detail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detail animated:YES];
    }else{
        PersonalCenterViewController *anchor = [PersonalCenterViewController new];
        [self.navigationController pushViewController:anchor animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
