//
//  RedPeopleViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedPeopleViewController : BaseViewController
@property (nonatomic ,assign)BOOL isConbution;
@property (nonatomic ,assign)NSInteger index;
@property(nonatomic ,copy)NSString *userID;

@end

NS_ASSUME_NONNULL_END
