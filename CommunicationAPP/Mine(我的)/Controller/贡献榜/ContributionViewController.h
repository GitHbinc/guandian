//
//  ContributionViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/7.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMPageController.h"
#import "UserModel.h"
NS_ASSUME_NONNULL_BEGIN
static CGFloat const kWMHeaderViewHeight = 130;
@interface ContributionViewController : WMPageController
@property (nonatomic, assign) CGFloat viewTop;
@property (nonatomic, copy) NSString *userID;
@property (nonatomic ,strong) UserModel *model;
@end

NS_ASSUME_NONNULL_END
