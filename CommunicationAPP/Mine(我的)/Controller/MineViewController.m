//
//  MineViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineViewController.h"
#import "MineHeadView.h"
#import "MineViewCell.h"
#import "MineItem.h"
#import "AuctionViewController.h"
#import "ContributionViewController.h"
#import "SearchViewController.h"
#import "SetUpViewController.h"
#import "PersonalCenterViewController.h"
#import "RecentlyWatchViewController.h"
#import "MyPacketViewController.h"
#import "TimePartnerViewController.h"
#import "SMAuthenticationViewController.h"
#import "ZBAuthenticationViewController.h"
#import "MineFollowViewController.h"
#import "ProfitViewController.h"
#import "MyMessageViewController.h"
#import "LoginViewController.h"
#import "QrcodeViewController.h"
#import "MineCollectedViewController.h"

@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource,PYSearchViewControllerDelegate>

@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)MineHeadView *headerView;
@property(nonatomic ,strong) UserModel *user;
@property (nonatomic, strong) NSMutableArray *dataSource; //数据源数组


@end

@implementation MineViewController

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:color333333,
                                NSFontAttributeName : [UIFont systemFontOfSize:16]}];
    
    [self.navigationItem setTitle:@"个人中心"];
    
    [self setNaviBarButtonItem];
    
    [self loadTableView];
    
    [self loadHeadView];
    
    [self getDataFromPlist];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:LOGINSTATUS] && [[userDefaults objectForKey:LOGINSTATUS] boolValue] == YES) {
        [self getDataFromSever];
    }
}

-(void)setNaviBarButtonItem{
    
    UIImage *leftImage = [[UIImage imageNamed:@"mine_sousuo"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc] initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];

    self.navigationItem.leftBarButtonItem = leftItem;
    self.navigationItem.rightBarButtonItem = rightItem;
    
   
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
     self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
   [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)searchAction{
    NSArray *hotSeaches = @[@"huahua", @"东晨", @"超哥", @"小白biubiu~", @"安然", @"夏日", @"乐乐", @"李大狗"];
    PYSearchViewController *searchViewController = [PYSearchViewController searchViewControllerWithHotSearches:hotSeaches searchBarPlaceholder:@"搜索你想要的内容" didSearchBlock:^(PYSearchViewController *searchViewController, UISearchBar *searchBar, NSString *searchText) {
        SearchViewController *search =  [[SearchViewController alloc] init];
        search.searchText = searchText;
        [searchViewController.navigationController pushViewController:search animated:YES];
        
    }];
    searchViewController.hotSearchStyle = PYHotSearchStyleRankTag;
    searchViewController.searchHistoryStyle = PYHotSearchStyleDefault;
    searchViewController.delegate = self;
    searchViewController.searchViewControllerShowMode = PYSearchViewControllerShowDefault;
    
    searchViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchViewController animated:YES];
}

- (void)didClickCancel:(PYSearchViewController *)searchViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PYSearchViewControllerDelegate
- (void)searchViewController:(PYSearchViewController *)searchViewController searchTextDidChange:(UISearchBar *)seachBar searchText:(NSString *)searchText
{
    if (searchText.length) {
        // Simulate a send request to get a search suggestions
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSMutableArray *searchSuggestionsM = [NSMutableArray array];
            for (int i = 0; i < arc4random_uniform(5) + 10; i++) {
                NSString *searchSuggestion = [NSString stringWithFormat:@"Search suggestion %d", i];
                [searchSuggestionsM addObject:searchSuggestion];
            }
            // Refresh and display the search suggustions
            searchViewController.searchSuggestions = searchSuggestionsM;
        });
    }
}


-(void)shareAction{
    
}

-(void)loadHeadView{
    MineHeadView *headerView = [[MineHeadView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 180)];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    headerView.inforClickBlock = ^{//头像点击
        if ([[userDefaults objectForKey:LOGINSTATUS] boolValue] == NO) {
            [self pushLoginViewController];
            return ;
        }
        
        PersonalCenterViewController *anchor = [PersonalCenterViewController new];
        anchor.isMineCenter = YES;
        anchor.userID = self.user.ID;
        anchor.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:anchor animated:YES];
    };
    
    headerView.qrClickBlock = ^{//二维码
        QrcodeViewController *anchor = [QrcodeViewController new];
        anchor.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:anchor animated:YES];
    };
    
    headerView.classfybuttonClick = ^(UIButton * _Nonnull btn) {
        
        if ([[userDefaults objectForKey:LOGINSTATUS] boolValue] == NO) {
            [self pushLoginViewController];
            return ;
        }
        
        switch (btn.tag) {
            case 1://回放
                {
                    
                }
                break;
            case 2://关注
            {
                MineFollowViewController *controller = [[MineFollowViewController alloc]init];
                controller.type = InterestTypeAttention;
                controller.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:controller animated:true];
                
            }
                break;
            case 3://粉丝
            {
                MineFollowViewController *controller = [[MineFollowViewController alloc]init];
                controller.type = PasswordTypeFans;
                controller.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:controller animated:true];
            }
                break;
            case 4://收藏
            {
                MineCollectedViewController *controller = [[MineCollectedViewController alloc]init];
                controller.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:controller animated:true];
            }
                break;
                
            default:
                break;
        }
    };
    self.headerView = headerView;
    self.tableView.tableHeaderView = headerView;
}

-(void)pushLoginViewController{
    UserHander *user = [UserHander shareHander];
    user.loginUserInfo = nil;
    [[UserHander shareHander] autoSave];
    
    //保存登录状态 跟 用户的ID
    [kUserDefault setObject:@(NO) forKey:LOGINSTATUS];
    [kUserDefault setObject:@"" forKey:USERID];
    
    [kUserDefault synchronize];
    LoginViewController *login = [LoginViewController new];
    login.isCanBack = YES;
    login.hidesBottomBarWhenPushed =YES;
    [self.navigationController pushViewController:login animated:YES];
    
}

/** 数据源*/
- (void)getDataFromPlist{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"MineList" ofType:@"plist"];
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:plistPath];
    for (NSDictionary *dic in array) {
        MineItem *item = [[MineItem alloc]initWithDictionary:dic error:nil];
        [self.dataSource addObject:item];
    }
    [self.tableView reloadData];
}

-(void)getDataFromSever{
    
    [[WYNetworking sharedWYNetworking] POST:user_userinfo parameters:@{
                                                                    @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                                                   
                                                                   } success:^(id  _Nonnull responseObject) {
       
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            UserModel *user = [[UserModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            self.headerView.model = user;
            self.user = user;
            
            [[UserHander shareHander] setLoginUserInfo:user];
            //自动保存登录信息
            [[UserHander shareHander] autoSave];
            
            MineItem * item1 = [self getItem:3];
            item1.type =user.mydian;
            
            MineItem * item2 = [self getItem:4];
            item2.type =user.today;
            
            MineItem * item3 = [self getItem:6];
            item3.type =user.is_profile;
            
            MineItem * item4 = [self getItem:7];
            item4.type =user.jigoureal;
            
            [self.tableView reloadData];

        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(MineItem *)getItem:(NSUInteger)index{
     MineItem * item = [self.dataSource objectAtIndex:index];
     return item;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return 6;
    }
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"MineViewCell";
    MineViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[MineViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    if (indexPath.section == 0) {
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section];
    }else if (indexPath.section == 1) {
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section*3];
    }else{
         cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +(indexPath.section+4)];
    }
    
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([[userDefaults objectForKey:LOGINSTATUS] boolValue] == NO) {
        [self pushLoginViewController];
        return ;
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row==0) {//我的消息
            MyMessageViewController *controller = [[MyMessageViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
            
        }else if (indexPath.row ==1){ //最近观看
            
            RecentlyWatchViewController *controller = [[RecentlyWatchViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
            
        }else{ //我的围城
            
            
        }
        
    }else if (indexPath.section == 1){
        if (indexPath.row==0) {//我的点包
            
            MyPacketViewController *controller = [[MyPacketViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
            
        }else if (indexPath.row ==1){ //我的收益
            
            ProfitViewController *controller = [[ProfitViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }else{ //成为时间合伙人
            
            TimePartnerViewController *controller = [[TimePartnerViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }
    }else{
        if (indexPath.row==0) {//实名认证
            
            SMAuthenticationViewController *controller = [[SMAuthenticationViewController alloc]init];
            controller.type = AuthenticationTypeRealName;
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }else if (indexPath.row ==1){ //机构认证
          
            SMAuthenticationViewController *controller = [[SMAuthenticationViewController alloc]init];
            controller.type = AuthenticationTypeOrganization;
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }else if (indexPath.row ==2){ //主播申请认证
            
            ZBAuthenticationViewController *controller = [[ZBAuthenticationViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }else if (indexPath.row ==3){ //贡献榜
            ContributionViewController *detail = [[ContributionViewController alloc]init];
            detail.userID = self.user.ID;
            detail.model = self.user;
            detail.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detail animated:YES];
        }else if (indexPath.row ==4){ //我的竞拍
            
            AuctionViewController *controller = [[AuctionViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }else{ //设置
            SetUpViewController *controller = [[SetUpViewController alloc]init];
            controller.hidesBottomBarWhenPushed = true;
            [self.navigationController pushViewController:controller animated:true];
            
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 30;
    }
   return 10.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 10.0;
    }else
        return 0.0;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
     return [[UIView alloc]init];
    
}




@end
