//
//  RecentlyWatchViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "RecentlyWatchViewController.h"
#import "RecentlyWatchCell.h"
#import "RecentListModel.h"
#import "CGXPickerView.h"

@interface RecentlyWatchViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy)NSString *time;
@end

@implementation RecentlyWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"最近观看"];
    [self createTableView];
    [self createRightBtn];
    
    self.currentPage = 1;
    self.time = nil;
    [self getDataFromSever:self.time];
    __weak typeof(self)weakSelf = self;
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        [weakSelf getDataFromSever:self.time];
    }];
    
}

-(void)getDataFromSever:(NSString *)time{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@(self.currentPage),
                            @"date":time?:@"",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_recently parameters:param success:^(id  _Nonnull responseObject) {
       if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[RecentListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            }else{
                if ([RecentListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    [MBProgressHUD showError:@"真的没有了~" toView:self.view];
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[RecentListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_rili"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    [CGXPickerView showDatePickerWithTitle:@"选择日期" DateType:UIDatePickerModeDate DefaultSelValue:nil MinDateStr:nil MaxDateStr:nil IsAutoSelect:NO Manager:nil ResultBlock:^(NSString *selectValue) {
        NSLog(@"%@",selectValue);
        self.time = selectValue;
        self.currentPage = 1;
        [self getDataFromSever:selectValue];
       
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  92;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"RecentlyWatchCellID";
    RecentlyWatchCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[RecentlyWatchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
