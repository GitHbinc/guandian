//
//  AuctionViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionViewController.h"
#import "AuctionCell.h"
#import "AuctionListModel.h"

@interface AuctionViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation AuctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的竞价";
    self.view.backgroundColor = colorf3f3f3;
    
    [self createTableView];
    
    [self getDataFromSever];
    
    // Do any additional setup after loading the view.
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_auction parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
                [weakSelf.dataSource addObjectsFromArray:[AuctionListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                
            }
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark 创建tableView
-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = colorf3f3f3;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
}

#pragma mark tableView delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 160;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"AuctionCellID";
    AuctionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AuctionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.block = ^(AuctionListModel *model) {
        
        [[WYNetworking sharedWYNetworking] POST:user_auction_del parameters:@{@"id":model.ID} success:^(id  _Nonnull responseObject) {
            if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                
                { [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                    
                    [self getDataFromSever];
                  
                }
                
            }else{
                
                [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            }
            
        } failure:^(NSError * _Nonnull error) {
            
        }];
    };
        
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
  
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
