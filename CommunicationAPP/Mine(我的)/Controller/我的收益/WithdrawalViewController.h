//
//  WithdrawalViewController.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "BaseViewController.h"
#import "ProfitModel.h"

@interface WithdrawalViewController : BaseViewController
@property(nonatomic ,strong)ProfitModel *model;
@end
