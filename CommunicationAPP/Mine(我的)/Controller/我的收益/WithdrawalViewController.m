//
//  WithdrawalViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "WithdrawalViewController.h"
#import "WithdrawalHeaderView.h"
#import "RechargeCell.h"
#import "ChoosePayTypeView.h"
#import "YFMPaymentView.h"
#import "STPopup.h"
#import "PayTypeModel.h"

@interface WithdrawalViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)PayTypeModel *paymodel;
@property (nonatomic, strong)NSString *timeStr;

@end

@implementation WithdrawalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"出售"];
    [self createRightBtn];
    [self createTableView];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    WithdrawalHeaderView *headerView = [WithdrawalHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 150)];
    __weak typeof(WithdrawalHeaderView *)weakSelf = headerView;
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.timeText = ^(NSString *time) {
        self->_timeStr = time;
    };
    headerView.totalSellClick = ^{
        [weakSelf refreshData];
         self->_timeStr = self.model.times;
    };
    headerView.model = self.model;
    //[headerView refreshData];
    _tableView.tableHeaderView = headerView;
    
    [self createFooterView];
}


-(void)createFooterView{
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, kSCREENWIDTH, 120)];
    footerView.backgroundColor = [UIColor clearColor];
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"确认出售" forState:UIControlStateNormal];
    btn.layer.cornerRadius = 14.0;
    btn.tag = 101;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(footerView.mas_left).offset(30);
        make.right.equalTo(footerView.mas_right).offset(-30);
        make.top.equalTo(footerView.mas_top).offset(20);
        make.height.equalTo(@44);
    }];
    
    _tableView.tableFooterView = footerView;
    
}
-(void)footerBtnClick:(UIButton *)senderBtn{
    if ([self isEmpty:_timeStr]) {
        [MBProgressHUD showError:@"请输入出售时间" toView:self.view];
        return;
    }
    
    if ([self isEmpty:_paymodel.type]) {
        [MBProgressHUD showError:@"请选择支付方式" toView:self.view];
        return;
    }
    
    NSDictionary *param = @{@"times":_timeStr?:@"",
                            @"pay_type":self.paymodel.type?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_sell parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
           
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"RechargePayCellID";
    RechargePayCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[RechargePayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor whiteColor];
    [cell refreshModel:self.paymodel];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *payTypeArr = @[@{@"pic":@"mine_zhifubao",
                              @"title":@"支付宝",
                              @"type":@"alipay"},
                            @{@"pic":@"mine_weixin",
                              @"title":@"微信",
                              @"type":@"wxpay"}
                           ];
    
    YFMPaymentView *pop = [[YFMPaymentView alloc]initTotalPay:@"" vc:self dataSource:payTypeArr];
    STPopupController *popVericodeController = [[STPopupController alloc] initWithRootViewController:pop];
    popVericodeController.style = STPopupStyleBottomSheet;
    [popVericodeController presentInViewController:self];
    
    pop.payType = ^(NSString *type,NSString *balance) {
        NSLog(@"选择了支付方式:%@\n需要支付金额:%@",type,balance);
        if ([type isEqualToString:@"alipay"]) {
            PayTypeModel *model = [PayTypeModel new];
            model.img = @"mine_zhifubao";
            model.name = @"支付宝";
            model.type = @"1";
            self->_paymodel = model;
            [self.tableView reloadData];
        }else{
            PayTypeModel *model = [PayTypeModel new];
            model.img = @"mine_weixin";
            model.name = @"微信";
            model.type = @"2";
            self->_paymodel = model;
            [self.tableView reloadData];
        }
    };
//    [ChoosePayTypeView showWithStyle:PayTypezhifubao ChooseSendBlock:^(NSString *text) {
//
//    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
