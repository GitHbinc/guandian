//
//  ProfitViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "ProfitViewController.h"
#import "ProfitHeaderView.h"
#import "MyPacketCell.h"
#import "WithdrawalViewController.h"
#import "PacketDetailViewController.h"
#import "ProfitModel.h"

@interface ProfitViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) ProfitModel *model ;
@property (nonatomic, strong) ProfitHeaderView *headerView;
@end

@implementation ProfitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"收益"];
    _titleArray = @[@"出售记录",@"出售规则"];
    
    [self createRightBtn];
    
    [self createTableView];
    
    [self getDataFromSever];
}

-(void)getDataFromSever{
    NSDictionary *param = @{
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_profit parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
      
            ProfitModel *model = [[ProfitModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            self->_model = model;
            self->_headerView.model = model;
                
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    ProfitHeaderView *headerView = [ProfitHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 220)];
    headerView.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = headerView;
    _headerView = headerView;
    [self createFooterView];
}



-(void)createFooterView{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, kSCREENWIDTH, 120)];
    footerView.backgroundColor = [UIColor clearColor];
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"时间出售" forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
//    btn.layer.cornerRadius = 14.0;
//    btn.backgroundColor = color666666;
    btn.tag = 101;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(footerView.mas_left).offset(30);
        make.right.equalTo(footerView.mas_right).offset(-30);
        make.top.equalTo(footerView.mas_top).offset(20);
        make.height.equalTo(@44);
    }];
    
//    UIButton *btn1 = [[UIButton alloc]init];
//    [btn1 setTitle:@"兑换券" forState:UIControlStateNormal];
////    btn1.layer.cornerRadius = 14.0;
////    btn1.backgroundColor = color666666;
//    [btn1 setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
//    btn1.tag = 102;
//    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btn1 addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    btn1.titleLabel.font = [UIFont systemFontOfSize:15];
//    [footerView addSubview:btn1];
//    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.equalTo(footerView.mas_left).offset(30);
//        make.right.equalTo(footerView.mas_right).offset(-30);
//        make.top.equalTo(btn.mas_bottom).offset(15);
//        make.height.equalTo(@44);
//    }];
    
    _tableView.tableFooterView = footerView;
    
}
-(void)footerBtnClick:(UIButton *)senderBtn{
    WithdrawalViewController *controller = [[WithdrawalViewController alloc]init];
    controller.model = _model;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _titleArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"MyPacketCellID";
    MyPacketCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[MyPacketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor whiteColor];
    [cell refreshData:_titleArray[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        PacketDetailViewController *controller = [[PacketDetailViewController alloc]init];
        controller.type = RecordTypeSell;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
