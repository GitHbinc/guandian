//
//  TimePartnerViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "TimePartnerViewController.h"
#import "TimePartnerHeaderView.h"
#import "TimePartnerCell.h"

@interface TimePartnerViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *placeholderArray;

@end

@implementation TimePartnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"成为时间合伙人"];
    _titleArray = @[@"姓名",@"公司名称",@"当前地区",@"当前代理",@"邮箱",@"手机号",@"验证码"];
    _placeholderArray = @[@"请填写姓名",@"请输入公司名称",@"请输入地区",@"星系传媒",@"请输入邮箱",@"请输入手机号",@"请输入验证码"];
    [self createRightBtn];
    [self createTableView];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
//    _tableView.backgroundColor = color999999;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    TimePartnerHeaderView *headerView = [TimePartnerHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 390)];
    headerView.backgroundColor = [UIColor whiteColor];
    _tableView.tableHeaderView = headerView;
    [self createFooterView];
}

-(void)createFooterView{
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 100)];
    
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(40, 20, kSCREENWIDTH - 80, 40)];
    [footerBtn setTitle:@"立即申请" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerView addSubview:footerBtn];
    
    UIButton *phoneBtn = [[UIButton alloc]initWithFrame:CGRectMake((kSCREENWIDTH - 150)/2, 70, 150, 20)];
    [phoneBtn setTitleColor:color333333 forState:UIControlStateNormal];
    [phoneBtn setTitle:@"客服电话：400-800-8000" forState:UIControlStateNormal];
    phoneBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [footerView addSubview:phoneBtn];
    _tableView.tableFooterView = footerView;
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _titleArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  75;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 5) {
        
        TimePartnerCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimePartnerCodeCellID"];
        if (cell == nil) {
            cell = [[TimePartnerCodeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimePartnerCodeCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }

    TimePartnerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimePartnerCellID"];
    if (cell == nil) {
        cell = [[TimePartnerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimePartnerCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    [cell refreshData:_titleArray[indexPath.row] placeholderStr:_placeholderArray[indexPath.row] tag:indexPath.row + 101];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
