//
//  SetUpViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SetUpViewController.h"
#import "AccountWithSecurityController.h"
#import "MessageSetupController.h"
#import "PrivacyViewController.h"
#import "BlacklistViewController.h"
#import "LoginViewController.h"
#import "SetUpViewCell.h"
#import "SetUpMiddleCell.h"
#import "MineItem.h"

@interface SetUpViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation SetUpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"设置"];
     [self setNaviBarButtonItem];
     [self loadTableView];
     NSArray *titleArray = @[@"账号与安全",
                            @"隐私",
                            @"消息设置",
                            @"黑名单",
                            @"清除缓存",
                            @"检查更新",
                            @"给我们评分",
                            @"关于我们",
                            @"切换账户",
                            @"退出登录"];
    for (int i = 0; i <titleArray.count; i++) {
        MineItem *item = [[MineItem alloc]init];
        item.name = titleArray[i];
        [self.dataSource addObject:item];
    }
     [self.tableView reloadData];
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }else if (section ==2){
        return 3;
    }
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        static NSString *identifier = @"SetUpViewCell";
        SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section];
        
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.section == 1){
        static NSString *identifier = @"SetUpViewCell";
        SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section *4];
        
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if (indexPath.section == 2){
        static NSString *identifier = @"SetUpViewCell";
        SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +(indexPath.section +3)];
        
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else{
        
        static NSString *identifier = @"SetUpMiddleCell";
        SetUpMiddleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[SetUpMiddleCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +(indexPath.section +5)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row==0) {//账户与安全
            
            AccountWithSecurityController *controller = [[AccountWithSecurityController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }else if (indexPath.row ==1){//隐私
            PrivacyViewController *controller = [[PrivacyViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }else if (indexPath.row ==2){//消息设置
            MessageSetupController *controller = [[MessageSetupController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }else{ //黑名单
            BlacklistViewController *controller = [[BlacklistViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        
    }else if (indexPath.section == 1){//清除缓存
        [self clearMemory];
       
    }else  if (indexPath.section == 2){
        if (indexPath.row==0) {//检查更新
            
            
        }else if (indexPath.row ==1){ //给我们评分
            
            
        }else{ //关于我们
           
            
        }
    }else if (indexPath.section == 3){//切换账户
        
    }else{//退出登录
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定要退出嘛?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"点击了取消");
        }];
        
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            UserHander *user = [UserHander shareHander];
            user.loginUserInfo = nil;
            [[UserHander shareHander] autoSave];
            
            //保存登录状态 跟 用户的ID
            [kUserDefault setObject:@(NO) forKey:LOGINSTATUS];
            [kUserDefault setObject:@"" forKey:USERID];
            
            [kUserDefault synchronize];
            LoginViewController *controller = [[LoginViewController alloc]init];
            [self.navigationController pushViewController:controller animated:true];
           
        }];
        [alertController addAction:action];
        [alertController addAction:action1];
        [self presentViewController:alertController animated:YES completion:nil];

    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==4) {
        return 0.0;
    }else
        return 10.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc]init];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)shareAction{
    
}

-(void)clearMemory{
    NSFileManager *manager = [NSFileManager defaultManager];
    NSArray *paths = nil;
    paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSMutableString *cachepath = [paths objectAtIndex:0];
    NSError *error = nil;
    NSArray *fileList = [manager contentsOfDirectoryAtPath:cachepath error:&error];
    for (NSString *file in fileList) {
        NSString *path = [cachepath stringByAppendingPathComponent:file];
        if ([manager isDeletableFileAtPath:path]) {
            [manager removeItemAtPath:path error:nil];
        }
    }
    [[SDImageCache sharedImageCache]clearMemory];
    [MBProgressHUD showOnlyTextToView:self.view title:@"缓存已清理"];
    [self.tableView reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
