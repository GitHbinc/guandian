//
//  PacketDetailViewController.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSInteger, RecordType)
{
    RecordTypeDianbao,       //点包明细
    RecordTypeSell,          //出售明细
    
};


@interface PacketDetailViewController : BaseViewController
@property(nonatomic ,assign)RecordType type;

@end
