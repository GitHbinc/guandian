//
//  AddBankCardViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "AddBankCardViewController.h"
#import "AddBankCardCell.h"

@interface AddBankCardViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *placeholderArray;

@end

@implementation AddBankCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"添加银行卡"];
    _titleArray = @[@"姓名",@"证件号",@"开户行",@"银行卡号",@"手机号"];
    _placeholderArray = @[@"请输入持卡人姓名",@"请输入证件号",@"请选择开户银行",@"请输入银行卡号",@"请输入银行预留手机号码"];
    [self createRightBtn];
    [self createTableView];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [self createFooterView];
}


-(void)createFooterView{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, kSCREENWIDTH, 60)];
    footerView.backgroundColor = [UIColor clearColor];
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"下一步" forState:UIControlStateNormal];
    btn.layer.cornerRadius = 14.0;
    btn.backgroundColor = color666666;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(footerView.mas_left).offset(30);
        make.right.equalTo(footerView.mas_right).offset(-30);
        make.centerY.equalTo(footerView.mas_centerY);
        make.height.equalTo(@44);
    }];
    
    _tableView.tableFooterView = footerView;
    
}
-(void)footerBtnClick{
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return 2;
    }
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, kSCREENWIDTH, 20)];
        label.textColor = color666666;
        label.font = [UIFont systemFontOfSize:13];
        label.text = @"请绑定持卡本人的银行卡";
        [view addSubview:label];
        return view;
    }
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 30;
    }
    
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AddBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddBankCardCellID"];
    if (cell == nil) {
        cell = [[AddBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddBankCardCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    NSInteger row = indexPath.row;
    if (indexPath.section == 1) {
        row= 2 + indexPath.row;
    }
    [cell refreshModel:_titleArray[row] placeholderStr:_placeholderArray[row] row:row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
