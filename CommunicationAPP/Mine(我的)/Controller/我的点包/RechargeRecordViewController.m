//
//  RechargeRecordViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "RechargeRecordViewController.h"
#import "PacketDetailCell.h"

@interface RechargeRecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation RechargeRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"购买记录"];
    [self createTableView];
    [self createRightBtn];
    
     //[self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_blacklist parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
//            [weakSelf.dataSource addObjectsFromArray:[BlacklistModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    //    _tableView.backgroundColor = color999999;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  65;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PacketDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PacketDetailCellID"];
    if (cell == nil) {
        cell = [[PacketDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PacketDetailCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
