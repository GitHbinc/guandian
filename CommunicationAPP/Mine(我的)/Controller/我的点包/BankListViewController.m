//
//  BankListViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "BankListViewController.h"
#import "BankListCell.h"
#import "AddBankCardViewController.h"

@interface BankListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation BankListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"银行卡"];
    [self createRightBtn];
    [self createTableView];
    [self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_banklist parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            {
//                [weakSelf.dataSource addObjectsFromArray:[BlacklistModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                
            }
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    //    _tableView.backgroundColor = color999999;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [self createFooterView];
}


-(void)createFooterView{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, kSCREENWIDTH, 40)];
    footerView.backgroundColor = [UIColor clearColor];
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"+ 添加银行卡" forState:UIControlStateNormal];
    btn.layer.cornerRadius = 14.0;
    btn.backgroundColor = color666666;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(footerView.mas_centerX);
        make.centerY.equalTo(footerView.mas_centerY);
        make.height.equalTo(@27);
        make.width.equalTo(@100);
    }];
    
    _tableView.tableFooterView = footerView;
    
}
-(void)footerBtnClick{
    AddBankCardViewController *bank = [[AddBankCardViewController alloc]init];
    [self.navigationController pushViewController:bank animated:YES];
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  160;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 15;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        BankListNoneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankListNoneCellID"];
        if (cell == nil) {
            cell = [[BankListNoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BankListNoneCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }
    BankListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankListCellID"];
    if (cell == nil) {
        cell = [[BankListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BankListCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    [cell refreshModel];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
