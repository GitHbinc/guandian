//
//  RechargeViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "RechargeViewController.h"
#import "MineRechargeView.h"
#import "RechargeCell.h"
#import "ChoosePayTypeView.h"
#import "YFMPaymentView.h"
#import "STPopup.h"
#import "MealModel.h"
#import "RechargeRecordViewController.h"
#import "MyWebViewController.h"

@interface RechargeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *leftArray;
@property (nonatomic, strong)PayTypeModel *paymodel;
@property (nonatomic ,strong)MineRechargeView *headView;
@property (nonatomic, strong)DianListModel *model;
@property (nonatomic, copy)NSString *money;
@end

@implementation RechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"购买"];
    
    [self createRightBtn];
    
    [self createTableView];
    
    [self getDataFromSever];
    
}

-(void)getDataFromSever{
    
     __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_setmeal parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            MealModel* model = [[MealModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            weakSelf.headView.model = model;
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor =  HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    MineRechargeView *headerView = [MineRechargeView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 270)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.selectDianClick = ^(DianListModel * _Nonnull model) {//点币选择
        self->_model = model;
    };
    _tableView.tableHeaderView = headerView;
    _headView = headerView;
    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 120)];
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 25, kSCREENWIDTH - 76, 44)];
    [footerBtn setTitle:@"确认购买" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    footerBtn.tag = 101;
    [footerBtn addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerBtn];
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"已阅读并同意《用户购买协议》" forState:UIControlStateNormal];
    [btn setTitleColor:color666666 forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    btn.tag = 102;
    [btn addTarget:self action:@selector(footerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(footerBtn.mas_bottom).offset(5);
        make.centerX.equalTo(footerView.mas_centerX);
        make.width.equalTo(@200);
    }];
    _tableView.tableFooterView = footerView;
    
}

-(void)footerBtnClick:(UIButton *)senderBtn{
    
    if (senderBtn.tag == 101) {
        
    if ([self isEmpty:_model.timer]&&[self isEmpty:_money]) {
        [MBProgressHUD showError:@"请输入购买金额" toView:self.view];
        return;
    }
    
    NSString *moneyStr = nil;
    NSString *timeStr = nil;
    if (![self isEmpty:_money]) {
        moneyStr = _money;
        timeStr = [NSString stringWithFormat:@"%.1f",[_money floatValue]/6];
    }else{
        if ([_model.timer isEqualToString:@"时间合伙人"]) {
            moneyStr = @"3600";
            timeStr = @"1000";
        }else{
            moneyStr = _model.prices;
            timeStr = _model.timer;
        }
        
    }

    if ([self isEmpty:_paymodel.type]) {
        [MBProgressHUD showError:@"请选择支付方式" toView:self.view];
        return;
    }
    
    NSDictionary *param = @{@"times":timeStr,
                            @"payprice":moneyStr,
                            @"pay_type":self.paymodel.type?:@"",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_buytimeorder parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
    }else{
        
        MyWebViewController *controller = [[MyWebViewController alloc] init];
        controller.url = @"http://agent.gdhime.com/Wechat.php/agent/tixianguize";
        controller.naviTitle = @"用户购买协议";
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)createRightBtn{
    
    UIBarButtonItem * rightItem=  [self getBarButtonItemWithTitleStr:@"购买记录" Sel:@selector(rightItemAction)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)rightItemAction{
    RechargeRecordViewController * controller = [[RechargeRecordViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES]; 
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 63;
    }
    return  44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        RechargeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RechargeCellID"];
        if (cell == nil) {
            cell = [[RechargeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RechargeCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        cell.inputClick = ^(NSString *time) {//输入点币
            self.money = time;
        };
        return cell;
    }
    RechargePayCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RechargePayCellID"];
    if (cell == nil) {
        cell = [[RechargePayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RechargePayCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell refreshModel:self.paymodel];
  
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        NSArray *payTypeArr = @[@{@"pic":@"mine_zhifubao",
                                  @"title":@"支付宝",
                                  @"type":@"alipay"},
                                @{@"pic":@"mine_weixin",
                                  @"title":@"微信",
                                  @"type":@"wxpay"}
                                ];
        
        YFMPaymentView *pop = [[YFMPaymentView alloc]initTotalPay:@"" vc:self dataSource:payTypeArr];
        STPopupController *popVericodeController = [[STPopupController alloc] initWithRootViewController:pop];
        popVericodeController.style = STPopupStyleBottomSheet;
        [popVericodeController presentInViewController:self];
        
        pop.payType = ^(NSString *type,NSString *balance) {
            NSLog(@"选择了支付方式:%@\n需要支付金额:%@",type,balance);
            if ([type isEqualToString:@"alipay"]) {
                PayTypeModel *model = [PayTypeModel new];
                model.img = @"mine_zhifubao";
                model.name = @"支付宝";
                model.type = @"1";
                self->_paymodel = model;
                [self.tableView reloadData];
            }else{
                PayTypeModel *model = [PayTypeModel new];
                model.img = @"mine_weixin";
                model.name = @"微信";
                model.type = @"2";
                self->_paymodel = model;
                [self.tableView reloadData];
            }
        };
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
