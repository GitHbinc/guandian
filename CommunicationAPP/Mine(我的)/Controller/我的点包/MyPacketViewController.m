//
//  MyPacketViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "MyPacketViewController.h"
#import "MyPacketCell.h"
#import "MyPacketView.h"
#import "BankListViewController.h"
#import "PacketDetailViewController.h"
#import "PacketModel.h"
#import "RechargeViewController.h"


@interface MyPacketViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *leftArray;
@property (nonatomic, strong) PacketModel *model ;
@property (nonatomic, strong) MyPacketView *headerView;
@end

@implementation MyPacketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"我的点包"];
    [self createTableView];
    [self createRightBtn];
    _leftArray = @[@"银行卡",@"点包明细",@"直播间收支记录",@"冻结记录"];
    
    [self getDataFromSever];
    
}

-(void)getDataFromSever{
    NSDictionary *param = @{
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_balancesinfo parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            PacketModel *model = [[PacketModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            self->_model = model;
            self->_headerView.model = model;
 
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor =  HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    MyPacketView *headerView = [MyPacketView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 220)];
    [headerView refreshModel];
    _headerView = headerView;
    _tableView.tableHeaderView = headerView;
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    UIView *footerView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 74)];
    
    UIButton *footerBtn = [[UIButton alloc]initWithFrame:CGRectMake(38, 30, kSCREENWIDTH - 38*2, 44)];
    [footerBtn setTitle:@"我要购买" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [footerBtn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:footerBtn];
    _tableView.tableFooterView = footerView;
    
}

-(void)footerBtnClick{
    RechargeViewController *controller = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _leftArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  44;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"MyPacketCellID";
    MyPacketCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[MyPacketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    [cell refreshData:_leftArray[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
//        BankListViewController *controller = [[BankListViewController alloc]init];
//        [self.navigationController pushViewController:controller animated:YES];
        
    }else if (indexPath.row == 1){
        PacketDetailViewController *controller = [[PacketDetailViewController alloc]init];
        controller.type = RecordTypeDianbao;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 2){
        
    }else{
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
