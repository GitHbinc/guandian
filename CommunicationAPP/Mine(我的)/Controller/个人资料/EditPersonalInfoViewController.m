//
//  EditPersonalInfoViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "EditPersonalInfoViewController.h"
#import "EditPersonInfoCell.h"
#import "EditHeaderView.h"
#import "ChangeNickNameViewController.h"
#import "AutographViewController.h"
#import "CGXPickerView.h"

@interface EditPersonalInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *leftArray;
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic, strong) UserModel *user;
@property (nonatomic, strong) EditHeaderView *headView;
@end

@implementation EditPersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"编辑个人资料"];
    [self createTableView];
    _leftArray = @[@"昵称",@"观点号",@"性别",@"生日",@"签名",@"所在地"];
   
    [self createRightBtn];
   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self getDataFromSever];
}

-(void)getDataFromSever{
    
    [[WYNetworking sharedWYNetworking] POST:user_userinfo parameters:@{
                                                                       @"uid" :[kUserDefault objectForKey:USERID]?:@"",
                                                                       
} success:^(id  _Nonnull responseObject) {
                                                                           
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
        
        UserModel *user = [[UserModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
        self.user = user;
        [[UserHander shareHander] setLoginUserInfo:user];
        //自动保存登录信息
        [[UserHander shareHander] autoSave];
        
            self.dataSource= [@[![self isEmpty:user.username]?user.username:@"",
                               user.ID,
                               ![self isEmpty:user.sex]?[user.sex isEqualToString:@"1"]?@"男":@"女":@"",
                                 ![self isEmpty:user.birth]?user.birth:@"",
                                 ![self isEmpty:user.autograph]?user.autograph:@"",
                                 ![self isEmpty:user.area]?user.area:@""
                                 ]mutableCopy];
            
            [self->_headView.headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,user.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
            
            [self.tableView reloadData];
            
        }
    
} failure:^(NSError * _Nonnull error) {
    
}];
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    EditHeaderView *headView =[EditHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 180)];
    headView.headbuttonClick = ^(UIButton *btn) {
        UIAlertController *sheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action1  = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self openCamera];
        }];
        [sheet addAction:action1];
        
        UIAlertAction *action2  = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self openAlbum];
        }];
        [sheet addAction:action2];
        UIAlertAction *action3  = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [sheet addAction:action3];
        [self presentViewController:sheet animated:YES completion:nil];
    };
    self.headView = headView;
    _tableView.tableHeaderView = headView;
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _leftArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellID = @"editPersonInfoCellID";
    EditPersonInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[EditPersonInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.leftLabel.text = _leftArray[indexPath.row];
    if (self.dataSource.count>0) {
         [cell refreshData:self.dataSource[indexPath.row]];
    }
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            {
                ChangeNickNameViewController *controller = [[ChangeNickNameViewController alloc]init];
                controller.name = self.user.username;
                [self.navigationController pushViewController:controller animated:true];
            }
            break;
        case 2:
        {
            [CGXPickerView showStringPickerWithTitle:@"性别" DefaultSelValue:nil IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
                NSString *sex = nil;
                if ([selectRow floatValue] == 1) {
                    sex = @"2";
                }else if ([selectRow floatValue] == 2){
                    sex = @"1";
                }else{
                    return;
                }
                NSDictionary *param = @{
                                        @"sex" :sex,
                                        @"uid" :[kUserDefault objectForKey:USERID]?:@""
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:user_editsex parameters:param success:^(id  _Nonnull responseObject) {
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                    
                    if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                        
                        [self getDataFromSever];
                    }
                    
                } failure:^(NSError * _Nonnull error) {
                    
                }];
            } Style:CGXStringPickerViewStyleGender];
        }
            
            break;
        case 3:
        {
            [CGXPickerView showDatePickerWithTitle:@"生日" DateType:UIDatePickerModeDate DefaultSelValue:nil MinDateStr:nil MaxDateStr:nil IsAutoSelect:NO Manager:nil ResultBlock:^(NSString *selectValue) {
                
                NSLog(@"%@",selectValue);
                NSDictionary *param = @{
                                        @"birth" :selectValue,
                                        @"uid" :[kUserDefault objectForKey:USERID]?:@""
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:user_editbirth parameters:param success:^(id  _Nonnull responseObject) {
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                    
                    if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                        
                        [self getDataFromSever];
                    }
                    
                } failure:^(NSError * _Nonnull error) {
                    
                }];
               
         }];
        }
            break;
        case 4:
           {
               AutographViewController *controller = [[AutographViewController alloc]init];
               [self.navigationController pushViewController:controller animated:true];
            
           }
               break;
        case 5:
        {
            [CGXPickerView showAddressPickerWithTitle:@"请选择你的城市" DefaultSelected:@[@4, @0,@0] IsAutoSelect:NO Manager:nil ResultBlock:^(NSArray *selectAddressArr, NSArray *selectAddressRow) {
                NSLog(@"%@-%@",selectAddressArr,selectAddressRow);
                NSString *str = [selectAddressArr componentsJoinedByString:@"-"];
                NSDictionary *param = @{
                                        @"area" :str,
                                        @"uid" :[kUserDefault objectForKey:USERID]?:@""
                                        };
                
                [[WYNetworking sharedWYNetworking] POST:user_editarea parameters:param success:^(id  _Nonnull responseObject) {
                    [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                    
                    if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                        
                        [self getDataFromSever];
                    }
                    
                } failure:^(NSError * _Nonnull error) {
                    
                }];
               
            }];
        }
            
            break;
        default:
            break;
    }
}


//相机
- (void)openCamera
{
    UIImagePickerController *pc = [[UIImagePickerController alloc] init];
    pc.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //制定资源的类型 ,要先判断资源类型是否可用，如果可用再设置
        [pc setSourceType:UIImagePickerControllerSourceTypeCamera];
        //允许编辑
        pc.allowsEditing = YES;
        //根据开发习惯，UIImagePickerController一般他的出现方式时用presentViewCOntroller
        [self presentViewController:pc animated:YES completion:nil];
    } else {
        [MBProgressHUD showError:@"无法使用相机功能" toView:self.view];
    }
}

//相册
- (void)openAlbum
{
    UIImagePickerController *pc = [[UIImagePickerController alloc] init];
    pc.navigationBar.translucent = NO;
    pc.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        [pc setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        pc.allowsEditing = YES;
        [self presentViewController:pc animated:YES completion:nil];
    } else {
        [MBProgressHUD showError:@"无法使用相机功能" toView:self.view];
    }
}

#pragma mark --- UIImagePickerController 代理
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //能选择的照片和视频
    NSString *typeStr = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([typeStr isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        UIImage *img = [ImageService straightenAndScaleImage:image maxDimension:200];
       
        [self uploadImageWithImageData:[self imageBsae64DataWithImage:img]];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//UIImage图片转成Base64字符串：
- (NSString *)imageBsae64DataWithImage:(UIImage *)image
{
    NSData *data = UIImageJPEGRepresentation(image, 1.0f);
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return encodedImageStr;
    //NSLog(@"encodedImageStr == %@",encodedImageStr);
}

#pragma mark ----  上传文件 -----------
- (void)uploadImageWithImageData:(NSString *)imageData{
    NSDictionary *param = @{
                            @"imgKey" :imageData,
                            @"uid" :[kUserDefault objectForKey:USERID]?:@""
                            };
    
    [[WYNetworking sharedWYNetworking] POST:user_uploadphoto parameters:param success:^(id  _Nonnull responseObject) {
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            [self getDataFromSever];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
