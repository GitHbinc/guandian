//
//  ChangeNickNameViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "ChangeNickNameViewController.h"

@interface ChangeNickNameViewController ()

@property (nonatomic, strong)UITextField *nickNameTextField;

@end

@implementation ChangeNickNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self setTitle:@"昵称"];
    [self setUI];
    [self createRightBtn];
    
    _nickNameTextField.text = self.name;
}

-(void)setUI{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, Navi_STATUS_HEIGHT+20, kSCREENWIDTH, 44)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    _nickNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, kSCREENWIDTH-20, 44)];
    _nickNameTextField.backgroundColor = [UIColor whiteColor];
    _nickNameTextField.textColor = color333333;
    _nickNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _nickNameTextField.font = [UIFont systemFontOfSize:14];
    _nickNameTextField.placeholder = @"请输入昵称";
    [view addSubview:_nickNameTextField];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10,Navi_STATUS_HEIGHT+70, 200, 20)];
    label.textColor = color999999;
    label.font = [UIFont systemFontOfSize:10];
    label.text = @"15天内只能修改一次";
    [self.view addSubview:label];
    
}


-(void)createRightBtn{
    UIBarButtonItem * saveBtn = [self getBarButtonItemWithTitleStr:@"保存" Sel:@selector(rightItemAction)];
    self.navigationItem.rightBarButtonItems = @[saveBtn];
}

-(void)rightItemAction{
    
    NSDictionary *param = @{
                            @"username" :self.nickNameTextField.text?:@"",
                             @"uid" :[kUserDefault objectForKey:USERID]?:@""
                            };
    
    [[WYNetworking sharedWYNetworking] POST:user_editusername parameters:param success:^(id  _Nonnull responseObject) {
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            __weak typeof(self)weakSelf = self;
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0/*延迟执行时间*/ * NSEC_PER_SEC));
            
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
               [weakSelf.navigationController popViewControllerAnimated:YES];
            });
            
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}
    




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
