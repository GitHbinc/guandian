//
//  AutographViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "AutographViewController.h"

@interface AutographViewController ()<UITextViewDelegate>

@property (nonatomic, strong)IQTextView *textView;
@property (nonatomic, strong)UILabel *wordsNumberLabel;

@end

@implementation AutographViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"签名"];
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self createRightBtn];
    [self createTextView];
    
}

-(void)createTextView{
    _textView = [[IQTextView alloc]initWithFrame:CGRectMake(0, Navi_STATUS_HEIGHT+20, kSCREENWIDTH, 100)];
    _textView.font = [UIFont systemFontOfSize:14];
    _textView.textColor = color333333;
    _textView.placeholder = @"填写个性签名更容易获得别人的关注哦～";
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.delegate = self;
    [self.view addSubview:_textView];
    
    _wordsNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, Navi_STATUS_HEIGHT+130, 200, 20)];
    _wordsNumberLabel.text = @"剩余输入字数20";
    _wordsNumberLabel.textColor = color999999;
    _wordsNumberLabel.font = [UIFont systemFontOfSize:11];
    [self.view addSubview:_wordsNumberLabel];
    
}

-(void)createRightBtn{
    UIBarButtonItem * saveBtn = [self getBarButtonItemWithTitleStr:@"保存" Sel:@selector(rightItemAction)];
    self.navigationItem.rightBarButtonItems = @[saveBtn];
}

-(void)rightItemAction{
    NSDictionary *param = @{
                            @"autograph" :self.textView.text?:@"",
                            @"uid" :[kUserDefault objectForKey:USERID]?:@""
                            };
    
    [[WYNetworking sharedWYNetworking] POST:user_editautograph parameters:param success:^(id  _Nonnull responseObject) {
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            __weak typeof(self)weakSelf = self;
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0/*延迟执行时间*/ * NSEC_PER_SEC));
            
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            });
            
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];

}


// 可输入的最大字数
#define MAX_LIMIT_NUMS 2000
#pragma mark - UITextViewDelegate
// 代理方法
- (void)textViewDidChange:(UITextView *)textView {
    NSString *lang = textView.textInputMode.primaryLanguage;// 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) {
        // 中文输入
        UITextRange *selectedRange = [textView markedTextRange];
        if (!selectedRange) {
            // 没有有高亮
            [self textSelectionRangeWithText:textView];
        }
    } else {
        [self textSelectionRangeWithText:textView];
    }
}

// 定位光标
- (void)textSelectionRangeWithText:(UITextView *)textView {
    NSRange oldRange = _textView.selectedRange;
    // 判断是否有候选字符，如果不为nil，代表有候选字符
    if(textView.markedTextRange == nil){
        [self textViewSetText:textView];
        textView.selectedRange = oldRange;  // 改变内容时光标位置不变
    }
}

// 自己的textView显示文字
- (void)textViewSetText:(UITextView *)textView {
    NSString  *nsTextContent = textView.text;
    NSInteger existTextNum = nsTextContent.length;
    if (existTextNum > 20) {
        //截取到最大位置的字符
        
//        [textView.text substringToIndex:20];
        NSString *string = [nsTextContent substringToIndex:20];
//        NSMutableAttributedString *attributedString = [self textAttributedStringWithString:string];
        [textView setText:string];
//        textView.attributedText = attributedString;
//        [_textView scrollRangeToVisible:NSMakeRange(_textView.text.length, 1)];
        _wordsNumberLabel.text = [NSString stringWithFormat:@"剩余输入字数0"];
    }   else {
//        NSMutableAttributedString *attributedString = [self textAttributedStringWithString:nsTextContent];
//        textView.attributedText = attributedString;
        // 提示label更新字数提示信息
        _wordsNumberLabel.text = [NSString stringWithFormat:@"剩余输入字数%ld", 20 - existTextNum];
    }
    
}

//// 富文本设置
//- (NSMutableAttributedString *)textAttributedStringWithString:(NSString *)string {
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
//    // 字号
//    [attributedString addAttribute: NSFontAttributeName value:[UIFont systemFontOfSize:16] range: NSMakeRange(0, attributedString.length)];
//    // 字体颜色
//    [attributedString addAttribute: NSForegroundColorAttributeName value:[UIColor mediaTableSectionColor] range: NSMakeRange(0, attributedString.length)];
//    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
//    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
//    paraStyle.alignment = NSTextAlignmentLeft;
//    paraStyle.lineSpacing = 12;
//    paraStyle.paragraphSpacing = 18;
//    [attributedString addAttribute: NSParagraphStyleAttributeName value:paraStyle range: NSMakeRange(0, attributedString.length)];
//    return attributedString;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
