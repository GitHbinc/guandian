//
//  PersonalCenterViewController.m
//  PersonalCenter
//
//  Created by Arch on 2017/6/16.
//  Copyright © 2017年 mint_bin. All rights reserved.
//

#import "PersonalCenterViewController.h"
#import "SegmentView.h"
#import "SecondViewController.h"
#import "CenterTouchTableView.h"
#import "QMUIButton.h"
#import "PersonalInformationController.h"
#import "PersonalWorksController.h"
#import "MineDynamicViewController.h"
#import "ContributionViewController.h"
#import "EditPersonalInfoViewController.h"

//static CGFloat const HeaderImageViewHeight = 230;

#define HeaderImageViewHeight (190 + STATUS_BAR_HEIGHT)

@interface PersonalCenterViewController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
@property (nonatomic, strong) CenterTouchTableView *mainTableView;
@property (nonatomic, strong) SegmentView *segmentView;
@property (nonatomic, strong) UIView *naviView;
@property (nonatomic, strong) UIImageView *headerImageView;
@property (nonatomic, strong) UIView *headerContentView;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *nunberLabel;
@property (nonatomic, strong) UILabel *attentionLabel;
@property (nonatomic, strong) UILabel *fensiLabel;
@property (nonatomic, strong) QMUIButton *livingBtn;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *messageButton;
/** mainTableView是否可以滚动 */
@property (nonatomic, assign) BOOL canScroll;
/** segmentHeaderView到达顶部, mainTableView不能移动 */
@property (nonatomic, assign) BOOL isTopIsCanNotMoveTabView;
/** segmentHeaderView离开顶部,childViewController的滚动视图不能移动 */
@property (nonatomic, assign) BOOL isTopIsCanNotMoveTabViewPre;
/** 是否正在pop */
@property (nonatomic, assign) BOOL isBacking;
@property(nonatomic ,strong) UserModel *user;

@end

@implementation PersonalCenterViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人中心";
//    if (@available(iOS 11.0, *)) {
//        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
//    }else {
//        self.automaticallyAdjustsScrollViewInsets = NO;
//    }
    //如果使用自定义的按钮去替换系统默认返回按钮，会出现滑动返回手势失效的情况
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self setupSubViews];
    //注册允许外层tableView滚动通知-解决和分页视图的上下滑动冲突问题
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptMsg:) name:@"leaveTop" object:nil];
    //分页的scrollView左右滑动的时候禁止mainTableView滑动，停止滑动的时候允许mainTableView滑动
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptMsg:) name:IsEnablePersonalCenterVCMainTableViewScroll object:nil];
    //切换分页选项的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptMsg:) name:CurrentSelectedChildViewControllerIndex object:nil];
    
    if (!self.isMineCenter) {
        [self creatBottonView];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:LOGINSTATUS] && [[userDefaults objectForKey:LOGINSTATUS] boolValue] == YES) {
        [self getDataFromSever];
    }
     self.navigationController.navigationBarHidden = YES;
    self.naviView.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.isBacking = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:PersonalCenterVCBackingStatus object:nil userInfo:@{@"isBacking" : @(self.isBacking)}];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.isBacking = YES;
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:PersonalCenterVCBackingStatus object:nil userInfo:@{@"isBacking" : @(self.isBacking)}];
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentAutomatic];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO];
    self.naviView.hidden = YES;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)getDataFromSever{
    NSDictionary *param = @{
                            @"uid":self.userID?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_userinfo parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            NSDictionary *dic = [responseObject objectForKey:@"data"];
            UserModel *user = [[UserModel alloc]initWithDictionary:[responseObject objectForKey:@"data"] error:nil];
            self.user = user;
            [self->_avatarImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[dic objectForKey:@"img"]]] placeholderImage:[UIImage imageNamed:@"default_square"]];
            
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[dic objectForKey:@"img"]]]];
            UIImage *result = [UIImage imageWithData:data];
          self-> _headerImageView.image =[result qmui_imageWithAlpha:0.6];
//            [self->_headerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[dic objectForKey:@"img"]]] placeholderImage:[UIImage imageNamed:@"default_long"]];
            
            self->_nickNameLabel.text = [dic objectForKey:@"username"];
            self->_nunberLabel.text =[NSString stringWithFormat:@"观点号：%@",[dic objectForKey:@"id"]];
            self->_attentionLabel.text = [NSString stringWithFormat:@"%@关注",[dic objectForKey:@"myfollow"]];
            self->_fensiLabel.text = [NSString stringWithFormat:@"%@粉丝",[dic objectForKey:@"myfans"]];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:PersonalCenterVCUSERDATA object:nil userInfo:dic];
            
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark - Private Methods
-(void)creatBottonView{
    UIView * bottomView = [UIView new];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    bottomView.layer.shadowOffset = CGSizeMake(5, -5);
    bottomView.layer.shadowOpacity = 0.2;
    bottomView.layer.shadowRadius = 3;
    bottomView.layer.shadowColor = [UIColor grayColor].CGColor;
    
    self.bottomView = bottomView;
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(@50);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(kIs_iPhoneX?-34:0);
    }];
    
    QMUIButton *btn1 = [QMUIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setTitle:@"私聊" forState:UIControlStateNormal];
    btn1.spacingBetweenImageAndTitle = 2;
    btn1.tag = 1001;
    [btn1.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btn1 setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    [btn1 setImage:[UIImage imageNamed:@"rank_pinglun"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(self.bottomView);
        make.width.mas_equalTo(kSCREENWIDTH/2);
        
    }];
    
    QMUIButton *btn2 = [QMUIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setTitle:@"关注" forState:UIControlStateNormal];
    [btn2 setTitle:@"已关注" forState:UIControlStateSelected];
    [btn2 setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    btn2.spacingBetweenImageAndTitle = 2;
    btn2.tag = 1002;
    [btn2.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btn2 setImage:[UIImage imageNamed:@"tianjiahaoyou"] forState:UIControlStateNormal];
    [btn2 setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
    [btn2 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btn2];
    
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(self.bottomView);
        make.width.mas_equalTo(kSCREENWIDTH/2);
        
    }];
    
    UILabel *line = [UILabel new];
    line.backgroundColor =  [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
    [bottomView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(1, 30));
        make.center.mas_equalTo(bottomView);
        
    }];
    
    
}

-(void)clickAction:(QMUIButton *)sender{
    switch (sender.tag) {//私聊
        case 1001:
            {
                
            }
            break;
        case 1002://关注
        {
            sender.selected = !sender.selected;
        }
            break;
            
        default:
            break;
    }
}

- (void)setupSubViews {
    [self.view addSubview:self.mainTableView];
    [self.view addSubview:self.naviView];
    [self.headerContentView addSubview:self.headerImageView];
    [self.headerContentView addSubview:self.avatarImageView];
    [self.headerContentView addSubview:self.nickNameLabel];
    [self.headerContentView addSubview:self.nunberLabel];
    [self.headerContentView addSubview:self.attentionLabel];
    [self.headerContentView addSubview:self.fensiLabel];
    [self.headerContentView addSubview:self.livingBtn];
    [self.mainTableView addSubview:self.headerContentView];
    
    UILabel *lineLabel = [UILabel new];
    lineLabel.backgroundColor = [UIColor whiteColor];
    [self.headerContentView addSubview:lineLabel];
    
    UIView *bottonView = [UIView new];
    bottonView.backgroundColor = [UIColor whiteColor];
    [self.headerContentView addSubview:bottonView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(clickCon)];
    [bottonView addGestureRecognizer:tap];
    
    UIImageView *love = [UIImageView new];
    love.image = [UIImage imageNamed:@"rank_love"];
    [bottonView addSubview:love];
    
    UILabel *conLabel = [UILabel new];
    conLabel.text = @"贡献榜";
    conLabel.textColor = HEXCOLOR(0x111111);
    conLabel.font = [UIFont systemFontOfSize:14];
    [bottonView addSubview:conLabel];
    
    UIImageView *rightImage = [UIImageView new];
    rightImage.image = [UIImage imageNamed:@"mine_xiangyou"];
    [bottonView addSubview:rightImage];
    
    
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.headerContentView.mas_bottom).offset(-60);
        make.top.left.right.mas_equalTo(self.headerContentView);
    }];
    
    [bottonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerImageView.mas_bottom);
        make.left.right.mas_equalTo(self.headerContentView);
        make.height.equalTo(@50);
    }];
    
    [love mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bottonView).offset(10);
        make.centerY.mas_equalTo(bottonView);
        make.size.mas_equalTo(CGSizeMake(20, 16));
    }];
    
    [conLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(love.mas_right).offset(10);
        make.centerY.mas_equalTo(bottonView);
    }];
    
    [rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bottonView.mas_right).offset(-20);
        make.centerY.mas_equalTo(bottonView);
        make.size.mas_equalTo(CGSizeMake(8, 10));
    }];
    
    
    
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerImageView).offset(STATUS_BAR_HEIGHT + 44 +5);
        make.size.mas_equalTo(CGSizeMake(64, 64));
        make.left.equalTo(self.headerImageView).offset(20);
       
    }];
    
    [self.nunberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.avatarImageView.mas_right).offset(10);
            make.width.mas_lessThanOrEqualTo(200);
            make.centerY.mas_equalTo(self.avatarImageView.mas_centerY);
        }];
    
    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
        make.width.mas_lessThanOrEqualTo(200);
        make.bottom.mas_equalTo(self.nunberLabel.mas_top).offset(-6);
    }];
    
    [self.attentionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
        make.width.mas_lessThanOrEqualTo(200);
        make.top.mas_equalTo(self.nunberLabel.mas_bottom).offset(6);
    }];
    
    [lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.attentionLabel.mas_right).offset(10);
        make.width.equalTo(@2);
        make.height.mas_equalTo(@12);
        make.centerY.mas_equalTo(self.attentionLabel.mas_centerY);
    }];
    
    [self.fensiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineLabel.mas_right).offset(10);
        make.width.mas_lessThanOrEqualTo(200);
        make.top.mas_equalTo(self.nunberLabel.mas_bottom).offset(6);
    }];
    
    [self.livingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headerContentView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(90, 30));
        make.bottom.mas_equalTo(self.attentionLabel.mas_bottom);
    }];
    
    for (int i =0; i<3; i++) {
        CGFloat imageX = i *(-25)-20;
        UIImageView *image = [[UIImageView alloc]init];
        image.image = [UIImage imageNamed:@"center_avatar.jpeg"];
        image.layer.masksToBounds = YES;
        image.layer.cornerRadius = 18;
        [bottonView addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(rightImage.mas_right).offset(imageX);
            make.size.mas_equalTo(CGSizeMake(36, 36));
            make.top.mas_equalTo(bottonView.mas_top).offset(10);
            
        }];
    }
    
    
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)gotoMessagePage:(UIButton *)sender{
    switch (sender.tag) {
        case 0:
        {
            EditPersonalInfoViewController *edit = [EditPersonalInfoViewController new];
            [self.navigationController pushViewController:edit animated:YES];
        }
            break;
        case 1:
        {
            
        }
            break;
            
        default:
            break;
    }
}

-(void)clickCon{
    ContributionViewController *detail = [ContributionViewController new];
    detail.model = self.user;
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark -  Notification
- (void)acceptMsg:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    
    if ([notification.name isEqualToString:@"leaveTop"]) {
        NSString *canScroll = userInfo[@"canScroll"];
        if ([canScroll isEqualToString:@"1"]) {
            self.canScroll = YES;
        }
    } else if ([notification.name isEqualToString:IsEnablePersonalCenterVCMainTableViewScroll]) {
        NSString *canScroll = userInfo[@"canScroll"];
        if ([canScroll isEqualToString:@"1"]) {
            self.mainTableView.scrollEnabled = YES;
        }else if([canScroll isEqualToString:@"0"]) {
            self.mainTableView.scrollEnabled = NO;
        }
    }else if ([notification.name isEqualToString:CurrentSelectedChildViewControllerIndex]){
        NSNumber *index = userInfo[@"selectedPageIndex"];
        if ([index integerValue] !=0) {
            self.bottomView.hidden = YES;
        }else{
            self.bottomView.hidden = NO;
        }
    }
}

#pragma mark - UiScrollViewDelegate
/**
 * 处理联动
 * 因为要实现下拉头部放大的问题，tableView设置了contentInset，所以试图刚加载的时候会调用一遍这个方法，所以要做一些特殊处理，
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.mainTableView) {
        //当前y轴偏移量
        CGFloat yOffset  = scrollView.contentOffset.y;
        //临界点偏移量(吸顶临界点)
        CGFloat tabyOffset = [self.mainTableView rectForSection:0].origin.y - STATUS_BAR_HEIGHT - 44;
        
        //第一部分: 更改导航栏的背景图的透明度
        CGFloat alpha = 0;
        if (-yOffset <= STATUS_BAR_HEIGHT + 44) {
            alpha = 1;
        } else if ((-yOffset > STATUS_BAR_HEIGHT + 44) && -yOffset < HeaderImageViewHeight) {
            alpha = (HeaderImageViewHeight + yOffset) / (HeaderImageViewHeight - STATUS_BAR_HEIGHT - 44);
        }else {
            alpha = 0;
        }
        self.naviView.backgroundColor = kRGB(165, 57, 183, alpha);
        
        //第二部分：
        //利用contentOffset处理内外层scrollView的滑动冲突问题
        if (yOffset >= tabyOffset) {
            scrollView.contentOffset = CGPointMake(0, tabyOffset);
            _isTopIsCanNotMoveTabView = YES;
        }else{
            _isTopIsCanNotMoveTabView = NO;
        }
        
        _isTopIsCanNotMoveTabViewPre = !_isTopIsCanNotMoveTabView;
        
        if (!_isTopIsCanNotMoveTabViewPre) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goTop" object:nil userInfo:@{@"canScroll":@"1"}];
            _canScroll = NO;
        } else{
            if (!_canScroll) {
                _mainTableView.contentOffset = CGPointMake(0, tabyOffset);
            }
        }
        
        //第三部分：
        /**
         * 处理头部自定义背景视图 (如: 下拉放大)
         * 图片会被拉伸多出状态栏的高度
         */
        if(yOffset <= -HeaderImageViewHeight) {
            if (_isEnlarge) {
                CGRect f = self.headerImageView.frame;
                //改变HeadImageView的frame
                //上下放大
                f.origin.y = yOffset;
                f.size.height = -yOffset;
                //左右放大
                f.origin.x = (yOffset * kSCREENWIDTH / HeaderImageViewHeight + kSCREENWIDTH) / 2;
                f.size.width = -yOffset * kSCREENWIDTH / HeaderImageViewHeight;
                //改变头部视图的frame
                self.headerImageView.frame = f;
            }else{
                scrollView.bounces = NO;
            }
        }else {
            scrollView.bounces = YES;
        }
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSCREENHEIGHT - STATUS_BAR_HEIGHT - 44;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:self.setPageViewControllers];
    return cell;
}

#pragma mark - Lazy
- (UIView *)naviView {
    if (!_naviView) {
        _naviView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, STATUS_BAR_HEIGHT + 44)];
        _naviView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        //添加返回按钮
        UIButton *backButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [backButton setImage:[UIImage imageNamed:@"back"] forState:(UIControlStateNormal)];
        backButton.frame = CGRectMake(5, 8 + STATUS_BAR_HEIGHT, 28, 25);
        backButton.adjustsImageWhenHighlighted = YES;
        [backButton addTarget:self action:@selector(backAction) forControlEvents:(UIControlEventTouchUpInside)];
        [_naviView addSubview:backButton];
        
        //添加编辑按钮
        UIButton *messageButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [messageButton setImage:[UIImage imageNamed:@"bainji"] forState:(UIControlStateNormal)];
         messageButton.frame = CGRectMake(kSCREENWIDTH - 35 -25 -10, 8 + STATUS_BAR_HEIGHT, 25, 25);
        
        messageButton.adjustsImageWhenHighlighted = YES;
        messageButton.tag = 0;
        [messageButton addTarget:self action:@selector(gotoMessagePage:) forControlEvents:(UIControlEventTouchUpInside)];
        if ([self.userID isEqualToString:[kUserDefault objectForKey:USERID]]) {
            messageButton.hidden = NO;
        }else{
            messageButton.hidden = YES;
        }
        
        [_naviView addSubview:messageButton];
        _messageButton = messageButton;
        
        //添加分享按钮
        UIButton *shareButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [shareButton setImage:[UIImage imageNamed:@"rank_fenxiang"] forState:(UIControlStateNormal)];
        shareButton.frame = CGRectMake(kSCREENWIDTH - 35, 8 + STATUS_BAR_HEIGHT, 25, 25);
        shareButton.adjustsImageWhenHighlighted = YES;
        shareButton.tag = 1;
        [shareButton addTarget:self action:@selector(gotoMessagePage:) forControlEvents:(UIControlEventTouchUpInside)];
        [_naviView addSubview:shareButton];
        
        
    }
    return _naviView;
}

- (UITableView *)mainTableView {
    if (!_mainTableView) {
        //⚠️这里的属性初始化一定要放在mainTableView.contentInset的设置滚动之前, 不然首次进来视图就会偏移到临界位置，contentInset会调用scrollViewDidScroll这个方法。
        //初始化变量
        self.canScroll = YES;
        self.isTopIsCanNotMoveTabView = NO;
        
        self.mainTableView = [[CenterTouchTableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.showsVerticalScrollIndicator = NO;
        //注意：这里不能使用动态高度_headimageHeight, 不然tableView会往下移，在iphone X下，头部不放大的时候，上方依然会有白色空白
        _mainTableView.contentInset = UIEdgeInsetsMake(HeaderImageViewHeight, 0, 0, 0);//内容视图开始正常显示的坐标为(0, HeaderImageViewHeight)
    }
    return _mainTableView;
}

- (UIView *)headerContentView {
    if (!_headerContentView) {
        _headerContentView = [[UIView alloc]init];
        _headerContentView.backgroundColor = HEXCOLOR(0xf6f6fa);
        _headerContentView.frame = CGRectMake(0, -HeaderImageViewHeight, kSCREENWIDTH, HeaderImageViewHeight);
    }
    return _headerContentView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [[UIImageView alloc] init];
        _avatarImageView.userInteractionEnabled = YES;
        _avatarImageView.layer.masksToBounds = YES;
        _avatarImageView.layer.cornerRadius = 32;
    }
    return _avatarImageView;
}

- (UILabel *)nickNameLabel {
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] init];
        _nickNameLabel.font = [UIFont boldSystemFontOfSize:14];
        _nickNameLabel.textColor = [UIColor whiteColor];
        _nickNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
    }
    return _nickNameLabel;
}

- (UILabel *)nunberLabel {
    if (!_nunberLabel) {
        _nunberLabel = [[UILabel alloc] init];
        _nunberLabel.font = [UIFont systemFontOfSize:11];
        _nunberLabel.textColor = [UIColor whiteColor];
        _nunberLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
    }
    return _nunberLabel;
}

- (UILabel *)attentionLabel {
    if (!_attentionLabel) {
        _attentionLabel = [[UILabel alloc] init];
        _attentionLabel.font =  [UIFont boldSystemFontOfSize:11];
        _attentionLabel.textColor = [UIColor whiteColor];
        _attentionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
    }
    return _attentionLabel;
}

- (UILabel *)fensiLabel {
    if (!_fensiLabel) {
        _fensiLabel = [[UILabel alloc] init];
        _fensiLabel.font =  [UIFont boldSystemFontOfSize:11];
        _fensiLabel.textColor = [UIColor whiteColor];
        _fensiLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
    }
    return _fensiLabel;
}

-(QMUIButton *)livingBtn{
    if (!_livingBtn) {
        _livingBtn = [QMUIButton buttonWithType:UIButtonTypeCustom];
        _livingBtn.spacingBetweenImageAndTitle = 5.0;
        [_livingBtn setTitle:@"正在直播" forState:UIControlStateNormal];
        [_livingBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [_livingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_livingBtn setImage:[UIImage imageNamed:@"rank_living"] forState:UIControlStateNormal];
        _livingBtn.hidden = YES;
        [_livingBtn setImagePosition:QMUIButtonImagePositionLeft];
        _livingBtn.userInteractionEnabled = NO;
        _livingBtn.layer.masksToBounds = YES;
        _livingBtn.layer.cornerRadius = 15;
        _livingBtn.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.3];
        
    }
    return _livingBtn;
}

- (UIImageView *)headerImageView {
    if (!_headerImageView) {
        _headerImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"default_long"] qmui_imageWithAlpha:.5]];
        _headerImageView.userInteractionEnabled = YES;
        _headerImageView.clipsToBounds = YES;
    [_headerImageView setContentMode:UIViewContentModeScaleAspectFill];
//        _headerImageView.frame = CGRectMake(0, -HeaderImageViewHeight, kSCREENWIDTH, HeaderImageViewHeight);
    }
    return _headerImageView;
}



/*
 * 这里可以设置替换你喜欢的segmentView
 */
- (UIView *)setPageViewControllers {
    if (!_segmentView) {
        //设置子控制器
        PersonalInformationController *firstVC  = [[PersonalInformationController alloc] init];
        MineDynamicViewController *secondVC = [[MineDynamicViewController alloc] init];
        PersonalWorksController *thirdVC  = [[PersonalWorksController alloc] init];

        NSArray *controllers = @[firstVC, secondVC, thirdVC];
        NSArray *titleArray = @[@"资料", @"动态", @"作品"];
        SegmentView *segmentView = [[SegmentView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT - STATUS_BAR_HEIGHT - 44) controllers:controllers titleArray:(NSArray *)titleArray parentController:self];
        //注意：不能通过初始化方法传递selectedIndex的初始值，因为内部使用的是Masonry布局的方式, 否则设置selectedIndex不起作用
        segmentView.selectedIndex = self.selectedIndex;
        _segmentView = segmentView;
    }
    return _segmentView;
}



@end
