//
//  PersonalCenterViewController.h
//  PersonalCenter
//
//  Created by Arch on 2017/6/16.
//  Copyright © 2017年 mint_bin. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PersonalCenterViewController : UIViewController

@property (nonatomic, assign) BOOL isEnlarge;
@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, readonly, assign) BOOL isBacking;
@property (nonatomic, assign) BOOL isMineCenter;
@property (nonatomic ,copy)NSString *userID;//用户ID

@end
