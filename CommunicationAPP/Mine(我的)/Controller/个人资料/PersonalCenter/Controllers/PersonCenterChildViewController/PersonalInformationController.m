//
//  PersonalInformationController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/10.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PersonalInformationController.h"
#import "PersonalInformationOneCell.h"
#import "PersonalInformationTwoCell.h"

@interface PersonalInformationController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property(nonatomic ,strong)UIView *bottomView;
@property(nonatomic ,strong)NSDictionary *dataDic;
@end

@implementation PersonalInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getUserData:) name:PersonalCenterVCUSERDATA object:nil];
    
}

-(void)getUserData:(NSNotification*)dic{
    NSDictionary *userDic =dic.userInfo;
    self.dataDic = userDic;
    [self.tableView reloadData];
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = [UIColor whiteColor];
    //设置自动计算行号模式
    tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    tableView.estimatedRowHeight = 200;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *cellID = @"PersonalInformationOneCell";
        PersonalInformationOneCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[PersonalInformationOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        [cell layoutIfNeeded];
        cell.backgroundColor = [UIColor whiteColor];
        cell.dic = self.dataDic;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else{
        static NSString *cellID = @"PersonalInformationTwoCell";
        PersonalInformationTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[PersonalInformationTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        [cell layoutIfNeeded];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.numStr = [self.dataDic objectForKey:@"id"];
        cell.copybuttonClick = ^(NSString * _Nonnull num) { //复制
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = num;
            [MBProgressHUD showError:@"复制成功" toView:self.view];
        };
        return cell;
    }
    
   return nil;
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
