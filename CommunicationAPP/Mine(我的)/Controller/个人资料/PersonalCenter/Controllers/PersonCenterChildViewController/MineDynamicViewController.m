//
//  MineDynamicViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/12.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineDynamicViewController.h"
#import "MineDynamicViewCell.h"
#import "DynamicDetailController.h"
#import "VideoDynamicDetailController.h"
@interface MineDynamicViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic ,copy)NSString *userID;
@property (nonatomic, strong) NSMutableArray *dataSource; //数据源数组
@end

@implementation MineDynamicViewController

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableView];
    
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getUserData:) name:PersonalCenterVCUSERDATA object:nil];
    
   
}

-(void)getUserData:(NSNotification*)dic{
    NSDictionary *userDic =dic.userInfo;
    self.userID = [userDic objectForKey:@"id"];
    self.currentPage = 1;
    [self getDataFromSever];
    
     __weak typeof(self)weakSelf = self;
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.currentPage++;
        [weakSelf getDataFromSever];
    }];

}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"v_uid":[kUserDefault objectForKey:USERID]?:@"",
                            @"type":@"1",
                            @"index_page":@(self.currentPage),
                            @"index_size":@"10",
                            @"uid":self.userID?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:dynamic_typelist parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf endRefresh];
            if (self.currentPage == 1) {
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataSource addObjectsFromArray:[DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            }else{
                if ([DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil].count == 0) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    [MBProgressHUD showError:@"真的没有了~" toView:self.view];
                    return ;
                }else{
                    [weakSelf.dataSource addObjectsFromArray:[DynamicListModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [weakSelf endRefresh];
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    //设置自动计算行号模式
    tableView.rowHeight = UITableViewAutomaticDimension;
    //设置预估行高
    tableView.estimatedRowHeight = 200;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view).offset(0);
        make.height.mas_equalTo(kSCREENHEIGHT - STATUS_BAR_HEIGHT - 44 - SegmentHeaderViewHeight);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DynamicListModel *model = [self.dataSource objectAtIndex:indexPath.section];
    [self pushNextController:model];

}

-(void)pushNextController:(DynamicListModel *)model{
    if ([model.pub_type isEqualToString:@"1"]) {//短视频
        VideoDynamicDetailController *detail = [VideoDynamicDetailController new];
        detail.model = model;
        detail.URLString = [NSString stringWithFormat:@"%@%@",kWEBURL,model.video];
        detail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detail animated:YES];
        
    }else{//图片
        DynamicDetailController *detail = [DynamicDetailController new];
        detail.model = model;
        detail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detail animated:YES];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"MineDynamicViewCell";
    MineDynamicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[MineDynamicViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.dataSource objectAtIndex:indexPath.section];
    
    [cell layoutIfNeeded];
    return cell;
    
}


- (void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
