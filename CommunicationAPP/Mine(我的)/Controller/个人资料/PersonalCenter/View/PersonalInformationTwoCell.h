//
//  PersonalInformationTwoCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/10.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalInformationTwoCell : UITableViewCell
@property (nonatomic, copy)NSString *numStr;
@property (nonatomic, copy) void(^copybuttonClick)(NSString *num);

@end

NS_ASSUME_NONNULL_END
