//
//  PersonalInformationTwoCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/10.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PersonalInformationTwoCell.h"
@interface PersonalInformationTwoCell()
@property (strong , nonatomic)UILabel *numLabel;

@end
@implementation PersonalInformationTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}


#pragma mark - UI
- (void)setUpUI
{
    UILabel *line = [UILabel new];
    line.backgroundColor = HEXCOLOR(0x9C386A);
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        [make.top.mas_equalTo(self.contentView)setOffset:20];
        make.size.mas_equalTo(CGSizeMake(3, 14));
    }];
    
    UILabel *title = [UILabel new];
    title.textColor = [UIColor blackColor];
    title.font = [UIFont boldSystemFontOfSize:14];
    title.text = @"观点号";
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(line.mas_right)setOffset:5];
        make.centerY.mas_equalTo(line);
    }];
    
    _numLabel = [UILabel new];
    _numLabel.textColor = HEXCOLOR(0x666666);
    _numLabel.font = [UIFont systemFontOfSize:13];
//    _numLabel.text = [NSString stringWithFormat:@"%@",[[UserHander shareHander] loginUserInfo].ID];
    [self.contentView addSubview:_numLabel];
    [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(line.mas_bottom)setOffset:10];
        make.left.mas_equalTo(line);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-20);

    }];
    
    UIButton *copyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [copyBtn addTarget:self action:@selector(clickCopy) forControlEvents:UIControlEventTouchUpInside];
    copyBtn.clipsToBounds = YES;
    [copyBtn.layer setCornerRadius:8];
    [copyBtn.layer setBorderWidth:1];
    [copyBtn.layer setBorderColor:HEXCOLOR(0x9C386A).CGColor];
    [copyBtn setTitle:@"复制" forState:UIControlStateNormal];
    [copyBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [copyBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    [self.contentView addSubview:copyBtn];
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self->_numLabel);
        make.left.mas_equalTo(self->_numLabel.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(40, 16));
       
    }];
    
    
}

-(void)setNumStr:(NSString *)numStr{
    _numLabel.text = [NSString stringWithFormat:@"%@",numStr];
}

-(void)clickCopy{
    if (_copybuttonClick) {
        _copybuttonClick([[UserHander shareHander] loginUserInfo].ID);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
