//
//  PersonalInformationCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/10.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PersonalInformationOneCell.h"
@interface PersonalInformationOneCell()
@property (strong , nonatomic)UILabel *hometown;
@property (strong , nonatomic)UILabel *sexLabel;
@property (strong , nonatomic)UILabel *signLabel;

@end
@implementation PersonalInformationOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}


#pragma mark - UI
- (void)setUpUI
{
    UILabel *line = [UILabel new];
    line.backgroundColor = HEXCOLOR(0x9C386A);
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        [make.top.mas_equalTo(self.contentView)setOffset:20];
        make.size.mas_equalTo(CGSizeMake(3, 14));
    }];
    
    UILabel *title = [UILabel new];
    title.textColor = [UIColor blackColor];
    title.font = [UIFont boldSystemFontOfSize:14];
    title.text = @"个人信息";
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(line.mas_right)setOffset:5];
        make.centerY.mas_equalTo(line);
    }];
    
    _hometown = [UILabel new];
    _hometown.textColor = HEXCOLOR(0x666666);
    _hometown.font = [UIFont systemFontOfSize:13];
//    _hometown.text = [NSString stringWithFormat:@"家乡：%@",[[UserHander shareHander] loginUserInfo].area];
    [self.contentView addSubview:_hometown];
    [_hometown mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(line.mas_bottom)setOffset:10];
        make.left.mas_equalTo(line);
    }];
    
    _sexLabel = [UILabel new];
    _sexLabel.textColor = HEXCOLOR(0x666666);
    _sexLabel.font = [UIFont systemFontOfSize:13];
//    _sexLabel.text = [NSString stringWithFormat:@"性别：%@",[[[UserHander shareHander] loginUserInfo].sex isEqualToString:@"1"]?@"男":@"女"];
    [self.contentView addSubview:_sexLabel];
    [_sexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(self->_hometown.mas_bottom)setOffset:10];
        make.left.mas_equalTo(line);
    }];
    
    _signLabel = [UILabel new];
    _signLabel.textColor = HEXCOLOR(0x666666);
    _signLabel.numberOfLines = 0;
    _signLabel.lineBreakMode = NSLineBreakByCharWrapping;
    _signLabel.font = [UIFont systemFontOfSize:13];
//    _signLabel.text = [NSString stringWithFormat:@"个性签名：%@",[[UserHander shareHander] loginUserInfo].autograph];
    [self.contentView addSubview:_signLabel];
    [_signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.mas_equalTo(self->_sexLabel.mas_bottom)setOffset:10];
        make.left.mas_equalTo(line);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-50);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-20);
        
    }];
    
}

-(void)setDic:(NSDictionary *)dic{
    _hometown.text = [NSString stringWithFormat:@"家乡：%@",[dic objectForKey:@"area"]];
    _sexLabel.text = [NSString stringWithFormat:@"性别：%@",[[dic objectForKey:@"sex"] isEqualToString:@"1"]?@"男":@"女"];
    _signLabel.text = [NSString stringWithFormat:@"个性签名：%@",[dic objectForKey:@"autograph"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
