//
//  PersonalWorksCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/10.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PersonalWorksCell.h"
#import "ContentBottomView.h"
@interface PersonalWorksCell()
@property (nonatomic ,strong)UIImageView *mainImage;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *dateLabel;
@property (nonatomic ,strong)UILabel *typeLabel;
@property (nonatomic ,strong)UIButton *rubbishBtn;
@property (nonatomic ,strong)ContentBottomView *bottomView;

@end
@implementation PersonalWorksCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _headImage = [[UIImageView alloc]init];
    _headImage.image = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:20];
    [self.contentView addSubview:_headImage];
    
    _titleLabel = [UILabel new];
    _titleLabel.text = @"佛系主播 礼物随缘";
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_titleLabel];
    
    _rubbishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rubbishBtn setImage:[UIImage imageNamed:@"dele_icon"] forState:UIControlStateNormal];
    [self.contentView addSubview:_rubbishBtn];
   
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"11月15日";
    _dateLabel.textColor = HEXCOLOR(0x666666);
    _dateLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_dateLabel];
    
    _mainImage = [[UIImageView alloc]init];
    _mainImage.image = [UIImage imageNamed:@"test_icon"];
    [self.contentView addSubview:_mainImage];
    
    _typeLabel = [UILabel new];
    _typeLabel.text = @"回放";
    _typeLabel.textAlignment = NSTextAlignmentCenter;
    _typeLabel.textColor = [UIColor whiteColor];
    _typeLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2];
    _typeLabel.layer.cornerRadius = 5;
    _typeLabel.font = [UIFont systemFontOfSize:10];
    [_mainImage addSubview:_typeLabel];
    
    _bottomView = [[ContentBottomView alloc]init];
    _bottomView.backgroundColor = UIColor.whiteColor;
    
//    _bottomView.block = ^(UIButton *senderBtn){
//        if (weakSelf.bottomBlock) {
//            weakSelf.bottomBlock(senderBtn);
//        }
//
//    };
    [self.contentView addSubview:_bottomView];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(10);
        [make.top.mas_equalTo(self.contentView.mas_top)setOffset:10];
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:-10];
    }];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_headImage.mas_right)setOffset:10];
        [make.centerY.mas_equalTo(self->_headImage.mas_centerY)setOffset:10];
    }];
    
    [_rubbishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self.contentView)setOffset:-10];
        make.centerY.mas_equalTo(self->_headImage.mas_centerY);
    }];
    
    
    
    [_mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(10);
        make.right.mas_equalTo(self.contentView).offset(-10);
        [make.top.mas_equalTo(self->_headImage.mas_bottom)setOffset:10];
        make.height.mas_equalTo(@220);
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.top.right.mas_equalTo(self->_mainImage)setOffset:0];
         make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self->_mainImage.mas_bottom).offset(5);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    [_bottomView refreshModel];
   
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
