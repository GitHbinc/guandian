//
//  ChangeNickNameViewController.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "BaseViewController.h"

@interface ChangeNickNameViewController : BaseViewController
@property(nonatomic ,copy)NSString *name;
@end
