//
//  QrcodeViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/24.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "QrcodeViewController.h"

@interface QrcodeViewController ()
@property(nonatomic , strong)UIImageView *headImage;
@property(nonatomic , strong)UIImageView *qrImage;
@property(nonatomic , strong)UILabel *name;

@end

@implementation QrcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"二维码名片"];
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
    
    [self setNaviBarButtonItem];
    
    [self creatUI];
    
    [self getDataFromSever];
    
    
    // Do any additional setup after loading the view.
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_erweima parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            NSDictionary *dic = [responseObject objectForKey:@"data"];
            [weakSelf.headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[dic objectForKey:@"img"]]] placeholderImage:[UIImage imageNamed:@"default_square"]];
            weakSelf.name.text =[dic objectForKey:@"username"];
            [weakSelf.qrImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,[dic objectForKey:@"code"]]] placeholderImage:[UIImage imageNamed:@"default_square"]];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)creatUI{
    UIView *backView = [UIView new];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT+50);
        make.left.equalTo(self.view).offset(40);
        make.right.equalTo(self.view).offset(-40);
        make.height.mas_equalTo(kSCREENHEIGHT*0.55);
    }];
    
    _headImage = [[UIImageView alloc]init];
    _headImage.clipsToBounds = YES;
    [_headImage.layer setCornerRadius:40];
    [backView addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView).offset(30);
        make.left.equalTo(backView).offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    _name = [UILabel new];
    _name.font = [UIFont systemFontOfSize:14];
    _name.textColor = HEXCOLOR(0x333333);
    [backView addSubview:_name];
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headImage.mas_right).offset(20);
        make.centerY.equalTo(self->_headImage.mas_centerY);
    }];
    
    _qrImage = [[UIImageView alloc]init];
    [backView addSubview:_qrImage];
    [_qrImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headImage.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH - 120, kSCREENWIDTH - 120));
    make.centerX.equalTo(backView.mas_centerX);
    }];
    
    UILabel *label = [UILabel new];
    label.text = @"扫一扫上面的二维码图案，加我好友";
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = HEXCOLOR(0x666666);
    label.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_qrImage.mas_bottom).offset(20);
        make.centerX.equalTo(self->_qrImage.mas_centerX);
    }];
    
    
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
