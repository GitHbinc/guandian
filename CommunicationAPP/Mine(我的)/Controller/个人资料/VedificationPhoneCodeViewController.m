//
//  VedificationPhoneCodeViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "VedificationPhoneCodeViewController.h"

@interface VedificationPhoneCodeViewController ()

@end

@implementation VedificationPhoneCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"验证手机号"];
    [self createUI];
    [self createRightBtn];
}

-(void)createUI{
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, kSCREENWIDTH, 30)];
    label.text = @"请输入手机156****0124收到的验证码";
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = color999999;
    [self.view addSubview:label];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 30, kSCREENWIDTH, 44)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, kSCREENWIDTH - 100, 44)];
    textField.textColor = color333333;
    textField.font = [UIFont systemFontOfSize:14];
    textField.placeholder = @"手机验证码";
    [view addSubview:textField];
    
    UIButton *codeBtn = [[UIButton alloc]initWithFrame:CGRectMake(kSCREENWIDTH - 10 - 100, 0, 100, 44)];
    [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [codeBtn setTitleColor:color666666 forState:UIControlStateNormal];
    codeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    codeBtn.tag = 101;
    [codeBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:codeBtn];
    
    UIButton *bottomBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, 100, kSCREENWIDTH - 60, 44)];
    [bottomBtn setTitle:@"完成" forState:UIControlStateNormal];
    [bottomBtn setTitleColor:color666666 forState:UIControlStateNormal];
    bottomBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    bottomBtn.tag = 101;
    [bottomBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bottomBtn];
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(void)btnClick:(UIButton *)senderBtn{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
