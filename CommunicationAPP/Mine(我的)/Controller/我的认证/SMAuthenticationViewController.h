//
//  SMAuthenticationViewController.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSInteger, AuthenticationType)
{
    AuthenticationTypeRealName,       //实名认证
    AuthenticationTypeOrganization,   //机构认证
    
};
@interface SMAuthenticationViewController : BaseViewController

@property(nonatomic ,assign)AuthenticationType type;

@end
