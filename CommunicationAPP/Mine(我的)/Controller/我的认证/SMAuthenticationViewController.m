//
//  SMAuthenticationViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "SMAuthenticationViewController.h"
#import "AuthenticationPersonalInfoViewController.h"
#import "SMAuthenticationResultViewController.h"

@interface SMAuthenticationViewController ()

@end

@implementation SMAuthenticationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.type == AuthenticationTypeRealName) {
        self.navigationItem.title =@"实名认证";
    }else{
        self.navigationItem.title =@"机构认证";
    }
    
     [self setNaviBarButtonItem];
   
    [self setUI];
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}

-(void)setUI{
    
    UIImageView *headerImageView = [[UIImageView alloc]init];
    headerImageView.image = [UIImage imageNamed:@"mine_shiming"];
    [self.view addSubview:headerImageView];
    [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT+ 50);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    NSArray *array = [NSArray array];
    if (self.type == AuthenticationTypeRealName) {
         array = @[@"根据相关法规，直播、出售需要实名认证",@"现需要认证后才能继续操作",@"认证信息观点会严格保密"];
    }else{
        array = @[@"根据相关法规，直播、出售需要机构认证",@"现需要认证后才能继续操作",@"认证信息观点会严格保密"];
    }
   
    
    for (int i = 0; i<array.count; i++) {
        UILabel *label = [self createLabel:array[i] color:color666666 font:16];
        [self.view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(0);
            make.right.equalTo(self.view).offset(0);
            make.top.equalTo(headerImageView.mas_bottom).offset(32 + (i * 25));
            make.height.equalTo(@20);
        }];
    }
    
    UIButton *footerBtn = [[UIButton alloc]init];
    if (self.type == AuthenticationTypeRealName) {
         [footerBtn setTitle:@"开始实名认证" forState:UIControlStateNormal];
    }else{
         [footerBtn setTitle:@"开始机构认证" forState:UIControlStateNormal];
    }
   
    [footerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [footerBtn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    footerBtn.backgroundColor = MAINCOLOR;
    footerBtn.clipsToBounds = YES;
    [footerBtn.layer setCornerRadius:5.0];
    [self.view addSubview:footerBtn];
    [footerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerImageView.mas_bottom).offset(148);
        make.left.equalTo(self.view).offset(27);
        make.right.equalTo(self.view).offset(-27);
        make.height.equalTo(@43);
    }];
}

-(void)footerBtnClick{
    if (self.type == AuthenticationTypeRealName) {
        AuthenticationPersonalInfoViewController *controller = [[AuthenticationPersonalInfoViewController alloc]init];
        controller.type = AuthenticationInfoTypeRealName;
        [self.navigationController pushViewController:controller animated:YES];
    }else{

        AuthenticationPersonalInfoViewController *controller = [[AuthenticationPersonalInfoViewController alloc]init];
        controller.type = AuthenticationInfoTypeOrganization;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}

-(UILabel *)createLabel:(NSString *)titleStr color:(UIColor *)color font:(CGFloat)font{
    
    UILabel *label = [[UILabel alloc]init];
    label.text = titleStr;
    label.font = [UIFont systemFontOfSize:font];
    label.textColor = color;
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
