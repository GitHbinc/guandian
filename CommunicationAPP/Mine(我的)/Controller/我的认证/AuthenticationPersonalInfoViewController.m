//
//  AuthenticationPersonalInfoViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "AuthenticationPersonalInfoViewController.h"
#import "SMAuthenticationResultViewController.h"
#import "AddBankCardCell.h"

@interface AuthenticationPersonalInfoViewController ()<UITableViewDelegate,UITableViewDataSource,AddBankCardCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *placeholderArray;
@property (nonatomic ,strong)UITextField *nameText;
@property (nonatomic ,strong)UITextField *IDcardText;
@property (nonatomic ,strong)UITextField *phoneText;
@property (nonatomic ,strong)UITextField *codeText;
@property (nonatomic ,strong)UITextField *addressText;
@property (nonatomic ,strong)UITextField *companyText;




@end

@implementation AuthenticationPersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.type == AuthenticationInfoTypeRealName) {
        self.navigationItem.title =@"认证个人信息";
        _titleArray = @[@"真实姓名",@"身份证号",@"手机号",@"验证码"];
        _placeholderArray = @[@"请输入您的真实姓名",@"请输入您的身份证号码",@"请输入手机号",@"请输入验证码"];
    }else{
         self.navigationItem.title =@"认证机构信息";
        _titleArray = @[@"公司名称",@"所在地区",@"联系人",@"手机号",@"验证码"];
        _placeholderArray = @[@"请输入公司名称",@"请输入地区",@"请输入联系人姓名",@"请输入手机号",@"请输入验证码"];
    }
    
    [self createRightBtn];
    [self createTableView];
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [self createFooterView];
}

-(void)createFooterView{
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 70)];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(27, 25, kSCREENWIDTH - 54, 44)];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.backgroundColor = MAINCOLOR;
    btn.clipsToBounds = YES;
    [btn.layer setCornerRadius:5.0];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:btn];
    _tableView.tableFooterView = footerView;
    
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{

}

-(void)btnClick{
    if (self.type == AuthenticationInfoTypeRealName) {
        
        [self certainRealNameAction];
    }else{
         [self certainOrganizationAction];
    }
    
    
}

-(void)certainRealNameAction{
    
    if ([self isEmpty:self.nameText.text]) {
        [MBProgressHUD showError:@"请输入您的真实姓名" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.IDcardText.text]) {
        [MBProgressHUD showError:@"请输入您的身份证号码" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.phoneText.text]) {
        [MBProgressHUD showError:@"请输入手机号码" toView:self.view];
        return;
    }
    
    if (![CheckNumber checkTelNumber:self.phoneText.text]) {
        [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.codeText.text]) {
        [MBProgressHUD showError:@"请输入验证码" toView:self.view];
        return;
    }
    
    
    NSDictionary *param = @{
                            @"realName":self.nameText.text,
                            @"cardNo":self.IDcardText.text,
                            @"phone":self.phoneText.text,
                            @"code":self.codeText.text,
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_realname parameters:param success:^(id  _Nonnull responseObject) {
        
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            __weak typeof(self)weakSelf = self;
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0/*延迟执行时间*/ * NSEC_PER_SEC));
            
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                SMAuthenticationResultViewController *controller = [[SMAuthenticationResultViewController alloc]init];
                [weakSelf.navigationController pushViewController:controller animated:YES];
                
            });
            
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];

}

-(void)certainOrganizationAction{
    if ([self isEmpty:self.companyText.text]) {
        [MBProgressHUD showError:@"请输入公司名称" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.addressText.text]) {
        [MBProgressHUD showError:@"请输入地区名称" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.nameText.text]) {
        [MBProgressHUD showError:@"请输入联系人姓名" toView:self.view];
        return;
    }

    
    if ([self isEmpty:self.phoneText.text]) {
        [MBProgressHUD showError:@"请输入手机号码" toView:self.view];
        return;
    }
    
    if (![CheckNumber checkTelNumber:self.phoneText.text]) {
        [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
        return;
    }
    
    if ([self isEmpty:self.codeText.text]) {
        [MBProgressHUD showError:@"请输入验证码" toView:self.view];
        return;
    }
    
    
    NSDictionary *param = @{
                            @"name":self.companyText.text,
                            @"people":self.nameText.text,
                            @"address":self.addressText.text,
                            @"phone":self.phoneText.text,
                            @"code":self.codeText.text,
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_agency parameters:param success:^(id  _Nonnull responseObject) {
        
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            
            __weak typeof(self)weakSelf = self;
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0/*延迟执行时间*/ * NSEC_PER_SEC));
            
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                SMAuthenticationResultViewController *controller = [[SMAuthenticationResultViewController alloc]init];
                [weakSelf.navigationController pushViewController:controller animated:YES];
                
            });
            
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _titleArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]init];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 37)];
    if (self.type ==AuthenticationInfoTypeRealName ) {
         label.text = @"请填写真实个人信息";
    }else{
        label.text = @"请填写真实机构信息";
    }
   
    label.textColor = color666666;
    label.font = [UIFont systemFontOfSize:13];
    [view addSubview:label];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 37;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"AddBankCardCellID";
    AddBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AddBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.codeClick = ^(UIButton *sender) {
        if ([self isEmpty:self.phoneText.text]) {
            [MBProgressHUD showError:@"请输入手机号码" toView:self.view];
            return;
        }
        
        if (![CheckNumber checkTelNumber:self.phoneText.text]) {
            [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
            return;
        }

        NSDictionary *param = @{
                                @"phone" :self.phoneText.text?:@"",
                                @"type":@"3"
                                };
        
        [[WYNetworking sharedWYNetworking] POST:user_getcode parameters:param success:^(id  _Nonnull responseObject) {
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
            
        } failure:^(NSError * _Nonnull error) {
            [MBProgressHUD showError:@"验证码发送失败" toView:self.view];
        }];
    };

    [cell refreshModel:_titleArray[indexPath.row] placeholderStr:_placeholderArray[indexPath.row] row:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

-(void)updateTextFieldAtCell:(AddBankCardCell *)cell myTextField:(UITextField *)textField{
    NSIndexPath *index =[self.tableView indexPathForCell:cell];
    if (self.type == AuthenticationInfoTypeRealName) {
        if (index.row == 0) {
            self.nameText = textField;
            
        }else if (index.row == 1){
            self.IDcardText = textField;
            
        }else if (index.row == 2){
            self.phoneText = textField;
            
        }else{
            self.codeText = textField;
            
        }
    }else{
        if (index.row == 0) {
            self.companyText = textField;
            
        }else if (index.row == 1){
            self.addressText = textField;
            
        }else if (index.row == 2){
            self.nameText = textField;
            
        }else if (index.row == 3){
            self.phoneText = textField;
            
        }else{
            self.codeText = textField;
            
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
