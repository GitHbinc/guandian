//
//  AuthenticationPersonalInfoViewController.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSInteger, AuthenticationInfoType)
{
    AuthenticationInfoTypeRealName,       //实名认证
    AuthenticationInfoTypeOrganization,   //机构认证
    
};
@interface AuthenticationPersonalInfoViewController : BaseViewController
@property(nonatomic ,assign)AuthenticationInfoType type;

@end
