//
//  ZBAuthenticationViewController.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "ZBAuthenticationViewController.h"
#import "ZBAuthenticationHeaderView.h"
#import "ZBAuthenticationCell.h"
@interface ZBAuthenticationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSArray *sectionTitleArray;

@end

@implementation ZBAuthenticationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"主播申请认证"];
    _titleArray = @[@"实名认证",@"粉丝数达1万以上",@"有效直播10条以上",@"获得个人礼物收益达10万点以上"];
    _contentArray = @[@"通过平台实名认证",@"违禁用户不计入有效粉丝数内",@"当天累计直播≥60分钟算有效直播",@"包含直播收益、竞拍收益、礼物"];
    _imageArray = @[@"zhubo_shiming",@"zhubo_fensi",@"zhubo_youxiao",@"zhubo_liwu"];
    _sectionTitleArray = @[@"基础条件",@"申请类型",@"申请说明"];
    [self createTableView];
    [self createRightBtn];
    
}

-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setContentInset:UIEdgeInsetsZero];
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    ZBAuthenticationHeaderView *headerView = [ZBAuthenticationHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 100)];
    headerView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [headerView refreshModel];
    _tableView.tableHeaderView = headerView;
    
    [self createFooterView];
}

-(void)createFooterView{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 100)];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(38, 30, kSCREENWIDTH - 76, 40)];
    [btn setTitle:@"立即申请" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setBackgroundImage:[UIImage imageNamed:@"mine_btn_bg_icon"] forState:UIControlStateNormal];
//    btn.backgroundColor = color333333;
    [btn addTarget:self action:@selector(footerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    _tableView.tableFooterView = view;
}

-(void)createRightBtn{
    
    UIButton *rightBtn  = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightBtn setImage:[[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)rightItemAction{
    
}

-(void)footerBtnClick{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return _titleArray.count;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return 75;
    }else if (indexPath.section == 1){
        return 165;
    }
    return  60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 44)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 44)];
    label.textColor = color333333;
    label.text = _sectionTitleArray[section];
    label.font = [UIFont systemFontOfSize:15];
    [view addSubview:label];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43, kSCREENWIDTH, 1)];
    lineView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [view addSubview:lineView];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 44;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        ZBAuthenticationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBAuthenticationCellID"];
        if (cell == nil) {
            cell = [[ZBAuthenticationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZBAuthenticationCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        [cell refreshData:_imageArray[indexPath.row] titleStr:_titleArray[indexPath.row] contentStr:_contentArray[indexPath.row] stateStr:@"未完成"];
        return cell;
    }else if (indexPath.section == 1){
        ZBAuthenticationTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBAuthenticationTypeCellID"];
        if (cell == nil) {
            cell = [[ZBAuthenticationTypeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZBAuthenticationTypeCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
    
    ZBAuthenticationInstructionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZBAuthenticationInstructionCellID"];
    if (cell == nil) {
        cell = [[ZBAuthenticationInstructionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZBAuthenticationInstructionCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    [cell refreshData:@"1.完成基础条件后，才能申请。" contentStr:@"1.评级结果会以私信反馈，请注意查收。"];
    return cell;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
