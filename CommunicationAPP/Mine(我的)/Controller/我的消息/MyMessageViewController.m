//
//  MyMessageViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/7.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "MyMessageViewController.h"
#import "MineItem.h"
#import "WithoutAttentionedViewCell.h"
#import "LittleAssistantViewCell.h"
#import "MyMessageHeadView.h"
#import "GroupMemberModel.h"
#import "SystemMessageViewController.h"

@interface MyMessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation MyMessageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"消息";
    [self setNaviBarButtonItem];
    [self loadTableView];
   
    NSArray *nameArray = @[@"系统",@"动态",@"私信",@"好友"];
    NSArray *iconArray = @[@"message_xitong",@"message_dongtai",@"message_sixin",@"message_friend"];
    for (int i =0; i <nameArray.count; i++) {
        MineItem *item = [MineItem new];
        item.name = nameArray[i];
        item.icon = iconArray[i];
        [self.dataSource addObject:item];
    }
     [self loadHeadView];
     [self.tableView reloadData];
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"message_jiahaoyou"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(addAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)loadHeadView{
    MyMessageHeadView *head = [[MyMessageHeadView alloc]init];
    head.messageclickAction = ^(UIButton * _Nonnull sender) {
        switch (sender.tag) {
            case 0://系统
            {
                SystemMessageViewController *controller = [[SystemMessageViewController alloc]init];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            case 1://动态
            {
                
            }
                break;
            case 2://私信
            {
                
            }
                break;
            case 3://好友
            {
                
            }
                break;
                
            default:
                break;
        }
    };
    head.frame = CGRectMake(0, 0, kSCREENWIDTH, 115);
    GroupMemberModel *model = [GroupMemberModel new];
    model.array = self.dataSource;
    head.model = model;
    self.tableView.tableHeaderView = head;
}

-(void)addAction{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *identifier = @"WithoutAttentionedViewCell";
        WithoutAttentionedViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[WithoutAttentionedViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:identifier];
        }

        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    static NSString *identifier = @"LittleAssistantViewCell";
        LittleAssistantViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[LittleAssistantViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
