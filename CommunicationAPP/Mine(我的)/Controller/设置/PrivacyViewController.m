//
//  PrivacyViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PrivacyViewController.h"
#import "PrivacySelectSeeViewCell.h"
#import "MessageSetupCell.h"
#import "PrivacyMemberSwitchCell.h"
#import "MineItem.h"

@interface PrivacyViewController ()<UITableViewDelegate,UITableViewDataSource,PrivacyMemberSwitchCellDelegate,MessageSetupCellDelegate>
@property(nonatomic ,strong)UITableView *topTableView;
@property(nonatomic ,strong)UITableView *middletableView;
@property(nonatomic ,strong)UITableView *bottomTableView;
@property(nonatomic ,strong)NSArray *dataArray;
@end

@implementation PrivacyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"隐私"];
    [self setNaviBarButtonItem];
    [self loadTableView];
    
    NSArray *titleArray = @[@"需要验证身份",
                            @"不需要验证身份",
                            @"拒绝任何人添加",
                            @"拒收未关注人的消息",
                            @"不允许通过手机号找到我*开启后，其他人将不能通过手机联系人查看到你",
                            @"所有人可见",
                            @"所有人不可见"
                            ];
    for (int i = 0; i <titleArray.count; i++) {
        MineItem *item = [[MineItem alloc]init];
        item.name = titleArray[i];
        [self.dataSource addObject:item];
    }
    [self.topTableView reloadData];
    
    [self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    [[WYNetworking sharedWYNetworking] POST:user_privacy parameters:@{@"uid":[kUserDefault objectForKey:USERID]?:@""} success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            NSDictionary *dic = [responseObject objectForKey:@"data"];
            NSArray *array = @[[dic objectForKey:@"data_pages"],
                               [dic objectForKey:@"comment"],
                               [dic objectForKey:@"contact_view"],
                               [dic objectForKey:@"friends_validation"]
                              
                               ];
            
            self.dataArray = array;

            [weakSelf.topTableView reloadData];
            [weakSelf.middletableView reloadData];
            [weakSelf.bottomTableView reloadData];
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}

- (void)loadTableView {
    UITableView *middletableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.middletableView = middletableView;
    middletableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    middletableView.tableFooterView = [[UIView alloc]init];
    middletableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    middletableView.delegate = self;
    middletableView.dataSource = self;
    [self.view addSubview:middletableView];
    [middletableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    
    UITableView *topTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
     topTableView.frame = CGRectMake(0, 0, kSCREENWIDTH, 180);
    self.topTableView = topTableView;
    topTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    topTableView.tableFooterView = [[UIView alloc]init];
    topTableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    topTableView.delegate = self;
    topTableView.dataSource = self;
    self.middletableView.tableHeaderView = topTableView;
    
    UITableView *bottomTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    bottomTableView.frame = CGRectMake(0, 0, kSCREENWIDTH, 130);
    self.bottomTableView = bottomTableView;
    bottomTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    bottomTableView.tableFooterView = [[UIView alloc]init];
    bottomTableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    bottomTableView.delegate = self;
    bottomTableView.dataSource = self;
    self.middletableView.tableFooterView = bottomTableView;
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.topTableView) {
        return 3;
    }
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.topTableView) {
       
        static NSString *identifier = @"PrivacySelectSeeViewCell";
        PrivacySelectSeeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[PrivacySelectSeeViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            
        }
        cell.item = (MineItem *)[self.dataSource objectAtIndex:indexPath.row + indexPath.section];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString *index = [self.dataArray firstObject];
            [self tableView:self.topTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:[index floatValue]-1 inSection:0]];
            [self.topTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[index floatValue]-1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            

        return cell;
        
    }else if (tableView == self.middletableView){
        if (indexPath.row == 0) {
            static NSString *identifier = @"MessageSetupCell";
            MessageSetupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[MessageSetupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                
            }
             cell.delegate = self;
              MineItem *item = [self.dataSource objectAtIndex:indexPath.row + indexPath.section+3];
            item.type = [self.dataArray objectAtIndex:1];
            cell.mineItem = item;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }else{
            static NSString *identifier = @"PrivacyMemberSwitchCell";
            PrivacyMemberSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[PrivacyMemberSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                
            }
            cell.delegate = self;
            MineItem *item = [self.dataSource objectAtIndex:indexPath.row + indexPath.section+3];
             item.type = [self.dataArray objectAtIndex:2];
            cell.mineItem = item;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
        
    }else{
        static NSString *identifier = @"PrivacySelectSeeViewCell";
        PrivacySelectSeeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[PrivacySelectSeeViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.item = (MineItem *)[self.dataSource objectAtIndex:indexPath.row + indexPath.section+5];
         NSString *index = [self.dataArray lastObject];
            [self tableView:self.bottomTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:[index floatValue]-1 inSection:0]];
            [self.bottomTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[index floatValue]-1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
       
        return cell;
}
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView ==self.topTableView) {
        NSLog(@"*****上%ld*****",(long)indexPath.row);
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:@(indexPath.row +1) forKey:@"data_pages"];
        if ([[param objectForKey:@"data_pages"] floatValue]>0) {
              [self editPrivacy:param];
        }
       
        
    }else if(tableView ==self.bottomTableView){
        NSLog(@"*****下%ld*****",(long)indexPath.row);
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:@(indexPath.row +1) forKey:@"friends_validation"];
        if ([[param objectForKey:@"friends_validation"] floatValue]>0) {
            [self editPrivacy:param];
        }

    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = HEXCOLOR(0xf6f6fa);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.textColor = HEXCOLOR(0x666666);
    headerLabel.font = [UIFont systemFontOfSize:13];
    if (tableView ==self.bottomTableView ) {
        headerLabel.text = @"好友验证";
    }else if(tableView == self.topTableView){
         headerLabel.text = @"资料页显示位置信息设置";
    }
   [headerView addSubview:headerLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.middletableView) {
        return 1;
    }
    return 30.0;
    
}

-(void)updateSwitchAtCell:(MessageSetupCell *)cell mySwitch:(UISwitch *)mySwitch{
     NSMutableDictionary *param = [NSMutableDictionary dictionary];
     [param setValue:mySwitch.on?@"1":@"2" forKey:@"comment"];
     [self editPrivacy:param];
}


-(void)updatePrivacySwitchAtCell:(PrivacyMemberSwitchCell *)cell mySwitch:(UISwitch *)mySwitch{
     NSMutableDictionary *param = [NSMutableDictionary dictionary];
     [param setValue:mySwitch.on?@"1":@"2" forKey:@"contact_view"];
    [self editPrivacy:param];
}


-(void)editPrivacy:(NSMutableDictionary *)param{
    
    [param setValue:[kUserDefault objectForKey:USERID]?:@"" forKey:@"uid"];
    [[WYNetworking sharedWYNetworking] POST:user_privacy_eidt parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            //[MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];

}

- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
    CGFloat sectionHeaderHeight =30;
    if(scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0)
    {scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y,0,0,0);
        
    }else if(scrollView.contentOffset.y>=sectionHeaderHeight) {scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight,0,0,0);
        
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
