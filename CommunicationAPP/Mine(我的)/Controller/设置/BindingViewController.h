//
//  BindingViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, BindingType)
{
    BindingTypePhone,             //绑定手机号
    BindingTypeEmail,             //绑定邮箱
   
};
@interface BindingViewController : BaseViewController
@property(nonatomic ,assign)BindingType type;
@end

NS_ASSUME_NONNULL_END
