//
//  BindingViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BindingViewController.h"

@interface BindingViewController ()
@property(nonatomic ,strong)UILabel *tipLabel;
@property(nonatomic ,strong)UILabel *WaitLabel;
@property(nonatomic ,strong)UILabel *phone;
@property(nonatomic ,strong)UILabel *Verification;
@property(nonatomic ,strong)UITextField *phoneTF;
@property(nonatomic ,strong)UITextField *verificationTF;
@property(nonatomic ,strong)UIButton *verificationBtn;
@property(nonatomic ,strong)NSTimer *countTime;//倒计时
@end

@implementation BindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type == BindingTypePhone) {
         [self.navigationItem setTitle:@"绑定手机号"];
    }else{
         [self.navigationItem setTitle:@"绑定邮箱"];
    }
   
    [self setNaviBarButtonItem];
    [self setUpUI];
    self.view.backgroundColor = HEXCOLOR(0xf6f6fa);
}


-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}

-(void)setUpUI{
    
    _tipLabel = [UILabel new];
    _tipLabel.text = self.type == BindingTypePhone?@"为了您的账户安全，请您绑定手机号":@"为了您的账户安全，请您绑定邮箱";
    _tipLabel.textColor = HEXCOLOR(0x666666);
    _tipLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:_tipLabel];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.equalTo(self.view).offset(10);
        make.height.mas_equalTo(@40);
    }];
    
    _phone = [UILabel new];
    _phone.text = self.type == BindingTypePhone?@"手机号":@"邮箱账号";
    _phone.backgroundColor = [UIColor whiteColor];
    _phone.textAlignment = NSTextAlignmentCenter;
    _phone.textColor = HEXCOLOR(0x333333);
    _phone.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:_phone];
    
    [_phone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_tipLabel.mas_bottom);
        make.left.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(80, 50));
    }];
    
    _phoneTF = [[UITextField alloc]init];
    _phoneTF.backgroundColor = [UIColor whiteColor];
    _phoneTF.keyboardType = UIKeyboardTypePhonePad;
    NSString *holderText = self.type == BindingTypePhone?@"请输入手机号":@"请输入您的邮箱账号";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x999999)
                        range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont boldSystemFontOfSize:15]
                        range:NSMakeRange(0, holderText.length)];
    _phoneTF.attributedPlaceholder = placeholder;
     [self.view addSubview:_phoneTF];
    
    [_phoneTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self->_phone);
        make.left.equalTo(self->_phone.mas_right);
        make.right.equalTo(self.view);
    }];
    
    _Verification = [UILabel new];
    _Verification.text = @"验证码";
    _Verification.backgroundColor = [UIColor whiteColor];
    _Verification.textAlignment = NSTextAlignmentCenter;
    _Verification.textColor = HEXCOLOR(0x333333);
    _Verification.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:_Verification];
    
    [_Verification mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_phone.mas_bottom);
        make.left.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(80, 50));
    }];
    
    _verificationTF = [[UITextField alloc]init];
    _verificationTF.backgroundColor = [UIColor whiteColor];
    _verificationTF.keyboardType = UIKeyboardTypePhonePad;
    NSString *holder = @"请输入短信验证码";
    NSMutableAttributedString *placeholders = [[NSMutableAttributedString alloc] initWithString:holder];
    [placeholders addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x999999)
                        range:NSMakeRange(0, holder.length)];
    [placeholders addAttribute:NSFontAttributeName
                        value:[UIFont boldSystemFontOfSize:15]
                        range:NSMakeRange(0, holder.length)];
    _verificationTF.attributedPlaceholder = placeholders;
    [self.view addSubview:_verificationTF];
    
    [_verificationTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self->_Verification);
        make.left.equalTo(self->_Verification.mas_right);
        make.right.equalTo(self.view);
    }];
    
    _verificationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_verificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_verificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_verificationBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    _verificationBtn.backgroundColor = HEXCOLOR(0x9C386A);
    _verificationBtn.clipsToBounds = YES;
    _verificationBtn.layer.cornerRadius = 15.0;
    [_verificationBtn addTarget:self action:@selector(getVerificationCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_verificationBtn];
    
    [_verificationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-15);
        make.centerY.equalTo(self->_verificationTF.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    _WaitLabel = [UILabel new];
    _WaitLabel.backgroundColor = HEXCOLOR(0x9C386A);
    _WaitLabel.textColor = [UIColor whiteColor];
    _WaitLabel.font = [UIFont systemFontOfSize:12];
    _WaitLabel.clipsToBounds = YES;
    _WaitLabel.textAlignment = NSTextAlignmentCenter;
    _WaitLabel.layer.cornerRadius = 15.0;
    _WaitLabel.hidden = YES;
    [self.view addSubview:_WaitLabel];
    [_WaitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-10);
        make.centerY.equalTo(self->_verificationTF.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    
    UIButton *completeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [completeBtn setTitle:@"完成" forState:UIControlStateNormal];
    [completeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [completeBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    completeBtn.backgroundColor = HEXCOLOR(0x9C386A);
    completeBtn.clipsToBounds = YES;
    completeBtn.layer.cornerRadius = 8.0;
    [completeBtn addTarget:self action:@selector(completeAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:completeBtn];
    
    [completeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_Verification.mas_bottom).offset(50);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH *0.8, 40));
    }];
    
}

-(void)completeAction{
    
}

- (void)getVerificationCodeAction:(UIButton *)sender{
     [self startTimer];
}

- (void)startTimer{
    [self.verificationTF becomeFirstResponder];
    self.verificationBtn.hidden = YES;
    self.WaitLabel.hidden = NO;
    self.WaitLabel.tag = 10;
    self.WaitLabel.text = @"10s";
    self.countTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
    
}

//验证码倒计时
- (void)countdown{
    NSInteger count = self.WaitLabel.tag;
    count --;
    self.WaitLabel.tag = count;
    if (count<0){
        self.WaitLabel.hidden = YES;
        self.verificationBtn.hidden = NO;
        [self.countTime invalidate];
    }else{
        self.WaitLabel.text = [NSString stringWithFormat:@"%lds",(long)count];;
        
    }
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    [self.view endEditing:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
