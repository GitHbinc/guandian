//
//  BlacklistViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BlacklistViewController.h"
#import "BlacklistViewCell.h"
#import "BlacklistModel.h"

@interface BlacklistViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;

@end

@implementation BlacklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"黑名单"];
    [self setNaviBarButtonItem];
    
    [self loadTableView];
    
    [self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    NSDictionary *param = @{
                            @"index_page":@"1",
                            @"index_size":@"10",
                            @"uid":[kUserDefault objectForKey:USERID]?:@"",
                            };
    [[WYNetworking sharedWYNetworking] POST:user_blacklist parameters:param success:^(id  _Nonnull responseObject) {
        [self.dataSource removeAllObjects];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [weakSelf.dataSource addObjectsFromArray:[BlacklistModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"data"] error:nil]];
            
            [weakSelf.tableView reloadData];
        }else{
            
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}

- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"BlacklistViewCell";
    BlacklistViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[BlacklistViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.dataSource objectAtIndex:indexPath.row];
    cell.removeBlacklist = ^(BlacklistModel * _Nonnull model) {//移除黑名单
        [[WYNetworking sharedWYNetworking] POST:user_blacklist_edit parameters:@{
                                                                                 @"id":model.ID?:@""
                                                                                 } success:^(id  _Nonnull responseObject) {
                                                                                     
                                                                                     if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
                                                                                         {
                                                                                             [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                                                                                             [self getDataFromSever];
                                                                                             [self.tableView reloadData];
                                                                                             
                                                                                         }
                                                                                     }else{
                                                                                         
                                                                                         [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
                                                                                     }
                                                                                     
                                                                                 } failure:^(NSError * _Nonnull error) {
            
        }];
    };
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
