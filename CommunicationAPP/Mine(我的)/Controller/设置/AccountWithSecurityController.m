//
//  AccountWithSecurityController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AccountWithSecurityController.h"
#import "BindingViewController.h"
#import "ResetPasswordViewController.h"
#import "DeviceManagementController.h"

#import "SetUpViewCell.h"
#import "MineItem.h"

@interface AccountWithSecurityController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation AccountWithSecurityController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"账号与安全"];
    [self setNaviBarButtonItem];
    [self loadTableView];
    NSArray *titleArray = @[@"手机绑定",
                            @"微信",
                            @"QQ",
                            @"新浪微博",
                            @"邮箱",
                            @"支付宝",
                            @"实名认证",
                            @"重置密码",
                            @"登录设备管理"];
    for (int i = 0; i <titleArray.count; i++) {
        MineItem *item = [[MineItem alloc]init];
        item.name = titleArray[i];
        [self.dataSource addObject:item];
    }
    [self.tableView reloadData];
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 6;
    }
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"SetUpViewCell";
    SetUpViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[SetUpViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    if (indexPath.section == 0) {
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section];
    }else{
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row +indexPath.section*6];
    }
    
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row==0) {//手机绑定
            BindingViewController * controller = [[BindingViewController alloc]init];
            controller.type = BindingTypePhone;
            [self.navigationController pushViewController:controller animated:YES];
            
        }else if (indexPath.row ==1){//微信
            
            
        }else if (indexPath.row ==2){//QQ
            
            
        }else if (indexPath.row ==3){//新浪微博
            
            
        }else if (indexPath.row ==4){//邮箱
            BindingViewController * controller = [[BindingViewController alloc]init];
            controller.type = BindingTypeEmail;
            [self.navigationController pushViewController:controller animated:YES];
            
        }else{ //支付宝
            
            
        }
    }else{
        if (indexPath.row==0) {//实名认证
            
            
        }else if (indexPath.row ==1){//重置密码
            ResetPasswordViewController* controller = [[ResetPasswordViewController alloc]init];
            controller.type = PasswordTypeReset;
            [self.navigationController pushViewController:controller animated:YES];
            
        }else{//登录设备管理
            DeviceManagementController* controller = [[DeviceManagementController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==1) {
        return 0.0;
    }else
        return 10.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc]init];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)shareAction{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
