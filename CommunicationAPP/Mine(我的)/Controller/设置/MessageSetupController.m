//
//  MessageSetupControllerViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MessageSetupController.h"
#import "MessageSetupCell.h"
#import "MineItem.h"

@interface MessageSetupController ()<UITableViewDelegate,UITableViewDataSource,MessageSetupCellDelegate>
@property(nonatomic ,strong)UITableView *tableView;
@end

@implementation MessageSetupController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"消息设置"];
    [self setNaviBarButtonItem];
    [self loadTableView];
    
    NSArray *titleArray = @[@"接受推送通知",
                            @"评论",
                            @"关注",
                            @"私信",
                            @"开播提醒",
                            @"免打扰"
                            ];
    for (int i = 0; i <titleArray.count; i++) {
        MineItem *item = [[MineItem alloc]init];
        item.name = titleArray[i];
        [self.dataSource addObject:item];
    }
    [self.tableView reloadData];
    
    [self getDataFromSever];
}

-(void)getDataFromSever{
    __weak typeof(self)weakSelf = self;
    [[WYNetworking sharedWYNetworking] POST:user_information parameters:@{@"uid":[kUserDefault objectForKey:USERID]?:@""} success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            NSDictionary *dic = [responseObject objectForKey:@"data"];
            NSArray *array = @[[dic objectForKey:@"push_notification"],
                               [dic objectForKey:@"comment"],
                               [dic objectForKey:@"attention"],
                               [dic objectForKey:@"private_letter"],
                               [dic objectForKey:@"started_remind"],
                               [dic objectForKey:@"bother"]
                               ];
            
            for (int i=0; i <self.dataSource.count; i++) {
                MineItem *item = self.dataSource[i];
                item.type = array[i];
            }
            
            [weakSelf.tableView reloadData];
            
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


-(void)setNaviBarButtonItem{
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)shareAction{
    
}


- (void)loadTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tableView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableFooterView = [[UIView alloc]init];
    tableView.backgroundColor = HEXCOLOR(0xf6f6fa);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(Navi_STATUS_HEIGHT);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section ==0) {
        return 1;
    }else if (section ==1){
        return 3;
    }
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *identifier = @"MessageSetupCell";
        MessageSetupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[MessageSetupCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
    cell.delegate = self;
    if (indexPath.section ==2) {
         cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row + indexPath.section*2];
    }else{
        cell.mineItem = (MineItem *)[self.dataSource objectAtIndex:indexPath.row + indexPath.section];
    }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 30)];
    headerView.backgroundColor = HEXCOLOR(0xf6f6fa);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    headerLabel.font = [UIFont systemFontOfSize:13];
    headerLabel.textColor = HEXCOLOR(0x555555);
    if (section==1) {
        headerLabel.text = @"互动通知";
    }else if(section==2){
        headerLabel.text = @"直播通知";
    }
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section ==0) {
        return 1.0;
    }
    
    return 30.0;
    
}


-(void)updateSwitchAtCell:(MessageSetupCell *)cell mySwitch:(UISwitch *)mySwitch{
    NSIndexPath *index =[self.tableView indexPathForCell:cell];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (index.section == 0) {

        [param setValue:mySwitch.on?@"1":@"2" forKey:@"push_notification"];
        
    }else if (index.section == 1){
        switch (index.row) {
            case 0:
            {
            [param setValue:mySwitch.on?@"1":@"2" forKey:@"comment"];
            }
                break;
            case 1:
            {
            [param setValue:mySwitch.on?@"1":@"2" forKey:@"attention"];
            }
                break;
            case 2:
            {
            [param setValue:mySwitch.on?@"1":@"2" forKey:@"private_letter"];
            }
                break;
                
            default:
                break;
        }
    }else{
        switch (index.row) {
            case 0:
            {
            [param setValue:mySwitch.on?@"1":@"2" forKey:@"started_remind"];
            }
                break;
            case 1:
            {
            [param setValue:mySwitch.on?@"1":@"2" forKey:@"bother"];
            }
                break;
            
                
            default:
                break;
        }
    }
    
     [param setValue:[kUserDefault objectForKey:USERID]?:@"" forKey:@"uid"];
    
    [[WYNetworking sharedWYNetworking] POST:user_information_edit parameters:param success:^(id  _Nonnull responseObject) {
        
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
           // [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];

}

- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
    CGFloat sectionHeaderHeight =30;
    if(scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0)
    {scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y,0,0,0);
        
    }else if(scrollView.contentOffset.y>=sectionHeaderHeight) {scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight,0,0,0);
        
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end 
