//
//  ResetPasswordViewController.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "MyMD5.h"
#import "SetPasswordSuccessViewController.h"
#import "LoginViewController.h"

@interface ResetPasswordViewController ()
@property(nonatomic ,strong)UILabel *WaitLabel;
@property(nonatomic ,strong)UITextField *phoneTF;
@property(nonatomic ,strong)UITextField *verificationTF;
@property(nonatomic ,strong)UITextField *passwordTF;
@property(nonatomic ,strong)UIButton *verificationBtn;
@property(nonatomic ,strong)NSTimer *countTime;//倒计时

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type == PasswordTypeForget) {
        [self.navigationItem setTitle:@"忘记密码"];
    }else{
        [self.navigationItem setTitle:@"重置密码"];
    }
    [self setNaviBarButtonItem];
    [self setUpUI];
}

-(void)setNaviBarButtonItem{
    
    UIImage *rightImage = [[UIImage imageNamed:@"mine_fenxiang"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *rightItem =[[UIBarButtonItem alloc] initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(shareAction)];
    if (self.type == PasswordTypeReset) {
         self.navigationItem.rightBarButtonItem = rightItem;
    }
   
}

-(void)shareAction{
    
}

-(void)setUpUI{
    UIImageView *mainImage = [[UIImageView alloc]init];
    mainImage.image = [UIImage imageNamed:@"header_logo_icon"];
    [self.view addSubview:mainImage];
    
    [mainImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30 +Navi_STATUS_HEIGHT);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(64, 64));
    }];
    //手机号
    _phoneTF = [[UITextField alloc]init];
    _phoneTF.backgroundColor = [UIColor whiteColor];
    _phoneTF.keyboardType = UIKeyboardTypePhonePad;
    NSString *holderText = @"请输入您的手机号码";
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x666666)
                        range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:12]
                        range:NSMakeRange(0, holderText.length)];
    _phoneTF.attributedPlaceholder = placeholder;
    [self.view addSubview:_phoneTF];
    
    [_phoneTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mainImage.mas_bottom).offset(40);
        make.left.equalTo(self.view).offset(40);
        make.right.equalTo(self.view).offset(-40);
        make.height.mas_equalTo(@40);
    }];
    
    UIView *line1 = [UIView new];
    line1.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:0.6];
    [self.view addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_phoneTF.mas_bottom);
        make.left.right.equalTo(self->_phoneTF);
        make.height.mas_equalTo(@1);
    }];
    
    //验证码
    _verificationTF = [[UITextField alloc]init];
    _verificationTF.backgroundColor = [UIColor whiteColor];
    _verificationTF.keyboardType = UIKeyboardTypePhonePad;
    NSString *verificationholder = @"请输入验证码";
    NSMutableAttributedString *verificationplaceholder = [[NSMutableAttributedString alloc] initWithString:verificationholder];
    [verificationplaceholder addAttribute:NSForegroundColorAttributeName
                        value:HEXCOLOR(0x666666)
                        range:NSMakeRange(0, verificationholder.length)];
    [verificationplaceholder addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:12]
                        range:NSMakeRange(0, verificationholder.length)];
    _verificationTF.attributedPlaceholder = verificationplaceholder;
    [self.view addSubview:_verificationTF];
    
    [_verificationTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_phoneTF.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(40);
        make.right.equalTo(self.view).offset(-40);
        make.height.mas_equalTo(@40);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:0.6];
    [self.view addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_verificationTF.mas_bottom);
        make.left.right.equalTo(self->_verificationTF);
        make.height.mas_equalTo(@1);
    }];
    
    _verificationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_verificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_verificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_verificationBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    _verificationBtn.backgroundColor = HEXCOLOR(0x9C386A);
    _verificationBtn.clipsToBounds = YES;
    _verificationBtn.layer.cornerRadius = 15.0;
    [_verificationBtn addTarget:self action:@selector(getVerificationCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_verificationBtn];
    
    [_verificationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_verificationTF);
        make.centerY.equalTo(self->_verificationTF.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    _WaitLabel = [UILabel new];
    _WaitLabel.backgroundColor = HEXCOLOR(0x9C386A);
    _WaitLabel.textColor = [UIColor whiteColor];
    _WaitLabel.font = [UIFont systemFontOfSize:12];
    _WaitLabel.clipsToBounds = YES;
    _WaitLabel.textAlignment = NSTextAlignmentCenter;
    _WaitLabel.layer.cornerRadius = 15.0;
    _WaitLabel.hidden = YES;
    [self.view addSubview:_WaitLabel];
    [_WaitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_verificationTF);
        make.centerY.equalTo(self->_verificationTF.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    //密码
    _passwordTF = [[UITextField alloc]init];
    _passwordTF.backgroundColor = [UIColor whiteColor];
    NSString *passwordholder = @"请重置登录密码，不小于6位";
    NSMutableAttributedString *passwordplaceholder = [[NSMutableAttributedString alloc] initWithString:passwordholder];
    [passwordplaceholder addAttribute:NSForegroundColorAttributeName
                                    value:HEXCOLOR(0x666666)
                                    range:NSMakeRange(0, passwordholder.length)];
    [passwordplaceholder addAttribute:NSFontAttributeName
                                    value:[UIFont systemFontOfSize:12]
                                    range:NSMakeRange(0, passwordholder.length)];
    _passwordTF.attributedPlaceholder = passwordplaceholder;
    [self.view addSubview:_passwordTF];
    
    [_passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_verificationTF.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(40);
        make.right.equalTo(self.view).offset(-40);
        make.height.mas_equalTo(@40);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:0.6];
    [self.view addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_passwordTF.mas_bottom);
        make.left.right.equalTo(self->_passwordTF);
        make.height.mas_equalTo(@1);
    }];
    
    UIButton *completeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [completeBtn setTitle:@"确定" forState:UIControlStateNormal];
    [completeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [completeBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    completeBtn.backgroundColor = HEXCOLOR(0x9C386A);
    completeBtn.clipsToBounds = YES;
    completeBtn.layer.cornerRadius = 8.0;
    [completeBtn addTarget:self action:@selector(certainAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:completeBtn];
    
    [completeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_passwordTF.mas_bottom).offset(50);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH *0.8, 40));
    }];
}

-(void)certainAction{
   
        //检查手机号
        NSString *phonestr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![CheckNumber checkTelNumber:phonestr]) {
            [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
            return;
        }
        //检查验证码
        NSString *codestr = [self.verificationTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![CheckNumber checkVerificationCode:codestr]) {
            [MBProgressHUD showError:@"请输入正确的验证码" toView:self.view];
            return;
        }
        
        //检查密码
        NSString *passWordStr = [self.passwordTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![CheckNumber checkPassword:passWordStr]) {
            [MBProgressHUD showError:@"密码格式错误" toView:self.view];
            return;
        }
        
        NSDictionary *param = @{
                                @"phone" :self.phoneTF.text?:@"",
                                @"code":self.verificationTF.text?:@"",
                                @"password":[MyMD5 md5uncapitalized:self.passwordTF.text] ?:@""
                                };
        
    [[WYNetworking sharedWYNetworking] POST:user_forgetPassword parameters:param success:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            if (self.type == PasswordTypeForget) {
                SetPasswordSuccessViewController *controller = [[SetPasswordSuccessViewController alloc]init];
                [self.navigationController pushViewController:controller animated:true];
            }else{
                
                LoginViewController *controller = [[LoginViewController alloc]init];
                [self.navigationController pushViewController:controller animated:true];
            }
        }else{
            [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
   
}

- (void)getVerificationCodeAction:(UIButton *)sender{
    //检查手机号
    NSString *phonestr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![CheckNumber checkTelNumber:phonestr]) {
        [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
        return;
    }
    NSDictionary *param = @{
                            @"phone" :self.phoneTF.text?:@"",
                            @"type":@"2"
                            };
    
    [[WYNetworking sharedWYNetworking] POST:user_getcode parameters:param success:^(id  _Nonnull responseObject) {
        [MBProgressHUD showError:[responseObject objectForKey:@"msg"] toView:self.view];
        if ([[responseObject objectForKey:@"status"] integerValue] == 1) {
            [self startTimer];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD showError:@"验证码发送失败" toView:self.view];
    }];
    
}

- (void)startTimer{
    [self.verificationTF becomeFirstResponder];
    self.verificationBtn.hidden = YES;
    self.WaitLabel.hidden = NO;
    self.WaitLabel.tag = 10;
    self.WaitLabel.text = @"10s";
    self.countTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
    
}

//验证码倒计时
- (void)countdown{
    NSInteger count = self.WaitLabel.tag;
    count --;
    self.WaitLabel.tag = count;
    if (count<0){
        self.WaitLabel.hidden = YES;
        self.verificationBtn.hidden = NO;
        [self.countTime invalidate];
    }else{
        self.WaitLabel.text = [NSString stringWithFormat:@"%lds",(long)count];;
        
    }
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
