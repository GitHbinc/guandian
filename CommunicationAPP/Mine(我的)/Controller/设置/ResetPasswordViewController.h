//
//  ResetPasswordViewController.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSInteger, PasswordType)
{
    PasswordTypeForget,            //忘记密码
    PasswordTypeReset,             //重置密码
    
};
NS_ASSUME_NONNULL_BEGIN

@interface ResetPasswordViewController : BaseViewController
@property(nonatomic ,assign)PasswordType type;

@end

NS_ASSUME_NONNULL_END
