//
//  CertificateCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface CertificateCell : UITableViewCell

-(void)refreshModel:(NSInteger)tag;

@end
