//
//  SMAuthenticationResultCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import <UIKit/UIKit.h>

@interface SMAuthenticationResultCell : UITableViewCell

-(void)refreshData:(NSString *)titleStr contentStr:(NSString *)contentStr;

@end
