//
//  RechargeCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "RechargeCell.h"

@interface RechargeCell(){
    
    UILabel *_moneyLabel;
    UITextField *_moneyTextField;
    
}

@end

@implementation RechargeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color333333;
    label.text = @"其他购买金额";
    label.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    _moneyTextField = [[UITextField alloc]init];
    _moneyTextField.placeholder = @"请输入金额（元）";
    _moneyTextField.textColor = color333333;
    _moneyTextField.font = [UIFont systemFontOfSize:13];
    [_moneyTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:_moneyTextField];
    [_moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_left).offset(0);
        make.top.equalTo(label.mas_bottom).offset(8);
        make.width.equalTo(@150);
    }];
    
    _moneyLabel = [[UILabel alloc]init];
    _moneyLabel.textColor = color666666;
    _moneyLabel.font = [UIFont systemFontOfSize:13];
    _moneyLabel.text = @"= 0 点币";
    [self.contentView addSubview:_moneyLabel];
    [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_moneyTextField.mas_right).offset(20);
        make.top.equalTo(self->_moneyTextField.mas_top).offset(0);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor =  HEXCOLOR(0xf6f6fa);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
    
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    NSLog(@"文字改变：%@",textChange.text);
    _moneyLabel.text = [NSString stringWithFormat:@"= %@ 点币",[self isEmpty:textChange.text]?@"0":textChange.text];
    if (_inputClick) {
        _inputClick(textChange.text);
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@interface RechargePayCell(){
    
    UILabel *_titleLabel;
    UILabel *_typeLabel;
    UIImageView *_typeImageView;
    
}

@end

@implementation RechargePayCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.text  =@"选择支付方式";
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    
    _typeLabel = [[UILabel alloc]init];
    _typeLabel.textColor = color333333;
     _typeLabel.text = @"支付宝";
    _typeLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(0);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _typeImageView = [[UIImageView alloc]init];
    _typeImageView.image = [UIImage imageNamed:@"mine_zhifubao"];
    [self.contentView addSubview:_typeImageView];
    [_typeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_typeLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
}

-(void)refreshModel:(PayTypeModel *)model{
    _typeLabel.text = model.name;
    _typeImageView.image = [UIImage imageNamed:model.img];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

