//
//  FollowCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "FollowCell.h"

@interface FollowCell(){
    
    UILabel*_titleLabel;
    UILabel*_descLabel;
    UIImageView *_zhiboImageView;
    UIImageView *_leftImageView;
    UIButton *_cancelBtn;
    UIButton *_headBtn;
}

@end

@implementation FollowCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _leftImageView = [[UIImageView alloc]init];
    _leftImageView.layer.cornerRadius = 22.0;
    _leftImageView.clipsToBounds = YES;
    [_leftImageView.layer setCornerRadius:22];
    [self.contentView addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];
    
    _headBtn = [[UIButton alloc]init];
    _headBtn.layer.cornerRadius = 10.0;
    [_headBtn addTarget:self action:@selector(headBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_headBtn];
    [_headBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];

    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(10);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    _zhiboImageView = [[UIImageView alloc]init];
    _zhiboImageView.image = [UIImage imageNamed:@"mine_zhibozhong"];
    _zhiboImageView.hidden = YES;
    [self.contentView addSubview:_zhiboImageView];
    [_zhiboImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_right).offset(10);
        make.top.equalTo(self->_titleLabel.mas_top).offset(0);
    }];
    
    _descLabel = [[UILabel alloc]init];
    _descLabel.textColor = color999999;
    _descLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(10);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
    }];
    
    _cancelBtn = [[UIButton alloc]init];
    [_cancelBtn setTitle:@"关注" forState:UIControlStateNormal];
    [_cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_cancelBtn setTitle:@"取关" forState:UIControlStateSelected];
    [_cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _cancelBtn.backgroundColor = HEXCOLOR(0x9C386A);
    _cancelBtn.layer.cornerRadius = 10.0;
    _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    [_cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_cancelBtn];
    [_cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(@20);
        make.width.equalTo(@45);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@1);
        make.bottom.equalTo(self.contentView).offset(-1);
    }];
    
}

-(void)setModel:(AttentionListModel *)model{
    _model = model;
     [_leftImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _titleLabel.text =model.username;
    _descLabel.text = model.autograph;
    if ([model.zhibotype isEqualToString:@"1"]) {
        _zhiboImageView.hidden = NO;
    }else{
        _zhiboImageView.hidden = YES;
    }
    
    if ([model.status isEqualToString:@"1"]) {
        _cancelBtn.selected = YES;
        _cancelBtn.backgroundColor = HEXCOLOR(0xdddddd);
    }else{
        _cancelBtn.selected = NO;
        _cancelBtn.backgroundColor = HEXCOLOR(0x9C386A);
    }
}



-(void)cancelBtnClick:(UIButton *)senderBtn{
    senderBtn.selected = !senderBtn.selected;
    if (senderBtn.selected) {
        senderBtn.backgroundColor = HEXCOLOR(0xdddddd);
    }else{
        senderBtn.backgroundColor = HEXCOLOR(0x9C386A);
    }
    
    if (_attentionbuttonClick) {
        _attentionbuttonClick(_model);
    }
}

-(void)headBtnClick:(UIButton *)sender{
    if (_headBtnClick) {
        _headBtnClick(_model);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
