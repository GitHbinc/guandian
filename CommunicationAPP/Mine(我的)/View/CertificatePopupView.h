//
//  CertificatePopupView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface CertificatePopupView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

@end
