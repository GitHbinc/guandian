//
//  CertificateHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface CertificateHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refreshData;

@end
