//
//  PacketDetailCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "PacketDetailCell.h"

@interface PacketDetailCell(){
    
    UIImageView *_leftImageView;
    UILabel *_titleLabel;
    UILabel *_timeLabel;
    UILabel *_moneyLabel;
    UILabel *_allMoneyLabel;
    UILabel *_rechargeMoneyLabel;
    
}

@end

@implementation PacketDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _leftImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImageView];
    _leftImageView.image = [UIImage imageNamed:@"mine_tixian"];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(5);
        make.top.equalTo(self.contentView).offset(13);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color999999;
    _timeLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.bottom.equalTo(self.contentView).offset(-13);
    }];
    
    _moneyLabel = [[UILabel alloc]init];
    _moneyLabel.textColor = color333333;
    _moneyLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_moneyLabel];
    [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(13);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = color999999;
    _allMoneyLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_moneyLabel.mas_right).offset(0);
        make.bottom.equalTo(self.contentView).offset(-13);
    }];
    
    _rechargeMoneyLabel = [[UILabel alloc]init];
    _rechargeMoneyLabel.textColor = color333333;
    _rechargeMoneyLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_rechargeMoneyLabel];
    [_rechargeMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
}

//-(void)refreshModel:(BOOL)isPacket{
//    _titleLabel.text = @"直播收益";
//    _timeLabel.text = @"2018-11-19";
//    if (isPacket) {
//        _moneyLabel.text = @"+1802";
//        _allMoneyLabel.text = @"1458.05";
//    }else{
//        _rechargeMoneyLabel.text = @"1802";
//    }
//}

-(void)setModel:(ProfitListModel *)model{
    _model = model;
    _titleLabel.text = @"出售";
    _timeLabel.text = model.create_time;
    _moneyLabel.text = [NSString stringWithFormat:@"%@h",model.times];
    _allMoneyLabel.text = [NSString stringWithFormat:@"%@点",model.money];;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
