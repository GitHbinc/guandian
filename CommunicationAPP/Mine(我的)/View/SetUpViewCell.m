//
//  SetUpViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SetUpViewCell.h"
@interface SetUpViewCell()
/* 标题 */
@property (strong , nonatomic)UILabel *titleLabel;
/* 内容 */
@property (strong , nonatomic)UILabel *contentLabel;
@end
@implementation SetUpViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
   
    _contentLabel = [UILabel new];
    _contentLabel.text = @"100点";
    _contentLabel.textColor = HEXCOLOR(0xC7C7C7);
    _contentLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_contentLabel];
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        make.center.mas_equalTo(self.contentView);
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self.contentView.mas_right)setOffset:0];
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    
}

#pragma mark - Setter Getter Methods
-(void)setModel:(OptionModel *)model{
    _model = model;
    if (![model.title isEqualToString:@"去设置"] && ![model.name isEqualToString:@"围城名称"] ) {
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            [make.right.mas_equalTo(self.contentView.mas_right)setOffset:-20];
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
    _titleLabel.text = model.name;
    _contentLabel.text = model.title;
}

- (void)setMineItem:(MineItem *)mineItem{
    _mineItem = mineItem;
    self.titleLabel.text = mineItem.name;
    if ([mineItem.name isEqualToString:@"清除缓存"]){
        self.contentLabel.text = [self getCacheSize];
    }else if ([mineItem.name isEqualToString:@"检查更新"]){
        self.contentLabel.text = @"当前版本1.1.0";
    }else if ([mineItem.name isEqualToString:@"手机绑定"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"微信"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"QQ"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"新浪微博"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"邮箱"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"支付宝"]){
        self.contentLabel.text = @"未绑定";
    }else if ([mineItem.name isEqualToString:@"实名认证"]){
        self.contentLabel.text = @"未绑定";
    }else{
        self.contentLabel.text = @"";
    }
}


#pragma mark - Get cache size
- (NSString *)getCacheSize{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = nil;
    paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSMutableString *cachepath = [paths objectAtIndex:0];
    NSError *error = nil;
    //文件夹下所有的目录和文件大小
    float cacheSize = 0.0f;
    //fileList便是包含有该文件夹下所有文件的文件名及文件夹名的数组
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:cachepath error:&error];
    BOOL isDir = NO;
    //在上面那段程序中获得的fileList中列出文件夹名
    for (NSString *file in fileList) {
        NSString *path = [cachepath stringByAppendingPathComponent:file];
        [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
        //获取文件属性
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
        //获取文件的创建日期
        NSDate *modificationDate = (NSDate*)[fileAttributes objectForKey: NSFileModificationDate];
        //                NSLog(@"===%d======%d",(int)[[NSDate date] timeIntervalSince1970],(int)[modificationDate timeIntervalSince1970]);
        int timeSepearte = (int)[[NSDate date] timeIntervalSince1970]-(int)[modificationDate timeIntervalSince1970];
        if (timeSepearte>(3*86400)) {
            if ([fileManager isDeletableFileAtPath:path]) {
                [fileManager removeItemAtPath:path error:nil];
            }
        }else{
            if (isDir) {
                cacheSize = cacheSize + [self fileSizeForDir:path];
            }
        }
        isDir = NO;
    }
    NSString *cacheSizeString = @"";
    if (cacheSize >1024*1024) {
        float cacheSize_M = cacheSize/(1024*1024);
        cacheSizeString = [NSString stringWithFormat:@"%.1f M",cacheSize_M];
    }else if (cacheSize>1024&&cacheSize<1024*1024) {
        float cacheSize_KB = cacheSize/(1024);
        cacheSizeString = [NSString stringWithFormat:@"%.1f KB",cacheSize_KB];
    }else{
        float cacheSize_BYT = cacheSize/(1024);
        cacheSizeString = [NSString stringWithFormat:@"%.1f B",cacheSize_BYT];
    }
    return cacheSizeString;
}

-(float)fileSizeForDir:(NSString*)path//计算文件夹下文件的总大小
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    float size =0;
    NSArray* array = [fileManager contentsOfDirectoryAtPath:path error:nil];
    for(int i = 0; i<[array count]; i++)
    {
        NSString *fullPath = [path stringByAppendingPathComponent:[array objectAtIndex:i]];
        
        BOOL isDir;
        if ( !([fileManager fileExistsAtPath:fullPath isDirectory:&isDir] && isDir) )
        {
            NSDictionary *fileAttributeDic=[fileManager attributesOfItemAtPath:fullPath error:nil];
            size+= fileAttributeDic.fileSize;
        }
        else
        {
            [self fileSizeForDir:fullPath];
        }
    }
    return size;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
