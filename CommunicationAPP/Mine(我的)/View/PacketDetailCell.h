//
//  PacketDetailCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>
#import "ProfitListModel.h"

@interface PacketDetailCell : UITableViewCell
@property(nonatomic ,strong)ProfitListModel *model;

@end
