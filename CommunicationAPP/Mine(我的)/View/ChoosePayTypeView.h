//
//  ChoosePayTypeView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PayType) {
    PayTypezhifubao,
    PayTypeweixin,
    PayTypeother,
};

@interface ChoosePayTypeView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

+(void)showWithStyle:(PayType)style ChooseSendBlock:(void(^)(NSString *text))sendBlock;

@end
