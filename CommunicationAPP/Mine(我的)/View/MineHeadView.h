//
//  MineHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MineHeadView : UIView
@property (nonatomic ,strong)UserModel *model;
@property (nonatomic, copy) dispatch_block_t inforClickBlock;
@property (nonatomic, copy) dispatch_block_t qrClickBlock;

@property (nonatomic, copy) void(^classfybuttonClick)(UIButton * btn);

@end

NS_ASSUME_NONNULL_END
