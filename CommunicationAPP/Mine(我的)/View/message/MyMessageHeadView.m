//
//  MyMessageHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/7.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "MyMessageHeadView.h"
#import "MineItem.h"
#define imageHeight 55
@implementation MyMessageHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
       // [self setUpUI];
        
    }
    return self;
}

-(void)setModel:(GroupMemberModel *)model{
    _model = model;
    for (int i =0; i <model.array.count; i++) {
         MineItem *item = (MineItem *)model.array[i];
        CGFloat wr =(kSCREENWIDTH - imageHeight*4)/8;
        CGFloat btn_x = wr*(i*2+1) + imageHeight *i;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor whiteColor];
        btn.frame = CGRectMake(btn_x, 20, imageHeight, imageHeight);
        [btn setBackgroundImage:[UIImage imageNamed:item.icon] forState:UIControlStateNormal];
        btn.tag = i;
        [btn addTarget:self action:@selector(messageClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        UILabel *name = [UILabel new];
        name.text = item.name;
        name.textAlignment = NSTextAlignmentCenter;
        name.font = [UIFont systemFontOfSize:14];
        name.textColor = HEXCOLOR(0x333333);
        [self addSubview:name];
        
        [name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(btn.mas_bottom).offset(10);
            make.centerX.mas_equalTo(btn);
            make.width.mas_equalTo(btn);
    
        }];
    }
}

-(void)messageClick:(UIButton *)sender{
    if (_messageclickAction) {
        _messageclickAction(sender);
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
