//
//  MyMessageHeadView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/7.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupMemberModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMessageHeadView : UIView
@property(nonatomic ,strong)GroupMemberModel *model;
@property (nonatomic, copy) void(^messageclickAction)(UIButton *sender);

@end

NS_ASSUME_NONNULL_END
