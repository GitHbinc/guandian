//
//  WithoutAttentionedViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/7.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "WithoutAttentionedViewCell.h"

@implementation WithoutAttentionedViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"message_nosixin"];
    [self.contentView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"未关注人的私信";
    titleLabel.textColor = HEXCOLOR(0x333333);
    titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(image.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self.contentView);
    }];
    
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
