//
//  SystemMessageViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/9.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "SystemMessageViewCell.h"
@interface SystemMessageViewCell()
@property(nonatomic ,strong)UIImageView *headImg;
@property(nonatomic ,strong)UILabel *titleLabel;
@property(nonatomic ,strong)UILabel *contentLabel;
@property(nonatomic ,strong)UIButton *agreeBtn;
@end

@implementation SystemMessageViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _headImg = [[UIImageView alloc]init];
    _headImg.image = [UIImage imageNamed:@"message_huodong"];
    _headImg.clipsToBounds = YES;
    [_headImg.layer setCornerRadius:20];
    [self.contentView addSubview:_headImg];
    
    
    
    _titleLabel = [UILabel new];
    _titleLabel.text = @"慢慢";
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"请求添加好友";
    _contentLabel.textColor = HEXCOLOR(0x999999);
    _contentLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_contentLabel];
    
    _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _agreeBtn.clipsToBounds = YES;
    [_agreeBtn.layer setCornerRadius:5];
    [_agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
    [_agreeBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_agreeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _agreeBtn.backgroundColor = HEXCOLOR(0xEE1C1B);
    [_agreeBtn addTarget:self action:@selector(agreeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_agreeBtn];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_headImg mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:70];
        [make.centerY.mas_equalTo(self.contentView.mas_centerY)setOffset:-10];
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:70];
        [make.centerY.mas_equalTo(self.contentView)setOffset:10];
    }];
    
    [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(self.contentView)setOffset:-20];
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(50, 25));
    }];
    
}


-(void)agreeAction:(UIButton *)sender{
    [sender setTitle:@"已添加" forState:UIControlStateNormal];
    [sender setTitleColor:HEXCOLOR(0x999999) forState:UIControlStateNormal];
    sender.backgroundColor = [UIColor whiteColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
