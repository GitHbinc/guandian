//
//  LittleAssistantViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/7.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "LittleAssistantViewCell.h"

@interface LittleAssistantViewCell()
@property(nonatomic ,strong)UILabel *titleLabel;
@property(nonatomic ,strong)UILabel *contentLabel;
@property(nonatomic ,strong)UIImageView *guanfang;

@end
@implementation LittleAssistantViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"message_huodong"];
    [self.contentView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"观点活动小助手";
    titleLabel.textColor = HEXCOLOR(0x333333);
    titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    _titleLabel = titleLabel;
    
    UIImageView *guan = [[UIImageView alloc]init];
    guan.image = [UIImage imageNamed:@"message_guan"];
    [self.contentView addSubview:guan];
    _guanfang = guan;
    
    UILabel *contentLabel = [UILabel new];
    contentLabel.text = @"观点最新版本上线啦！！！！";
    contentLabel.textColor = HEXCOLOR(0x999999);
    contentLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:contentLabel];
    _contentLabel = contentLabel;
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:70];
        [make.centerY.mas_equalTo(self.contentView.mas_centerY)setOffset:-10];
    }];
    
    [_guanfang mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self->_titleLabel.mas_right)setOffset:10];
        make.centerY.mas_equalTo(self->_titleLabel.mas_centerY);
      
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:70];
        [make.centerY.mas_equalTo(self.contentView)setOffset:10];
    }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
