//
//  MyPacketView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>
#import "PacketModel.h"

@interface MyPacketView : UIView
@property(nonatomic ,strong)PacketModel *model;
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refreshModel;

@end
