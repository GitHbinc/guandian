//
//  MineViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItem.h"
NS_ASSUME_NONNULL_BEGIN


@interface MineViewCell : UITableViewCell
/* 消息模型 */
@property (strong , nonatomic)MineItem *mineItem;
@end

NS_ASSUME_NONNULL_END
