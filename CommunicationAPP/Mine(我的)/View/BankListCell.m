//
//  BankListCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "BankListCell.h"

@interface BankListCell(){
    
    UIImageView *_bgImageView;
    UIImageView *_titleImageView;
    UILabel *_titleLabel;
    UILabel *_typeLabel;
    UILabel *_numLabel;
    
}

@end

@implementation BankListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _bgImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_bgImageView];
    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(0);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    _titleImageView = [[UIImageView alloc]init];
    [_bgImageView addSubview:_titleImageView];
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgImageView.mas_top).offset(22);
        make.left.equalTo(_bgImageView.mas_left).offset(27);
        make.width.equalTo(@34);
        make.height.equalTo(@34);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = [UIFont systemFontOfSize:17];
    [_bgImageView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleImageView.mas_right).offset(10);
        make.centerY.equalTo(_titleImageView.mas_centerY);
    }];
    
    _typeLabel = [[UILabel alloc]init];
    _typeLabel.textColor = [UIColor whiteColor];
    _typeLabel.font = [UIFont systemFontOfSize:17];
    [_bgImageView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_bgImageView.mas_right).offset(-30);
        make.top.equalTo(_titleLabel.mas_top).offset(0);
    }];
    
    _numLabel = [[UILabel alloc]init];
    _numLabel.textColor = [UIColor whiteColor];
    _numLabel.font = [UIFont systemFontOfSize:22];
    [_bgImageView addSubview:_numLabel];
    [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleImageView.mas_bottom).offset(40);
        make.centerX.equalTo(_bgImageView.mas_centerX);
    }];
}

-(void)refreshModel{
    
    _bgImageView.image = [UIImage imageNamed:@"logo"];
    _titleImageView.image = [UIImage imageNamed:@"tipRow1"];
    _titleLabel.text = @"华夏银行卡";
    _typeLabel.text = @"储蓄卡";
    _numLabel.text = @"**** **** 6465";
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation BankListNoneCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"logo"];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(0);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    UIImageView *imageView1 = [[UIImageView alloc]init];
    imageView1.image = [UIImage imageNamed:@"tipRow1"];
    [imageView addSubview:imageView1];
    [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_top).offset(22);
        make.left.equalTo(imageView.mas_left).offset(27);
        make.width.equalTo(@34);
        make.height.equalTo(@34);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"观点";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:17];
    [imageView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView1.mas_right).offset(10);
        make.centerY.equalTo(imageView1.mas_centerY);
    }];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"tipRow1"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(addBank) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(imageView.mas_centerX);
        make.centerY.equalTo(imageView.mas_centerY);
        make.width.equalTo(@48);
        make.height.equalTo(@48);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = [UIColor whiteColor];
    label1.text = @"添加银行卡";
    label1.font = [UIFont systemFontOfSize:12];
    [imageView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btn.mas_bottom).offset(18);
        make.centerX.equalTo(imageView.mas_centerX);
    }];
}

-(void)addBank{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

