//
//  ChoosePayTypeCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "ChoosePayTypeCell.h"

@interface ChoosePayTypeCell(){
    
    UIImageView *_leftImageView;
    UILabel *_titleLabel;
    UIImageView *_chooseImageView;
    
}

@end

@implementation ChoosePayTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _leftImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
//    _chooseImageView = [[UIImageView alloc]init];
//    _chooseImageView.image = [UIImage imageNamed:@""];
//    _chooseImageView.hidden = true;
//    [self.contentView addSubview:_chooseImageView];
//    [_chooseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.contentView).offset(-15);
//        make.centerY.equalTo(self.contentView.mas_centerY);
//    }];
    
}


-(void)refreshModel:(NSString *)str{
    _titleLabel.text = str;
    _chooseImageView.image = [UIImage imageNamed:@""];
}

-(void)setModel:(PayTypeModel *)model{
    _model = model;
    _titleLabel.text = model.name;
    _leftImageView.image = [UIImage imageNamed:model.img];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
