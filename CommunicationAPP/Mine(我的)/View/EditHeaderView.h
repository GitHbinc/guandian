//
//  EditHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import <UIKit/UIKit.h>

@interface EditHeaderView : UIView
@property (nonatomic, copy) void(^headbuttonClick)(UIButton * btn);
@property(nonatomic ,strong)UIImageView *headImage;
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

@end
