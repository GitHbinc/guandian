//
//  SMAuthenticationResultHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import <UIKit/UIKit.h>

@interface SMAuthenticationResultHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property (nonatomic, strong) UILabel *titleLabel;
@end
