//
//  PrivacyMemberSwitchCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItem.h"

NS_ASSUME_NONNULL_BEGIN

@class PrivacyMemberSwitchCell;

@protocol PrivacyMemberSwitchCellDelegate <NSObject>
-(void)updatePrivacySwitchAtCell:(PrivacyMemberSwitchCell *)cell mySwitch:(UISwitch *)mySwitch;

@end

@interface PrivacyMemberSwitchCell : UITableViewCell
@property (strong , nonatomic)MineItem *mineItem;
@property (weak,nonatomic)id<PrivacyMemberSwitchCellDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
