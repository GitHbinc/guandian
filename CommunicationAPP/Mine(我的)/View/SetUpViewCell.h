//
//  SetUpViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItem.h"
#import "OptionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SetUpViewCell : UITableViewCell
@property (strong , nonatomic)MineItem *mineItem;
@property (strong , nonatomic)OptionModel *model;

@end

NS_ASSUME_NONNULL_END
