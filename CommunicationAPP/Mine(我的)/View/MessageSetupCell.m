//
//  MessageSetupCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MessageSetupCell.h"
@interface MessageSetupCell()
@property (strong , nonatomic)UILabel *titleLabel;
@property (strong , nonatomic)UISwitch *messageSwitch;
@end
@implementation MessageSetupCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}


#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    _titleLabel.text = @"评论";
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
    _messageSwitch = [[UISwitch alloc]init];
    _messageSwitch.onTintColor = HEXCOLOR(0x9C386A);
    [_messageSwitch addTarget:self action:@selector(switchDidClick:) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:_messageSwitch];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView.mas_left).offset(20);
    }];
    
    [_messageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
    }];
    
   
}

- (void)setMineItem:(MineItem *)mineItem{
    _mineItem = mineItem;
    self.titleLabel.text = mineItem.name;
    if ([mineItem.type isEqualToString:@"1"]) {
        _messageSwitch.on = YES;
    }else{
        _messageSwitch.on = NO;
    }
}


-(void)switchDidClick:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(updateSwitchAtCell:mySwitch:)]) {
        [self.delegate updateSwitchAtCell:self mySwitch:sender];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
