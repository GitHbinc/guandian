//
//  EditPersonInfoCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "EditPersonInfoCell.h"

@interface EditPersonInfoCell(){

    UILabel*_rightLabel;
    UIImageView *_arrowImageView;
    
}

@end

@implementation EditPersonInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _leftLabel = [[UILabel alloc]init];
    _leftLabel.textColor = color333333;
    _leftLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_leftLabel];
    [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _rightLabel = [[UILabel alloc]init];
    _rightLabel.textColor = color999999;
    _rightLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_rightLabel];
    [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(0);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
}

-(void)refreshData:(NSString *)contentStr{
    _rightLabel.text = contentStr;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
