//
//  CertificateCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "CertificateCell.h"

@interface CertificateCell(){
    
    UILabel *_titleLabel;
    UILabel *_shopLabel;
    UILabel *_timeLabel;
    UIButton *_moneyBtn;
    
}

@end

@implementation CertificateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).offset(5);
        make.top.equalTo(self.contentView.mas_top).offset(10);
    }];
    
    _shopLabel = [[UILabel alloc]init];
    _shopLabel.textColor = color999999;
    _shopLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_shopLabel];
    [_shopLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel.mas_left).offset(0);
        make.top.equalTo(_titleLabel.mas_bottom).offset(3);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color999999;
    _timeLabel.font = [UIFont systemFontOfSize:9];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel.mas_left).offset(0);
        make.top.equalTo(_shopLabel.mas_bottom).offset(3);
    }];
    
    _moneyBtn = [[UIButton alloc]init];
    [_moneyBtn setTitleColor:color333333 forState:UIControlStateNormal];
    _moneyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    _moneyBtn.layer.borderColor = color333333.CGColor;
    _moneyBtn.layer.cornerRadius = 14.0;
    _moneyBtn.layer.borderWidth = 1.0;
    [_moneyBtn addTarget:self action:@selector(moneyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_moneyBtn];
    [_moneyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(@28);
        make.width.equalTo(@75);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = color999999;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
}


-(void)refreshModel:(NSInteger)tag{
    
    _titleLabel.text = @"京东50元购物券";
    _shopLabel.text = @"任何店铺";
    _timeLabel.text = @"2018.11.04-2018.12.04";
    [_moneyBtn setTitle:@"￥50" forState:UIControlStateNormal];
    _moneyBtn.tag = tag;
}

-(void)moneyBtnClick:(UIButton *)senderBtn{
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
