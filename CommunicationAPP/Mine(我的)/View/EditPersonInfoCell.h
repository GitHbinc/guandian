//
//  EditPersonInfoCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import <UIKit/UIKit.h>

@interface EditPersonInfoCell : UITableViewCell

@property (nonatomic,strong) UILabel *leftLabel;
-(void)refreshData:(NSString *)contentStr;

@end
