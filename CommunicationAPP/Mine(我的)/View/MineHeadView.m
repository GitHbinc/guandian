//
//  MineHeadView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineHeadView.h"
#import "classfyBtn.h"
@interface MineHeadView()
@property (nonatomic ,strong)UIView *inforView;
@property (nonatomic ,strong)UIImageView *headImage;
@property (nonatomic ,strong)UIImageView *GDImage;
@property (nonatomic ,strong)UIImageView *rightImage;
@property (nonatomic ,strong)UIButton *qrBtn;
@property (nonatomic ,strong)UILabel *nickName;
@property (nonatomic ,strong)UILabel *numLabel;
@property (nonatomic ,strong)UILabel *signLabel;
@property (nonatomic ,strong)UILabel *loginLabel;
@property (nonatomic ,strong)classfyBtn *huifang;
@property (nonatomic ,strong)classfyBtn *guanzhu;
@property (nonatomic ,strong)classfyBtn *fensi;
@property (nonatomic ,strong)classfyBtn *shoucang;

@end
@implementation MineHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self setUpUI];
        
    }
    return self;
}

-(void)setUpUI{
    
    _inforView = [UIView new];
    _inforView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_inforView];
    
    
    _headImage = [[UIImageView alloc] init];
    _headImage.layer.cornerRadius = 40.0;
    _headImage.clipsToBounds = YES;
    _headImage.image = [UIImage imageNamed:@"default_square"];
    [_inforView addSubview:_headImage];
    
    _nickName = [[UILabel alloc] init];
    _nickName.font = [UIFont boldSystemFontOfSize:14];
    _nickName.textColor = HEXCOLOR(0x333333);
    _nickName.textAlignment = NSTextAlignmentCenter;
    _nickName.hidden = YES;
    [_inforView addSubview:_nickName];
    
    _GDImage = [[UIImageView alloc] init];
    _GDImage.image = [UIImage imageNamed:@"mine_gd"];
    _GDImage.hidden = YES;
    [_inforView addSubview:_GDImage];
    
    _rightImage = [[UIImageView alloc] init];
    _rightImage.image = [UIImage imageNamed:@"guan_right"];
    [_inforView addSubview:_rightImage];
    
    _numLabel = [[UILabel alloc] init];
    _numLabel.textColor = HEXCOLOR(0x333333);
    _numLabel.font = [UIFont systemFontOfSize:11];
    _numLabel.textAlignment = NSTextAlignmentCenter;
    _numLabel.hidden = YES;
    [_inforView addSubview:_numLabel];
    
    _loginLabel = [[UILabel alloc] init];
    _loginLabel.textColor = HEXCOLOR(0x333333);
    _loginLabel.text = @"登录/注册";
    _loginLabel.font = [UIFont systemFontOfSize:15];
    _loginLabel.textAlignment = NSTextAlignmentCenter;
    [_inforView addSubview:_loginLabel];
    
    _signLabel = [[UILabel alloc] init];
    _signLabel.textColor = [UIColor darkGrayColor];
    _signLabel.textColor = HEXCOLOR(0x666666);
    _signLabel.font = [UIFont systemFontOfSize:11];
    _signLabel.textAlignment = NSTextAlignmentCenter;
    _signLabel.hidden = YES;
    [_inforView addSubview:_signLabel];
    
    _qrBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_qrBtn setImage:[UIImage imageNamed:@"mine_qr"] forState:UIControlStateNormal];
    [_qrBtn addTarget:self action:@selector(qrAction) forControlEvents:UIControlEventTouchUpInside];
    [_inforView addSubview:_qrBtn];
    
    
    _huifang = [classfyBtn new];
    _huifang.name.text = @"回放";
    _huifang.btn.tag = 1;
    [_huifang.btn addTarget:self action:@selector(classAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_huifang];
    
    _guanzhu = [classfyBtn new];
    _guanzhu.name.text = @"关注";
    _guanzhu.btn.tag = 2;
    [_guanzhu.btn addTarget:self action:@selector(classAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_guanzhu];
    
    _fensi = [classfyBtn new];
    _fensi.name.text = @"粉丝";
    _fensi.btn.tag = 3;
    [_fensi.btn addTarget:self action:@selector(classAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_fensi];
    
    _shoucang = [classfyBtn new];
    _shoucang.name.text = @"收藏";
     _shoucang.btn.tag = 4;
     [_shoucang.btn addTarget:self action:@selector(classAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_shoucang];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inforViewAction)];
    [_inforView addGestureRecognizer:tapGesture];
    
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
     __weak __typeof(self)weakSelf = self;
    [_inforView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.equalTo(self).offset(0);
        make.size.height.equalTo(@120);
        
    }];
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.inforView).offset(20);
        make.centerY.equalTo(weakSelf.inforView.mas_centerY);
        make.width.height.mas_equalTo(80);
        
    }];
    
    
    [_nickName  mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(weakSelf.headImage).offset(5);
       make.left.equalTo(weakSelf.headImage.mas_right).offset(10);
    }];
    
    [_GDImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nickName.mas_right).offset(10);
        make.centerY.equalTo(weakSelf.nickName.mas_centerY);
        
    }];
    
    [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nickName.mas_left);
        make.centerY.equalTo(weakSelf.headImage.mas_centerY);
    }];
    
    [_loginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nickName.mas_left);
        make.centerY.equalTo(weakSelf.headImage.mas_centerY);
    }];
    
    
    [_signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nickName.mas_left);
        make.bottom.equalTo(weakSelf.headImage.mas_bottom).offset(-5);
    }];
    
    
    [_rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.inforView).offset(-20);
        make.centerY.equalTo(weakSelf.inforView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(10, 18));
        
    }];
    
    [_qrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.inforView).offset(-50);
        make.centerY.equalTo(weakSelf.inforView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(28, 28));
        
    }];
    
    
    [_huifang mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.inforView).offset(0);
        make.top.mas_equalTo(weakSelf.inforView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH/4, 60));
        
    }];
    
    [_guanzhu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.huifang.mas_right).offset(0);
        make.top.mas_equalTo(weakSelf.huifang.mas_top);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH/4, 60));
        
    }];
    
    [_fensi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.guanzhu.mas_right).offset(0);
        make.top.mas_equalTo(weakSelf.huifang.mas_top);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH/4, 60));
        
    }];
    
    [_shoucang mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.fensi.mas_right).offset(0);
        make.top.mas_equalTo(weakSelf.huifang.mas_top);
        make.size.mas_equalTo(CGSizeMake(kSCREENWIDTH/4, 60));
        
    }];
    
}

-(void)setModel:(UserModel *)model{
    _model = model;
    _nickName.hidden = NO;
    _numLabel.hidden = NO;
    _signLabel.hidden = NO;
    _GDImage.hidden = NO;
    _loginLabel.hidden = YES;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nickName.text = model.username;
    _numLabel.text = [NSString stringWithFormat:@"观点号：%@",model.ID];
    _signLabel.text = model.autograph.length >0?model.autograph:@"您还没有签名哦～";
    _huifang.num.text = model.myplayback;
    _guanzhu.num.text = model.myfollow;
    _fensi.num.text = model.myfans;
    _shoucang.num.text = model.mycollect;
}

-(void)inforViewAction{
    !_inforClickBlock ? : _inforClickBlock();
}

-(void)qrAction{
     !_qrClickBlock ? : _qrClickBlock();
}


-(void)classAction:(UIButton *)btn{
    if (_classfybuttonClick) {
        _classfybuttonClick(btn);
    }
}


@end
