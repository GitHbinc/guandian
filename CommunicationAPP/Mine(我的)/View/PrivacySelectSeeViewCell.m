//
//  PrivacySelectSeeViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PrivacySelectSeeViewCell.h"
@interface PrivacySelectSeeViewCell()
@property(nonatomic ,strong)UILabel *nameLabel;
@end
@implementation PrivacySelectSeeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

-(void)setUpUI{
    _selectSeeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_selectSeeBtn setImage:[UIImage imageNamed:@"weixuanzhong"] forState:UIControlStateNormal];
    [_selectSeeBtn setImage:[UIImage imageNamed:@"xuanzhong"] forState:UIControlStateSelected];
    [self.contentView addSubview:_selectSeeBtn];
    
    _nameLabel = [UILabel new];
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_nameLabel];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;
    
    [_selectSeeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.contentView);
       
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.selectSeeBtn.mas_right)setOffset:10];
         make.centerY.mas_equalTo(weakSelf.contentView);
    }];
    
}

-(void)setItem:(MineItem *)item{
    _item = item;
    _nameLabel.text = item.name;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectSeeBtn.selected = selected;
    
    // Configure the view for the selected state
}

@end

