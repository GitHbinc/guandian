//
//  ZBAuthenticationCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import <UIKit/UIKit.h>

@interface ZBAuthenticationCell : UITableViewCell

-(void)refreshData:(NSString *)imageStr titleStr:(NSString *)titleStr contentStr:(NSString *)contentStr stateStr:(NSString *)stateStr;

@end

@interface ZBAuthenticationTypeCell : UITableViewCell


@end

@interface ZBAuthenticationInstructionCell : UITableViewCell

-(void)refreshData:(NSString *)titleStr contentStr:(NSString *)contentStr;

@end
