//
//  TimePartnerHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "TimePartnerHeaderView.h"

@implementation TimePartnerHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"time_bg_icon"];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.height.equalTo(@100);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"时间合伙人申请表";
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:16];
    [imageView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(imageView.mas_centerX);
        make.centerY.equalTo(imageView.mas_centerY);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.text = @"观点自2018年12月起，观点时间代理项目正式启动，面向全国招募时间合伙人，负责观点网络平台在各城市的市场开拓、时间管理等业务内容，邀请心怀梦想的时间合伙人一起开疆扩土，共同打造中国领先的娱乐交互平台。";
    label1.textColor = color999999;
    label1.font = [UIFont systemFontOfSize:13];
    label1.numberOfLines = 0;
    [self addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.right.equalTo(self).offset(-10);
    }];
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.text = @"选择成为时间合伙人";
    label2.textColor = color333333;
    label2.font = [UIFont systemFontOfSize:19];
    [self addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(35);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    CGFloat width = kSCREENWIDTH / 3;
    
    
    NSArray *imageArray = @[@"time_big_icon",@"time_zongdaili",@"time_shijian"];
    NSArray *titleArray = @[@"大区",@"总代理",@"时间合伙人"];
    NSArray *moneyArray = @[@"120W",@"20W",@"2.4K"];
    for (int i = 0; i<titleArray.count; i++) {
        UIView *view = [self createTypeView:imageArray[i] titleStr:titleArray[i] moneyStr:moneyArray[i] tag:101 + i];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(width * i);
            make.bottom.equalTo(self).offset(-10);
            make.width.equalTo(@(width));
            make.height.equalTo(@100);
        }];
    }
}

-(UIView *)createTypeView:(NSString *)imageStr titleStr:(NSString *)titleStr moneyStr:(NSString *)moneyStr tag:(NSInteger)tag{
    UIView *view = [[UIView alloc]init];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    btn.tag = tag;
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_top).offset(5);
        make.centerX.equalTo(view.mas_centerX);
        make.width.equalTo(@60);
        make.height.equalTo(@60);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color333333;
    label.font = [UIFont systemFontOfSize:15];
    label.text = titleStr;
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btn.mas_bottom).offset(16);
        make.centerX.equalTo(view.mas_centerX);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.text = moneyStr;
    label1.textColor = color999999;
    label1.font = [UIFont systemFontOfSize:11];
    [view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(2);
        make.centerX.equalTo(view.mas_centerX);
    }];
    
    return view;
}

-(void)btnClick:(UIButton *)senderBtn{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
