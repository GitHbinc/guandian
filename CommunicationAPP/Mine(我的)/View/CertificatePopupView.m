//
//  CertificatePopupView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "CertificatePopupView.h"

@implementation CertificatePopupView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 5.0;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@182);
        make.height.equalTo(@100);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"确定兑换购物券？";
    label.textColor = color999999;
    label.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView.mas_top).offset(25);
        make.centerX.equalTo(bgView.mas_centerX);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = color999999;
    [bgView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(0);
        make.right.equalTo(bgView.mas_right).offset(0);
        make.bottom.equalTo(bgView.mas_bottom).offset(-33);
        make.height.equalTo(@1);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = color999999;
    [bgView addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.bottom.equalTo(bgView.mas_bottom).offset(0);
        make.centerX.equalTo(bgView.mas_centerX);
        make.width.equalTo(@1);
    }];
    
    UIButton *canelBtn = [[UIButton alloc]init];
    [canelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [canelBtn setTitleColor:color999999 forState:UIControlStateNormal];
    canelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    canelBtn.tag = 101;
    [canelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:canelBtn];
    [canelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.right.equalTo(lineView1.mas_left).offset(0);
        make.bottom.equalTo(bgView.mas_bottom).offset(0);
    }];
    
    UIButton *sureBtn = [[UIButton alloc]init];
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:color999999 forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    sureBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView.mas_right).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.left.equalTo(lineView1.mas_right).offset(0);
        make.bottom.equalTo(bgView.mas_bottom).offset(0);
    }];
}

-(void)btnClick:(UIButton *)senderBtn{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
