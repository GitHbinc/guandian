//
//  MineRechargeView.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineRechargeView.h"
@interface MineRechargeView(){
    
    UILabel *_allMoneyLabel;
    UILabel *_availableBalanceLabel;
    UILabel *_purchasedPointsLabel;
    UILabel *_freezeMoneyLabel;
    
    UITextField *_moneyTextField;
    UITextField *_timeTextField;
    
}

@end

@implementation MineRechargeView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.dataArray = [NSMutableArray array];
        //[self initView];
    }
    return self;
}

-(void)setModel:(MealModel *)model{
    _model = model;
    [self.dataArray addObjectsFromArray:[DianListModel arrayOfModelsFromDictionaries:model.meal error:nil]];
    
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.image = [UIImage imageNamed:@"mine_chongzhi"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.top.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(@100);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = [UIColor whiteColor];
    _allMoneyLabel.text = model.alldian;
    _allMoneyLabel.font = [UIFont systemFontOfSize:35];
    [bgImageView addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImageView.mas_top).offset(20);
        make.centerX.equalTo(bgImageView.mas_centerX);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"点币";
    [bgImageView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(10);
        make.centerX.equalTo(bgImageView.mas_centerX);
    }];
    
    CGFloat width = (kSCREENWIDTH - 50) / 3;
//    NSArray *titleStr = @[@"42点币",@"84点币",@"126点币",@"168点币",@"210点币"];
//    NSArray *moneyStr = @[@"42.00",@"84.00",@"126.00",@"168.00",@"210.00"];
    for (int i = 0; i<self.dataArray.count; i++) {
        DianListModel *model = self.dataArray[i];
        NSString *dianStr = [NSString stringWithFormat:@"%@点币",model.dian];
        NSString *priceStr = [NSString stringWithFormat:@"¥%@",model.prices];
        if (i==self.dataArray.count-1) {
            dianStr = model.timer;
        }
        UIButton *btn = [self setBtn:dianStr moneyStr:priceStr tag:101+i];
        
        [self addSubview:btn];
        
        CGFloat x = 15 + (width + 10)*i;
        CGFloat y = 15;
        if (i > 2) {
            x = 15 + (width + 10) * (i - 3);
            y = 15 + (54 + 15);
        }
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(x);
            make.top.equalTo(bgImageView.mas_bottom).offset(y);
            make.width.equalTo(@(width));
            make.height.equalTo(@54);
        }];
    }
    
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    btn.clipsToBounds = YES;
//    [btn.layer setCornerRadius:5.0];
//    btn.layer.borderWidth = 1;
//    btn.layer.borderColor = color999999.CGColor;
//    btn.tag = 106;
//
//    [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:btn];
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self).offset(-15);
//        make.top.equalTo(bgImageView.mas_bottom).offset(84);
//        make.width.equalTo(@(width));
//        make.height.equalTo(@54);
//    }];
//
//    UILabel *label1 = [[UILabel alloc]init];
//    label1.text = @"时间合伙人";
//    label1.textColor = color999999;
//    label1.font = [UIFont systemFontOfSize:13];
//    [btn addSubview:label1];
//    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(btn.mas_top).offset(8);
//        make.centerX.equalTo(btn.mas_centerX);
//    }];
//
//    _timeTextField = [[UITextField alloc]init];
//    _timeTextField.placeholder = @"请入金额";
//    _timeTextField.textColor = color999999;
//    _timeTextField.font = [UIFont systemFontOfSize:12];
//    [btn addSubview:_timeTextField];
//    [_timeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(label1.mas_bottom).offset(5);
//        make.centerX.equalTo(btn.mas_centerX);
//    }];
}

-(UIButton *)setBtn:(NSString *)titleStr moneyStr:(NSString *)moneyStr tag:(NSInteger)tag{
    
   UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = color999999.CGColor;
    btn.clipsToBounds = YES;
    [btn.layer setCornerRadius:5.0];
    btn.tag = tag;
    [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label = [self setLabel:titleStr font:14 color:color333333];
    label.tag = tag + 6;
    [btn addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btn.mas_top).offset(8);
        make.centerX.equalTo(btn.mas_centerX);
    }];
    
    UILabel *label2 = [self setLabel:moneyStr font:13 color:color999999];
    label2.tag = tag + 12;
    [btn addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(5);
        make.centerX.equalTo(btn.mas_centerX);
    }];
    
    return btn;
}

-(UILabel *)setLabel:(NSString *)titleStr font:(CGFloat)fontStr color:(UIColor *)color{
    
    UILabel *label = [[UILabel alloc]init];
    label.text = titleStr;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:fontStr];
    return label;
    
}

-(void)allBtnClick:(UIButton *)senderBtn{
    senderBtn.layer.borderColor = HEXCOLOR(0x9C386A).CGColor;
    [senderBtn setBackgroundImage:[UIImage imageNamed:@"mine_xuanzhong"] forState:UIControlStateNormal];
    UILabel *label = [self viewWithTag:senderBtn.tag + 6];
    label.textColor = HEXCOLOR(0x9C386A);
    UILabel *label1 = [self viewWithTag:senderBtn.tag + 12];
    label1.textColor = HEXCOLOR(0x9C386A);
    DianListModel *model = [self.dataArray objectAtIndex:senderBtn.tag-101];
    if (_selectDianClick) {
        _selectDianClick(model);
    }
    _allMoneyLabel.text = model.prices;
    for (int i = 0; i < 6; i++) {
        if (i + 101 == senderBtn.tag) {
            continue;
        }
        
        UIButton *btn = [self viewWithTag:i + 101];
        btn.layer.borderColor = color999999.CGColor;
        [btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        UILabel *lab = [self viewWithTag:i + 107];
        lab.textColor = color333333;
        UILabel *labe = [self viewWithTag:i + 113];
        labe.textColor = color999999;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
