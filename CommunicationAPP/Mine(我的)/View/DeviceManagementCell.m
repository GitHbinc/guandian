//
//  DeviceManagementCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "DeviceManagementCell.h"
@interface DeviceManagementCell()
@property (strong , nonatomic)UILabel *titleLabel;
@property (strong , nonatomic)UILabel *dateLabel;
@property (strong , nonatomic)UILabel *mineLabel;


@end
@implementation DeviceManagementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    _titleLabel.text = @"iphone1";
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    
    _dateLabel = [UILabel new];
    _dateLabel.text = @"2018/11/23 11:20";
    _dateLabel.textColor = HEXCOLOR(0x999999);
    _dateLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_dateLabel];
    
    _mineLabel = [UILabel new];
    _mineLabel.text = @"本机";
    _mineLabel.textColor = HEXCOLOR(0x999999);
    _mineLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_mineLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView).offset(-10);
        make.left.mas_equalTo(self.contentView.mas_left).offset(10);
    }];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.left.mas_equalTo(self.contentView.mas_left).offset(10);
    }];
    
    [_mineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
    }];
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
