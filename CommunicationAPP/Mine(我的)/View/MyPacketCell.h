//
//  MyPacketCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface MyPacketCell : UITableViewCell

-(void)refreshData:(NSString *)titleStr;

@end
