//
//  AddBankCardCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "AddBankCardCell.h"
#define codeTag  10

@interface AddBankCardCell()<UITextFieldDelegate>{

    UIImageView *_arrowImageView;
    UILabel *_titleLabel;
    UITextField *_contentTextField;
    UIButton *_codeBtn;
    NSTimer     * _countTime;
    NSInteger _codetag;
    
}

@end

@implementation AddBankCardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
     _codetag = codeTag;
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _contentTextField = [[UITextField alloc]init];
    _contentTextField.textColor = color333333;
    _contentTextField.font = [UIFont systemFontOfSize:14];
    _contentTextField.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_contentTextField];
     [_contentTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_right).offset(20);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _arrowImageView = [[UIImageView alloc]init];
    _arrowImageView.image = [UIImage imageNamed:@""];
    _arrowImageView.hidden = true;
    [self.contentView addSubview:_arrowImageView];
    [_arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _codeBtn = [[UIButton alloc]init];
    [_codeBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _codeBtn.hidden = true;
    _codeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [_codeBtn setBackgroundImage:[UIImage imageNamed:@"code_normal_icon"] forState:UIControlStateNormal];
    [_codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-20);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
}

-(void)refreshModel:(NSString *)titleStr placeholderStr:(NSString *)placeholderStr row:(NSInteger)row{
    _titleLabel.text = titleStr;
    _contentTextField.placeholder = placeholderStr;
    if ([titleStr isEqualToString:@"验证码"]) {
        _codeBtn.hidden = false;
    }
    
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    NSLog(@"文字改变：%@",textChange.text);
      if ([self.delegate respondsToSelector:@selector(updateTextFieldAtCell:myTextField:)]) {
        [self.delegate updateTextFieldAtCell:self myTextField:textChange];
    }
}


-(void)codeBtnClick:(UIButton *)sender{
  
    if (_codeClick) {
        _codeClick(sender);
    }
    
    _codeBtn.userInteractionEnabled = NO;
    [_codeBtn setTitle:[NSString stringWithFormat:@"%lds",(long)_codetag] forState:UIControlStateNormal];
    _countTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
}

//验证码倒计时
- (void)countdown{
    NSInteger count = _codetag;
    count --;
    _codetag = count;
    if (count<0){
        _codeBtn.userInteractionEnabled = YES;
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countTime invalidate];
        _codetag = codeTag;
    }else{
        [_codeBtn setTitle:[NSString stringWithFormat:@"%lds",(long)_codetag] forState:UIControlStateNormal];
        
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
