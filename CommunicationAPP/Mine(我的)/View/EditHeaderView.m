//
//  EditHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import "EditHeaderView.h"


@implementation EditHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    _headImage = [[UIImageView alloc]init];
    _headImage.clipsToBounds = YES;
     _headImage.layer.cornerRadius = 40.0;
    [self addSubview:_headImage];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(25);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@80);
        make.height.equalTo(@80);
    }];
    
    UIButton *headerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    headerBtn.tag = 101;
    [headerBtn addTarget:self action:@selector(headerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    headerBtn.layer.cornerRadius = 40.0;
    [self addSubview:headerBtn];
    [headerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(25);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@80);
        make.height.equalTo(@80);
    }];
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:@"点击更换头像" forState:UIControlStateNormal];
    [btn setTitleColor:color333333 forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    btn.tag = 102;
    [btn addTarget:self action:@selector(headerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerBtn.mas_bottom).offset(16);
        make.centerX.equalTo(self.mas_centerX);
    }];
}

-(void)headerBtnClick:(UIButton *)senderBtn{
    if (_headbuttonClick) {
        _headbuttonClick(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
