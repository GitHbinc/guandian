//
//  WithdrawalHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "WithdrawalHeaderView.h"

@interface WithdrawalHeaderView(){
    
    UILabel *_allMoneyLabel;
    UITextField *_moneyTextField;
    
}

@end

@implementation WithdrawalHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color666666;
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"出售时间";
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.top.equalTo(self).offset(15);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = color333333;
    label1.font = [UIFont systemFontOfSize:25];
    label1.text = @"￥";
    [self addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_left).offset(0);
        make.top.equalTo(label.mas_bottom).offset(20);
        make.width.equalTo(@20);
    }];
    
    _moneyTextField = [[UITextField alloc]init];
    _moneyTextField.textColor = color333333;
    _moneyTextField.font = [UIFont systemFontOfSize:20];
    [_moneyTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:_moneyTextField];
    [_moneyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_right).offset(5);
        make.right.equalTo(self).offset(-5);
        make.centerY.equalTo(label1.mas_centerY);
        make.height.equalTo(@40);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(-30);
        make.height.equalTo(@1);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = color666666;
    _allMoneyLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_left).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(6);
    }];
    
    UIButton *allBtn = [[UIButton alloc]init];
    [allBtn setTitle:@"全部出售" forState:UIControlStateNormal];
    [allBtn setTitleColor:HEXCOLOR(0x9D396B) forState:UIControlStateNormal];
    allBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [allBtn addTarget:self action:@selector(allBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:allBtn];
    [allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self->_allMoneyLabel.mas_centerY);
    }];
    
}

-(void)allBtnClick{
    if (_totalSellClick) {
        _totalSellClick();
    }
}

//监听改变方法
- (void)textFieldTextDidChange:(UITextField *)textChange{
    NSLog(@"文字改变：%@",textChange.text);
    if (_timeText) {
        _timeText(textChange.text);
    }
}

-(void)setModel:(ProfitModel *)model{
    _model = model;
    _allMoneyLabel.text = [NSString stringWithFormat:@"余额%@小时",model.times];
}

-(void)refreshData{
 _moneyTextField.text = _model.times;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
