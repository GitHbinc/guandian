//
//  RecentlyWatchCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>
#import "RecentListModel.h"

@interface RecentlyWatchCell : UITableViewCell
@property(nonatomic ,strong)RecentListModel *model;
-(void)refreshModel;

@end
