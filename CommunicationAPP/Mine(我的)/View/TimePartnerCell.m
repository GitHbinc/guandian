//
//  TimePartnerCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "TimePartnerCell.h"

@interface TimePartnerCell(){
    
    UILabel *_titleLabel;
    UITextField *_contentTextField;
    
}

@end


@implementation TimePartnerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont systemFontOfSize:13];
    _titleLabel.textColor = color333333;
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.top.equalTo(self.contentView).offset(20);
    }];
    
    _contentTextField = [[UITextField alloc]init];
    _contentTextField.textColor = color333333;
    _contentTextField.font = [UIFont systemFontOfSize:13];
    _contentTextField.layer.cornerRadius = 5.0;
    _contentTextField.layer.borderColor = color999999.CGColor;
    _contentTextField.layer.borderWidth = 1.0;
    [self.contentView addSubview:_contentTextField];
    [_contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel.mas_left).offset(-2);
        make.bottom.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(-18);
        make.height.equalTo(@33);
    }];
    
}


-(void)refreshData:(NSString *)titleStr placeholderStr:(NSString *)placeholderStr tag:(NSInteger)tag{
    _titleLabel.text = titleStr;
    _contentTextField.placeholder = placeholderStr;
    _contentTextField.tag = tag;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation TimePartnerCodeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.textColor = color333333;
    titleLabel.text = @"手机号";
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.top.equalTo(self.contentView).offset(20);
    }];
    
    UITextField *contentTextField = [[UITextField alloc]init];
    contentTextField.textColor = color333333;
    contentTextField.font = [UIFont systemFontOfSize:13];
    contentTextField.layer.cornerRadius = 5.0;
    contentTextField.placeholder = @"请输入手机号";
    contentTextField.layer.borderColor = color999999.CGColor;
    contentTextField.tag = 106;
    contentTextField.layer.borderWidth = 1.0;
    [self.contentView addSubview:contentTextField];
    [contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left).offset(-2);
        make.bottom.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(-133);
        make.height.equalTo(@33);
    }];
    UIButton *codeBtn = [[UIButton alloc]init];
    [codeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [codeBtn setTitleColor:color999999 forState:UIControlStateNormal];
    codeBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [codeBtn addTarget:self action:@selector(codeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    codeBtn.layer.borderWidth = 1.0;
    codeBtn.layer.cornerRadius = 5.0;
    codeBtn.layer.borderColor = color333333.CGColor;
    [self.contentView addSubview:codeBtn];
    [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-18);
        make.top.equalTo(contentTextField.mas_top).offset(0);
        make.width.equalTo(@100);
        make.height.equalTo(@33);
    }];
}

-(void)codeBtnClick{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


