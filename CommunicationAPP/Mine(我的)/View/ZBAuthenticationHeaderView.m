//
//  ZBAuthenticationHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "ZBAuthenticationHeaderView.h"

@interface ZBAuthenticationHeaderView(){
    
    UIImageView *_headerImageView;
    UILabel *_titleLabel;
    UILabel *_accountLabel;
    
}

@end

@implementation ZBAuthenticationHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    _headerImageView.layer.cornerRadius = 25.0;
    _headerImageView.clipsToBounds = YES;
    [_headerImageView.layer setCornerRadius:25.0];
    [view addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(15);
        make.centerY.equalTo(view.mas_centerY);
        make.width.equalTo(@50);
        make.height.equalTo(@50);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [view addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_headerImageView.mas_top).offset(8);
        make.left.equalTo(self->_headerImageView.mas_right).offset(10);
    }];
    
    _accountLabel = [[UILabel alloc]init];
    _accountLabel.textColor = color666666;
    _accountLabel.font = [UIFont systemFontOfSize:11];
    [view addSubview:_accountLabel];
    [_accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.bottom.equalTo(self->_headerImageView.mas_bottom).offset(-8);
    }];
    
}

-(void)refreshModel{
     UserModel *model = [[UserHander shareHander] loginUserInfo];
     [_headerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_square"]];
   
    _titleLabel.text = model.username;
    _accountLabel.text = [NSString stringWithFormat:@"观点号：%@",model.ID];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
