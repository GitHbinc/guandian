//
//  classfyBtn.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface classfyBtn : UIView
@property(nonatomic ,strong)UIButton *btn;
@property(nonatomic ,strong)UILabel *num;
@property(nonatomic ,strong)UILabel *name;

@end

NS_ASSUME_NONNULL_END
