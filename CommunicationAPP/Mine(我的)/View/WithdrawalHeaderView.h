//
//  WithdrawalHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>
#import "ProfitModel.h"

@interface WithdrawalHeaderView : UIView
@property(nonatomic ,strong)ProfitModel *model;
@property (nonatomic, copy)void(^timeText)(NSString *time);
@property (nonatomic, copy) void(^totalSellClick)(void);
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refreshData;

@end
