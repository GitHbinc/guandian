//
//  AddBankCardCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>
@class AddBankCardCell;

@protocol AddBankCardCellDelegate <NSObject>
-(void)updateTextFieldAtCell:(AddBankCardCell *)cell myTextField:(UITextField *)textField;
@end


@interface AddBankCardCell : UITableViewCell

@property (weak,nonatomic)id<AddBankCardCellDelegate>delegate;

@property (nonatomic, copy)void(^codeClick)(UIButton * sender);
-(void)refreshModel:(NSString *)titleStr placeholderStr:(NSString *)placeholderStr row:(NSInteger)row;

@end
