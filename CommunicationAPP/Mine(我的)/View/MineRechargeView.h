//
//  MineRechargeView.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/21.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MealModel.h"
#import "DianListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineRechargeView : UIView
@property(nonatomic ,strong)NSMutableArray *dataArray;
@property(nonatomic ,strong)MealModel *model;
@property (nonatomic, copy) void(^selectDianClick)(DianListModel * model);
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
