//
//  TimePartnerCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import <UIKit/UIKit.h>

@interface TimePartnerCell : UITableViewCell

-(void)refreshData:(NSString *)titleStr placeholderStr:(NSString *)placeholderStr tag:(NSInteger)tag;

@end

@interface TimePartnerCodeCell : UITableViewCell

@end
