//
//  RechargeCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>
#import "PayTypeModel.h"

@interface RechargeCell : UITableViewCell

@property (nonatomic, copy) void(^inputClick)(NSString * time);

@end

@interface RechargePayCell : UITableViewCell

-(void)refreshModel:(PayTypeModel *)model;

@end
