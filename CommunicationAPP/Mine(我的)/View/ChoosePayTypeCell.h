//
//  ChoosePayTypeCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import <UIKit/UIKit.h>
#import "PayTypeModel.h"

@interface ChoosePayTypeCell : UITableViewCell
@property(nonatomic ,strong)PayTypeModel *model;
-(void)refreshModel:(NSString *)str;

@end
