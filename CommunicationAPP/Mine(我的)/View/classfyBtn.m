//
//  classfyBtn.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "classfyBtn.h"

@implementation classfyBtn

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
        
    }
    return self;
}

-(void)setUpUI{
    
    _btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:_btn];
    
    _num = [[UILabel alloc] init];
    _num.text = @"0";
    _num.textColor = HEXCOLOR(0x333333);
    _num.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    _num.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_num] ;
    
    
    _name = [[UILabel alloc] init];
    _name.text = @"回放";
    _name.font = [UIFont systemFontOfSize:12];
    _name.textColor = HEXCOLOR(0x909090);
    _name.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_name];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    //__weak __typeof(self)weakSelf = self;
    [_btn  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(self);
        make.centerX.equalTo(self.mas_centerX).offset(0);
        make.centerY.equalTo(self.mas_centerY).offset(0);
    }];
    
    
    [_num  mas_makeConstraints:^(MASConstraintMaker *make) {
       make.centerX.equalTo(self.mas_centerX).offset(0);
       make.centerY.equalTo(self.mas_centerY).offset(-10);
    }];
    
   [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX).offset(0);
        make.centerY.equalTo(self.mas_centerY).offset(10);
    }];
    
}


@end
