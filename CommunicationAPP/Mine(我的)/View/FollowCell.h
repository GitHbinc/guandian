//
//  FollowCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/12.
//

#import <UIKit/UIKit.h>
#import "AttentionListModel.h"

@interface FollowCell : UITableViewCell
@property (nonatomic ,strong)AttentionListModel *model;
@property (nonatomic, copy) void(^attentionbuttonClick)(AttentionListModel *model);
@property (nonatomic, copy) void(^headBtnClick)(AttentionListModel *model);

-(void)refreshModel;

@end
