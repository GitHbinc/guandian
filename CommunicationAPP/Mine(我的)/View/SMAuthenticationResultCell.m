//
//  SMAuthenticationResultCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "SMAuthenticationResultCell.h"


@interface SMAuthenticationResultCell(){
    
    UILabel *_titleLabel;
    UILabel *_contentLabel;
    
}

@end

@implementation SMAuthenticationResultCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = color333333;
    _contentLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_right).offset(20);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
}
    
-(void)refreshData:(NSString *)titleStr contentStr:(NSString *)contentStr{
    
    _titleLabel.text = titleStr;
    _contentLabel.text = contentStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
