//
//  ZBAuthenticationCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import "ZBAuthenticationCell.h"


@interface ZBAuthenticationCell(){
    
    UIImageView *_headerImageView;
    UILabel *_titleLabel;
    UILabel *_contentLabel;
    UILabel *_stateLabel;
}

@end

@implementation ZBAuthenticationCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _headerImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@42);
        make.height.equalTo(@42);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_right).offset(10);
        make.top.equalTo(self->_headerImageView.mas_top).offset(5);
    }];
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = color999999;
    _contentLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.bottom.equalTo(self->_headerImageView.mas_bottom).offset(-5);
    }];
    
    _stateLabel = [[UILabel alloc]init];
    _stateLabel.textColor = color999999;
    _stateLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_stateLabel];
    [_stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
}

-(void)refreshData:(NSString *)imageStr titleStr:(NSString *)titleStr contentStr:(NSString *)contentStr stateStr:(NSString *)stateStr{
    
    _headerImageView.image = [UIImage imageNamed:imageStr];
    _titleLabel.text = titleStr;
    _contentLabel.text = contentStr;
    _stateLabel.text = stateStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation ZBAuthenticationTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    NSArray *titleArray = @[@"女神",@"男神",@"萌妹",@"音乐",@"模特",@"情感",@"旅游",@"搞笑",@"美食",@"时尚",@"明星",@"现场"];
    for (int i = 0; i<titleArray.count; i++) {
        UIButton *btn = [[UIButton alloc]init];
        [btn setTitle:titleArray[i] forState:UIControlStateNormal];
        [btn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        btn.layer.cornerRadius = 10.0;
        btn.layer.borderColor = MAINCOLOR.CGColor;
        btn.layer.borderWidth = 1.0;
        btn.tag = 101 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset((kSCREENWIDTH - 308)/2 + (i % 4)*80);
            make.top.equalTo(self.contentView).offset(15 + (i / 4)*48);
            make.height.equalTo(@28);
            make.width.equalTo(@68);
        }];
        
    }
}

-(void)btnClick:(UIButton *)senderBtn{
    
    senderBtn.selected = !senderBtn.selected;
    if (senderBtn.selected) {
        [senderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        senderBtn.backgroundColor = MAINCOLOR;
    }else{
        [senderBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
        senderBtn.backgroundColor = [UIColor whiteColor];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@interface ZBAuthenticationInstructionCell(){
    
    UILabel *_titleLabel;
    UILabel *_contentLabel;
 
}

@end

@implementation ZBAuthenticationInstructionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.top.equalTo(self.contentView.mas_top).offset(10);
    }];
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = color333333;
    _contentLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_contentLabel];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(9);
    }];
}

-(void)refreshData:(NSString *)titleStr contentStr:(NSString *)contentStr{
    _titleLabel.text = titleStr;
    _contentLabel.text = contentStr;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


