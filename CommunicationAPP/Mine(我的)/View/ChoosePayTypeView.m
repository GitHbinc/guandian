//
//  ChoosePayTypeView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "ChoosePayTypeView.h"
#import "ChoosePayTypeCell.h"
#import "PayTypeModel.h"


@interface ChoosePayTypeView()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>{
    
    UILabel *_allMoneyLabel;
    UILabel *_availableBalanceLabel;
    UILabel *_purchasedPointsLabel;
    UILabel *_freezeMoneyLabel;
    
    UITextField *_moneyTextField;
    UITextField *_timeTextField;
    
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, assign) PayType style;
@property (nonatomic, copy) void(^sendBlcok)(NSString *text);
@property (nonatomic ,strong)NSMutableArray *dataArr;
@property (nonatomic ,strong)UIView *backView;
@end

@implementation ChoosePayTypeView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
     self.backgroundColor = [UIColor clearColor];
    self.frame = [UIScreen mainScreen].bounds;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgViewClick)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
    
    UIView *bottomView = [[UIView alloc]init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bottomView];
    _bottomView = bottomView;
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
        make.height.equalTo(@255);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(10);
        make.top.equalTo(bottomView.mas_top).offset(10);
        make.width.equalTo(@12);
        make.height.equalTo(@12);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"选择支付方式";
    label.textColor = color333333;
    label.font = [UIFont systemFontOfSize:15];
    [bottomView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(10);
        make.centerX.equalTo(bottomView.mas_centerX);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = HEXCOLOR(0xf6f6fa);
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(0);
        make.right.equalTo(bottomView.mas_right).offset(0);
        make.top.equalTo(bottomView.mas_top).offset(33);
        make.height.equalTo(@1);
    }];
    
    _tableView = [[UITableView alloc]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.tableFooterView = [[UIView alloc]init];
    [bottomView addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(0);
        make.right.equalTo(bottomView.mas_right).offset(0);
        make.bottom.equalTo(bottomView.mas_bottom).offset(0);
        make.top.equalTo(lineView.mas_bottom).offset(0);
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChoosePayTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChoosePayTypeCellID"];
    if (cell == nil) {
        cell = [[ChoosePayTypeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChoosePayTypeCellID"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.model = [self.dataArr objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     //PayTypeModel *model = [self.dataArr objectAtIndex:indexPath.row];
    if (_sendBlcok) {
         _sendBlcok([NSString stringWithFormat:@"%ld",indexPath.row+1]);
    }
   
}

+(void)showWithStyle:(PayType)style ChooseSendBlock:(void(^)(NSString *text))sendBlock{
    ChoosePayTypeView *typeView = [[ChoosePayTypeView alloc] initWithStyle:style];
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:typeView];
    
    typeView.sendBlcok = [sendBlock copy];
    //[typeView show];
    
    
}

- (instancetype)initWithStyle:(PayType)style
{
    self = [super init];
    if (self) {
        
        _style = style;
        
        NSArray *imageArray = @[@"mine_zhifubao",@"mine_weixin"];
        NSArray *nameArray = @[@"支付宝",@"微信"];
        
        self.dataArr = [NSMutableArray array];
        for (int i=0; i<nameArray.count; i++) {
            PayTypeModel *model = [[PayTypeModel alloc]init];
            model.name = nameArray[i];
            model.img = imageArray[i];
            [self.dataArr addObject:model];
        }
        
        /** 创建UI */
        [self  initView];
       
    }
    return self;
}

-(void)bgViewClick{
   // [self hide];
}

-(void)hide{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.bottomView.frame;
        frame.origin.y = kSCREENHEIGHT;
        self.bottomView.frame = frame;
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}



-(void)cancelBtnClick{
    
}

-(void)setupUI{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
