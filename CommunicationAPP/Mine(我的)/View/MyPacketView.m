//
//  MyPacketView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "MyPacketView.h"

@interface MyPacketView(){
    
    UILabel *_allMoneyLabel;
    UILabel *_availableBalanceLabel;
    UILabel *_purchasedPointsLabel;
    UILabel *_freezeMoneyLabel;
    UILabel *_allTimeLabel;
}

@end

@implementation MyPacketView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *allMoneyView = [[UIView alloc]init];
    [self addSubview:allMoneyView];
    [allMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(25);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@90);
        make.height.equalTo(@30);
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"mine_packet"];
    [allMoneyView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(allMoneyView.mas_left).offset(5);
        make.centerY.equalTo(allMoneyView.mas_centerY);
        make.width.equalTo(@14);
        make.height.equalTo(@14);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color333333;
    label.text = @"总资产";
    label.font = [UIFont systemFontOfSize:16];
    [allMoneyView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(allMoneyView.mas_right).offset(-5);
        make.centerY.equalTo(allMoneyView.mas_centerY);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = MAINCOLOR;
    _allMoneyLabel.font = [UIFont systemFontOfSize:35];
    [self addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(allMoneyView.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    
    _allTimeLabel = [[UILabel alloc]init];
    _allTimeLabel.textColor = color333333;
    _allTimeLabel.font = [UIFont systemFontOfSize:20];
    [self addSubview:_allTimeLabel];
    [_allTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    CGFloat width = kSCREENWIDTH / 2;
    UIView *availableBalanceView = [[UIView alloc]init];
    [self addSubview:availableBalanceView];
    [availableBalanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(50);
        make.left.equalTo(self).offset(0);
        make.width.equalTo(@(width));
        make.height.equalTo(@50);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = color666666;
    label1.text = @"可用余额";
    label1.font = [UIFont systemFontOfSize:14];
    [availableBalanceView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(availableBalanceView.mas_top).offset(10);
        make.centerX.equalTo(availableBalanceView.mas_centerX);
    }];
    
    
    _availableBalanceLabel = [[UILabel alloc]init];
    _availableBalanceLabel.textColor = color333333;
    _availableBalanceLabel.font = [UIFont systemFontOfSize:20];
    [availableBalanceView addSubview:_availableBalanceLabel];
    [_availableBalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(5);
        make.centerX.equalTo(availableBalanceView.mas_centerX);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = color999999;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(availableBalanceView.mas_right).offset(0);
        make.width.equalTo(@1);
        make.height.equalTo(@20);
        make.centerY.equalTo(availableBalanceView.mas_centerY);
    }];
    
    UIView *purchasedPoints = [[UIView alloc]init];
    [self addSubview:purchasedPoints];
    [purchasedPoints mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(30);
        make.left.equalTo(availableBalanceView.mas_right).offset(1);
        make.width.equalTo(@(width));
        make.height.equalTo(@50);
    }];
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.textColor = color666666;
    label2.text = @"已购时间";
    label2.font = [UIFont systemFontOfSize:14];
    [purchasedPoints addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(availableBalanceView.mas_top).offset(10);
        make.centerX.equalTo(purchasedPoints.mas_centerX);
    }];
    
    
    _purchasedPointsLabel = [[UILabel alloc]init];
    _purchasedPointsLabel.textColor = color333333;
    _purchasedPointsLabel.font = [UIFont systemFontOfSize:20];
    [purchasedPoints addSubview:_purchasedPointsLabel];
    [_purchasedPointsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label2.mas_bottom).offset(5);
        make.centerX.equalTo(purchasedPoints.mas_centerX);
    }];
    
    UIView *lineView1 = [[UIView alloc]init];
    lineView1.backgroundColor = color999999;
    [self addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(purchasedPoints.mas_right).offset(0);
        make.width.equalTo(@1);
        make.height.equalTo(@20);
        make.centerY.equalTo(purchasedPoints.mas_centerY);
    }];
    
    UIView *freezeView = [[UIView alloc]init];
    [self addSubview:freezeView];
    [freezeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(30);
        make.right.equalTo(self).offset(0);
        make.width.equalTo(@(width));
        make.height.equalTo(@50);
    }];
    
//    UILabel *label3 = [[UILabel alloc]init];
//    label3.textColor = color666666;
//    label3.text = @"冻结金额";
//    label3.font = [UIFont systemFontOfSize:14];
//    [freezeView addSubview:label3];
//    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(freezeView.mas_top).offset(10);
//        make.centerX.equalTo(freezeView.mas_centerX);
//    }];
//
//
//    _freezeMoneyLabel = [[UILabel alloc]init];
//    _freezeMoneyLabel.textColor = color333333;
//    _freezeMoneyLabel.font = [UIFont systemFontOfSize:20];
//    [freezeView addSubview:_freezeMoneyLabel];
//    [_freezeMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(label3.mas_bottom).offset(5);
//        make.centerX.equalTo(freezeView.mas_centerX);
//    }];
}

-(void)setModel:(PacketModel *)model{
    _model = model;
    _allMoneyLabel.text  =[NSString stringWithFormat:@"%@h",model.times];
    _allTimeLabel.text = [NSString stringWithFormat:@"%@点",model.dian];
    _availableBalanceLabel.text =[NSString stringWithFormat:@"%@点",model.available];
    _purchasedPointsLabel.text = [NSString stringWithFormat:@"%@h",model.alreadytime];
}


-(void)refreshModel{
    _allMoneyLabel.text  =@"100";
    _allTimeLabel.text = @"10";
    _availableBalanceLabel.text = @"0";
    _purchasedPointsLabel.text = @"0";
    //_freezeMoneyLabel.text = @"0";
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
