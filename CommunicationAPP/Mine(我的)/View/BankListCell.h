//
//  BankListCell.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface BankListCell : UITableViewCell

-(void)refreshModel;

@end

@interface BankListNoneCell : UITableViewCell

@end
