//
//  MineViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MineViewCell.h"


@interface MineViewCell ()

/* 标题 */
@property (strong , nonatomic)UILabel *titleLabel;
/* 图标 */
@property (strong , nonatomic)UIImageView *iconImage;
/* 内容 */
@property (strong , nonatomic)UILabel *contentLabel;

@end

@implementation MineViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
    _iconImage = [[UIImageView alloc] init];
    [self.contentView addSubview:_iconImage];
    
    _contentLabel = [UILabel new];
    _contentLabel.text = @"100点";
    _contentLabel.textColor = HEXCOLOR(0xC7C7C7);
    _contentLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_contentLabel];
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
     __weak __typeof(self)weakSelf = self;
    [_iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self.contentView)setOffset:20];
         make.centerY.mas_equalTo(self.contentView);
         make.size.mas_equalTo(CGSizeMake(18, 16));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         [make.left.mas_equalTo(weakSelf.iconImage.mas_right)setOffset:10];
         make.center.mas_equalTo(self.contentView);
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    
}

#pragma mark - Setter Getter Methods
- (void)setMineItem:(MineItem *)mineItem{
    _mineItem = mineItem;
    self.titleLabel.text = mineItem.name;
    self.iconImage.image = [UIImage imageNamed:mineItem.icon];
    if ([mineItem.name isEqualToString:@"我的点包"]){
        self.contentLabel.text = [NSString stringWithFormat:@"%@点",mineItem.type];
    }else if ([mineItem.name isEqualToString:@"我的收益"]){
         self.contentLabel.text = [NSString stringWithFormat:@"%@点",mineItem.type];
    }else if ([mineItem.name isEqualToString:@"实名认证"]){
        if ([mineItem.type isEqualToString:@"1"]) {
            self.contentLabel.text = @"已认证";
        }else{
             self.contentLabel.text = @"未认证";
        }
    }else if ([mineItem.name isEqualToString:@"机构认证"]){
        if ([mineItem.type isEqualToString:@"1"]) {
            self.contentLabel.text = @"已认证";
        }else{
            self.contentLabel.text = @"未认证";
        }
    }else{
         self.contentLabel.text = @"";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
