//
//  AuctionCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/5.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "AuctionCell.h"

@interface AuctionCell(){
    
    UIImageView *_headerImageView;
    UILabel *_headerTitleLabel;
    UILabel *_rightLabel;
    UIImageView *_contentImageView;
    UILabel *_contentTitleLabel;
    UILabel *_timeLabel;
    UILabel *_moneyLabel;
    UIButton *_deleBtn;
}

@end
@implementation AuctionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UIView *headerView = [[UIView alloc]init];
    [self.contentView addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@44);
    }];
    
    _headerImageView = [[UIImageView alloc]init];
    [headerView addSubview:_headerImageView];
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(20);
        make.centerY.equalTo(headerView.mas_centerY);
        make.width.equalTo(@29);
        make.height.equalTo(@29);
    }];
    
    _headerTitleLabel = [[UILabel alloc]init];
    _headerTitleLabel.textColor = color333333;
    _headerTitleLabel.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:_headerTitleLabel];
    [_headerTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_headerImageView.mas_right).offset(15);
        make.centerY.equalTo(headerView.mas_centerY);
        make.right.equalTo(headerView.mas_right).offset(-90);
    }];
    
    _rightLabel = [[UILabel alloc]init];
    _rightLabel.textColor = color666666;
    _rightLabel.font = [UIFont systemFontOfSize:13];
    [headerView addSubview:_rightLabel];
    [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerView.mas_right).offset(-20);
        make.centerY.equalTo(headerView.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorD6D6D6;
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left).offset(20);
        make.right.equalTo(headerView.mas_right).offset(-20);
        make.top.equalTo(headerView.mas_bottom).offset(-1);
        make.height.equalTo(@1);
    }];
    
    UIImageView *contentBgImageView = [[UIImageView alloc]init];
    contentBgImageView.userInteractionEnabled = true;
    contentBgImageView.image = [UIImage imageNamed:@"JPcontent_bg_icon"];
    [self.contentView addSubview:contentBgImageView];
    [contentBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.top.equalTo(headerView.mas_bottom).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    _contentImageView = [[UIImageView alloc]init];
    [contentBgImageView addSubview:_contentImageView];
    [_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentBgImageView.mas_left).offset(13);
        make.centerY.equalTo(contentBgImageView.mas_centerY);
        make.height.equalTo(@75);
        make.width.equalTo(@133);
    }];
    
    _contentTitleLabel = [[UILabel alloc]init];
    _contentTitleLabel.textColor = color333333;
    _contentTitleLabel.font = [UIFont systemFontOfSize:13];
    [contentBgImageView addSubview:_contentTitleLabel];
    [_contentTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_contentImageView.mas_right).offset(20);
        make.top.equalTo(self->_contentImageView.mas_top).offset(5);
        make.right.equalTo(contentBgImageView.mas_right).offset(-10);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color999999;
    _timeLabel.font = [UIFont systemFontOfSize:10];
    [contentBgImageView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_contentTitleLabel.mas_left).offset(0);
        make.top.equalTo(self->_contentTitleLabel.mas_bottom).offset(10);
        make.right.equalTo(self->_contentTitleLabel.mas_right).offset(0);
    }];
    
    _moneyLabel = [[UILabel alloc]init];
    _moneyLabel.textColor = MAINCOLOR;
    _moneyLabel.font = [UIFont systemFontOfSize:15];
    [contentBgImageView addSubview:_moneyLabel];
    [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_contentTitleLabel.mas_left).offset(0);
        make.bottom.equalTo(self->_contentImageView.mas_bottom).offset(-3);
        make.right.equalTo(contentBgImageView.mas_right).offset(-50);
    }];
    
    _deleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleBtn setImage:[UIImage imageNamed:@"dele_icon"] forState:UIControlStateNormal];
    [_deleBtn addTarget:self action:@selector(deleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [contentBgImageView addSubview:_deleBtn];
    [_deleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentBgImageView.mas_right).offset(-20);
        make.bottom.equalTo(contentBgImageView.mas_bottom).offset(-20);
    }];
}


-(void)setModel:(AuctionListModel *)model{
    _model = model;
    [_headerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.profile]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _headerTitleLabel.text = model.username;
    if ([model.status isEqualToString:@"1"]) {
        _rightLabel.text = @"竞价成功";
        _rightLabel.textColor = [UIColor redColor];
    }else{
        _rightLabel.text = @"竞价失败";
        _rightLabel.textColor = color666666;
    }
  
    [_contentImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,model.img]] placeholderImage:[UIImage imageNamed:@"default_long"]];
    _contentTitleLabel.text = model.title;
    _timeLabel.text = model.time;
    _moneyLabel.text = [NSString stringWithFormat:@"%@点币",model.money];
    
}

-(void)deleBtnClick:(UIButton *)senderBtn{
    NSLog(@"=================%ld",senderBtn.tag);
    if (self.block) {
        self.block(_model);
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
