//
//  MessageSetupCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItem.h"

NS_ASSUME_NONNULL_BEGIN

@class MessageSetupCell;

@protocol MessageSetupCellDelegate <NSObject>
-(void)updateSwitchAtCell:(MessageSetupCell *)cell mySwitch:(UISwitch *)mySwitch;
@end

@interface MessageSetupCell : UITableViewCell
@property (strong , nonatomic)MineItem *mineItem;
@property (weak,nonatomic)id<MessageSetupCellDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
