//
//  TimePartnerHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>

@interface TimePartnerHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

@end
