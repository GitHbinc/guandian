//
//  ProfitHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "ProfitHeaderView.h"

@interface ProfitHeaderView(){
    
    UILabel *_allMoneyLabel;
    UILabel *_allTimeLabel;
    UILabel *_todayProfitLabel;
    UILabel *_allProfitLabel;
    
}

@end

@implementation ProfitHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *allMoneyView = [[UIView alloc]init];
    [self addSubview:allMoneyView];
    [allMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(25);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@90);
        make.height.equalTo(@30);
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"mine_packet"];
    [allMoneyView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(allMoneyView.mas_left).offset(5);
        make.centerY.equalTo(allMoneyView.mas_centerY);
        make.width.equalTo(@14);
        make.height.equalTo(@14);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color333333;
    label.text = @"总资产";
    label.font = [UIFont systemFontOfSize:16];
    [allMoneyView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(allMoneyView.mas_right).offset(-5);
        make.centerY.equalTo(allMoneyView.mas_centerY);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = MAINCOLOR;
    _allMoneyLabel.font = [UIFont systemFontOfSize:35];
    [self addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(allMoneyView.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    _allTimeLabel = [[UILabel alloc]init];
    _allTimeLabel.textColor = color333333;
    _allTimeLabel.font = [UIFont systemFontOfSize:20];
    [self addSubview:_allTimeLabel];
    [_allTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    
    
    CGFloat width = kSCREENWIDTH / 2;
    UIView *todayProfitView = [[UIView alloc]init];
    [self addSubview:todayProfitView];
    [todayProfitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(50);
        make.left.equalTo(self).offset(0);
        make.width.equalTo(@(width));
        make.height.equalTo(@50);
    }];
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.textColor = color666666;
    label1.text = @"今日收益";
    label1.font = [UIFont systemFontOfSize:14];
    [todayProfitView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(todayProfitView.mas_top).offset(10);
        make.centerX.equalTo(todayProfitView.mas_centerX);
    }];
    
    
    _todayProfitLabel = [[UILabel alloc]init];
    _todayProfitLabel.textColor = color333333;
    _todayProfitLabel.font = [UIFont systemFontOfSize:20];
    [todayProfitView addSubview:_todayProfitLabel];
    [_todayProfitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(5);
        make.centerX.equalTo(todayProfitView.mas_centerX);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = color999999;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(@1);
        make.height.equalTo(@20);
        make.centerY.equalTo(todayProfitView.mas_centerY);
    }];
    
    UIView *allProfitView = [[UIView alloc]init];
    [self addSubview:allProfitView];
    [allProfitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_allMoneyLabel.mas_bottom).offset(50);
        make.right.equalTo(self).offset(0);
        make.width.equalTo(@(width));
        make.height.equalTo(@50);
    }];
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.textColor = color666666;
    label2.text = @"累计收益";
    label2.font = [UIFont systemFontOfSize:14];
    [allProfitView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(allProfitView.mas_top).offset(10);
        make.centerX.equalTo(allProfitView.mas_centerX);
    }];
    
    
    _allProfitLabel = [[UILabel alloc]init];
    _allProfitLabel.textColor = color333333;
    _allProfitLabel.font = [UIFont systemFontOfSize:20];
    [allProfitView addSubview:_allProfitLabel];
    [_allProfitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label2.mas_bottom).offset(5);
        make.centerX.equalTo(allProfitView.mas_centerX);
    }];
}



-(void)setModel:(ProfitModel *)model{
    _model = model;
    _allMoneyLabel.text =[NSString stringWithFormat:@"%@点",model.spot];
    _allTimeLabel.text =[NSString stringWithFormat:@"%@h",model.times];
    _todayProfitLabel.text = [NSString stringWithFormat:@"%@点",model.today];
    _allProfitLabel.text = [NSString stringWithFormat:@"%@点",model.sum];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
