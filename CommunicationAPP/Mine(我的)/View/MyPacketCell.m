//
//  MyPacketCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "MyPacketCell.h"

@interface MyPacketCell(){

    UILabel *_titleLabel;

}

@end

@implementation MyPacketCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color666666;
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
//    UIImageView *arrowImageView = [[UIImageView alloc]init];
//    arrowImageView.image = [UIImage imageNamed:@"mine_xiangyou"];
//    [self.contentView addSubview:arrowImageView];
//    [arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.contentView).offset(-15);
//        make.centerY.equalTo(self.contentView.mas_centerY);
//    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
    
}

-(void)refreshData:(NSString *)titleStr{
    _titleLabel.text = titleStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
