//
//  PrivacyMemberSwitchCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "PrivacyMemberSwitchCell.h"
@interface PrivacyMemberSwitchCell()
@property (strong , nonatomic)UILabel *titleLabel;
@property (strong , nonatomic)UILabel *contentLabel;
@property (strong , nonatomic)UISwitch *messageSwitch;
@end
@implementation PrivacyMemberSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    //_titleLabel.text = @"评论";
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
    _contentLabel = [UILabel new];
    //_contentLabel.text = @"评论";
    _contentLabel.textColor = HEXCOLOR(0x999999);
    _contentLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_contentLabel];
    
    _messageSwitch = [[UISwitch alloc]init];
    _messageSwitch.onTintColor = HEXCOLOR(0x9C386A);
    [_messageSwitch addTarget:self action:@selector(switchDidClick:) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:_messageSwitch];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.centerY.mas_equalTo(self.contentView)setOffset:-8];
        make.left.mas_equalTo(self.contentView.mas_left).offset(20);
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.centerY.mas_equalTo(self.contentView)setOffset:8];
        make.left.mas_equalTo(self.contentView.mas_left).offset(20);
    }];
    
    [_messageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
    }];
    
    
}

- (void)setMineItem:(MineItem *)mineItem{
    _mineItem = mineItem;
     NSArray *array = [mineItem.name componentsSeparatedByString:@"*"];
    self.titleLabel.text = [array firstObject];
    self.contentLabel.text = [array lastObject];
    if ([mineItem.type isEqualToString:@"1"]) {
        _messageSwitch.on = YES;
    }else{
        _messageSwitch.on = NO;
    }
}

-(void)switchDidClick:(UISwitch *)sender {
    if ([self.delegate respondsToSelector:@selector(updatePrivacySwitchAtCell:mySwitch:)]) {
        [self.delegate updatePrivacySwitchAtCell:self mySwitch:sender];
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
