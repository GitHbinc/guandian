//
//  CertificateHeaderView.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import "CertificateHeaderView.h"

@interface CertificateHeaderView(){
    
    UILabel *_allMoneyLabel;
    
}

@end

@implementation CertificateHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"可用收益";
    label.textColor = color333333;
    label.font = [UIFont systemFontOfSize:16];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(30);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    _allMoneyLabel = [[UILabel alloc]init];
    _allMoneyLabel.textColor = color333333;
    _allMoneyLabel.font = [UIFont systemFontOfSize:35];
    [self addSubview:_allMoneyLabel];
    [_allMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(15);
        make.centerX.equalTo(self.mas_centerX);
    }];
}

-(void)refreshData{
    
    _allMoneyLabel.text = @"0";
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
