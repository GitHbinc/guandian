//
//  SetUpMiddleCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/14.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "SetUpMiddleCell.h"
@interface SetUpMiddleCell()
/* 标题 */
@property (strong , nonatomic)UILabel *titleLabel;
@end
@implementation SetUpMiddleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

#pragma mark - UI
- (void)setUpUI
{
    _titleLabel = [UILabel new];
    _titleLabel.textColor = HEXCOLOR(0x333333);
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_titleLabel];
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.contentView);
    }];
}

#pragma mark - Setter Getter Methods
- (void)setMineItem:(MineItem *)mineItem{
    _mineItem = mineItem;
    self.titleLabel.text = mineItem.name;

}

-(void)setModel:(OptionModel *)model{
    _model = model;
    if ([model.name isEqualToString:@"清空聊天记录"]) {
        self.titleLabel.textColor = HEXCOLOR(0x9C386A);
    }
     self.titleLabel.text = model.name;
}

#pragma mark - Setter Getter Methods
//- (void)setMineItem:(MineItem *)mineItem{
//    _mineItem = mineItem;
//    self.titleLabel.text = mineItem.name;
//    if ([mineItem.name isEqualToString:@"清除缓存"]){
//        self.contentLabel.text = @"16.7k";
//    }else if ([mineItem.name isEqualToString:@"检查更新"]){
//        self.contentLabel.text = @"当前版本1.1.0";
//    }else{
//        self.contentLabel.text = @"";
//    }
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
