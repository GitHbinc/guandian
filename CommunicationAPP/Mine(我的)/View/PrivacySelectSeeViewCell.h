//
//  PrivacySelectSeeViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mineItem.h"

@interface PrivacySelectSeeViewCell : UITableViewCell
@property(nonatomic ,strong)UIButton *selectSeeBtn;
@property(nonatomic ,strong)MineItem *item;
@end





