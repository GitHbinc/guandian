//
//  ZBAuthenticationHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/17.
//

#import <UIKit/UIKit.h>

@interface ZBAuthenticationHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refreshModel;

@end
