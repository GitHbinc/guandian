//
//  RecentlyWatchCell.m
//  MiaoWo
//
//  Created by chenlu on 2018/12/13.
//

#import "RecentlyWatchCell.h"

@interface RecentlyWatchCell(){
    
    UILabel *_titleLabel;
    UILabel *_descLabel;
    UILabel *_timeLabel;
    UIImageView *_leftImageView;
    
}

@end

@implementation RecentlyWatchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    _leftImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(@62);
        make.width.equalTo(@111);
    }];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = color333333;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(10);
        make.top.equalTo(self.contentView).offset(20);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    _descLabel = [[UILabel alloc]init];
    _descLabel.textColor = color999999;
    _descLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_titleLabel.mas_bottom).offset(5);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = color999999;
    _timeLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_titleLabel.mas_left).offset(0);
        make.top.equalTo(self->_descLabel.mas_bottom).offset(5);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@1);
        make.bottom.equalTo(self.contentView).offset(-1);
    }];
    
}


-(void)refreshModel{
    
    _leftImageView.image = [UIImage imageNamed:@"test_icon"];
    _titleLabel.text = @"佛系主播 礼物随缘";
    _descLabel.text = @"已看至45%";
    _timeLabel.text = @"2018-01-01";
}

-(void)setModel:(RecentListModel *)model{
    _model = model;
    [_leftImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,_model.logo]] placeholderImage:[UIImage imageNamed:@"default_long"]];
    _titleLabel.text = model.title;
    _descLabel.text =   [NSString stringWithFormat:@"已看至%@%%",model.running_time];
    _timeLabel.text = model.createtime;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
