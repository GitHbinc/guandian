//
//  BlacklistViewCell.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlacklistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BlacklistViewCell : UITableViewCell
@property (nonatomic ,strong)BlacklistModel *model;
@property (nonatomic, copy) void(^removeBlacklist)(BlacklistModel *model);

@end

NS_ASSUME_NONNULL_END
