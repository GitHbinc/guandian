//
//  ProfitHeaderView.h
//  MiaoWo
//
//  Created by chenlu on 2018/12/14.
//

#import <UIKit/UIKit.h>
#import "ProfitModel.h"

@interface ProfitHeaderView : UIView
@property(nonatomic ,strong)ProfitModel *model;

//-(void)refreshModel;
+ (instancetype)customHeadViewWithFrame:(CGRect)frame;

@end
