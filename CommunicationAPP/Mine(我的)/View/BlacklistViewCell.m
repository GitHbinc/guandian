//
//  BlacklistViewCell.m
//  CommunicationAPP
//
//  Created by 陈华 on 2018/12/17.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BlacklistViewCell.h"
@interface BlacklistViewCell()
@property(nonatomic ,strong)UIImageView *headImage;
@property(nonatomic ,strong)UILabel *nameLabel;
@property(nonatomic ,strong)UILabel *contentLabel;
@property(nonatomic ,strong)UIButton *removeBtn;

@end
@implementation BlacklistViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}


#pragma mark - UI
- (void)setUpUI
{
    _headImage = [[UIImageView alloc]init];
    //_headImage.image  = [UIImage imageNamed:@"test_icon"];
    [_headImage.layer setMasksToBounds:YES];
    [_headImage.layer setCornerRadius:25];
    [self.contentView addSubview:_headImage];
    
    _nameLabel = [UILabel new];
    //_nameLabel.text = @"吴彦祖";
    _nameLabel.textColor = HEXCOLOR(0x333333);
    _nameLabel.font =  [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_nameLabel];
    
    
    _contentLabel = [UILabel new];
    //_contentLabel.text = @"气质与美貌并存的姑娘";
    _contentLabel.textColor = HEXCOLOR(0x999999);
    _contentLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_contentLabel];
    
    _removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_removeBtn setTitle:@"移除黑名单" forState:UIControlStateNormal];
    [_removeBtn.titleLabel setFont:[UIFont systemFontOfSize:11]];
    _removeBtn.clipsToBounds = YES;
    [_removeBtn.layer setBorderWidth:1.0];
    [_removeBtn.layer setBorderColor:HEXCOLOR(0x9C386A).CGColor];
    [_removeBtn.layer setCornerRadius:12.0];
    [_removeBtn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    [_removeBtn addTarget:self action:@selector(clickRemove) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_removeBtn];
    
    
}

#pragma mark - 布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak __typeof(self)weakSelf = self;

    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(self)setOffset:10];
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.top.mas_equalTo(weakSelf.headImage.mas_top)setOffset:5];
    }];
    
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.left.mas_equalTo(weakSelf.headImage.mas_right)setOffset:10];
        [make.bottom.mas_equalTo(weakSelf.headImage.mas_bottom)setOffset:-5];
    }];
    
    
    [_removeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        [make.right.mas_equalTo(weakSelf.contentView.mas_right)setOffset:-20];
        make.centerY.mas_equalTo(weakSelf.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70, 24));
    }];
    
}

-(void)setModel:(BlacklistModel *)model{
    _model = model;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWEBURL,_model.bimg]] placeholderImage:[UIImage imageNamed:@"default_square"]];
    _nameLabel.text = model.bname;
    _contentLabel.text = model.bselfhood;
    
}

-(void)clickRemove{
    if (_removeBlacklist) {
        _removeBlacklist(_model);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
