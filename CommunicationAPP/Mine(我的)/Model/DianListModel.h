//
//  DianListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/23.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DianListModel : JSONModel
@property(nonatomic ,copy) NSString <Optional>*dian;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *prices;
@property(nonatomic ,copy) NSString *timer;
@end

NS_ASSUME_NONNULL_END
