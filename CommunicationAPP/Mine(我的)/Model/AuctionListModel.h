//
//  AuctionListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/21.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuctionListModel : JSONModel
@property(nonatomic ,copy) NSString *img;
@property(nonatomic ,copy) NSString *money;
@property(nonatomic ,copy) NSString *profile;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *status;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *username;
@property(nonatomic ,copy) NSString *time;

@end

NS_ASSUME_NONNULL_END
