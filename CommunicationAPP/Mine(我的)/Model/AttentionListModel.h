//
//  AttentionListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/25.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AttentionListModel : JSONModel
@property(nonatomic ,copy) NSString *autograph;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *img;
@property(nonatomic ,copy) NSString *status;
@property(nonatomic ,copy) NSString *stock_id;
@property(nonatomic ,copy) NSString *username;
@property(nonatomic ,copy) NSString *zhibotype;

@end

NS_ASSUME_NONNULL_END
