//
//  ProfitModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/22.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfitModel : JSONModel
@property(nonatomic ,copy) NSString *spot;
@property(nonatomic ,copy) NSString *sum;
@property(nonatomic ,copy) NSString *times;
@property(nonatomic ,copy) NSString *today;
@end

NS_ASSUME_NONNULL_END
