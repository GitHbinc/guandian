//
//  DianListModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/23.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "DianListModel.h"

@implementation DianListModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
