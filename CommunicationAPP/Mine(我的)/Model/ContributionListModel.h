//
//  ContributionListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/21.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContributionListModel : JSONModel
@property(nonatomic ,copy) NSString *money;
@property(nonatomic ,copy) NSString *profile;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *create_time;
@property(nonatomic ,copy) NSString *uid;
@property(nonatomic ,copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
