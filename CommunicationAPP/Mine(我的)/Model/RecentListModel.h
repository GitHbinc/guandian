//
//  RecentListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/23.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecentListModel : JSONModel

@property(nonatomic ,copy) NSString *createtime;
@property(nonatomic ,copy) NSString *logo;
@property(nonatomic ,copy) NSString *pid;
@property(nonatomic ,copy) NSString *running_time;
@property(nonatomic ,copy) NSString *title;
@property(nonatomic ,copy) NSString *video;
@end

NS_ASSUME_NONNULL_END
