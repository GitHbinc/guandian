//
//  MineItem.h
//  CommunicationAPP
//
//  Created by 陈华 on 2018/11/29.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MineItem : JSONModel

@property(nonatomic ,copy) NSString *icon;//图标
@property(nonatomic ,copy) NSString *name;//名字
@property(nonatomic ,copy) NSString <Optional> *type;


@end




