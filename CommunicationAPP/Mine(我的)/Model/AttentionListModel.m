//
//  AttentionListModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/25.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "AttentionListModel.h"

@implementation AttentionListModel
+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
