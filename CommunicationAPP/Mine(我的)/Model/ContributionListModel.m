//
//  ContributionListModel.m
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/21.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "ContributionListModel.h"

@implementation ContributionListModel

+(JSONKeyMapper*)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{
                                                                 @"ID": @"id"
                                                                 
                                                                 }];
}
@end
