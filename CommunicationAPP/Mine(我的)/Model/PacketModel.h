//
//  PacketModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/23.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PacketModel : JSONModel
@property(nonatomic ,copy) NSString *alreadytime;
@property(nonatomic ,copy) NSString *available;
@property(nonatomic ,copy) NSString *dian;
@property(nonatomic ,copy) NSString *ID;
@property(nonatomic ,copy) NSString *in_use;
@property(nonatomic ,copy) NSString *times;
@end

NS_ASSUME_NONNULL_END
