//
//  ProfitListModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/22.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfitListModel : JSONModel
@property(nonatomic ,copy) NSString *status;
@property(nonatomic ,copy) NSString *times;
@property(nonatomic ,copy) NSString *money;
@property(nonatomic ,copy) NSString *create_time;
@end

NS_ASSUME_NONNULL_END
