//
//  PayTypeModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/22.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PayTypeModel : JSONModel
@property(nonatomic ,copy) NSString *name;
@property(nonatomic ,copy) NSString *img;
@property(nonatomic ,copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
