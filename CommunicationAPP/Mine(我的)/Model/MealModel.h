//
//  MealModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/23.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"
#import "DianListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MealModel : JSONModel
@property(nonatomic ,copy) NSString *alldian;
@property(nonatomic ,copy) NSString *alltime;
@property(nonatomic ,copy) NSString *jishu;
@property(nonatomic ,strong)NSArray<DianListModel*>* meal;
@end

NS_ASSUME_NONNULL_END
