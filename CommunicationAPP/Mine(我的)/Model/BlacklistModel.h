//
//  BlacklistModel.h
//  CommunicationAPP
//
//  Created by 陈华 on 2019/1/17.
//  Copyright © 2019年 宋晓翩. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BlacklistModel : JSONModel
@property(nonatomic ,copy) NSString *bimg;
@property(nonatomic ,copy) NSString *bname;
@property(nonatomic ,copy) NSString *bselfhood;
@property(nonatomic ,copy) NSString *ID;
@end

NS_ASSUME_NONNULL_END
