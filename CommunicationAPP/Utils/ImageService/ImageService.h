//
//  ImageService.h
//  MiaoWo
//
//  Created by WPY on 2017/1/19.
//  Copyright © 2017年 WPY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageService : NSObject

+ (UIImage*) imageWithColor:(UIColor*)color andHeight:(CGFloat)height;

//根据颜色生成图片
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;

//图片切圆角
+ (UIImage *) createRoundedRectImage:(UIImage*)image size:(CGSize)size cornerRadius:(CGFloat)radius;
//变化尺寸
+ (UIImage *)getImageOrangImage:(UIImage*)image withNewSize:(CGSize)size;

//图片裁剪
+(UIImage*)straightenAndScaleImage:(UIImage *)image maxDimension:(int) maxDimension;

+ (CAGradientLayer *)haloWithFrame:(CGRect)frame colors:(NSArray *)colors;

@end
