//
//  ContentBottomView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ContentBottomView.h"

@interface ContentBottomView(){
    
    //赞、赏图片
    UIImageView *_leftImageView;
    //赞、赏数量
    UILabel *_leftLabel;
    //点赞评分
    UILabel *_fabulousLabel;
    //评论数量
    UILabel *_commentLabel;
    //转发数量
    UILabel *_forwardLabel;
    
}

@end

@implementation ContentBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    _leftImageView = [[UIImageView alloc]init];
    [self addSubview:_leftImageView];
    [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _leftLabel = [[UILabel alloc]init];
    _leftLabel.textColor = colorF46C02;
    _leftLabel.font = [UIFont systemFontOfSize:9];
    [_leftImageView addSubview:_leftLabel];
    [_leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_left).offset(25);
        make.centerY.equalTo(self->_leftImageView.mas_centerY);
    }];
    
    UIButton *fabulousBtn = [[UIButton alloc]init];
    [fabulousBtn setImage:[UIImage imageNamed:@"dianzan_normal_icon"] forState:UIControlStateNormal];
    fabulousBtn.tag = 101;
    [fabulousBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fabulousBtn];
    [fabulousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_leftImageView.mas_right).offset(60);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _fabulousLabel = [[UILabel alloc]init];
    _fabulousLabel.textColor = color666666;
    _fabulousLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_fabulousLabel];
    [_fabulousLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fabulousBtn.mas_right).offset(8);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIButton *commentBtn = [[UIButton alloc]init];
    [commentBtn setImage:[UIImage imageNamed:@"message_normal_icon"] forState:UIControlStateNormal];
    commentBtn.tag = 102;
    [commentBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:commentBtn];
    [commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_fabulousLabel.mas_right).offset(30);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _commentLabel = [[UILabel alloc]init];
    _commentLabel.textColor = color666666;
    _commentLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_commentLabel];
    [_commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentBtn.mas_right).offset(8);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIButton *forwardBtn = [[UIButton alloc]init];
    [forwardBtn setImage:[UIImage imageNamed:@"zhuanfa_normal_icon"] forState:UIControlStateNormal];
    forwardBtn.tag = 103;
    [forwardBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:forwardBtn];
    [forwardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_commentLabel.mas_right).offset(30);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    _forwardLabel = [[UILabel alloc]init];
    _forwardLabel.textColor = color666666;
    _forwardLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_forwardLabel];
    [_forwardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(forwardBtn.mas_right).offset(8);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIButton *moreBtn = [[UIButton alloc]init];
    [moreBtn setImage:[UIImage imageNamed:@"bottom_more_normal_icon"] forState:UIControlStateNormal];
    moreBtn.tag = 104;
    [moreBtn addTarget:self action:@selector(bottomViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-12);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
}

-(void)refreshModel{
    
    _leftImageView.image = [UIImage imageNamed:@"shang_icon"];
    _leftLabel.text = @"1626";
    _fabulousLabel.text = @"9.6";
    _commentLabel.text = @"50";
    _forwardLabel.text = @"26";
}

-(void)setModel:(RankVideoDetailModel *)model{
    _model = model;
    if ([model.zstype isEqualToString:@"1"]) {
        _leftImageView.image = [UIImage imageNamed:@"bottom_zan_icon"];
        _leftLabel.text = ![self isEmpty:model.s_num]?model.s_num:@"0";
    }else{
        _leftImageView.image = [UIImage imageNamed:@"bottom_shang_icon"];
        _leftLabel.text =  ![self isEmpty:model.s_num]?model.s_num:@"0";
    }
    
    _fabulousLabel.text = ![self isEmpty:model.z_num]?model.z_num:@"0";
    _commentLabel.text =  ![self isEmpty:model.comment]?model.comment:@"0";
    _forwardLabel.text = ![self isEmpty:model.forwar]?model.forwar:@"0";
}

-(void)setDynamicmodel:(DynamicListModel *)dynamicmodel{
    _dynamicmodel = dynamicmodel;
    if ([dynamicmodel.zstype isEqualToString:@"1"]) {
        _leftImageView.image = [UIImage imageNamed:@"bottom_zan_icon"];
        _leftLabel.text = ![self isEmpty:dynamicmodel.s_num]?dynamicmodel.s_num:@"0";
    }else{
        _leftImageView.image = [UIImage imageNamed:@"bottom_shang_icon"];
        _leftLabel.text = ![self isEmpty:dynamicmodel.s_num]?dynamicmodel.s_num:@"0";
    }
    
    _fabulousLabel.text = ![self isEmpty:dynamicmodel.z_num]?dynamicmodel.z_num:@"0";
    _commentLabel.text =  ![self isEmpty:dynamicmodel.comment_count]?dynamicmodel.comment_count:@"0";
    _forwardLabel.text =  ![self isEmpty:dynamicmodel.sent_count]?dynamicmodel.sent_count:@"0";
}

-(void)bottomViewBtnClick:(UIButton *)senderBtn{
    
    senderBtn.selected = !senderBtn.selected;
    
    if (self.block) {
        self.block(senderBtn);
    }
    
    switch (senderBtn.tag) {
        case 101:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"dianzan_normal_icon" imageSelStr:@"dianzan_selected_icon"];

        }
            break;
        case 102:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"message_normal_icon" imageSelStr:@"message_selected_icon"];

        }
            break;
        case 103:
        {
            
            [self changeBtnImage:senderBtn imageNormalStr:@"zhuanfa_normal_icon" imageSelStr:@"zhuanfa_selected_icon"];

        }
            break;
        case 104:
        {
            [self changeBtnImage:senderBtn imageNormalStr:@"bottom_more_normal_icon" imageSelStr:@"bottom_more_selected_icon"];

        }
            break;
        default:
            break;
    }
//    NSLog(@"======%ld",senderBtn.tag);
}

-(void)changeBtnImage:(UIButton *)senderBtn imageNormalStr:(NSString *)imageNormalStr imageSelStr:(NSString *)imageSelStr{
    if (senderBtn.selected) {
        [senderBtn setImage:[UIImage imageNamed:imageSelStr] forState:UIControlStateNormal];
    }else{
        [senderBtn setImage:[UIImage imageNamed:imageNormalStr] forState:UIControlStateNormal];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
