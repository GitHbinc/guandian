//
//  ContentBottomView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/30.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankVideoDetailModel.h"
#import "DynamicListModel.h"
typedef void (^ButtonBlock)(UIButton *button);

NS_ASSUME_NONNULL_BEGIN

@interface ContentBottomView : UIView
@property(nonatomic ,strong)RankVideoDetailModel *model;
@property(nonatomic ,strong)DynamicListModel *dynamicmodel;

@property(nonatomic,copy)ButtonBlock block;
-(void)refreshModel;

@end

NS_ASSUME_NONNULL_END
