//
//  MyMD5.h
//  MiaoWo
//
//  Created by WPY on 2017/1/19.
//  Copyright © 2017年 WPY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyMD5 : NSObject

//返回大写
+ (NSString *)md5:(NSString *)str;
//返回小写
+ (NSString *)md5uncapitalized:(NSString *)str;

@end
