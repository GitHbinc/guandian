//
//  LiveBroadcastViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "LiveBroadcastViewController.h"
#import "LiveBroadcastHeaderView.h"
#import "RechargeView.h"

@interface LiveBroadcastViewController ()

@end

@implementation LiveBroadcastViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    RechargeView *view = [RechargeView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
    
    [self.view addSubview:view];
//    self.view.backgroundColor = [UIColor greenColor];
//    AuctionView *headerView = [AuctionView customHeadViewWithFrame:CGRectMake(0, 15, kSCREENWIDTH, 50)];
//    headerView.backgroundColor = UIColor.clearColor;
//    [headerView refreshLocation:@"上海市"];
//    headerView.block = ^(UIButton *button) {
//        [self allBtnClick:button.tag];
//    };
//    [self.view addSubview:headerView];
//
//    LiveBroadcastMiddleView *middleView = [LiveBroadcastMiddleView customHeadViewWithFrame:CGRectMake(0, 100, kSCREENWIDTH, 255)];
//    middleView.backgroundColor = UIColor.clearColor;
//    [middleView refresh];
//    middleView.block = ^(UIButton *button) {
//        [self allBtnClick:button.tag];
//    };
//    [self.view addSubview:middleView];
//
//    LiveBroadcastBottomView *bottomViwe = [LiveBroadcastBottomView customHeadViewWithFrame:CGRectMake(0, kSCREENHEIGHT - 70, kSCREENWIDTH, 58)];
//    bottomViwe.backgroundColor = UIColor.clearColor;
//    bottomViwe.block = ^(UIButton *button) {
//        [self allBtnClick:button.tag];
//    };
//    [self.view addSubview:bottomViwe];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

-(void)allBtnClick:(NSInteger)tag{
    
    switch (tag) {
        case 101://转换前置后置摄像头
            
            break;
        case 102://横屏竖屏
            
            break;
        case 103://返回按钮
            [self dismissViewControllerAnimated:true completion:nil];
            break;
        case 104://选择分类按钮
            
            break;
        case 105://添加封面
            
            break;
        case 106://赏
            
            break;
        case 107://赞
            
            break;
        case 108://微信
            
            break;
        case 109://朋友圈
            
            break;
        case 110://qq
            
            break;
        case 111://空间
            
            break;
        case 112://新浪微博
            
            break;
        case 113://美颜
            
            break;
        case 114://开始直播
            
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
