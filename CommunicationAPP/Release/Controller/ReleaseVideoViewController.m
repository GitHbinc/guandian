//
//  ReleaseVideoViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseVideoViewController.h"
#import "ReleaseVideoCell.h"
#import "ControllerHeaderView.h"
#import "ReleaseVideoHeaderView.h"
#import "TotalClassfyViewController.h"


@interface ReleaseVideoViewController ()<UITableViewDelegate,UITableViewDataSource,TotalClassfyViewControllerDelegate>

@property (nonatomic,copy) UITableView *tableView;

@end

@implementation ReleaseVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTableView];
//    self.view.backgroundColor = colorf3f3f3;
    ControllerHeaderView *headerView = [ControllerHeaderView customHeadViewWithFrame:CGRectMake(0, 20, kSCREENWIDTH, 50)];
    headerView.backgroundColor = colorf3f3f3;
    __weak typeof(self)weakSelf = self;
    headerView.block = ^(UIButton *button) {
        [weakSelf controllerHeaderBtnClick:button];
    };
    [self.view addSubview:headerView];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

#pragma mark controller头部点击事件
-(void)controllerHeaderBtnClick:(UIButton *)senderBtn{
    switch (senderBtn.tag) {
        case 101://取消
//            [self.view removeFromSuperview];
            [self dismissViewControllerAnimated:true completion:nil];
            break;
        case 102://发布
            [self dismissViewControllerAnimated:true completion:nil];
            break;
        default:
            break;
    }
}

#pragma mark创建ctableView
-(void)createTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 70, kSCREENWIDTH, kSCREENHEIGHT- 70) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = UIColor.whiteColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    ReleaseVideoHeaderView *headerView = [ReleaseVideoHeaderView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, 250)];
    [headerView refresh];
    _tableView.tableHeaderView = headerView;
    
}

#pragma mark tableView delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        ReleaseVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReleaseVideoCellID"];
        if (cell == nil) {
            cell = [[ReleaseVideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReleaseVideoCellID"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell refreshModel:@"音乐"];
        return cell;
    }
    
    ReleaseVideoBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReleaseVideoBottomCell"];
    if (cell == nil) {
        cell = [[ReleaseVideoBottomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReleaseVideoBottomCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        TotalClassfyViewController *controller = [[TotalClassfyViewController alloc]init];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:true];
    }
}


- (void)selectTypeOfTitles:(NSInteger)tag{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ReleaseVideoCell *cell =(ReleaseVideoCell *)[_tableView cellForRowAtIndexPath:indexPath];
    [cell refreshModel:@"视频"];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
