//
//  ReleaseVideoHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseVideoHeaderView.h"

@interface ReleaseVideoHeaderView(){
    UITextField *_contentTextField;
    UIButton *_addBtn;
    UILabel*_addressLabel;
    UIImageView *_contentImageView;
    
}

@end
@implementation ReleaseVideoHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    _contentTextField = [[UITextField alloc]init];
    _contentTextField.placeholder = @"这一秒的想法...";
    _contentTextField.font = [UIFont systemFontOfSize:14];
    [self addSubview:_contentTextField];
    [_contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(@80);
    }];
    
    _contentImageView = [[UIImageView alloc]init];
    [self addSubview:_contentImageView];
    [_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.top.equalTo(self->_contentTextField.mas_bottom).offset(10);
        make.height.equalTo(@100);
        make.width.equalTo(@100);
    }];
    
    UIImageView *locationImageView = [[UIImageView alloc]init];
    locationImageView.image = [UIImage imageNamed:@"location_icon"];
    [self addSubview:locationImageView];
    [locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_contentImageView.mas_left).offset(0);
        make.top.equalTo(self->_contentImageView.mas_bottom).offset(5);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _addressLabel = [[UILabel alloc]init];
    _addressLabel.textColor = color999999;
    _addressLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_addressLabel];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(locationImageView.mas_right).offset(5);
        make.top.equalTo(locationImageView.mas_top).offset(0);
        make.right.equalTo(self).offset(-10);
    }];
    
}

-(void)refresh{
    
    _contentImageView.image = [UIImage imageNamed:@"test_icon"];
    _addressLabel.text = @"上海市长宁区";
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
