//
//  ReleaseVideoCell.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ReleaseVideoCell.h"

@interface ReleaseVideoCell(){
    
    UIImageView *_typeImageView;
    UILabel *_typeLabel;
}

@end
@implementation ReleaseVideoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"选择分类";
    titleLabel.textColor = color333333;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIImageView *arrowImageView = [[UIImageView alloc]init];
    arrowImageView.image = [UIImage imageNamed:@"arrow_icon"];
    [self.contentView addSubview:arrowImageView];
    [arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _typeLabel = [[UILabel alloc]init];
    _typeLabel.textColor = MAINCOLOR;
    _typeLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(arrowImageView.mas_left).offset(-5);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    _typeImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:_typeImageView];
    [_typeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_typeLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@15);
        make.height.equalTo(@15);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
    
}

-(void)refreshModel:(NSString *)titleStr{
    _typeImageView.image = [UIImage imageNamed:@"music_icon"];
    _typeLabel.text = titleStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation ReleaseVideoBottomCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpView];
        
    }
    return self;
}

-(void)setUpView{
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"选择赞或赏";
    titleLabel.textColor = color333333;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UILabel *shangLabel = [[UILabel alloc]init];
    shangLabel.text = @"赏";
    shangLabel.textColor = colorF46C02;
    shangLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:shangLabel];
    [shangLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIButton *shangBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shangBtn setImage:[UIImage imageNamed:@"shang_normal_icon"] forState:UIControlStateNormal];
    shangBtn.tag = 101;
    [shangBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:shangBtn];
    [shangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(shangLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    
    UILabel *zanLabel = [[UILabel alloc]init];
    zanLabel.text = @"赞";
    zanLabel.textColor = colorEF1D1E;
    zanLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:zanLabel];
    [zanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(shangBtn.mas_left).offset(-30);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIButton *zanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [zanBtn setImage:[UIImage imageNamed:@"zan_selected_icon"] forState:UIControlStateNormal];
    zanBtn.tag = 102;
    [zanBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:zanBtn];
    [zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(zanLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = colorf3f3f3;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.equalTo(@1);
    }];
    
}

-(void)btnClick:(UIButton *)senderBtn{
    
    
    switch (senderBtn.tag) {
        case 102:
        {
            [senderBtn setImage:[UIImage imageNamed:@"zan_selected_icon"] forState:UIControlStateNormal];
            UIButton *btn = (UIButton *)[self viewWithTag:101];
            [btn setImage:[UIImage imageNamed:@"shang_normal_icon"] forState:UIControlStateNormal];
            
        }
            break;
        case 101:
        {
            [senderBtn setImage:[UIImage imageNamed:@"shang_selected_icon"] forState:UIControlStateNormal];
            UIButton *btn = (UIButton *)[self viewWithTag:102];
            [btn setImage:[UIImage imageNamed:@"zan_normal_icon"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

