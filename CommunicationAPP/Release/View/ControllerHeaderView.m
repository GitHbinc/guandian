//
//  ControllerHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "ControllerHeaderView.h"

@implementation ControllerHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = UIColor.whiteColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(-5);
    }];
    
    UIButton *cancelBtn = [[UIButton alloc]init];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:color333333 forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    cancelBtn.tag = 101;
    [cancelBtn addTarget:self action:@selector(headerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(15);
        make.centerY.equalTo(bgView.mas_centerY);
    }];
    
    UIButton *releaseBtn = [[UIButton alloc]init];
    [releaseBtn setTitle:@"发布" forState:UIControlStateNormal];
    [releaseBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    releaseBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    releaseBtn.tag = 102;
    [releaseBtn addTarget:self action:@selector(headerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:releaseBtn];
    [releaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView.mas_right).offset(-15);
        make.centerY.equalTo(bgView.mas_centerY);
    }];
}

-(void)headerBtnClick:(UIButton *)senderBtn{
    
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
