//
//  LiveBroadcastHeaderView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "LiveBroadcastHeaderView.h"

@interface LiveBroadcastHeaderView(){
    UILabel *_addressLabel;
}

@end
@implementation LiveBroadcastHeaderView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *locationImageView = [[UIImageView alloc]init];
    locationImageView.image = [UIImage imageNamed:@"location_white_icon"];
    [self addSubview:locationImageView];
    [locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(15);
        make.height.equalTo(@20);
        make.width.equalTo(@20);
    }];
    
    _addressLabel = [[UILabel alloc]init];
    _addressLabel.textColor = UIColor.whiteColor;
    _addressLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_addressLabel];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(locationImageView.mas_right).offset(5);
        make.top.equalTo(locationImageView.mas_top).offset(2);
    }];
    
    UIButton *cameraBtn = [[UIButton alloc]init];
    [cameraBtn setImage:[UIImage imageNamed:@"camera_icon"] forState:UIControlStateNormal];
    cameraBtn.tag = 101;
    [cameraBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cameraBtn];
    [cameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_addressLabel.mas_right).offset(20);
        make.top.equalTo(locationImageView.mas_top).offset(0);
    }];
    
    UIButton *directionBtn = [[UIButton alloc]init];
    [directionBtn setImage:[UIImage imageNamed:@"direction_icon"] forState:UIControlStateNormal];
    directionBtn.tag = 102;
    [directionBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:directionBtn];
    [directionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cameraBtn.mas_right).offset(20);
        make.top.equalTo(cameraBtn.mas_top).offset(0);
    }];
    
    UIButton *cancelBtn = [[UIButton alloc]init];
    [cancelBtn setImage:[UIImage imageNamed:@"cancel_icon"] forState:UIControlStateNormal];
    cancelBtn.tag = 103;
    [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-20);
        make.centerY.equalTo(self.mas_centerY);
        make.height.equalTo(@18);
        make.width.equalTo(@18);
    }];

}

-(void)refreshLocation:(NSString *)locationStr{
    
    _addressLabel.text = locationStr;
}

-(void)btnClick:(UIButton *)senderBtn{
    
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end



@interface LiveBroadcastMiddleView(){
    
    UILabel *_typeLabel;
    UIImageView *_typeImageView;
    UIImageView *_contentImageView;
    UILabel *_descLabel;
    UITextField *_descTextField;
    UILabel *_addressLabel;
}

@end
@implementation LiveBroadcastMiddleView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
//
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.userInteractionEnabled = true;
    bgImageView.image = [UIImage imageNamed:@"content_bg_icon"];
    [self addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self).offset(-10);
        make.bottom.equalTo(self).offset(0);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"直播分类";
    label.textColor = UIColor.whiteColor;
    label.font = [UIFont systemFontOfSize:14];
    [bgImageView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView.mas_left).offset(15);
        make.top.equalTo(bgImageView.mas_top).offset(15);
    }];
    
    UIButton *arrowBtn = [[UIButton alloc]init];
    [arrowBtn setImage:[UIImage imageNamed:@"arrow_icon"] forState:UIControlStateNormal];
    arrowBtn.tag = 104;
    [arrowBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:arrowBtn];
    [arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgImageView.mas_right).offset(-20);
        make.top.equalTo(bgImageView.mas_top).offset(13);
    }];
    
    _typeLabel = [[UILabel alloc]init];
    _typeLabel.textColor = MAINCOLOR;
    _typeLabel.font = [UIFont systemFontOfSize:13];
    [bgImageView addSubview:_typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(arrowBtn.mas_left).offset(-10);
        make.top.equalTo(arrowBtn.mas_top).offset(2);
    }];
    
    _typeImageView = [[UIImageView alloc]init];
    [self addSubview:_typeImageView];
    [_typeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self->_typeLabel.mas_left).offset(-5);
        make.top.equalTo(self->_typeLabel.mas_top).offset(0);
    }];
    
    UIImageView *lineImageView = [[UIImageView alloc]init];
    lineImageView.image = [UIImage imageNamed:@"line_icon"];
    [bgImageView addSubview:lineImageView];
    [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView.mas_left).offset(10);
        make.top.equalTo(label.mas_bottom).offset(10);
        make.right.equalTo(bgImageView.mas_right).offset(-10);
        make.height.equalTo(@1);
    }];
    
    _contentImageView = [[UIImageView alloc]init];
    _contentImageView.userInteractionEnabled = true;
    _contentImageView.image = [UIImage imageNamed:@"tupian_bg_icon"];
    [bgImageView addSubview:_contentImageView];
    [_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView.mas_left).offset(10);
        make.top.equalTo(lineImageView.mas_bottom).offset(10);
    }];
    
    UIButton *addBtn = [[UIButton alloc]init];
    [addBtn setImage:[UIImage imageNamed:@"add_icon"] forState:UIControlStateNormal];
    addBtn.tag = 105;
    [addBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_contentImageView addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self->_contentImageView.mas_centerX);
        make.centerY.equalTo(self->_contentImageView.mas_centerY);
    }];
    
    _descLabel = [[UILabel alloc]init];
    _descLabel.text = @"封面";
    _descLabel.textColor = UIColor.whiteColor;
    _descLabel.font = [UIFont systemFontOfSize:13];
    [_contentImageView addSubview:_descLabel];
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self->_contentImageView.mas_bottom).offset(-5);
        make.centerX.equalTo(self->_contentImageView.mas_centerX);
    }];
    
    _descTextField = [[UITextField alloc]init];
    _descTextField.textColor = MAINCOLOR;
    _descTextField.placeholder = @"填写标题吸引更多观众";
    _descTextField.font = [UIFont systemFontOfSize:17];
    [bgImageView addSubview:_descTextField];
    [_descTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self->_contentImageView.mas_right).offset(10);
        make.top.equalTo(lineImageView.mas_bottom).offset(17);
        make.right.equalTo(bgImageView.mas_right).offset(-10);
    }];
    
    UIImageView *line2 = [[UIImageView alloc]init];
    line2.image = [UIImage imageNamed:@"line_icon"];
    [bgImageView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView.mas_left).offset(10);
        make.right.equalTo(bgImageView.mas_right).offset(-10);
        make.top.equalTo(self->_contentImageView.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"选择赞或赏";
    titleLabel.textColor = color333333;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [bgImageView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView).offset(15);
        make.top.equalTo(line2.mas_bottom).offset(15);
    }];
    
    UILabel *shangLabel = [[UILabel alloc]init];
    shangLabel.text = @"赏";
    shangLabel.textColor = colorF46C02;
    shangLabel.font = [UIFont systemFontOfSize:15];
    [bgImageView addSubview:shangLabel];
    [shangLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgImageView.mas_right).offset(-15);
        make.top.equalTo(titleLabel.mas_top).offset(0);
    }];
    
    UIButton *shangBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shangBtn setImage:[UIImage imageNamed:@"shang_normal_icon"] forState:UIControlStateNormal];
    shangBtn.tag = 106;
    [shangBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:shangBtn];
    [shangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(shangLabel.mas_left).offset(-5);
        make.top.equalTo(titleLabel.mas_top).offset(-2);
    }];
    
    
    UILabel *zanLabel = [[UILabel alloc]init];
    zanLabel.text = @"赞";
    zanLabel.textColor = colorEF1D1E;
    zanLabel.font = [UIFont systemFontOfSize:15];
    [bgImageView addSubview:zanLabel];
    [zanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(shangBtn.mas_left).offset(-30);
        make.top.equalTo(titleLabel.mas_top).offset(0);
    }];
    
    UIButton *zanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [zanBtn setImage:[UIImage imageNamed:@"zan_selected_icon"] forState:UIControlStateNormal];
    zanBtn.tag = 107;
    [zanBtn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:zanBtn];
    [zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(zanLabel.mas_left).offset(-5);
        make.top.equalTo(titleLabel.mas_top).offset(-2);
    }];
    
    UIImageView *line3 = [[UIImageView alloc]init];
    line3.image = [UIImage imageNamed:@"line_icon"];
    [bgImageView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView.mas_left).offset(10);
        make.right.equalTo(bgImageView.mas_right).offset(-10);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];
    
    UILabel *titleLabel1 = [[UILabel alloc]init];
    titleLabel1.text = @"呼唤好友来看";
    titleLabel1.textColor = color333333;
    titleLabel1.font = [UIFont systemFontOfSize:13];
    [bgImageView addSubview:titleLabel1];
    [titleLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgImageView).offset(15);
        make.top.equalTo(line3.mas_bottom).offset(15);
    }];
    
    UIView *btnView = [[UIView alloc]init];
    btnView.backgroundColor = UIColor.clearColor;
    [bgImageView addSubview:btnView];
    [btnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel1.mas_right).offset(30);
        make.right.equalTo(bgImageView.mas_right).offset(0);
        make.bottom.equalTo(bgImageView.mas_bottom).offset(0);
        make.height.equalTo(@50);
    }];
    
    NSArray *imageArray = @[@"Zweixin_icon",@"Zpengyouquan_icon",@"Zqq_icon",@"Zkongjian_icon",@"Zxinlang_icon"];
    
    CGFloat space = 38;
    for (int i = 0; i < imageArray.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 108 + i;
        [btn addTarget:self action:@selector(allBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [btnView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(btnView.mas_left).offset(space * i);
            make.centerY.equalTo(btnView.mas_centerY);
        }];
    }
}


-(void)refresh{
    
    _typeLabel.text = @"音乐";
    _typeImageView.image = [UIImage imageNamed:@"music_icon"];
    
}
-(void)allBtnClick:(UIButton *)senderBtn{
    
    switch (senderBtn.tag) {
        case 107:
        {
            [senderBtn setImage:[UIImage imageNamed:@"zan_selected_icon"] forState:UIControlStateNormal];
            UIButton *btn = (UIButton *)[self viewWithTag:106];
            [btn setImage:[UIImage imageNamed:@"shang_normal_icon"] forState:UIControlStateNormal];
            
        }
            break;
        case 106:
        {
            [senderBtn setImage:[UIImage imageNamed:@"shang_selected_icon"] forState:UIControlStateNormal];
            UIButton *btn = (UIButton *)[self viewWithTag:107];
            [btn setImage:[UIImage imageNamed:@"zan_normal_icon"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
    if (self.block) {
        self.block(senderBtn);
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end


@implementation LiveBroadcastBottomView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIButton *meiyanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [meiyanBtn setImage:[UIImage imageNamed:@"meiyan_icon"] forState:UIControlStateNormal];
    [meiyanBtn addTarget:self action:@selector(bottomBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    meiyanBtn.tag = 113;
    [self addSubview:meiyanBtn];
    [meiyanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@((kSCREENWIDTH - 20) / 3 + 20));
        make.height.equalTo(@58);
    }];
    
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [startBtn setImage:[UIImage imageNamed:@"Zstart_icon"] forState:UIControlStateNormal];
    [startBtn addTarget:self action:@selector(bottomBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    startBtn.tag = 114;
    [self addSubview:startBtn];
    [startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(meiyanBtn.mas_right).offset(0);
        make.height.equalTo(@58);
    }];
}

-(void)bottomBtnClick:(UIButton *)senderBtn{
 
    if (self.block) {
        self.block(senderBtn);
    }
}

@end

