//
//  LiveBroadcastHeaderView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ButtonBlock)(UIButton *button);

NS_ASSUME_NONNULL_BEGIN

@interface LiveBroadcastHeaderView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refreshLocation:(NSString *)locationStr;
@property(nonatomic,copy)ButtonBlock block;

@end

NS_ASSUME_NONNULL_END


NS_ASSUME_NONNULL_BEGIN

@interface LiveBroadcastMiddleView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
-(void)refresh;
@property(nonatomic,copy)ButtonBlock block;

@end

NS_ASSUME_NONNULL_END


NS_ASSUME_NONNULL_BEGIN

@interface LiveBroadcastBottomView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property(nonatomic,copy)ButtonBlock block;

@end

NS_ASSUME_NONNULL_END
