//
//  Macro.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#ifndef Macro_h
#define Macro_h

#import "AppDelegate.h"
#import "Request.h"
#import "ImageService.h"
#import "UIColor+Extension.h"

//测试地址
#define kBASEURL   @"http://agent.gdhime.com/api.php/"
//网络图片
#define kWEBURL    @"http://agent.gdhime.com"


#define LOGINSTATUS      @"LOGINSTATUS"
#define USERID           @"USERID"
#define HEADIMG          @"HEADIMG"
#define USERNAME         @"USERNAME"

#define Latitude         @"Latitude"
#define Longitude        @"Longitude"

#define kUserDefault  [NSUserDefaults standardUserDefaults]

#define kAppDelegate  (AppDelegate *)[[UIApplication sharedApplication] delegate]
/** 屏幕宽度 */
#define kSCREENWIDTH [UIScreen mainScreen].bounds.size.width
/** 屏幕高度 */
#define kSCREENHEIGHT [UIScreen mainScreen].bounds.size.height
/** 判断是否为iPhoneX */
#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX kSCREENWIDTH >=375.0f && kSCREENHEIGHT >=812.0f&& kIs_iphone
/** 状态栏高度 */
#define STATUS_BAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
/** 导航栏高度 */
#define Navi_BAR_HEIGHT self.navigationController.navigationBar.frame.size.height
/** 导航栏高度+状态栏高度 */
#define Navi_STATUS_HEIGHT STATUS_BAR_HEIGHT + Navi_BAR_HEIGHT
/** Tabbar高度 */
#define TABBAR_BAR_HEIGHT (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))

#define Height_StatusTabBar ((kIs_iPhoneX == YES) ? 34.0f : 0.0f)


/** RGB颜色 */
#define kRGB(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define HEXCOLOR(hex) [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16)) / 255.0 green:((float)((hex & 0xFF00) >> 8)) / 255.0 blue:((float)(hex & 0xFF)) / 255.0 alpha:1]

// 空值判断
#define IsNullObject(obj) (obj == nil || obj == Nil || obj == NULL || [obj isKindOfClass:[NSNull class]] || ![obj isKindOfClass:[NSObject class]])

#define IsNullString(str) (IsNullObject(str) || ![str isKindOfClass:[NSString class]] || [str length] == 0)

#define MAINCOLOR   [UIColor colorWithHexString:@"9C386A"] //紫色
#define color666666   [UIColor colorWithHexString:@"666666"]
#define color999999   [UIColor colorWithHexString:@"999999"]
#define colorD6D6D6   [UIColor colorWithHexString:@"D6D6D6"]
#define colorf3f3f3   [UIColor colorWithHexString:@"f3f3f3"]
#define colorF46C02   [UIColor colorWithHexString:@"F46C02"]
#define colorEF1D1E   [UIColor colorWithHexString:@"EF1D1E"]
#define color333333   [UIColor colorWithHexString:@"333333"]
#define colorCCCCCC   [UIColor colorWithHexString:@"CCCCCC"]
#define color404040   [UIColor colorWithHexString:@"404040"]
#define colorf2f2f2   [UIColor colorWithHexString:@"f2f2f2"]
#define colorDDDDDD   [UIColor colorWithHexString:@"DDDDDD"]

/** notificationName */
#define IsEnablePersonalCenterVCMainTableViewScroll @"IsEnablePersonalCenterVCMainTableViewScroll"
#define CurrentSelectedChildViewControllerIndex @"CurrentSelectedChildViewControllerIndex"
#define PersonalCenterVCBackingStatus @"PersonalCenterVCBackingStatus"
#define PersonalCenterVCUSERDATA @"PersonalCenterUserData"

#endif /* Macro_h */
