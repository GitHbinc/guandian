//
//  NSData+Extension.h
//  GreenShow
//
//  Created by AaronLeo on 2017/10/19.
//  Copyright © 2017年 GreenShow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Extension)


/**
 *  把本地数据归档
 *
 *  @param archiverData      需要归档的数据（是个对象）
 *  @param directotyName     归档好以后的文件路径
 *  @param fileName          归档好以后的文件
 *  @param dataArrayKey      归档时数组的key（用于解档）
 *  @return                  归档是否成功
 */
+ (BOOL)archiverResourceArchiverWithData:(id)archiverData directotyName:(NSString *)directotyName fileName:(NSString *)fileName dataArrayKey:(NSString *)dataArrayKey;


/**
 *  把本地数据解档
 *
 *  @param directotyName     解档的文件路径
 *  @param fileName          解档的文件
 *  @param dataArrayKey      解档时数组的key（和归档时一致）
 */
+ (NSMutableArray *)unArchiverResourceUnArchiverWithDirectotyName:(NSString *)directotyName fileName:(NSString *)fileName dataArrayKey:(NSString *)dataArrayKey;

@end
