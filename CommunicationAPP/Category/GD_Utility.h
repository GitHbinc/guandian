//
//  GD_Utility.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GD_UserInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface GD_Utility : NSObject

@property (nonatomic,strong) GD_UserInfo *userInfo;

@property (nonatomic,assign) BOOL networkState;

+ (GD_Utility *)sharedUtility;

+ (void)EmptyData;

@end

NS_ASSUME_NONNULL_END
