//
//  GD_Utility.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "GD_Utility.h"

@implementation GD_Utility

@synthesize userInfo;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userInfo = [[GD_UserInfo alloc] init];
        
    }
    return self;
}

+ (GD_Utility *)sharedUtility
{
    static GD_Utility *sharedUtilityInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedUtilityInstance = [[self alloc] init];
    });
    return sharedUtilityInstance;
}

+ (void)EmptyData
{
//    [FXD_Tool saveUserDefaul:nil Key:Fxd_JUID];
//    [FXD_Tool saveUserDefaul:nil Key:kLoginFlag];
//    [FXD_Tool saveUserDefaul:nil Key:Fxd_Token];
//    [FXD_Tool saveUserDefaul:nil Key:UserName];
//    [FXD_Tool saveUserDefaul:nil Key:kInvitationCode];
//    [GD_Utility sharedUtility].loginFlage = 0;
    [GD_Utility sharedUtility].userInfo.juid = @"";
    [GD_Utility sharedUtility].userInfo.tokenStr = @"";
    [GD_Utility sharedUtility].userInfo.userMobilePhone = @"";
    [GD_Utility sharedUtility].userInfo.account_id = @"";
    [GD_Utility sharedUtility].userInfo.realName = @"";
    [GD_Utility sharedUtility].userInfo.pruductId = @"";
//    [DataWriteAndRead writeDataWithkey:UserInfomation value:nil];
//    [GD_Utility sharedUtility].isActivityShow = true;
//    [GD_Utility sharedUtility].isHomeChooseShow = true;
}

@end
