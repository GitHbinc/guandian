//
//  NSData+Extension.m
//  GreenShow
//
//  Created by AaronLeo on 2017/10/19.
//  Copyright © 2017年 GreenShow. All rights reserved.
//

#import "NSData+Extension.h"

@implementation NSData (Extension)

/**
 *  把本地数据归档
 *
 *  @param archiverData      需要归档的数据（是个对象）
 *  @param directotyName     归档好以后的文件路径
 *  @param fileName          归档好以后的文件
 *  @param dataArrayKey      归档时数组的key（用于解档）
 *  @return                  归档是否成功
 */
+ (BOOL)archiverResourceArchiverWithData:(id)archiverData directotyName:(NSString *)directotyName fileName:(NSString *)fileName dataArrayKey:(NSString *)dataArrayKey
{
    NSString *localDataPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    localDataPath = [localDataPath stringByAppendingPathComponent:directotyName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDirectory = YES;
    if(![fileManager fileExistsAtPath:localDataPath isDirectory:&isDirectory])
    {
        BOOL result = [fileManager createDirectoryAtPath:localDataPath withIntermediateDirectories:YES attributes:nil error:nil];
        if (result == NO) {
            return result;
        }
        localDataPath = [localDataPath stringByAppendingPathComponent:fileName];
        
    }else{
        localDataPath = [localDataPath stringByAppendingPathComponent:fileName];
        if ([fileManager fileExistsAtPath:localDataPath]) {
            [fileManager removeItemAtPath:localDataPath error:nil];
        }
    }
    
    if (archiverData == nil) {
        return NO;
    }
    
    NSMutableData *archLocalResourceData = [NSMutableData data];
    NSKeyedArchiver *archLocalResource = [[NSKeyedArchiver alloc] initForWritingWithMutableData:archLocalResourceData];
    [archLocalResource encodeObject:archiverData forKey:dataArrayKey];
    [archLocalResource finishEncoding];
    
    return [archLocalResourceData writeToFile:localDataPath atomically:YES];
}


/**
 *  
 *
 *  @param directotyName     解档的文件路径
 *  @param fileName          解档的文件
 *  @param dataArrayKey      解档时数组的key（和归档时一致）
 */
+ (id)unArchiverResourceUnArchiverWithDirectotyName:(NSString *)directotyName fileName:(NSString *)fileName dataArrayKey:(NSString *)dataArrayKey
{
    NSString *localDataPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    localDataPath = [localDataPath stringByAppendingPathComponent:directotyName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    if(![fileManager fileExistsAtPath:localDataPath isDirectory:&isDirectory]){
        return nil;
    }
    
    localDataPath = [localDataPath stringByAppendingPathComponent:fileName];
    if( ![fileManager fileExistsAtPath:localDataPath])
    {
        return nil;
    }
    
    NSData *unArchLocalResourceData = [NSData dataWithContentsOfFile:localDataPath];
    NSKeyedUnarchiver *unArchLocalResource = [[NSKeyedUnarchiver alloc] initForReadingWithData:unArchLocalResourceData];
    id resultDataArray = [unArchLocalResource decodeObjectForKey:dataArrayKey];
    [unArchLocalResource finishDecoding];
    
    return resultDataArray;
}

@end
