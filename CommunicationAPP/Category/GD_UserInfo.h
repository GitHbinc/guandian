//
//  GD_UserInfo.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GD_UserInfo : NSObject

@property (nonatomic,assign) NSInteger login_flag;
@property (nonatomic,assign) double userId;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,retain) NSString *tokenStr;
@property (nonatomic,retain) NSString *juid;
@property (nonatomic,retain) NSString *realName;
@property (nonatomic,assign) double creditLine;
@property (nonatomic,strong) NSString *uuidStr;
@property (nonatomic,strong) NSString *clientId;
@property (nonatomic,strong) NSString *userPass;
@property (nonatomic, copy) NSString *userIDNumber;
@property (nonatomic,assign) BOOL isUpdate;
@property (nonatomic, copy) NSString *userMobilePhone;
@property (nonatomic, copy) NSString *account_id;
@property (nonatomic, copy) NSString *pruductId;

@end

NS_ASSUME_NONNULL_END
