//
//  BaseViewController.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray *dataSource; //数据源数组

/**
 导航栏标题
 */
- (UIBarButtonItem *)getBarButtonItemWithTitleStr:(NSString *)titleStr Sel:(SEL)sel;

/**
 显示框
 */
- (void)startLoading;
- (void)startLoadingWithText:(NSString *)text;
- (void)stopLoading;
- (void)showTipLabelWithText:(NSString *)text;

/**
 *  字符串判空
 */
- (BOOL)isEmpty:(NSString *)string;
@end

NS_ASSUME_NONNULL_END
