//
//  MainTabBarViewController.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
