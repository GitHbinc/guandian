//
//  CenterTabBarView.h
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^ButtonBlock)(UIButton *button);

NS_ASSUME_NONNULL_BEGIN

@interface CenterTabBarView : UIView

+ (instancetype)customHeadViewWithFrame:(CGRect)frame;
@property(nonatomic,copy)ButtonBlock bottomBlock;

@end

NS_ASSUME_NONNULL_END
