//
//  CenterTabBarView.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "CenterTabBarView.h"

@implementation CenterTabBarView


+ (instancetype)customHeadViewWithFrame:(CGRect)frame {
    return [[self alloc] initWithFrame:frame];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self initView];
    }
    return self;
}
- (void)initView {
    
    UIImageView *bottomImageView = [[UIImageView alloc]init];
    bottomImageView.image = [UIImage imageNamed:@"bg_icon"];
    bottomImageView.userInteractionEnabled = true;
    [self addSubview:bottomImageView];
    [bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self).offset(0);
        make.height.equalTo(@190);
    }];
    
    UIButton *zhiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [zhiboBtn setImage:[UIImage imageNamed:@"zhibo_icon"] forState:UIControlStateNormal];
    [zhiboBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    zhiboBtn.tag = 101;
    [bottomImageView addSubview:zhiboBtn];
    [zhiboBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomImageView.mas_left).offset(55);
        make.top.equalTo(bottomImageView.mas_top).offset(30);
    }];
    
    UIButton *zhibo = [[UIButton alloc]init];
    [zhibo setTitle:@"发布直播" forState:UIControlStateNormal];
    [zhibo setTitleColor:color404040 forState:UIControlStateNormal];
    zhibo.titleLabel.font = [UIFont systemFontOfSize:11];
    zhibo.titleLabel.textAlignment = NSTextAlignmentCenter;
    zhibo.tag = 101;
    [zhibo addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomImageView addSubview:zhibo];
    [zhibo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(zhiboBtn.mas_left).offset(-20);
        make.right.equalTo(zhiboBtn.mas_right).offset(20);
        make.top.equalTo(zhiboBtn.mas_bottom).offset(10);
    }];
    
    UIButton *shipinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shipinBtn setImage:[UIImage imageNamed:@"shipin_icon"] forState:UIControlStateNormal];
    [shipinBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    shipinBtn.tag = 102;
    [bottomImageView addSubview:shipinBtn];
    [shipinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomImageView.mas_right).offset(-55);
        make.top.equalTo(bottomImageView.mas_top).offset(30);
    }];
    
    UIButton *shipin = [[UIButton alloc]init];
    [shipin setTitle:@"发布视频/图片" forState:UIControlStateNormal];
    [shipin setTitleColor:color404040 forState:UIControlStateNormal];
    shipin.titleLabel.font = [UIFont systemFontOfSize:11];
    shipin.tag = 102;
    shipin.titleLabel.textAlignment = NSTextAlignmentCenter;
    [shipin addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomImageView addSubview:shipin];
    [shipin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipinBtn.mas_left).offset(-20);
        make.right.equalTo(shipinBtn.mas_right).offset(20);
        make.top.equalTo(shipinBtn.mas_bottom).offset(10);
    }];
    
    UIView *cancelView = [[UIView alloc]init];
    cancelView.backgroundColor = colorf2f2f2;
    cancelView.userInteractionEnabled = true;
    [bottomImageView addSubview:cancelView];
    [cancelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomImageView.mas_left).offset(0);
        make.right.equalTo(bottomImageView.mas_right).offset(0);
        make.bottom.equalTo(bottomImageView.mas_bottom).offset(0);
        make.height.equalTo(@44);
    }];
    
    UIButton *cancelBtn = [[UIButton alloc]init];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    cancelBtn.tag = 103;
    [cancelBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(cancelView.mas_centerX);
        make.centerY.equalTo(cancelView.mas_centerY);
        make.height.equalTo(@44);
        make.width.equalTo(@50);
    }];
}

-(void)centerBtnClick:(UIButton *)senderBtn{
    
    if (self.bottomBlock) {
        self.bottomBlock(senderBtn);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
