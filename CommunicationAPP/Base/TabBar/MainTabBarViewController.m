//
//  MainTabBarViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/11/28.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "CenterTabBar.h"
#import "SceneViewController.h"
#import "RankingViewController.h"
#import "DynamicViewController.h"
#import "MineViewController.h"
#import "BaseNavigationController.h"
#import "CenterTabBarView.h"
#import "ReleaseVideoViewController.h"
#import "LiveBroadcastViewController.h"

@interface MainTabBarViewController ()<UITabBarDelegate,UITabBarControllerDelegate>

@property (nonatomic, strong) CenterTabBar *mcTabbar;
@property (nonatomic, assign) NSUInteger selectItem;//选中的item
@property (nonatomic, strong) CenterTabBarView *centerView;


@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITabBar *tabBarAppearance = [UITabBar appearance];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(-20, -6, kSCREENWIDTH + 47, self.tabBar.bounds.size.height + 6)];
    bgView.backgroundColor = UIColor.whiteColor;

    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, bgView.bounds.size.width, self.tabBar.bounds.size.height)];
    imageView.image = [UIImage imageNamed:@"tabBar_bg_icon"];
    //[bgView addSubview:imageView];
    [tabBarAppearance insertSubview:bgView atIndex:0];

    
    _mcTabbar = [[CenterTabBar alloc] init];
    //[_mcTabbar.centerBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    //选中时的颜色
    //    _mcTabbar.tintColor = [UIColor colorWithRed:27.0/255.0 green:118.0/255.0 blue:208/255.0 alpha:1];
    //透明设置为NO，显示白色，view的高度到tabbar顶部截止，YES的话到底部
    _mcTabbar.translucent = NO;
    //利用KVC 将自己的tabbar赋给系统tabBar
    [self setValue:_mcTabbar forKeyPath:@"tabBar"];
    
    self.selectItem = 0; //默认选中第一个
    self.delegate = self;
    [self addChildViewControllers];
}

//添加子控制器
- (void)addChildViewControllers{
    
    //图片大小建议32*32
    [self addChildrenViewController:[[SceneViewController alloc] init] andTitle:@"首页" andImageName:@"home_normal_icon" andSelectImage:@"home_selected_icon"];
    [self addChildrenViewController:[[RankingViewController alloc] init] andTitle:@"视频" andImageName:@"video_normal_icon" andSelectImage:@"video_selected_icon"];
     [self addChildrenViewController:[[RankingViewController alloc] init] andTitle:@"商城" andImageName:@"shopping_normal_icon" andSelectImage:@"shopping_selected_icon"];
    [self addChildrenViewController:[[DynamicViewController alloc] init] andTitle:@"聊天" andImageName:@"chat_normal_icon" andSelectImage:@"chat_selected_icon"];
    [self addChildrenViewController:[[MineViewController alloc] init] andTitle:@"我的" andImageName:@"mine_normal_icon" andSelectImage:@"mine_selected_icon"];
    
    [self setUpTabBarItemTextAttributes];
    
}
- (void)addChildrenViewController:(UIViewController *)childVC andTitle:(NSString *)title andImageName:(NSString *)imageName andSelectImage:(NSString *)selectedImage{
    
    childVC.tabBarItem.image = [UIImage imageNamed:imageName];
    childVC.tabBarItem.selectedImage =  [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVC.title = title;
    
    BaseNavigationController *baseNav = [[BaseNavigationController alloc] initWithRootViewController:childVC];
   
    [self addChildViewController:baseNav];
    
}

- (void)buttonAction:(UIButton *)button{
    
    [self createCenterBtnView];
    
}


-(void)createCenterBtnView{
    
    __weak typeof(self)weakSelf = self;
    _centerView = [CenterTabBarView customHeadViewWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT)];
    _centerView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    _centerView.bottomBlock = ^(UIButton *button) {
        
        [weakSelf centerBtnClick:button];
    };
    [self.view addSubview:_centerView];
    
}

-(void)centerBtnClick:(UIButton *)senderBtn{
    switch (senderBtn.tag) {
        case 101:
        {
            LiveBroadcastViewController *controller = [[LiveBroadcastViewController alloc]init];
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:controller];
            [self presentViewController:navi animated:YES completion:nil];
        }
            NSLog(@"========发布直播");
            break;
        case 102:
        {


            ReleaseVideoViewController *controller = [[ReleaseVideoViewController alloc]init];
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:controller];
            [self presentViewController:navi animated:YES completion:nil];
            
        }
            NSLog(@"========发布视频");
            
            break;
        case 103:
            
            [_centerView removeFromSuperview];
            
            break;
        default:
            break;
    }
}


//tabbar选择时的代理
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    [_mcTabbar.centerBtn.layer removeAllAnimations];
    self.selectItem = tabBarController.selectedIndex;
}

- (void)setUpTabBarItemTextAttributes {
    
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = color999999;
    
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = HEXCOLOR(0xFF5201);
    
    // 设置文字属性
    UITabBarItem *tabBar = [UITabBarItem appearance];
    
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    
}
- (void)setUpTabBar {
    [self setValue:[[CenterTabBar alloc] init] forKey:@"tabBar"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
