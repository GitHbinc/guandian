//
//  BaseViewController.m
//  CommunicationAPP
//
//  Created by 宋晓翩 on 2018/12/4.
//  Copyright © 2018年 宋晓翩. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()<UIGestureRecognizerDelegate>


@end

@implementation BaseViewController

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setLeftNavButton];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"back_icon"] forBarMetrics:UIBarMetricsDefault];
    //白色状态栏
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    //黑色
    //    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
//    //设置导航栏背景图片为一个空的image，这样就透明了
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    //去掉透明后导航栏下边的黑边
//    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : color333333,
                                                                      NSFontAttributeName : [UIFont systemFontOfSize:16]}];
    
   
}

- (void)setLeftNavButton {
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 120, 44);
    [leftButton addTarget:self action:@selector(gotoBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *backImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 15, 9, 17)];
    backImg.image = [UIImage imageNamed:@"back_icon"];
    [leftButton addSubview:backImg];
    
    [leftButton setFrame:CGRectMake(0, 0, 35, 44)];
    
    UIBarButtonItem *bItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = bItem;
}

- (void)gotoBack{
    
    [self.navigationController popViewControllerAnimated:YES];
//    if (self.navigationController.viewControllers.count > 1) {
//        [self.navigationController popViewControllerAnimated:YES];
//    } else if (self.isPresented) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
}

- (UIBarButtonItem *)getBarButtonItemWithTitleStr:(NSString *)titleStr Sel:(SEL)sel{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:titleStr forState:UIControlStateNormal];
    [btn setTitleColor:HEXCOLOR(0x9C386A) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn addTarget:self action:sel forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return btnItem;
}

- (void)startLoading {
    [QMUITips showLoadingInView:self.view];
}


- (void)startLoadingWithText:(NSString *)text {
    [QMUITips showLoading:text inView:self.view];
}

- (void)stopLoading {
    [QMUITips hideAllToastInView:self.view animated:YES];
}

- (void)showTipLabelWithText:(NSString *)text {
    [self stopLoading];
    [QMUITips showWithText:text inView:self.view hideAfterDelay:1.0];
}

- (BOOL)isEmpty:(NSString *)string{
    if (string == nil || string == NULL) {
        return YES;
        
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    
    return NO;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
