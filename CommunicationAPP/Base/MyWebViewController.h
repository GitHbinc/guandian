//
//  MyWebViewController.h
//  FlowerFriend
//
//  Created by WPY on 16/9/12.
//  Copyright © 2016年 frank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWebViewController : BaseViewController

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *naviTitle;

@property (nonatomic, assign) BOOL isNeedHideNavBar;

@end
