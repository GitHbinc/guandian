//
//  MyWebViewController.m
//  FlowerFriend
//
//  Created by WPY on 16/9/12.
//  Copyright © 2016年 frank. All rights reserved.
//

#import "MyWebViewController.h"
#import <WebKit/WebKit.h>

@interface MyWebViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;

@property (strong, nonatomic) UIView *navBarView;
@property (strong, nonatomic) UIButton *backBtn;
@property (strong, nonatomic) UILabel *titleLab;

@end

@implementation MyWebViewController

- (void)viewWillDisappear:(BOOL)animated
{
    [ super viewWillDisappear: animated];
    
//    [TalkingData trackPageEnd:@"detail"];
//    [MobClick endLogPageView:@"detail"];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    
//    [TalkingData trackPageBegin:@"detail"];
//    [MobClick beginLogPageView:@"detail"];
    
    if (self.isNeedHideNavBar) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
}


- (UIView *)navBarView
{
    if (!_navBarView) {
        _navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, Navi_BAR_HEIGHT)];
        _navBarView.backgroundColor = [UIColor whiteColor];
        
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"navgation_bar_back_white"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(gotoBack) forControlEvents:UIControlEventTouchUpInside];
        _backBtn.frame = CGRectMake(5, STATUS_BAR_HEIGHT, 44, 44);
        //18x18
        _backBtn.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
        [_navBarView addSubview:_backBtn];
        
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 120)/2.0, STATUS_BAR_HEIGHT, 120, 44)];
        _titleLab.textColor = HEXCOLOR(0x222222);
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = [UIFont systemFontOfSize:18];
        _titleLab.text = self.naviTitle;
        [_navBarView addSubview:_titleLab];
        
    }
    return _navBarView;
}


- (void)gotoBack
{
    if([self.webView canGoBack]){
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =self.naviTitle;
//    [self setNavTitle:self.naviTitle];
    [self initWebView];
    
    if (self.isNeedHideNavBar == YES) {
        [self.view addSubview:self.navBarView];
    }
}

- (void)initWebView
{
    if (self.isNeedHideNavBar == YES) {
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, kSCREENWIDTH, kSCREENHEIGHT - (STATUS_BAR_HEIGHT + Height_StatusTabBar))];
    }else{
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kSCREENWIDTH, kSCREENHEIGHT - (Navi_BAR_HEIGHT + STATUS_BAR_HEIGHT))];
    }
    
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    //加载的动画
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

// 加载完成
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.webView.hidden = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.webView.hidden = YES;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//#pragma mark - WKNavigationDelegate
///**
// *  页面开始加载时调用
// *
// *  @param webView    实现该代理的webview
// *  @param navigation 当前navigation
// */
//- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
//
//    NSLog(@"%s", __FUNCTION__);
//
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//}
//
///**
// *  当内容开始返回时调用
// *
// *  @param webView    实现该代理的webview
// *  @param navigation 当前navigation
// */
//- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
//
//    NSLog(@"%s", __FUNCTION__);
//}
//
///**
// *  页面加载完成之后调用
// *
// *  @param webView    实现该代理的webview
// *  @param navigation 当前navigation
// */
//- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//
//    NSLog(@"%s", __FUNCTION__);
//
//
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//}
//
///**
// *  加载失败时调用
// *
// *  @param webView    实现该代理的webview
// *  @param navigation 当前navigation
// *  @param error      错误
// */
//- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
//
//    self.webView.hidden = YES;
//
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//
//    NSLog(@"%s", __FUNCTION__);
//}
//
///**
// *  接收到服务器跳转请求之后调用
// *
// *  @param webView      实现该代理的webview
// *  @param navigation   当前navigation
// */
//- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
//
//    NSLog(@"%s", __FUNCTION__);
//}
//
///**
// *  在收到响应后，决定是否跳转
// *
// *  @param webView            实现该代理的webview
// *  @param navigationResponse 当前navigation
// *  @param decisionHandler    是否跳转block
// */
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
//
//    // 允许跳转
//    decisionHandler(WKNavigationResponsePolicyAllow);
//
//    // 不允许跳转
//    //    decisionHandler(WKNavigationResponsePolicyCancel);
//}
//
///**
// *  在发送请求之前，决定是否跳转
// *
// *  @param webView          实现该代理的webview
// *  @param navigationAction 当前navigation
// *  @param decisionHandler  是否调转block
// */
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
//{
//    // 允许跳转
//    decisionHandler(WKNavigationActionPolicyAllow);
//    // 不允许跳转
//    //    decisionHandler(WKNavigationActionPolicyCancel);
//}



@end

